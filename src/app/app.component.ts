import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from './services/auth/auth.service';

@Component({
  selector: 'farm-root',
  encapsulation: ViewEncapsulation.None,
  template:`<router-outlet></router-outlet>`,
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  constructor(protected authService: AuthService) {}

  ngOnInit() {
    if (this.authService.getToken()) {
      this.authService.getUserPermissionsByToken()
        .subscribe(permissions => {
          this.authService.storeUserPermissions(permissions);
        })
    }
  }


}
