import { UserGuardService } from './services/auth/user-guard.service';
import { AnonGuardService } from './services/auth/anon-guard.service';
import { AuthGuardService } from './services/auth/auth-guard.service';
import { ResetTokenGuardService } from './services/auth/reset-token-guard.service';
import { Routes, RouterModule, PreloadAllModules  } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { ErrorComponent } from './pages/error/error.component';
import { ForbiddenComponent } from './pages/error/forbidden/forbidden.component';

export const routes: Routes = [
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: 'pages', loadChildren: 'app/pages/pages.module#PagesModule', canActivate: [ AuthGuardService, UserGuardService ] },
  { path: 'iniciar-sesion', loadChildren: 'app/pages/login/login.module#LoginModule', canActivate: [ AnonGuardService ] },
  { path: 'recuperar-contrasena', loadChildren: 'app/pages/recover-password/recover-password.module#RecoverPasswordModule', canActivate: [ AnonGuardService ] },
  { path: 'reestablecer-contrasena/:email/:token', loadChildren: 'app/pages/reset-password/reset-password.module#ResetPasswordModule', canActivate: [ AnonGuardService, ResetTokenGuardService ] },
  { path: '403', component: ForbiddenComponent },
  { path: '404', component: ErrorComponent },
  { path: '**', redirectTo: '404' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
   // useHash: true
});
