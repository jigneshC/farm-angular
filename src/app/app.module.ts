import {LOCALE_ID, NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { registerLocaleData } from '@angular/common';
import localeCL from '@angular/common/locales/es-CL';
import { AmChartsModule } from "@amcharts/amcharts3-angular";

/**
 * Components
 */
import { AppComponent } from './app.component';
import { ErrorComponent } from './pages/error/error.component';
/**
 * Services
 */
import { AuthService } from './services/auth/auth.service';
import { AuthGuardService } from './services/auth/auth-guard.service';
import { AnonGuardService } from './services/auth/anon-guard.service';
import { UserGuardService } from './services/auth/user-guard.service';
import { ResetTokenGuardService } from './services/auth/reset-token-guard.service';
import { CapsWarningService } from './services/caps-warning.service';
import { TranslateService } from './services/translate.service';
import { TimeCalculatorService } from "./services/time-calculator.service";
import { TimeKeyInputService } from "./services/time-key-input.service";
/**
 * Interceptors
 */
import { AuthInterceptor } from './services/auth/auth.interceptor';

import { routing } from './app.routing';
import { AppConfig } from './app.config';

import 'pace';
import 'hammerjs';
import {PermissionGuardService} from './services/auth/permission-guard.service';
import {ToasterService} from 'angular2-toaster';
import {MatSnackBar, MatSnackBarModule} from '@angular/material';
import {OverlayModule} from '@angular/cdk/overlay';
import {ForbiddenComponent} from './pages/error/forbidden/forbidden.component';
import {MQTTService} from "./services/mqtt";
import {ConfigService} from "./services/config/config.service";
import {AgmCoreModule} from "@agm/core";


@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    ForbiddenComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule,
    AmChartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC1NDGMQ8ObuxW9YMXOg-ggKd1LYCm6Y7I'
    })
  ],
  providers: [
    AppConfig,
    AuthService,
    AuthGuardService,
    AnonGuardService,
    UserGuardService,
    PermissionGuardService,
    ResetTokenGuardService,
    {
      // Locale es-Chile
      provide: LOCALE_ID, useValue: 'es-CL'
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    CapsWarningService,
    TranslateService,
    TimeCalculatorService,
    TimeKeyInputService,
    MQTTService,
    ConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
    // Registramos los valores del Locale (nombre de dias, meses, moneda, etc)
    registerLocaleData(localeCL, 'es-CL');
  }

}
