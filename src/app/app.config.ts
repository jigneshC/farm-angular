import {Injectable} from '@angular/core';
import {Color, HEX} from './app.color';
import 'sass-to-js/js/src/sass-to-js.js';
import {environment} from '../environments/environment';


@Injectable()
export class AppConfig {

   sassVariables:any;
   config:any;

   constructor(){
		this.sassVariables = this.getSassVariables();
		this.config = {
			name: 'Antumalal Agricola',
			title: 'Gestion Agricola',
			version: '0.0.0',
            API_URL: environment.API_URL,
            API_LOGIN: environment.API_URL + environment.API_LOGIN,
            CLIENT_ID: environment.CLIENT_ID,
            CLIENT_SECRET: environment.CLIENT_SECRET,
			colors: {
				main: this.sassVariables['main-color'],
				default: this.sassVariables['default-color'],
				dark: this.sassVariables['dark-color'],
				primary: this.sassVariables['primary-color'],
				info: this.sassVariables['info-color'],
				success: this.sassVariables['success-color'],
				warning: this.sassVariables['warning-color'],
				danger: this.sassVariables['danger-color'],
				sidebarBgColor: this.sassVariables['sidebar-bg-color'],
				gray: this.sassVariables['gray'],
				grayLight: this.sassVariables['gray-light']
			},
			messages: {
				sure_delete: '¿Estás seguro de borrar este registro?',
				added: 'Registro agregado exitosamente',
				deleted: 'Registro borrado exitosamente',
				edited: 'Registro actualizado exitosamente'
			}
		}
   }

	getSassVariables() {
		let variables = jQuery('body').sassToJs({pseudoEl:"::after", cssProperty: "content"});
		return variables;
	}

	rgba(color, opacity){
		if(color.indexOf('#') >= 0){
			if(color.slice(1).length == 3){
				color= '#' + color.slice(1) + '' + color.slice(1);
			}
			return new Color(new HEX(color)).setAlpha(opacity).toString();
		}
		else{
			return 'rgba(255,255,255,0.7)';
		}
	}

}
