import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './../../services/auth/auth.service';
import { CapsWarningService } from './../../services/caps-warning.service';

@Component({
  selector: 'az-reset-password',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public error: boolean = false;
  public message: string = "";
  public password: AbstractControl;
  public repeat: AbstractControl;
  private email: string;
  private token: string;
  private paramsSubscription: any;
  private resetSubscription: any;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private capslock: CapsWarningService
  ) {
    this.form = fb.group({
        password: ['', Validators.compose([ Validators.required, Validators.minLength(8) ])],
        repeat: ['', Validators.compose([ Validators.required, Validators.minLength(8) ])]
    }, { validator: passwordMatchValidator });
    
		this.password = this.form.controls["password"];
		this.repeat = this.form.controls["repeat"];
  }

  public onSubmit( values: Object ): void {
    this.error = false;
    
    this.resetSubscription = this.auth.resetPassword( this.email, this.token, this.form.value.password )
    .subscribe(
      response => this.router.navigate(["/iniciar-sesion"]),
      error => {
        if ( error.errors && error.errors.password && error.errors.password[0] ) this.message = error.errors.password[0];
        else this.message = "Ha ocurrido un error. Por favor, intenta de nuevo.";
        this.error = true;
      }
    );
  }

  ngOnInit() {
		this.capslock.init();
    this.paramsSubscription = this.route.params.subscribe(params => {
       this.email = params.email;
       this.token = params.token;
    });
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
    this.resetSubscription && this.resetSubscription.unsubscribe();
  }
}


export function passwordMatchValidator( form: FormGroup ) {
   return form.get('password').value === form.get('repeat').value ? null : { 'mismatch': true };
}