import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResetPasswordComponent } from './reset-password.component';
import { MatTooltipModule } from '@angular/material/tooltip';

export const routes = [
  { path: '', component: ResetPasswordComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    RouterModule.forChild(routes),
  ],
  declarations: [ResetPasswordComponent]
})

export class ResetPasswordModule { }
