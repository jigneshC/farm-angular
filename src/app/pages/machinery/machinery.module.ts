import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {VehiclesComponent} from './vehicles/vehicles.component';
import {
  MatAutocompleteModule,
  MatCheckboxModule, MatDialogModule, MatInputModule, MatListModule, MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
  MatSortModule, MatTableModule
} from '@angular/material';
import {PipesModule} from '../../theme/pipes/pipes.module';
import {DirectivesModule} from '../../theme/directives/directives.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatTooltipModule} from '@angular/material/tooltip';
import {RouterModule} from '@angular/router';
import {VehicleService} from '../../services/api/machinery/vehicle.service';
import {CrudComponentsModule} from '../../theme/components/crud-components/crud-components.module';
import {VehicleFormComponent} from './vehicles/vehicle-form/vehicle-form.component';
import {ComponentsModule} from '../../theme/components/components.module';
import { MachinesComponent } from './machines/machines.component';
import {MachineService} from '../../services/api/machine/machine.service';
import { MachineFormComponent } from './machines/machine-form/machine-form.component';
import {TractorComponent} from './tractor/tractor.component';
import {TractorFormComponent} from './tractor/tractor-form/tractor-form.component';
import {TractorService} from '../../services/api/tractor/tractor.service';
import { MaintenancesComponent } from './maintenances/maintenances.component';
import {MaintenanceService} from '../../services/api/maintenance/maintenance.service';
import { MaintenanceFormComponent } from './maintenances/maintenance-form/maintenance-form.component';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {CostCenterService} from '../../services/api/cost-center/cost-center.service';
import {ExpenseService} from '../../services/api/expense/expense.service';
import {FilterFormModule} from '../applications/technical-reference/filter-form/filter-form.module';
import {ProviderService} from '../../services/api/provider/provider.service';
import {InvoiceService} from '../../services/api/invoice/invoice.service';
import {
  MaintenanceAdvanceFilterFormComponent
} from './maintenances/maintenance-advance-filter-form/maintenance-advance-filter-form.component';

export const routes = [
  { path: 'vehiculos', component: VehiclesComponent, data: { breadcrumb: 'vehículos' } },
  { path: 'maquinas', component: MachinesComponent, data: { breadcrumb: 'máquinas' }},
  { path: 'tractores', component: TractorComponent, data: { breadcrumb: 'tractores' }},
  { path: 'mantenciones', component: MaintenancesComponent, data: { breadcrumb: 'mantenciones' }}
];
@NgModule({
  entryComponents: [
    VehicleFormComponent,
    MachineFormComponent,
    TractorFormComponent,
    MaintenanceFormComponent,
    MaintenanceAdvanceFilterFormComponent
  ],
  imports: [
    CommonModule,
    CrudComponentsModule,
    FormsModule,
    DirectivesModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatListModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    ComponentsModule,
    DateTimePickerModule,
    RouterModule.forChild(routes),
    FilterFormModule
  ],
  declarations: [
    VehiclesComponent,
    VehicleFormComponent,
    MachinesComponent,
    MachineFormComponent,
    TractorFormComponent,
    TractorComponent,
    MaintenancesComponent,
    MaintenanceFormComponent,
    MaintenanceAdvanceFilterFormComponent
  ],
  providers: [
    VehicleService,
    MachineService,
    TractorService,
    MaintenanceService,
    CostCenterService,
    ExpenseService,
    ProviderService,
    InvoiceService,
    DatePipe
  ]
})
export class MachineryModule { }
