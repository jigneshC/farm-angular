import {Component, Inject, OnInit} from '@angular/core';
import {VehicleFormComponent} from '../../vehicles/vehicle-form/vehicle-form.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToasterService} from 'angular2-toaster';
import {FormBuilder} from '@angular/forms';
import {RestService} from '../../../../services/api/rest.service';
import {Tractor} from '../../../../models/tractor';
import {TractorService} from '../../../../services/api/tractor/tractor.service';

@Component({
  selector: 'az-tractor-form',
  templateUrl: './../../vehicles/vehicle-form/vehicle-form.component.html',
  styleUrls: ['./../../vehicles/vehicle-form/vehicle-form.component.scss'],
  providers: [
    {provide: RestService, useClass: TractorService}
  ],
})
export class TractorFormComponent extends VehicleFormComponent implements OnInit {

  public entityName = 'tractor';

  constructor(protected dialogRef: MatDialogRef<TractorFormComponent>,
              protected formBuilder: FormBuilder,
              protected restService: RestService<Tractor>,
              protected toaster: ToasterService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    super(dialogRef, formBuilder, restService, toaster, data);
  }

}
