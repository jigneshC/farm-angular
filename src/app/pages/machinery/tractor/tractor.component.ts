import {Component, OnInit} from '@angular/core';
import {RestService} from '../../../services/api/rest.service';
import {VehiclesComponent} from '../vehicles/vehicles.component';
import {MatDialog} from '@angular/material';
import {ToasterService} from 'angular2-toaster';
import {TractorFormComponent} from './tractor-form/tractor-form.component';
import {Tractor} from '../../../models/tractor';
import {TractorService} from '../../../services/api/tractor/tractor.service';
import {MachineryEntity} from '../../../models/machinery';

@Component({
  selector: 'az-machines',
  templateUrl: './../vehicles/vehicles.component.html',
  styleUrls: ['./../vehicles/vehicles.component.scss'],
  providers: [
    {provide: RestService, useClass: TractorService},
    {provide: MachineryEntity, useClass: Tractor},
  ]
})
export class TractorComponent extends VehiclesComponent implements OnInit {

  public title = 'LISTADO DE TRACTORES';
  public pluralEntityName = 'tractor';
  public entityName = 'tractores';

  protected machineryForm = TractorFormComponent;

  constructor(protected restService: RestService<Tractor>, protected  dialog: MatDialog, protected toaster: ToasterService) {
    super(restService, dialog, toaster);
  }



}
