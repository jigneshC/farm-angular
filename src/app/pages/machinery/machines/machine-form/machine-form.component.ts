import {Component, Inject, OnInit} from '@angular/core';
import {VehicleFormComponent} from '../../vehicles/vehicle-form/vehicle-form.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToasterService} from 'angular2-toaster';
import {FormBuilder} from '@angular/forms';
import {MachineService} from '../../../../services/api/machinery/machine.service';
import {RestService} from '../../../../services/api/rest.service';
import {Machine} from '../../../../models/machinery';

@Component({
  selector: 'az-machine-form',
  templateUrl: './../../vehicles/vehicle-form/vehicle-form.component.html',
  styleUrls: ['./../../vehicles/vehicle-form/vehicle-form.component.scss'],
  providers: [
    {provide: RestService, useClass: MachineService}
  ],
})
export class MachineFormComponent extends VehicleFormComponent implements OnInit {

  public entityName = 'máquina';

  constructor(protected dialogRef: MatDialogRef<MachineFormComponent>,
              protected formBuilder: FormBuilder,
              protected restService: RestService<Machine>,
              protected toaster: ToasterService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    super(dialogRef, formBuilder, restService, toaster, data);
  }

}
