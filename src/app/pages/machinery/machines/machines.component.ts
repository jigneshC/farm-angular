import {Component, OnInit} from '@angular/core';
import {RestService} from '../../../services/api/rest.service';
import {MachineService} from '../../../services/api/machinery/machine.service';
import {VehiclesComponent} from '../vehicles/vehicles.component';
import {MatDialog} from '@angular/material';
import {ToasterService} from 'angular2-toaster';
import {Machine} from '../../../models/machinery';
import {MachineFormComponent} from './machine-form/machine-form.component';

@Component({
  selector: 'az-machines',
  templateUrl: './../vehicles/vehicles.component.html',
  styleUrls: ['./../vehicles/vehicles.component.scss'],
  providers: [
    {provide: RestService, useClass: MachineService}
  ]
})
export class MachinesComponent extends VehiclesComponent implements OnInit {

  public title = 'LISTADO DE MAQUINAS';
  public pluralEntityName = 'máquina';
  public entityName = 'máquinas';

  protected machineryForm = MachineFormComponent;

  constructor(protected restService: RestService<Machine>, protected  dialog: MatDialog, protected toaster: ToasterService) {
    super(restService, dialog, toaster);
  }



}
