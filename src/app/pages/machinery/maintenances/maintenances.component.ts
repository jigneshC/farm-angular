import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Subject} from 'rxjs/Subject';
import {Subscription} from 'rxjs/Subscription';
import {Pagination} from '../../../services/api/pagination';
import {Observable} from 'rxjs/Observable';
import {DatePipe} from '@angular/common';
import {ToasterService} from 'angular2-toaster';
import {MaintenanceService} from '../../../services/api/maintenance/maintenance.service';
import {Maintenance} from '../../../models/maintenance';
import {HttpParams} from '@angular/common/http';
import {MaintenanceFormComponent} from './maintenance-form/maintenance-form.component';
import {Router} from '@angular/router';
import {ExpenseService} from '../../../services/api/expense/expense.service';
import {InvoiceService} from '../../../services/api/invoice/invoice.service';

@Component({
  selector: 'az-maintenances',
  templateUrl: './maintenances.component.html',
  styleUrls: ['./maintenances.component.scss']
})
export class MaintenancesComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  types = [
    { id: 0, value: 'Máquina'},
    { id: 1, value: 'Tractor'},
    { id: 2, value: 'Vehículo'},
  ];

  public columns = ['id', 'date', 'type', 'repairer', 'invoice', 'provider', 'amount', 'description', 'actions'];
  public source = new MatTableDataSource();
  public loadingResults = true;
  public error: string;
  haveError: boolean = false;
  public filter: string;
  public basicFilter: string = (new Date().getFullYear()) + "-" + (new Date().getMonth()+1);
  private isBasicFilter: boolean = true;
  public advanceFilter: any;
  timeout: any;
  total: number = 0;
  limit: number = 10;

  public filterChanged = new Subject<string>();
  public filterSubscription = new Subscription();
  protected formResultSubscription: Subscription;
  maintenance: Maintenance;

  constructor(
    private router: Router,
    private maintenanceService: MaintenanceService,
    private expenseService: ExpenseService,
    private invoiceService: InvoiceService,
    private dateTransform: DatePipe,
    private toaster: ToasterService,
    protected  dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  toggleFilters() {
    jQuery('#filters').slideToggle();
  }

  reload() {
    if ( this.timeout ) { clearTimeout(this.timeout) };
    this.source.data = [];
    this.filter = '';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  getTypeName(id: number): string {
    return this.types.filter(t => t.id === id).pop().value
  }

  downloadExpensePDF(id: number) {
    this.expenseService.downloadPDF(id)
      .subscribe(
        (res: any) => window.open(res.data.file_url),
        (error) => this.toaster.pop('error', 'Comprobante', 'Ha ocurrido un error al descargar el comprobante')
      );
  }

  downloadInvoicePDF(id: number) {
    this.invoiceService.downloadPDF(id)
      .subscribe(
        (res: any) => window.open(res.data.file_url),
        (error) => this.toaster.pop('error', 'Factura de compra', 'Ha ocurrido un error al descargar la factura')
      );
  }

  createOrEdit(row?: Maintenance) {
    this.maintenance = row;
    const data = this.maintenance ? {id: this.maintenance.id} : {};
    const form = this.dialog.open(MaintenanceFormComponent, {
      width: '800px',
      data
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.getMantenances();
      }
    })
  }

  delete() {
    this.maintenanceService.delete(String(this.maintenance.id))
      .subscribe(
        res => {
          if ( this.source.data.length === 1 && this.paginator.pageIndex > 0 ) { this.paginator.pageIndex-- };
          this.paginator.page.emit();
          this.toaster.pop('success', 'Mantención', 'Mantención eliminada exitosamente.');
        },
        error => {
          this.toaster.pop('error', 'Mantención', 'Ha ocurrido un error al eliminar la Mantención.');
        }
      );
  }

  goToInvoiceDetail(invoiceId: number) {
    this.router.navigateByUrl(`pages/contabilidad/compras/${invoiceId}`);
  }

  searchWithFilter(filters: any) {
    this.isBasicFilter = true;
    this.basicFilter = filters;
    this.search();
  }

  searchWithAdvanceFilter(filters: any) {
    this.isBasicFilter = false;
    this.advanceFilter = filters;
    this.search();
  }

  buildSearchParams(): HttpParams {
    let params = new HttpParams();

    if ( this.paginator.pageIndex >= 0 ) { params = params.append('page', `${ this.paginator.pageIndex + 1 }`) }
    if ( this.paginator.pageSize ) {params = params.append('p', `${this.paginator.pageSize}`) }
    if ( this.filter ) { params = params.append('q', this.filter) }
    if ( this.sort.active ) { params = params.append('sort_by', this.sort.active) }
    if ( this.sort.direction ) { params = params.append('sort_dir', this.sort.direction) }

    // basic filter
    if (this.basicFilter && this.isBasicFilter) {
      const since = this.basicFilter;
      const until = new Date(+this.basicFilter.split('-')[0], +this.basicFilter.split('-')[1], 0);
      params = params.append('from', this.dateTransform.transform(since, 'yyyy-MM-dd'));
      params = params.append('to', this.dateTransform.transform(until, 'yyyy-MM-dd'));
    }
    // advance filter
    if (this.advanceFilter && !this.isBasicFilter) {
      if (this.advanceFilter.start) {
        params = params.append('from', this.dateTransform.transform(this.advanceFilter.start, 'yyyy-MM-dd'));
      }
      if (this.advanceFilter.end) {
        params = params.append('to', this.dateTransform.transform(this.advanceFilter.end, 'yyyy-MM-dd'));
      }
      if (this.advanceFilter.tractor_id) { params = params.append('tractor_id', this.advanceFilter.tractor_id) };
      if (this.advanceFilter.machine_id) { params = params.append('machine_id', String(this.advanceFilter.machine_id)) };
      if (this.advanceFilter.vehicle_id) { params = params.append('vehicle_id', String(this.advanceFilter.vehicle_id)) };
    }

    return params;
  }

  getMantenances() {
    this.loadingResults = true;
    const params = this.buildSearchParams();

    this.maintenanceService.findPaginated(params)
      .catch(error => {
        this.error = error;
        return Observable.of([]);
      })
      .finally(() => {
        this.loadingResults = false;
      })
      .map((data: Pagination<Maintenance>) => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .subscribe((data: Maintenance[]) => {
          this.source.data = data;
        }
      );
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        const params = this.buildSearchParams();

        return this.maintenanceService.findPaginated(params);
      })
      .map((data: Pagination<Maintenance>) => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .catch( error => {
        this.loadingResults = false;
        this.haveError = true;
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data);
  }

  ngOnDestroy() {
    // this.formResultSubscription.unsubscribe();
  }

  keyupChange(): void {
    this.filterChanged.next(this.filter);
  }

  search() {
    if ( this.timeout ) { clearTimeout(this.timeout) }
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }
}
