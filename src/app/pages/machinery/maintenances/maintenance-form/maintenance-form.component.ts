import {Component, EventEmitter, Inject, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Maintenance} from '../../../../models/maintenance';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {TranslateService} from '../../../../services/translate.service';
import {DatePipe} from '@angular/common';
import {ToasterService} from 'angular2-toaster';
import {MaintenanceService} from '../../../../services/api/maintenance/maintenance.service';
import {TractorService} from '../../../../services/api/tractor/tractor.service';
import {VehicleService} from '../../../../services/api/machinery/vehicle.service';
import {MachineService} from '../../../../services/api/machine/machine.service';
import {Observable} from 'rxjs/Observable';
import {Vehicle} from '../../../../models/machinery';
import {Machine} from '../../../../models/machine';
import {Tractor} from '../../../../models/tractor';
import {CustomValidators} from 'ng2-validation';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {Expense} from '../../../../models/expense';
import {Invoice} from '../../../../models/invoice';
import {CostCenter} from '../../../../models/cost-center';
import {CostCenterService} from '../../../../services/api/cost-center/cost-center.service';
import {ExpenseService} from '../../../../services/api/expense/expense.service';
import {Provider} from '../../../../models/provider';
import {ProviderService} from '../../../../services/api/provider/provider.service';
import {InvoiceService} from '../../../../services/api/invoice/invoice.service';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'az-maintenance-form',
  templateUrl: './maintenance-form.component.html',
  styleUrls: ['./maintenance-form.component.scss']
})
export class MaintenanceFormComponent implements OnInit {

  @ViewChild('fileInput') fileInput;
  @Output() afterSave = new EventEmitter<string>(true);

  // standing data
  public EXPENSE_TYPES = {
    MACHINE: 0,
    TRACTOR: 1,
    VEHICLE: 2
  };

  types = [
    { id: 0, value: 'Máquina'},
    { id: 1, value: 'Tractor'},
    { id: 2, value: 'Vehículo'},
  ];

  states = [ { value: 0, name: 'NO PAGADO' }, { value: 1, name: 'PAGADO' } ];

  public title: string;
  public errorMessages = [];
  public form: FormGroup;
  public expenseForm: FormGroup;
  public isLoading = false;
  public isEdit = false;

  tractorCtrl: FormControl = new FormControl();
  vehicleCtrl: FormControl = new FormControl();
  machineCtrl: FormControl = new FormControl();
  costCenterCtrl: FormControl = new FormControl();
  providerCtrl: FormControl = new FormControl();
  filteredTractors: Observable<Tractor[]>;
  filteredVehicles: Observable<Vehicle[]>;
  filteredMachines: Observable<Machine[]>;
  filteredCostCenter: Observable<CostCenter[]>;
  filteredProviders: Observable<Provider[]>;
  filteredInvoices: Observable<Invoice[]>;

  maintenance: Maintenance = new Maintenance();
  vehicles: Vehicle[];
  machines: Machine[];
  tractors: Tractor[];
  costCenters: CostCenter[];
  providers: Provider[];
  invoices: Invoice[] = [];
  providerId: number;
  expense: Expense;
  invoice: Invoice;
  isInvoice = false;
  type: any;

  constructor(private maintenanceService: MaintenanceService,
              private tractorService: TractorService,
              private vehicleService: VehicleService,
              private machineService: MachineService,
              private costCenterService: CostCenterService,
              private expenseServices: ExpenseService,
              private invoiceService: InvoiceService,
              private providerServices: ProviderService,
              protected dialogRef: MatDialogRef<MaintenanceFormComponent>,
              protected formBuilder: FormBuilder,
              protected toaster: ToasterService,
              public translate: TranslateService,
              private dateTransform: DatePipe,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.isLoading = true;
    if (this.data.id) {
      this.initForEdition();
    } else {
      this.initForCreation();
    }
    this.createForm();
    this.createExpenseForm();
  }

  initForCreation() {
    this.isEdit = false;
    this.title = 'Crear Mantención';
    Observable.forkJoin(
      this.tractorService.findAll(),
      this.vehicleService.findAll(),
      this.machineService.findAll(),
      this.costCenterService.findAll(),
      this.providerServices.findAll()
    ).subscribe(data => {
      this.isLoading = false;
      this.isInvoice = false;
      this.tractors = data[0];
      this.vehicles = data[1];
      this.machines = data[2];
      this.costCenters = data[3];
      this.providers = data[4];
      this.expense =  new Expense();
      this.initAutoCompletes();
    }, error => {
      this.errorMessages = ['Ha ocurrido un error al inicializar la Mantención.'];
      this.isLoading = false;
    });
  }

  initForEdition() {
    this.isEdit = true;
    this.title = 'Editar Mantención';
    Observable.forkJoin(
      this.maintenanceService.findOne(this.data.id),
      this.tractorService.findAll(),
      this.vehicleService.findAll(),
      this.machineService.findAll(),
      this.costCenterService.findAll(),
      this.providerServices.findAll()
    ).subscribe(data => {
      this.isLoading = false;
      this.maintenance = data[0];
      this.tractors = data[1];
      this.vehicles = data[2];
      this.machines = data[3];
      this.costCenters = data[4];
      this.providers = data[5];
      this.initAutoCompletes();
      this.form.patchValue(data[0]);
      this.isInvoice = this.maintenance.invoice !== null;
      this.expense = this.maintenance.expense;
      this.type = this.maintenance.type;
      if (this.maintenance.expense) {
        this.expenseForm.patchValue(this.maintenance.expense);
      }
      if (this.maintenance.invoice) {
        this.initInvoiceAutoComplete([this.maintenance.invoice])
      }
    }, error => {
      this.errorMessages = ['Ha ocurrido un error al obtener la Mantención.'];
      this.isLoading = false;
    });
  }

  initAutoCompletes() {
    this.tractorCtrl = new FormControl(this.maintenance.tractor ? this.maintenance.tractor.identifier : null);
    this.machineCtrl = new FormControl(this.maintenance.machine ? this.maintenance.machine.identifier : null);
    this.vehicleCtrl = new FormControl(this.maintenance.vehicle ? this.maintenance.vehicle.identifier : null);
    this.costCenterCtrl = new FormControl(this.maintenance.expense ? this.maintenance.expense.costcenter_id : null);
    this.providerCtrl = new FormControl(this.maintenance.invoice ? this.maintenance.invoice.provider.name : null);
    this.filteredTractors = this.tractorCtrl.valueChanges
      .pipe(
        startWith(this.maintenance.tractor ? this.maintenance.tractor.brand + ' '
                                                + this.maintenance.tractor.model + ' ' + this.maintenance.tractor.year : ''),
        map(val => this.filterTractors(val))
      );
    this.filteredMachines = this.machineCtrl.valueChanges
      .pipe(
        startWith(this.maintenance.machine ? this.maintenance.machine.brand + ' '
                                                + this.maintenance.machine.model + ' ' + this.maintenance.machine.year : ''),
        map(val => this.filterMachines(val))
      );
    this.filteredVehicles = this.vehicleCtrl.valueChanges
      .pipe(
        startWith(this.maintenance.vehicle ? this.maintenance.vehicle.brand + ' '
                                                  + this.maintenance.vehicle.model + ' ' + this.maintenance.vehicle.year : ''),
        map(val => this.filterVehicles(val))
      );
    this.filteredCostCenter = this.costCenterCtrl.valueChanges
      .pipe(
        startWith(this.maintenance.expense ? this.maintenance.expense.costcenter_id : ''),
        map(val => this.filterCostCenter(String(val)))
      );
    this.filteredProviders = this.providerCtrl.valueChanges
      .pipe(
        startWith(this.maintenance.invoice ? this.maintenance.invoice.provider.name : ''),
        map(val => this.filterProviders(String(val)))
      );
  }

  initInvoiceAutoComplete(invoices: Invoice[]) {
    this.invoices = invoices;
    this.filteredInvoices = this.form.get('invoice_id').valueChanges
      .pipe(
        startWith(this.maintenance.invoice ? this.maintenance.invoice.id : ''),
        map(val => this.filterInvoices(+val))
      );
  }

  createExpenseForm() {
    // if ( this.fileInput ) { this.fileInput.nativeElement.value = '' };
    this.expenseForm = this.  formBuilder.group({
      expense_date: [ '', [ CustomValidators.date ] ],
      detail: [ '', [ Validators.maxLength(255) ] ],
      amount: [ '', [ Validators.required, CustomValidators.number ] ],
      notes: [ '', [ Validators.maxLength(255) ] ],
      state: [ 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      costcenter_id: [ null, [ Validators.required, CustomValidators.number ] ],
      invoice_id: [ null, [ CustomValidators.number ] ],
      file: [ null, this.isEdit ? [] : [ Validators.required ] ],
    });
  }

  createForm() {
    this.form = this.formBuilder.group({
      date: [ '', [ Validators.required, CustomValidators.date ] ],
      type: [ null, [ Validators.required ] ],
      repairer: [ '', [Validators.required, Validators.maxLength(80)]],
      description: [ '', [Validators.maxLength(255)]],
      tractor_id: [ null, []],
      machine_id: [ null, []],
      vehicle_id: [ null, []],
      invoice_id: [ null, []],
      expense_id: [ null, []]
    });
  }

  addTractor(tractor: any) { this.form.get('tractor_id').patchValue(tractor.id); }
  addMachine(machine: any) { this.form.get('machine_id').patchValue(machine.id); }
  addVehicle(vehicle: any) { this.form.get('vehicle_id').patchValue(vehicle.id); }
  addCostCenter(costCenter: any) { this.expenseForm.get('costcenter_id').patchValue(costCenter.id); }

  updateInvoiceValidation(validator: any) {
    this.form.get('invoice_id').patchValue(null);
    this.form.get('invoice_id').setValidators(validator);
    this.form.get('invoice_id').updateValueAndValidity();
  }

  updateFileValidation(validator: any) {
    this.expenseForm.get('file').patchValue('');
    this.expenseForm.get('file').setValidators(validator);
    this.expenseForm.get('file').updateValueAndValidity();
  }

  selectProvider(id: any) {
    this.isLoading = true;
    this.updateInvoiceValidation([Validators.required]);
    this.providerId = id;
    let params = new HttpParams();
    params = params.append('provider_id', String(this.providerId));
    params = params.append('p', String('-1'));
    this.invoiceService.findAll(params)
      .subscribe(invoices => {
        this.isLoading = false;
        this.initInvoiceAutoComplete(invoices);
      }, error => {
        this.isLoading = false;
        this.errorMessages.push('Ha ocurrido un error al intentar obtener las Facturas del Proveedor');
      });
  }

  selectInvoice(id: number) {
    this.isInvoice = true;
    this.form.get('invoice_id').patchValue(id);
    this.form.get('invoice_id').updateValueAndValidity();
  }

  addedFile( $event ) {
    if ( $event.target.files[0].type !== 'application/pdf' ) {
      $event.target.value = '';
      this.expenseForm.controls['file'].setValue(null);
      return this.expenseForm.controls['file'].setErrors({ 'type': true });
    }
    this.expenseForm.controls['file'].setErrors({ 'type': null });
    this.expenseForm.controls['file'].setValue($event.target.files[0]);
  }

  filterTractors(val: string): Tractor[] {
    return this.tractors.filter(tractor =>
      tractor.identifier.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  filterMachines(val: string): Machine[] {
    return this.machines.filter(machine =>
      machine.identifier.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  filterVehicles(val: string): Vehicle[] {
    return this.vehicles.filter(vehicle =>
      vehicle.identifier.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  filterCostCenter(val: string): CostCenter[] {
    return this.costCenters.filter(costcenter =>
      costcenter.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  filterProviders(val: string): Provider[] {
    return this.providers.filter(provider =>
      provider.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  filterInvoices(val: number): Invoice[] {
    return this.invoices.filter(invoice =>
      invoice.id === val);
  }

  inputProviderChange(value) {
    if (!value) {
      this.providerId = null;
      this.invoices = [];
      this.isInvoice = false;
      this.updateFileValidation([Validators.required]);
      this.updateInvoiceValidation([]);
    }
  }

  inputCostCenterChange(value) {
    if (!value) {
      this.expenseForm.get('costcenter_id').patchValue(null);
      this.expenseForm.get('costcenter_id').updateValueAndValidity();
    }
  }

  typeChange(typeId) {
    this.type = +typeId;
    if (this.EXPENSE_TYPES.TRACTOR === typeId) {
      this.form.get('tractor_id').setValidators([Validators.required]);
      this.form.get('vehicle_id').setValidators(null);
      this.form.get('machine_id').setValidators(null);
      this.form.get('vehicle_id').updateValueAndValidity();
      this.form.get('machine_id').updateValueAndValidity();
      this.form.get('tractor_id').updateValueAndValidity();
    } else if (this.EXPENSE_TYPES.MACHINE === typeId) {
      this.form.get('machine_id').setValidators([Validators.required]);
      this.form.get('vehicle_id').setValidators(null);
      this.form.get('tractor_id').setValidators(null);
      this.form.get('vehicle_id').updateValueAndValidity();
      this.form.get('tractor_id').updateValueAndValidity();
      this.form.get('machine_id').updateValueAndValidity();
    } else if (this.EXPENSE_TYPES.TRACTOR === typeId) {
      this.form.get('vehicle_id').setValidators([Validators.required]);
      this.form.get('machine_id').setValidators(null);
      this.form.get('tractor_id').setValidators(null);
      this.form.get('machine_id').updateValueAndValidity();
      this.form.get('vehicle_id').updateValueAndValidity();
    }
  }

  isType(type): boolean {
    return type === this.type;
  }

  close(): void {
    this.dialogRef.close();
  }

  parseForm(form): any {
    const formData = new FormData();
    for ( const key in form ) { if (key) {formData.append(key, form[key] )} }
    if (this.isEdit) { formData.append('_method', 'PUT'); }
    return formData;
  }

  purgeForm(form: any) {
    if (this.EXPENSE_TYPES.TRACTOR === this.type) {
      delete form.machine_id;
      delete form.vehicle_id;
    } else if (this.EXPENSE_TYPES.MACHINE === this.type) {
      delete form.tractor_id;
      delete form.vehicle_id;
    } else {
      delete form.tractor_id;
      delete form.machine_id;
    }

    if (!this.isInvoice) {
      delete form.invoice_id;
    } else {
      delete form.expense_id;
    }
    return form;
  }

  createMaintenance(form) {
    this.maintenanceService.create(this.purgeForm(form))
      .subscribe(maintenance => {
        this.isLoading = false;
        this.afterSave.emit('success');
        this.dialogRef.afterClosed().subscribe(() => {
          this.toaster.pop('success', 'Mantención', 'Mantención creada exitosamente.');
        });
        this.dialogRef.close();
      }, error => {
        this.isLoading = false;
        this.errorMessages.push('Ha ocurrido un error al intentar crear la Mantención.');
      });
  }

  saveMaintenance(form) {
    this.maintenanceService.update(String(this.maintenance.id), this.purgeForm(form))
      .subscribe(maintenance => {
        this.isLoading = false;
        this.afterSave.emit('success');
        this.dialogRef.afterClosed().subscribe(() => {
          this.toaster.pop('success', 'Mantención', 'Mantención editada exitosamente.');
        });
        this.dialogRef.close();
      }, error => {
        this.isLoading = false;
        this.errorMessages.push('Ha ocurrido un error al intentar guardar la Mantención.');
      });
  }

  save(form?: any, expenseForm?: any) {
    this.isLoading = true;
    this.form.value.date = this.dateTransform.transform(this.form.value.date, 'y-MM-dd');
    expenseForm.expense_date = this.form.value.date;
    expenseForm.detail = this.form.value.description;
    expenseForm.notes = this.form.value.description;

    if (this.isEdit) {
      if (!this.isInvoice) {
        if (!expenseForm.file) { delete expenseForm.file }
        if (!expenseForm.invoice_id) { delete expenseForm.invoice_id }
        this.expenseServices.update_post(String(this.expense.id), this.parseForm(expenseForm))
          .subscribe(
            expense => {
              this.saveMaintenance(form);
            },
            error => {
              this.isLoading = false;
              this.errorMessages.push('Ha ocurrido un error al intentar guardar el Gasto.');
            })
      } else {
        this.saveMaintenance(form);
      }
    } else {
      if (!this.isInvoice) {
        if (!expenseForm.invoice_id) { delete expenseForm.invoice_id }
        this.expenseServices.create(this.parseForm(expenseForm))
          .subscribe(
            expense => {
              this.isLoading = false;
              this.form.value.expense_id = expense.id;
              form.expense_id = expense.id;
              this.createMaintenance(form);
            },
            error => {
              this.isLoading = false;
              this.errorMessages.push('Ha ocurrido un error al intentar crear el Gasto.');
            });
      } else {
        this.createMaintenance(form);
      }
    }
  }

}
