import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TranslateService} from '../../../../services/translate.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {Machine} from '../../../../models/machine';
import {Vehicle} from '../../../../models/machinery';
import {Tractor} from '../../../../models/tractor';
import {TractorService} from '../../../../services/api/tractor/tractor.service';
import {VehicleService} from '../../../../services/api/machinery/vehicle.service';
import {MachineService} from '../../../../services/api/machine/machine.service';

@Component({
  selector: 'az-maintenance-advance-filter-form',
  templateUrl: './maintenance-advance-filter-form.component.html',
  styleUrls: ['./maintenance-advance-filter-form.component.scss']
})
export class MaintenanceAdvanceFilterFormComponent implements OnInit {

  tractorCtrl: FormControl = new FormControl();
  vehicleCtrl: FormControl = new FormControl();
  machineCtrl: FormControl = new FormControl();
  filteredTractors: Observable<Tractor[]>;
  filteredVehicles: Observable<Vehicle[]>;
  filteredMachines: Observable<Machine[]>;

  tractors: Tractor[];
  machines: Machine[];
  vehicles: Vehicle[];

  @Output() confirmFilters = new EventEmitter<boolean>();

  filtersForm: FormGroup;

  constructor(
    private tractorService: TractorService,
    private machineService: MachineService,
    private vehicleService: VehicleService,
    private fb: FormBuilder,
    public translate: TranslateService
  ) { }

  ngOnInit() {
    this.createFiltersForm();
    Observable.forkJoin(
      this.tractorService.findAll(),
      this.machineService.findAll(),
      this.vehicleService.findAll()
    ).subscribe(data => {
      this.tractors = data[0];
      this.machines = data[1];
      this.vehicles = data[2];
      this.initAutoCompletes();
    });
  }

  initAutoCompletes() {
    this.tractorCtrl = new FormControl(null);
    this.machineCtrl = new FormControl(null);
    this.vehicleCtrl = new FormControl(null);

    this.filteredTractors = this.tractorCtrl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filterTractors(val))
      );
    this.filteredMachines = this.machineCtrl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filterMachines(val))
      );
    this.filteredVehicles = this.vehicleCtrl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filterVehicles(val))
      );
  }

  filterTractors(val: string): Tractor[] {
    return this.tractors.filter(tractor =>
      tractor.identifier.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  filterMachines(val: string): Machine[] {
    return this.machines.filter(machine =>
      machine.identifier.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  filterVehicles(val: string): Vehicle[] {
    return this.vehicles.filter(vehicle =>
      vehicle.identifier.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  addTractor(tractor: any) { this.filtersForm.get('tractor_id').patchValue(tractor.id); }
  addMachine(machine: any) { this.filtersForm.get('machine_id').patchValue(machine.id); }
  addVehicle(vehicle: any) { this.filtersForm.get('vehicle_id').patchValue(vehicle.id); }

  search(form: any) {
    this.confirmFilters.emit(form);
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      start: [ '' ],
      end: [ '' ],
      tractor_id: [ null, []],
      machine_id: [ null, []],
      vehicle_id: [ null, []],
    });
  }

  resetFilters() {
    this.filtersForm.reset();
    this.vehicleCtrl.patchValue('');
    this.machineCtrl.patchValue('');
    this.tractorCtrl.patchValue('');
  }

}
