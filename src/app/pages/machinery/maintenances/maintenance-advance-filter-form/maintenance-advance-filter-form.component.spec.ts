import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceAdvanceFilterFormComponent } from './maintenance-advance-filter-form.component';

describe('MaintenanceAdvanceFilterFormComponent', () => {
  let component: MaintenanceAdvanceFilterFormComponent;
  let fixture: ComponentFixture<MaintenanceAdvanceFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceAdvanceFilterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceAdvanceFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
