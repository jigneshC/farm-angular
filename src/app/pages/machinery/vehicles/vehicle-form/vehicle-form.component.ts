import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {VehicleService} from '../../../../services/api/machinery/vehicle.service';
import {MachineryEntity, Vehicle} from '../../../../models/machinery';
import {HttpErrorResponse} from '@angular/common/http';
import {capitalize, isExtensionImage, toFormData} from '../../../../utils/utils';
import {RestService} from '../../../../services/api/rest.service';

@Component({
  selector: 'az-vehicle-form',
  templateUrl: './vehicle-form.component.html',
  styleUrls: ['./vehicle-form.component.scss'],
  providers: [
    {provide: RestService, useClass: VehicleService},
    {provide: MachineryEntity, useClass: Vehicle},
  ]
})
export class VehicleFormComponent implements OnInit {

  public title: string;
  public entityName = 'vehículo';
  public errorMessages = [];
  public form: FormGroup;
  public isLoading = false;
  public errors: any = {};
  public isEdit: boolean;

  @Output() afterSave = new EventEmitter<string>(true);

  constructor(protected dialogRef: MatDialogRef<any>,
              protected formBuilder: FormBuilder,
              protected restService: RestService<MachineryEntity>,
              protected toaster: ToasterService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.initForm();
    if (this.isThisEdit()) {
      this.initializeForEdition()
    } else {
      this.initializeForCreation()
    }
  }

  protected isThisEdit(): boolean {
    return this.data.entityId;
  }

  protected initForm() {
    const fileValidators = this.isThisEdit() ? [] : Validators.required;
    this.form = this.formBuilder.group({
      brand: ['', [Validators.required, Validators.maxLength(50)]],
      model: ['', [Validators.required, Validators.maxLength(50)]],
      year: ['', [Validators.required]],
      identifier: ['', [Validators.required]],
      notes: ['', [Validators.maxLength(255)]],
      file: ['', fileValidators]
    });
  }

  close(): void {
    this.dialogRef.close();
  }

  protected initializeForCreation(): void {
    this.isEdit = false;
    this.title = `Crear ${capitalize(this.entityName)}`;
  }

  protected initializeForEdition(): void {
    this.isEdit = true;
    this.title = `Editar ${capitalize(this.entityName)}`;
    this.isLoading = true;
    this.restService.findOne(this.data.entityId)
      .subscribe(
        result => {
          this.isLoading = false;
          this.updateForm(result);
        },
        error => {
          this.errorMessages = [`Ha ocurrido un error al obtener el ${this.entityName}.`];
          this.isLoading = false;
        });
  }

  submit(): void {
    if (this.isEdit) {
      const entity = this.buildEntityForSubmit();
      // Image param for editing only
      entity.append('_method', 'PUT');
      this.isLoading = true;
      this.restService.update_post(this.data.entityId, entity)
        .subscribe(
          res => {
            this.isLoading = false;
            this.afterSave.emit('success');
            this.dialogRef.afterClosed().subscribe(() => {
              this.toaster.pop('success', `${capitalize(this.entityName)}`, `${capitalize(this.entityName)} editado exitosamente.`);
            });
            this.dialogRef.close();
          },
          error => {
            this.isLoading = false;
            this.handleError(error);
          });
    } else {
      const entity = this.buildEntityForSubmit();
      this.isLoading = true;
      this.restService.create(entity)
        .subscribe(res => {
            this.isLoading = false;
            this.afterSave.emit('success');
            this.dialogRef.afterClosed().subscribe(() => {
              this.toaster.pop('success', `${capitalize(this.entityName)}`, `${capitalize(this.entityName)} guardado exitosamente.`);
            });
            this.dialogRef.close();
          },
          error => {
            this.isLoading = false;
            this.handleError(error);
          });
    }
  }

  // Workaround to update form validity.
  protected updateForm(value) {
    setTimeout(() => {
      this.form.patchValue(value);
    });
  }

  evaluateClassError(formControlName: string, errorAttribute?: string): any {
    if (formControlName && errorAttribute) {
      const form = this.form.get(formControlName);
      return {
        'has-danger': form.touched && (form.invalid || this.errors[errorAttribute]),
        'has-success': form.touched && (form.valid || !this.errors[errorAttribute])
      };
    } else if (formControlName && !errorAttribute) {
      const form = this.form.get(formControlName);
      return {
        'has-danger': form.touched && form.invalid,
        'has-success': form.touched && form.valid
      };
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error) {
      const errorObj = error.error;
      this.errorMessages = ['Ha ocurrido un error al intentar guardar.'];
      if (errorObj.errors) {
        this.errors = errorObj.errors;
      }
    }

  }

  protected buildEntityForSubmit(): any {
    const entity = Object.assign({id: null}, this.form.value);
    if (this.isEdit) {
      entity.id = this.data.entityId;
    }
    if (!this.form.get('file').value) {
      delete entity['file'];
    }
    return toFormData(entity);
  }

  public addedFile($event): void {
    const formControl = this.form.get('file');
    if ( !isExtensionImage($event.target.files[0].type) ) {
      $event.target.value = '';
      formControl.setValue(null);
      formControl.setErrors({ 'type': true });
    } else {
      formControl.setErrors(null);
      formControl.setValue($event.target.files[0]);
    }
  }

}
