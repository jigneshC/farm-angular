import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {Observable} from 'rxjs/Observable';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ISubscription} from 'rxjs/Subscription';
import {Subject} from 'rxjs/Subject';
import {MachineryEntity, Vehicle} from '../../../models/machinery';
import {Pagination} from '../../../services/api/pagination';
import {VehicleService} from '../../../services/api/machinery/vehicle.service';
import {VehicleFormComponent} from './vehicle-form/vehicle-form.component';
import {ImageDialogComponent} from '../../../theme/components/image-dialog/image-dialog.component';
import {RestService} from '../../../services/api/rest.service';
import {capitalize} from '../../../utils/utils';

@Component({
  selector: 'az-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss'],
  providers: [
    {provide: RestService, useClass: VehicleService},
    {provide: MachineryEntity, useClass: Vehicle},
  ]
})
export class VehiclesComponent implements OnInit, OnDestroy, AfterViewInit {

  public title = 'LISTADO DE VEHICULOS';
  public entityName = 'vehículo';
  public pluralEntityName = 'vehículos';

  protected machineryForm = VehicleFormComponent;

  // Table attributes.
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public total = 0;
  public tableData: MatTableDataSource<MachineryEntity>;
  public loadingResults = false;
  public displayedColumns = ['model', 'brand', 'year', 'identifier', 'notes', 'image', 'actions'];
  public selectedEntity: MachineryEntity;

  public error: string;
  public filter: string;

  public filterChanged = new Subject<string>();
  public subscriptions: ISubscription[] = [];

  constructor(protected restService: RestService<any>, protected  dialog: MatDialog, protected toaster: ToasterService) {

  }

  ngOnInit() {
    this.tableData = new MatTableDataSource<MachineryEntity>([]);
    this.subscriptions.push(
      this.filterChanged
        .debounceTime(300)
        .distinctUntilChanged()
        .subscribe(() => {
          this.getData()
        }));
    this.subscriptions.push(this.filterChanged);

  }

  getData(): void {
    this.paginator.page.emit();
  }

  refreshData(): void {
    this.paginator.pageIndex = 0;
    this.getData();
  }

  entityObservable(params): Observable<Pagination<MachineryEntity>> {
    this.setIsLoading(true);
    return this.restService.findPaginated(params);
  }

  createOrEdit(entityId?): void {
    const data = entityId ? {entityId: entityId} : {};
    const form = this.dialog.open(this.machineryForm, {
      width: '800px',
      data
    });
    this.subscriptions.push(form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.getData();
      }
    }));
  }

  delete() {
    this.restService.delete(String(this.selectedEntity.id))
      .subscribe(
        res => {
          this.toaster.pop('success', this.pluralEntityName, `${capitalize(this.entityName)} eliminado exitosamente.`);
          this.getData();
        },
        error => {
          this.toaster.pop('error', this.pluralEntityName, `Ha ocurrido un error al eliminar el ${this.entityName}.`);
        }
      );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      if (subscription && !subscription.closed) {
        subscription.unsubscribe();
      }
    }
  }

  keyupChange(): void {
    this.filterChanged.next(this.filter);
  }

  updatePaginator(pagination: Pagination<any>) {
    this.paginator.pageSize = pagination.per_page;
    this.paginator.pageIndex = pagination.current_page - 1;
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );
    this.subscriptions.push(Observable.merge(this.sort.sortChange, this.paginator.page)
      .switchMap(() => {
        return this.entityObservable(this.buildQueryParams());
      })
      .subscribe((result: Pagination<MachineryEntity>) => {
          this.loadingResults = false;
          this.updateData(result);
        },
        err => {
          this.loadingResults = false;
          this.toaster.pop('error', this.title, `Ha ocurrido un error al buscar los ${this.pluralEntityName}.`);
        }));
    this.paginator.page.emit();
  }

  openImageModal(url) {
    const data = {
      url: url
    };
    this.dialog.open(ImageDialogComponent, {
      data
    })
  }

  protected updateData(result: Pagination<MachineryEntity>) {
    this.total = result.total;
    this.updatePaginator(result);
    this.tableData.data = result.data;
  }

  protected buildQueryParams(): any {
    const params = <any> {};
    params.page = this.paginator.pageIndex + 1;
    params.p = this.paginator.pageSize;
    if (this.filter) {
      params.q = this.filter;
    }
    if (this.sort.active) {
      params.sort_by = this.sort.active;
    }
    if (this.sort.direction) {
      params.sort_dir = this.sort.direction;
    }
    return params;
  }

  protected setIsLoading(val: boolean): void {
    setTimeout(() => {
      this.loadingResults = val;
    })
  }

}
