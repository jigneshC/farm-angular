import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2CompleterModule } from 'ng2-completer';
import { FormValidationService } from './../../services/form-validation.service';
import { CrudComponentsModule } from './../../theme/components/crud-components/crud-components.module';
import { InvoicesComponent } from './invoices/invoices.component';
import { FormInvoicesComponent } from './invoices/form-invoice/form-invoice.component';
import { ViewInvoicesComponent } from './invoices/view-invoice/view-invoice.component';
import { SalesComponent } from './sales/sales.component';
import { FormSalesComponent } from './sales/form/form.component';
import { ViewSalesComponent } from './sales/view/view.component';
import { SaleItemsComponent } from './sale-items/sale-items.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { FiscalexpensesComponent } from './fiscalexpenses/fiscalexpenses.component';
import { CostCenterComponent } from './cost-center/cost-center.component';
import { ShoppingReportsComponent } from './shoppingReports/reports.component';
import { ProvidersComponent } from './providers/providers.component';
import { SeasonsComponent } from './seasons/seasons.component';
import { BankloansComponent } from './bankloans/bankloans.component';
import { SelectHelperService } from './../../services/select-helper.service';
import { DataTableModule } from 'angular2-datatable';
import { ApiService } from './../../services/api.service';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { CustomFormsModule } from 'ng2-validation'
import { MatPaginatorLabels } from './../../services/mat-paginator-labels.service';
import { DropzoneModule, DROPZONE_CONFIG, DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import {
  MatTooltipModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule,
  MatInputModule, MatPaginatorIntl, MatAutocompleteModule, MatCheckboxModule, MatSelectModule, MatProgressBarModule
} from '@angular/material';
import 'chart.js/dist/Chart.js';
import { CommitmentComponent } from './commitment/commitment.component';

import {NgxChartsModule} from '@swimlane/ngx-charts';
import {CommitmentService} from '../../services/api/commitment/commitment.service';
import {DueDatesComponent} from './due-dates/due-dates.component';
import {PurchasesBookComponent} from './purchasesbook/purchasesbook.component';
import {PurchasesBookFilterComponent} from "./purchasesbook/purchases-book-filter/purchases-book-filter.component";
import {ExpensesAdvanceFilterComponent} from "./expenses/expenses-advance-filter/expenses-advance-filter.component";
import {ExpensesSimpleFilterComponent} from "./expenses/expenses-simple-filter/expenses-simple-filter.component";
import {ShoppingReportsSimpleFilterComponent} from "./shoppingReports/reports-simple-filter/reports-simple-filter.component";
import {FilterFormModule} from '../applications/technical-reference/filter-form/filter-form.module';
import {DueDateService} from '../../services/api/due-date/due-date.service';
import {DueDatesAdvanceFilterFormComponent } from './due-dates/due-dates-advance-filter-form/due-dates-advance-filter-form.component';
import {ProviderService} from '../../services/api/provider/provider.service';
import {InvoiceService} from '../../services/api/invoice/invoice.service';
import {CostCenterService} from '../../services/api/cost-center/cost-center.service';
import {ExpenseService} from '../../services/api/expense/expense.service';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  url: ' '
};

export const routes = [
  { path: '', redirectTo: 'compras', pathMatch: 'full' },
  { path: 'compras', component: InvoicesComponent, data: { breadcrumb: 'Compras' } },
  { path: 'libro-de-compras', component: PurchasesBookComponent, data: { breadcrumb: 'Libro de Compras' } },
  { path: 'compras/crear', component: FormInvoicesComponent, data: { breadcrumb: 'Crear factura de compra' } },
  { path: 'compras/:id', component: ViewInvoicesComponent, data: { breadcrumb: 'Factura de compra' } },
  { path: 'compras/:id/editar', component: FormInvoicesComponent, data: { breadcrumb: 'Editar factura de compra' } },
  { path: 'ventas', component: SalesComponent, data: { breadcrumb: 'Ventas' } },
  { path: 'ventas/crear', component: FormSalesComponent, data: { breadcrumb: 'Crear factura de venta' } },
  { path: 'ventas/:id', component: ViewSalesComponent, data: { breadcrumb: 'Factura de venta' } },
  { path: 'ventas/:id/editar', component: FormSalesComponent, data: { breadcrumb: 'Editar factura de venta' } },
  { path: 'items-venta', component: SaleItemsComponent, data: { breadcrumb: 'Items de venta' } },
  { path: 'gastos', component: ExpensesComponent, data: { breadcrumb: 'Gastos' } },
  { path: 'gastos-fiscales', component: FiscalexpensesComponent, data: { breadcrumb: 'Gastos Fiscales' } },
  { path: 'centro-costos', component: CostCenterComponent, data: { breadcrumb: 'Centro de Costos' } },
  { path: 'reportes-compras', component: ShoppingReportsComponent, data: { breadcrumb: 'Reportes de Compras de Proveedores' } },
  { path: 'proveedores', component: ProvidersComponent, data: { breadcrumb: 'Proveedores' } },
  { path: 'temporadas', component: SeasonsComponent, data: { breadcrumb: 'Temporadas' } },
  { path: 'prestamos-bancarios', component: BankloansComponent, data: { breadcrumb: 'Préstamos bancarios' } },
  { path: 'compromisos', component: CommitmentComponent, data: { breadcrumb: 'Compromisos' } },
  { path: 'vencimientos/:mes/:anio', component: DueDatesComponent, data: { breadcrumb: 'Vencimientos' } },
  { path: 'vencimientos', component: DueDatesComponent, data: { breadcrumb: 'Vencimientos' } },
];

@NgModule({
  entryComponents: [
    DueDatesAdvanceFilterFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    DataTableModule,
    PipesModule,
    CrudComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2CompleterModule,
    NgxPaginationModule,
    DateTimePickerModule,
    CustomFormsModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatAutocompleteModule,
    MatCheckboxModule,
    MatSelectModule,
    DropzoneModule,
    RouterModule.forChild(routes),
    NgxChartsModule,
    FilterFormModule,
    MatProgressBarModule
  ],
  declarations: [
    InvoicesComponent,
    FormInvoicesComponent,
    ViewInvoicesComponent,
    SalesComponent,
    FormSalesComponent,
    ViewSalesComponent,
    SaleItemsComponent,
    ExpensesComponent,
    FiscalexpensesComponent,
    CostCenterComponent,
    PurchasesBookComponent,
    ShoppingReportsComponent,
    ProvidersComponent,
    SeasonsComponent,
    BankloansComponent,
    CommitmentComponent,
    DueDatesComponent,
    PurchasesBookFilterComponent,
    ExpensesAdvanceFilterComponent,
    ExpensesSimpleFilterComponent,
    ShoppingReportsSimpleFilterComponent,
    DueDatesAdvanceFilterFormComponent
  ],
  providers: [
    ApiService,
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorLabels
    },
    DatePipe,
    SelectHelperService,
    FormValidationService,
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    },
    CommitmentService,
    DueDateService,
    ProviderService,
    InvoiceService,
    CostCenterService,
    ProviderService,
    ExpenseService
  ]
})

export class AccountingModule { }
