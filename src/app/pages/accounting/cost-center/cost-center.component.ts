import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { AppConfig } from './../../../app.config';
import { ApiService } from './../../../services/api.service';
import {Component, ViewEncapsulation, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { HttpParams } from '@angular/common/http';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';

@Component({
	selector: 'cost-center',
	templateUrl: 'cost-center.component.html',
	styleUrls:['cost-center.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class CostCenterComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public columns = ['id', 'name', 'notes', 'actions'];
  public source = new MatTableDataSource();
  public loadingResults = true;
  haveError: boolean = false;
  timeout: any;
  total: number = 0;

	costs:Array<any> = [];
	config:any;
	costForm:FormGroup;
	create:boolean = true;
	costId:number = 0;
	messages:any;
	searchText:string = '';
	page:any;
	count:any;
	limit = 10;
	constructor(
		private API: ApiService,
		private _config: AppConfig,
		private _fb: FormBuilder,
		private toaster: ToasterService,
	) {
		this.config = this._config.config;
		this.costForm = _fb.group({
			name:['',Validators.compose([Validators.required])],
			notes:['',Validators.compose([Validators.required])],


		})
	}

	ngOnInit(){
		// this.loadCosts();
		this.messages = this._config.config.messages;

	}
	savecost(){
		this.API.save('costcenters', this.costForm.value)
				.subscribe(
					res => {
						this.toaster.pop('success', 'Centro de costo', 'El centro de costo ha sido guardado exitosamente');
						this.search();
					}, err => {
						this.toaster.pop('error', 'Centro de costo', 'Ha ocurrido un error al guardar el centro de costo');
					})
	}
	loadCosts() {
		let params = new HttpParams();

    if ( this.paginator.pageIndex >= 0 ) { params = params.append('page', `${ this.paginator.pageIndex + 1 }`) }
    if ( this.paginator.pageSize ) {params = params.append('p', `${this.paginator.pageSize}`) }
    if ( this.searchText ) { params = params.append('q', this.searchText) }
    if ( this.sort.active ) { params = params.append('sort_by', this.sort.active) }
    if ( this.sort.direction ) { params = params.append('sort_dir', this.sort.direction) }

		return this.API.index('costcenters', params);
	}
	editcost(){
		this.API.update('costcenters', this.costForm.value, this.costId)
				.subscribe(
					res => {
						this.toaster.pop('success', 'Centro de costo', 'El centro de costo ha sido actualizado exitosamente');
						this.resetStatus();
					}, err => {
						this.toaster.pop('error', 'Centro de costo', 'Ha ocurrido un error al actualizar el centro de costo');
					})
	}
	deletecost(){
		this.API.delete('costcenters', this.costId)
				.subscribe(
					res => {
						this.toaster.pop('success', 'Centro de costo', 'El centro de costo ha sido eliminado exitosamente');
						this.resetStatus();
					}, err => {
						this.toaster.pop('error', 'Centro de costo', 'Ha ocurrido un error al eliminar el centro de costo');
					})
	}
	detailcost(cost){
		this.costId = cost.id;
		this.costForm.setValue(
			{
				name: cost.name,
				notes:cost.notes,


			}
		);
		this.create = false;
	}
	resetStatus(){
		this.costForm.setValue({
			name: '',
			notes:''

		});
		this.create = true;
		this.search();
	}
	searchCostCenter() {
		this.API.searchModel('costcenters', this.searchText).subscribe(
			(res) => {
				this.total = res.json().total;
				this.limit = res.json().per_page;
				this.page = res.json().current_page;
				this.costs = res.json().data;
		})
	}
	handlePageChange(page: any): void {
		this.page = page;
		this.loadCosts();
	  }


  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        return this.loadCosts();
      }).map((data: any) => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .catch( err => {
        this.loadingResults = false;
        this.haveError = true;
        this.toaster.pop('error', 'Centros de costos', 'Ha ocurrido un error al buscar los centros de costos');
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data);
  }

  search() {
    if ( this.timeout ) { clearTimeout(this.timeout) };
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }
}
