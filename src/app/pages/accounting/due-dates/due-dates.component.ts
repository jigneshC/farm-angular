import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {Subscription} from 'rxjs/Subscription';
import {HttpParams} from '@angular/common/http';
import {DueDateService} from '../../../services/api/due-date/due-date.service';
import {DueDate} from '../../../models/due-date';
import {DatePipe} from '@angular/common';
import {Router, ActivatedRoute} from '@angular/router';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'az-due-dates',
  templateUrl: './due-dates.component.html',
  styleUrls: ['./due-dates.component.scss']
})
export class DueDatesComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public columns = ['provider_id', 'issue_date', 'gross_amount', 'currency', 'state'];
  public source = new MatTableDataSource();
  public loadingResults = true;
  public error: string;
  haveError: boolean = false;
  public filter: string;
  public basicFilter: string = (new Date().getFullYear())+"-"+(new Date().getMonth()+1);
  private isBasicFilter: boolean = true;
  public advanceFilter: any;
  public isToggledFilter: boolean = false;
  timeout: any;
  total: number = 0;
  limit: number = 10;

  currencies = [ { value: 0, name: 'PESOS' }, { value: 1, name: 'USD' } ];
  states = [ { value: 0, name: 'NO PAGADA' }, { value: 1, name: 'PAGADA' }, { value: 2, name: 'PAGADA PARCIALMENTE ENTREGADO' } ];
  totalesRow = {
    seasson: null,
    provider_id: '',
    issue_date: '',
    gross_amount: 0,
    currency: '',
    state: ''
  };

  public filterChanged = new Subject<string>();
  public filterSubscription = new Subscription();

  constructor(
    private toaster: ToasterService,
    private router: Router,
    private route: ActivatedRoute,
    private dueDateService: DueDateService,
    private dateTransform: DatePipe
  ) {}

  isTotalsRow = (_, rowData) => rowData.state === '';

  reload() {
    if ( this.timeout ) { clearTimeout(this.timeout) }
    this.source.data = [];
    this.filter = '';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  ngOnInit() {
    if (this.route.snapshot.params.mes) {
      this.isBasicFilter = true;
      this.basicFilter = this.route.snapshot.params.anio + '-' + (this.route.snapshot.params.mes);
    }
  }

  toggleFilters() {
    this.isToggledFilter = !this.isToggledFilter;
    jQuery('#filters').slideToggle();
  }

  searchWithFilter(filters: any) {
    this.isBasicFilter = true;
    console.log(filters);
    this.basicFilter = filters;
    this.search();
  }

  searchWithAdvanceFilter(filters: any) {
    this.isBasicFilter = false;
    this.advanceFilter = filters;
    this.search();
  }

  generatePDF() {
    if (this.isToggledFilter) { this.toggleFilters() }
    if (!this.basicFilter) {
      const auxDate = new Date();
      this.basicFilter = String(auxDate.getFullYear()) + '-' + String(auxDate.getMonth() + 1);
    }
    const params = this.basicFilter.split('-');
    window.open(this.dueDateService.downloadReport(params[1], params[0]));
  }

  buildSearchParams(): HttpParams {
    let params = new HttpParams();

    if ( this.paginator.pageIndex >= 0 ) { params = params.append('page', `${ this.paginator.pageIndex + 1 }`) }
    if ( this.paginator.pageSize ) {params = params.append('p', `${this.paginator.pageSize}`) }
    if ( this.filter ) { params = params.append('q', this.filter) }
    if ( this.sort.active ) { params = params.append('sort_by', this.sort.active) }
    if ( this.sort.direction ) { params = params.append('sort_dir', this.sort.direction) }

    // basic filter
    if (this.basicFilter && this.isBasicFilter) {
      const since = this.basicFilter;
      const until = new Date(+this.basicFilter.split('-')[0], +this.basicFilter.split('-')[1], 0);
      params = params.append('due_date_from', this.dateTransform.transform(since, 'yyyy-MM-dd'));
      params = params.append('due_date_to', this.dateTransform.transform(until, 'yyyy-MM-dd'));
    }
    // advance filter
    if (this.advanceFilter && !this.isBasicFilter) {
      if (this.advanceFilter.start) {
        params = params.append('due_date_from', this.dateTransform.transform(this.advanceFilter.start, 'yyyy-MM-dd'));
      }
      if (this.advanceFilter.end) {
        params = params.append('due_date_to', this.dateTransform.transform(this.advanceFilter.end, 'yyyy-MM-dd'));
      }
      if (this.advanceFilter.provider_id) { params = params.append('provider_id', this.advanceFilter.provider_id) };
    }

    return params;
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        const params = this.buildSearchParams();

        return this.dueDateService.getAll(params);
      })
      .map((data: any) => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.meta.total;
        this.limit = data.meta.per_page;
        this.totalesRow.gross_amount = data.meta.totals.total_gross;
        if (data.data.length > 0) { data.data.push(this.totalesRow) };
        return data.data;
      })
      .catch( error => {
        this.loadingResults = false;
        this.haveError = true;
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data);
  }

  ngOnDestroy() {
    // this.formResultSubscription.unsubscribe();
  }

  keyupChange(): void {
    this.filterChanged.next(this.filter);
  }

  search() {
    if ( this.timeout ) { clearTimeout(this.timeout) };
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }

}
