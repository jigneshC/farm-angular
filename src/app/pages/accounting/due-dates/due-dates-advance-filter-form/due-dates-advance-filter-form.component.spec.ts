import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DueDatesAdvanceFilterFormComponent } from './due-dates-advance-filter-form.component';

describe('DueDatesAdvanceFilterFormComponent', () => {
  let component: DueDatesAdvanceFilterFormComponent;
  let fixture: ComponentFixture<DueDatesAdvanceFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DueDatesAdvanceFilterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DueDatesAdvanceFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
