import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Provider} from '../../../../models/provider';
import {startWith} from 'rxjs/operators/startWith';
import {TranslateService} from '../../../../services/translate.service';
import {Observable} from 'rxjs/Observable';
import {ProviderService} from '../../../../services/api/provider/provider.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {map} from 'rxjs/operators/map';

@Component({
  selector: 'az-due-dates-advance-filter-form',
  templateUrl: './due-dates-advance-filter-form.component.html',
  styleUrls: ['./due-dates-advance-filter-form.component.scss']
})
export class DueDatesAdvanceFilterFormComponent implements OnInit {

  @Output() confirmFilters = new EventEmitter<boolean>();

  filtersForm: FormGroup;

  providerCtrl: FormControl = new FormControl();
  filteredProviders: Observable<Provider[]>;
  providers: Provider[] = [];

  constructor(
    private providerServices: ProviderService,
    private fb: FormBuilder,
    public translate: TranslateService
  ) { }

  ngOnInit() {
    this.providerServices.findAll()
      .subscribe(providers => {
        this.providers =  providers;
        this.initAutoCompletes();
      });
    this.createFiltersForm();
  }

  initAutoCompletes() {
    this.providerCtrl = new FormControl(null);
    this.filteredProviders = this.providerCtrl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filterProviders(val))
      );
  }

  filterProviders(val: string): Provider[] {
    return this.providers.filter(provider =>
      provider.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  selectProvider(id: number) {
    this.filtersForm.get('provider_id').patchValue(id);
  }

  search(form: any) {
    this.confirmFilters.emit(form);
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      start: [ '' ],
      end: [ '' ],
      provider_id: [ 0 ],
    });
  }

  resetFilters() {
    this.filtersForm.reset();
  }

}
