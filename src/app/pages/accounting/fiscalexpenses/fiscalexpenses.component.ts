import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';

import DateGMT from 'app/classes/date-gmt';

const MODEL = "fiscalexpenses";

@Component({
  selector: 'az-fiscalexpenses',
  templateUrl: './fiscalexpenses.component.html',
  styleUrls: ['./fiscalexpenses.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FiscalexpensesComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['fiscal_categories_id', 'date', 'state', 'amount', 'notes', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  filtersForm1: FormGroup;
  filtersForm2: FormGroup;
  seasonCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  seasons;
  seasonSub;
  lastYears: Array<any> = [];
  fiscalCategories: Array<any> = [];
  states = [ { value: 0, name: 'SIN PAGAR' }, { value: 1, name: 'PAGADO' } ];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
  ) {
    this.createForm();
    this.createFiltersForm1();
    this.createFiltersForm2();
    this.getLastYears();
    this.getFiscalCategories();
  }

  getFiscalCategories() {
    let params = new HttpParams();
    params = params.append('p', `-1`);

    this.API.index('fiscalcategories', params)
      .subscribe(
        res => {
          this.fiscalCategories = res.data;
        })
  }

  getLastYears() {
    let params = new HttpParams();
    params = params.append('p', `1`);
    params = params.append('sort_dir', `asc`);

    this.API.index(MODEL, params)
      .subscribe(
        res => {
          let calicata = res.data[0];
          if ( calicata ) {
            this.setLastYears(calicata.date)
          } else this.setLastYears();
        }, err => {
          this.setLastYears();
        })
  }

  setLastYears( date? ) {
    let start = moment(new Date(date || "2010-01-02"));
    let end = moment();
    do {
      this.lastYears.push(start.year());
      start.add(1, 'y');
    } while (start.year() <= end.year());

    this.createFiltersForm1();
  }

  createForm() {
    this.errorMessages = undefined;
    this.seasonCtrl = new FormControl(this.current.season ? this.current.season.name : "");
    this.seasonSub = this.seasonCtrl.valueChanges.startWith(this.current.season ? this.current.season.name : "").subscribe(q => this.searchAutoComplete(q, 'seasons', 'seasons'));

    this.form = this.fb.group({
      fiscal_categories_id: [ this.current.fiscal_categories_id || '', this.current.id ? [ Validators.required, CustomValidators.number ] : [ CustomValidators.number ] ],
      date: [ this.current.date || '', [ Validators.required, CustomValidators.date ] ],
      state: [ this.current.state || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
      season_id: [ this.current.season_id || '', [ Validators.required, CustomValidators.number ] ],
      amount: [ this.current.amount || '', this.current.id ? [ Validators.required, CustomValidators.number ] : [ CustomValidators.number ] ],
    });
  }

  createFiltersForm1() {
    this.filtersForm1 = this.fb.group({
      month: new Date().getMonth(),
      year: this.lastYears[this.lastYears.length - 1] || "2017"
    });
  }

  createFiltersForm2() {
    this.filtersForm2 = this.fb.group({
      from: '',
      to: '',
    });
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    row.date = DateGMT(row.date);
    this.current = row;
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( model == "seasons" && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model == "seasons" && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    form.date = DateGMT(form.date);
    form.date = this.dateTransform.transform(form.date, "y-MM-dd");

    if ( !form.notes ) delete form.notes;

    if ( method == 'save' ) {
      let index1 = 0;
      let index2 = 0;
      for ( let category of this.fiscalCategories ) {
        if ( category.amount ) {
          form.fiscal_categories_id = category.id;
          form.amount = category.amount;
          delete category.amount;
          this.API.save(MODEL, form)
            .subscribe(
              res => {
                index2++;
                if ( index2 == this.fiscalCategories.length ) {
                  this.toaster.pop('success', 'Gastos fiscales', `Los gastos fiscales han sido creados exitosamente`);
                  this.paginator.pageIndex = 0;
                  this.current.requesting = false;
                  this.paginator.page.emit();
                  jQuery('#form-modal').modal('toggle');
                }
              }, err => {
                index2++;
                if ( index2 == this.fiscalCategories.length ) {
                  this.paginator.pageIndex = 0;
                  this.current.requesting = false;
                  this.paginator.page.emit();
                  jQuery('#form-modal').modal('toggle');
                }
              });
        } else {
          if ( index1 == this.fiscalCategories.length - 1 ) {
            this.paginator.pageIndex = 0;
            this.current.requesting = false;
            this.paginator.page.emit();
            jQuery('#form-modal').modal('toggle');
          }
        }
        index1++;
      }
    } else {
      this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
            this.toaster.pop('success', 'Gasto fiscal', `El gasto fiscal ha sido actualizado exitosamente`);
            this.current.requesting = false;
            this.paginator.page.emit();
            jQuery('#form-modal').modal('toggle');
          }, err => {
            this.toaster.pop('error', 'Gasto fiscal', `Ha ocurrido un error al actualizar el gasto fiscal`);
            this.current.requesting = false;
            this.errorMessages = err.error.errors;
            for ( let name in this.errorMessages ) {
              this.form.controls[name].setValue('');
            }
          })
    }
  }

  delete() {
    this.loadingResults = true;
    this.API.delete(MODEL, this.current.id)
      .subscribe(
        res => {
          this.loadingResults = false;
          if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
          this.paginator.page.emit();
          this.toaster.pop('success', 'Gasto fiscal', 'El gasto fiscal ha sido eliminado exitosamente');
          this.current = {};
        }, err => {
          this.loadingResults = false;
          this.toaster.pop('error', 'Gasto fiscal', 'Ha ocurrido un error al eliminar el gasto fiscal');
        })
  }

  toggleFilters() {
    jQuery("#filters").slideToggle();
  }

  resetFilters() {
    this.createFiltersForm2();
    this.reload();
  }

  buildFilters(value, form) {
    this.reload();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        let params = new HttpParams();

        if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
        if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
        if ( this.filter ) params = params.append('q', this.filter);
        if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
        if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

        if ( this.filtersForm1.value.month && this.filtersForm1.value.year ) {
          let date = new Date( this.filtersForm1.value.year, this.filtersForm1.value.month ),
            from = new Date(date.getFullYear(), date.getMonth(), 1),
            to = new Date(date.getFullYear(), date.getMonth() + 1, 0);
          params = params.append('from', this.dateTransform.transform(from, "y-MM-dd"));
          params = params.append('to', this.dateTransform.transform(to, "y-MM-dd"));
        }

        if ( this.filtersForm2.value.from || this.filtersForm2.value.to ) {

          params = params.delete("from");
          params = params.delete("to");

          if ( this.filtersForm2.value.from ) params = params.append('from', this.dateTransform.transform(this.filtersForm2.value.from, "y-MM-dd"));
          if ( this.filtersForm2.value.to ) params = params.append('to', this.dateTransform.transform(this.filtersForm2.value.to, "y-MM-dd"));
        }

        return this.API.index(MODEL, params);
      })
      .map(data => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .catch( err => {
        this.loadingResults = false;
        this.haveError = true;
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data);
  }
}
