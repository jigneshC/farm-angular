import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiscalexpensesComponent } from './fiscalexpenses.component';

describe('FiscalexpensesComponent', () => {
  let component: FiscalexpensesComponent;
  let fixture: ComponentFixture<FiscalexpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiscalexpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiscalexpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
