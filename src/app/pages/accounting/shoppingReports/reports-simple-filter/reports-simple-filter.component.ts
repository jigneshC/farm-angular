import { Component } from '@angular/core';
import { AbstractCrudFilterComponent } from '../../../../classes/components/crud/abstract-crud-filter-component';
import { ApiService } from '../../../../services/api.service';
import { TranslateService } from '../../../../services/translate.service';
import { FormBuilder } from '@angular/forms';

import * as moment from 'moment';

@Component({
  selector: 'az-reports-simple-filter',
  templateUrl: './reports-simple-filter.component.html',
  styleUrls: ['./reports-simple-filter.component.scss']
})
export class ShoppingReportsSimpleFilterComponent extends AbstractCrudFilterComponent {

  lastYears: Array<any> = [];

  constructor(
    API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService
  ) {
    super(API);
    this.createForm();
    this.getLastYears();
  }

  getLastYears( date? ) {
    let start = new Date(date || "2010-01-02");
    let end = new Date();
    let years = moment(end).diff(start, 'years');
    for ( let year = 0; year < years +1; year++ )
      this.lastYears.push(start.getFullYear() + year);
  }

  createForm() {
    this.form = this.fb.group({
      from: '',
      to: ''
    });
  }
}
