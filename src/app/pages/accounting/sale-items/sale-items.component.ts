import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';

const MODEL = "saleitems";

@Component({
  selector: 'az-sale-items',
  templateUrl: './sale-items.component.html',
  styleUrls: ['./sale-items.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class SaleItemsComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['name', 'code', 'price', 'presentation', 'variety_id', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  varietyCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  varieties;
  varietySub;
  presentations = [ { value: 0, name: 'KG' }, { value: 1, name: 'CAJA' } ];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
  ) {
    this.createForm();
  }

  createForm() {
    this.errorMessages = undefined;
    this.varietyCtrl = new FormControl(this.current.variety ? this.current.variety.name : "");
    this.varietySub = this.varietyCtrl.valueChanges.startWith(this.current.variety ? this.current.variety.name : "").subscribe(q => this.searchAutoComplete(q, 'varieties', 'varieties'));

    this.form = this.fb.group({
      name: [ this.current.name || '', [ Validators.required, Validators.maxLength(45) ] ],
      code: [ this.current.code || '', [ Validators.required, Validators.maxLength(45) ] ],
      price: [ this.current.price || '', [ Validators.required, CustomValidators.number, CustomValidators.range([0, 999999.999999]) ] ],
      presentation: [ this.current.presentation || 0, [ Validators.maxLength(255), CustomValidators.number ] ],
      // presentation: [ this.current.presentation ? this.current.presentation == "KG" ? 0 : 1 : 0, [ Validators.maxLength(255), CustomValidators.number ] ],
      // notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
      variety_id: [ this.current.variety_id || '', [ Validators.required, CustomValidators.number ] ]
    });
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    this.current = row;
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( ["varieties"].indexOf(model) > -1 && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( ["varieties"].indexOf(model) > -1 && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Item de venta', `El item de venta ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						jQuery('#form-modal').modal('toggle');
          }, err => {
						this.toaster.pop('error', 'Item de venta', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el item de venta`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Item de venta', 'El item de venta ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Item de venta', 'Ha ocurrido un error al eliminar el item de venta');
          })
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
