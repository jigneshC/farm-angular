import { Component, OnInit } from '@angular/core';
import {Commitment} from '../../../models/commitment';
import {CommitmentService} from '../../../services/api/commitment/commitment.service';
import {ToasterService} from 'angular2-toaster';
import {Router} from '@angular/router';
import {DatePipe} from '@angular/common';

import * as moment from 'moment';

@Component({
  selector: 'az-commitment',
  templateUrl: './commitment.component.html',
  styleUrls: ['./commitment.component.scss']
})
export class CommitmentComponent implements OnInit {

  single = [];

  title = 'Total Compromisos';
  isLoading = false;
  commitments: Commitment;

  multi: any[];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Meses';
  showYAxisLabel = true;
  yAxisLabel = 'Total adeudado';

  color = ['#265a88', '#419641', '#2aabd2', '#eb9316', '#c12e2a', '#3c763d', '#31708f', '#8a6d3b', '#a94442'];
  colorScheme = {
    domain: this.color// ['#1B5E20', '#2E7D32', '#43A047', '#388E3C', '#4CAF50', '#66BB6A', '#81C784', '#A5D6A7', '#C8E6C9', '#E8F5E9']
  };

  constructor(
    private router: Router,
    private datePipe: DatePipe,
    private commitmentService: CommitmentService,
    private toaster: ToasterService) {
  }

  onSelect(event) {
    // const auxDate = this.datePipe.transform(event.name, 'yyyy-M').split('-');
    const auxDate = moment(event.name, 'YYYY-M').format('YYYY-M').split('-');
    this.router.navigateByUrl(`pages/contabilidad/vencimientos/${auxDate[1]}/${auxDate[0]}`);
  }

  ngOnInit() {
    this.isLoading = true;
    this.commitmentService.getAll()
      .subscribe(commitments => {
        this.isLoading = false;
        this.commitments = commitments;
        this.parse();
      }, error => {
        this.isLoading = false;
        this.toaster.pop('error', 'Compromisos', 'Ha ocurrido un error al obtener los Compromisos.');
      });
  }

  parse() {
    this.single = this.commitments.data.compromises.map(commitment => {
      return {
        name: this.datePipe.transform(commitment.date, 'MMM-yyyy'),
        value: commitment.amount
      };
    });
  }

  generateExportLink(output) {
    const params = new URLSearchParams();
    params.append('output', output)
    window.open(this.commitmentService.generateExportLink(params), '_blank');
  }
}
