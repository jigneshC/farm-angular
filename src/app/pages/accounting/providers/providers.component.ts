import { RequestOptions, Headers } from '@angular/http';
import { AuthService } from './../../../services/auth/auth.service';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { AppConfig } from './../../../app.config';
import { ApiService } from './../../../services/api.service';
import {Component, ViewEncapsulation, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import { FormValidationService } from './../../../services/form-validation.service';
import { CompleterService, CompleterData } from 'ng2-completer';
import { ToasterService } from 'angular2-toaster';
import { HttpParams } from '@angular/common/http';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {DateValidators} from '../../../utils/validators';
import {Observable} from 'rxjs/Observable';

@Component({
	selector: 'app-provider-component',
	templateUrl: 'providers.component.html',
	styleUrls: ['providers.component.scss'],
	encapsulation: ViewEncapsulation.None,
})

export class ProvidersComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public columns = ['name', 'contact', 'tax_id', 'business_activity', 'actions'];
  public source = new MatTableDataSource();
  public loadingResults = true;
  haveError: boolean = false;
  timeout: any;
  total: number = 0;

	providers:Array<any> = [];
	config:any;
	providerForm:FormGroup;
	create:boolean = true;
	providerId:number = 0;
	messages:any;
	searchText: string;
	error:any;
	errorMessages:any;
	public dataService;
	public count;
	public limit = 10;
	public page:any;
	public sales_person:Boolean = false;

	constructor(
		private API: ApiService,
		private _config: AppConfig,
		private _fb: FormBuilder,
		private _fv: FormValidationService,
		private completerService: CompleterService,
		private configService: AppConfig,
		private auth: AuthService,
		private toaster: ToasterService,
	) {
		this.config = this._config.config;
		this.providerForm = _fb.group({
			tax_id: ['', Validators.compose([Validators.required, DateValidators.validateRut])],
			name: ['', Validators.compose([Validators.required])],
			is_customer: ['', Validators.compose([Validators.required])],
			is_provider: [''],
			is_foreign: [''],
			business_activity: ['', Validators.compose([Validators.required])],
			phone: ['', Validators.compose([Validators.required])],
			email: ['', Validators.compose([Validators.required, _fv.emailValidator])],
			address: ['', Validators.compose([Validators.required])],
			commune_id: [''],
			city: ['', Validators.compose([Validators.required])],
			sales_person: [''],
			allow_invoice: [''],
			communeHolder:['']
		})
	}

	ngOnInit() {
		this.loadproviders();
		this.messages = this._config.config.messages;
		this.initSearchService();
	}

	saveprovider() {
		this.providerForm.controls['allow_invoice'].setValue(true);
		delete this.providerForm.value.communeHolder;
		if (!this.sales_person){
			this.API.save('providers', this.providerForm.value)
					.subscribe(
						res => {
							this.toaster.pop('success', 'Proveedor', 'El proveedor ha sido guardado exitosamente');
							this.resetStatus();
							this.search();
							jQuery('#new-provider-modal').modal('toggle');
							this.error = false;
						}, err => {
							this.toaster.pop('error', 'Proveedor', 'Ha ocurrido un error al guardar el proveedor');
							this.error = true;
							this.errorMessages = err.errors;
							for (var name in this.errorMessages) {
								this.providerForm.controls[name].setValue('');
							}
						})
		} else {
			this.toaster.pop('error', 'Proveedor', 'Ingrese un nombre y apellido válido para el vendedor');
		}
	}

	loadproviders() {
    let params = new HttpParams();

    if ( this.paginator.pageIndex >= 0 ) { params = params.append('page', `${ this.paginator.pageIndex + 1 }`) }
    if ( this.paginator.pageSize ) {params = params.append('p', `${this.paginator.pageSize}`) }
    if ( this.searchText ) { params = params.append('q', this.searchText) }
    if ( this.sort.active ) { params = params.append('sort_by', this.sort.active) }
    if ( this.sort.direction ) { params = params.append('sort_dir', this.sort.direction) }

		return this.API.index('providers', params);
	}

	editprovider() {
		delete this.providerForm.value.communeHolder;
		if (!this.sales_person){
			this.API.update('providers', this.providerForm.value, this.providerId)
			.subscribe(
				res => {
					this.toaster.pop('success', 'Proveedor', 'El proveedor ha sido actualizado exitosamente');
					this.resetStatus();
					this.search();
					jQuery('#new-provider-modal').modal('toggle');
					this.error = false;
				}, err => {
					this.toaster.pop('error', 'Proveedor', 'Ha ocurrido un error al actualizar el proveedor');
					this.error = true;
					this.errorMessages = err.error.errors;
					for (var name in this.errorMessages) {
						this.providerForm.controls[name].setValue('');
					}
				})
		} else {
			this.toaster.pop('error', 'Proveedor', 'Ingrese un nombre y apellido válido para el vendedor');
		}
	}

	deleteprovider() {
		this.API.delete('providers', this.providerId)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Proveedor', 'El proveedor ha sido eliminado exitosamente');
						this.resetStatus();
          }, err => {
						this.toaster.pop('error', 'Proveedor', 'Ha ocurrido un error al eliminar el proveedor');
          })
	}

	detailprovider(provider) {
		this.providerId = provider.id;
		this.providerForm.setValue({
			tax_id: provider.tax_id.split('.').join(""),
			name: provider.name,
			is_customer: provider.is_customer,
			is_provider: provider.is_provider,
			is_foreign: provider.is_foreign,
			business_activity: provider.business_activity,
			phone: provider.phone,
			email: provider.email,
			address: provider.address,
			commune_id: provider.commune_id,
			sales_person: provider.sales_person,
			city: provider.city,
			allow_invoice: provider.allow_invoice,
			communeHolder: provider.commune.name
		});
		this.create = false;
		this.error = false;
	}

	resetStatus() {
		this.providerForm.setValue({
			tax_id: '',
			name: '',
			is_customer: '',
			is_provider: '',
			is_foreign: '',
			business_activity: '',
			phone: '',
			email: '',
			address: '',
			commune_id: '',
			city: '',
			sales_person: '',
			allow_invoice: '',
			communeHolder:''
		});
		this.create = true;
    this.search();
	}

	searchProviders() {
		this.API.searchModel('providers', this.searchText).subscribe(
			(res) => {
				this.count = res.json().total;
				this.limit = res.json().per_page;
				this.page = res.json().current_page;
				this.providers = res.json().data;
		})
	}

	onCommuneSelect(event){
		this.providerForm.controls['commune_id'].setValue(event.originalObject.id);

	}

	initSearchService() {
		this.dataService = this.completerService.remote( this.configService.config.API_URL + `api/communes?q=`, 'name' , 'name');
		let options = new RequestOptions({ headers: new Headers() });
		options.headers.set("Authorization", `Bearer ${this.auth.getToken()}`);
		this.dataService.requestOptions(options);
		this.dataService.dataField("data");
	}

	handlePageChange(page: any): void {
		this.page = page;
		this.loadproviders( );
	}

	validateSalesName(text){
		if (!text) {
			return true;
		} else {
			let myString = text.split(" ");
			if (myString.length < 2) { this.sales_person = true; return false; }

			if (myString[0].length > 3 && myString[1].length > 3) { this.sales_person = false; return true; }
			this.sales_person = true;
			return false;
		}
	}

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        return this.loadproviders();
      }).map((data: any) => {
      this.loadingResults = false;
      this.haveError = false;
      this.total = data.total;
      this.limit = data.per_page;

      return data.data;
    })
      .catch( err => {
        this.loadingResults = false;
        this.haveError = true;
        this.toaster.pop('error', 'Proveedores', 'Ha ocurrido un error al buscar los proveedores');
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data);
  }

  search() {
    if ( this.timeout ) { clearTimeout(this.timeout) };
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }
}
