import { Component, ViewChild } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../../services/api.service';
import { TranslateService } from './../../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import DateGMT from 'app/classes/date-gmt';

const MODEL = "saleinvoices";

@Component({
  selector: 'az-form-sale',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormSalesComponent {
  @ViewChild('fileInput') fileInput;

  id: number;
  source: any = {};
  form: FormGroup;
  itemForm: FormGroup;
  customerCtrl: FormControl = new FormControl("");
  itemCtrl: FormControl = new FormControl("");
  itemsCtrls: any = {};
  loadingResults: boolean = true;
  haveError: boolean = false;
  errorMessages;
  customers;
  customerSub;
  items;
  itemSub;
  types = [ { value: 33, name: 'FACTURA ELECTRONICA' }, { value: 56, name: 'NOTA DE DEBITO ELECTRONICA' }, { value: 2, name: 'NOTA DE CREDITO ELECTRONICA' }, { value: 39, name: 'BOLETA ELECTRONICA' } ];
  states = [ { value: 0, name: 'NO PAGADA' }, { value: 1, name: 'PAGADA' }, { value: 2, name: 'PAGADA PARCIALMENTE ENTREGADO' }];
  tax = { 0: 19, 1: 19, 2: 19, 3: 19, 4: 19, 5: 19 };

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    route.params.subscribe( params => {
      this.id = params.id;
      if ( this.id ) this.load();
      else this.createForm();
    });
  }

  createForm() {
    this.errorMessages = undefined;
    this.customerCtrl = new FormControl(this.source.customer ? this.source.customer.name : "");
    this.customerSub = this.customerCtrl.valueChanges.startWith(this.source.customer ? this.source.customer.name : "").subscribe(q => this.searchAutoComplete(q, 'customers', 'providers'));

    this.form = this.fb.group({
      number: [ this.source.number || '', [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
      excempt_amount: [ this.source.excempt_amount || 0, [ Validators.required, CustomValidators.number, CustomValidators.min(0) ] ],
      gross_amount: [ this.source.gross_amount || 0, [ Validators.required, CustomValidators.number, CustomValidators.min(0) ] ],
      tax_amount: [ this.source.tax_amount || 0, [ Validators.required, CustomValidators.number, CustomValidators.min(0) ] ],
      net_amount: [ this.source.net_amount || 0, [ Validators.required, CustomValidators.number, CustomValidators.min(0) ] ],
      tax_rate: [ this.source.tax_rate || 0, [ Validators.required, CustomValidators.number, CustomValidators.min(0) ] ],
      type: [ this.source.type || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 3]) ] ],
      state: [ this.source.state || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 2]) ] ],
      issue_date: [ this.source.issue_date ? DateGMT(this.source.issue_date) : new Date(), [ Validators.required, CustomValidators.date ] ],
      due_date: [ this.source.due_date ? DateGMT(this.source.due_date) : new Date(), [ Validators.required, CustomValidators.date ] ],
      paid_date: [ this.source.paid_date ? DateGMT(this.source.paid_date) : '', [ CustomValidators.date ] ],
      customer_id: [ this.source.customer_id || '', [ Validators.required, CustomValidators.number ] ],
      notes: [ this.source.notes || '', [ Validators.maxLength(255) ] ],
      items: this.fb.array(this.initItems()),
    });
    this.createItemForm();
    this.loadingResults = false;
  }

  createItemForm() {
    this.itemCtrl = new FormControl("");
    this.itemSub = this.itemCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'items', 'saleitems'));

    this.itemForm = this.fb.group({
      item_id: [ '', [ Validators.required, CustomValidators.number ] ],
      item: [ '', [ ] ],
      quantity: [ 1, [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
      price: [ 1, [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
    });
  }

  initItems() {
    let all = [];
    if ( this.source.items ) {
      this.source.items.forEach( ( item, index ) => {
        this.itemsCtrls[index] = {
          ctrl: new FormControl(""),
          values: []
        }
        this.itemsCtrls[index].sub = this.itemsCtrls[index].ctrl.valueChanges.startWith(item.item ? item.item.name : "").subscribe(q => this.searchAutoComplete(q, 'itemsCtrls', 'saleitems', index))
        item = this.fb.group({
            item_id: [ item.id, [ Validators.required, CustomValidators.number ] ],
            item: [ item, [ ] ],
            quantity: [ item.pivot.quantity, [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
            price: [ item.pivot.price, [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
        });
        all.push(item);
      })
    }
    return all;
  }

  addItem( itemForm ) {
    const items = <FormArray>this.form.controls['items'];
    items.push(itemForm);

    this.createItemForm();

    let gross_amount = this.form.controls['gross_amount'];
    gross_amount.setValue( parseFloat(gross_amount.value) + ( parseFloat(itemForm.value.price) * itemForm.value.quantity ) );

    let index = items.length;
    this.itemsCtrls[index] = {
      ctrl: new FormControl(""),
      values: []
    }
    this.itemsCtrls[index].sub = this.itemsCtrls[index].ctrl.valueChanges.startWith(itemForm.value.item ? itemForm.value.item.name : "").subscribe(q => this.searchAutoComplete(q, 'itemsCtrls', 'saleitems', index))
  }

  removeItem( item, index ) {
    const items = <FormArray>this.form.controls['items'];
    items.removeAt(index);

    let gross_amount = this.form.controls['gross_amount'];
    gross_amount.setValue( gross_amount.value - ( item.price * item.quantity ) );

    delete this.itemsCtrls[index];
  }

  searchAutoComplete( q, variable, model, index? ) {
    this.loadingResults = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    if ( model == "providers" ) params = params.append('is_customer', "true");
    return this.API.index(model, params).subscribe(data => {
      this.loadingResults = false;
      if ( index && variable == 'itemsCtrls' ) this[variable][index].values = data.data;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    if ( form == "itemForm" ) {
      this[form].controls[model+"_id"].setValue(row.id);
      this[form].controls[model].setValue(row);
    } else this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.loadingResults = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.source.id ) method = "update";

    if ( method == "update" && !form.file ) delete form.file;

    if ( form.issue_date ) {
      form.issue_date = DateGMT(form.issue_date);
      form.issue_date = this.dateTransform.transform(form.issue_date, "y-MM-dd");
    }

    if ( form.due_date ) {
      form.due_date = DateGMT(form.due_date);
      form.due_date = this.dateTransform.transform(form.due_date, "y-MM-dd");
    }

    if ( form.paid_date ) {
      form.paid_date = DateGMT(form.paid_date);
      form.paid_date = this.dateTransform.transform(form.paid_date, "y-MM-dd");
    } else delete form.paid_date;

    if ( !form.items.length ) delete form.items;
    else {
      for ( let i = 0; i < form.items.length; i++ ) {
        form.items[i].id = form.items[i].item_id;
      }
    }

    this.API[method](MODEL, form, this.source.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Factura de venta', `La factura de venta ha sido ${ this.source.id ? "actualizada" : "creado" } exitosamente`);
            this.loadingResults = false;
            if ( !this.source.id ) this.router.navigate([`/pages/contabilidad/ventas/${res.id}`]);
          }, err => {
						this.toaster.pop('error', 'Factura de venta', `Ha ocurrido un error al ${ this.source.id ? "actualizar" : "crear" } la factura de venta`);
            this.loadingResults = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
						  this.form.controls[name].setValue('');
						}
          })
  }

  getTax() {
    return ( parseFloat(this.form.controls['tax_rate'].value) * parseFloat(this.form.controls['gross_amount'].value) ) / 100 || 0;
  }

  getTotal() {
    let total = parseFloat(this.form.controls['gross_amount'].value),
        tax = parseFloat(this.form.controls['tax_rate'].value),
        tax_amount = ( total * tax ) / 100;

    if ( this.form.controls['excempt_amount'].value ) total -= parseFloat(this.form.controls['excempt_amount'].value);
    this.form.controls['tax_amount'].setValue( tax_amount );
    total += tax_amount;

    this.form.controls['net_amount'].setValue( total );
    return total || 0;
  }

  load() {
    this.API.show(MODEL, this.id)
        .subscribe(
          data => {
            this.haveError = false;
            this.source = data;
            this.createForm();
          },
          err => {
            this.loadingResults = false;
            this.haveError = true;
          }
        );
  }
}
