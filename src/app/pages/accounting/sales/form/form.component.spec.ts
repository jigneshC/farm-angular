import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSalesComponent } from './form.component';

describe('FormSalesComponent', () => {
  let component: FormSalesComponent;
  let fixture: ComponentFixture<FormSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
