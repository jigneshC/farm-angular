import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppConfig } from './../../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../../services/api.service';

const MODEL = "saleinvoices";

@Component({
  selector: 'az-view-sale',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewSalesComponent {

  id: number;
  source: any = {};
  loadingResults: boolean = true;
  haveError: boolean = false;
  types = [ { value: 33, name: 'FACTURA ELECTRONICA' }, { value: 56, name: 'NOTA DE DEBITO ELECTRONICA' }, { value: 2, name: 'NOTA DE CREDITO ELECTRONICA' }, { value: 39, name: 'BOLETA ELECTRONICA' } ];
  states = [ { value: 0, name: 'NO PAGADA' }, { value: 1, name: 'PAGADA' }, { value: 2, name: 'PAGADA PARCIALMENTE ENTREGADO' }];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private route: ActivatedRoute,
  ) {
    route.params.subscribe( params => {
      this.id = params.id;
      this.load();
    });
  }

  getTotal() {
    let total = parseFloat(this.source.gross_amount);
    if ( this.source.exempt_amount ) total -= parseFloat(this.source.exempt_amount);
    total += parseFloat(this.source.tax_amount);
    if ( this.source.specific_tax_amount ) total += parseFloat(this.source.specific_tax_amount);
    return total;
  }

  load() {
    this.API.show(MODEL, this.id)
        .subscribe(
          data => {
            this.loadingResults = false;
            this.haveError = false;
            this.source = data;
          },
          err => {
            this.loadingResults = false;
            this.haveError = true;
          }
        );
  }

  download( id ) {
    this.API.custom('get', `api/${MODEL}/${id}/download`)
        .subscribe(
          res => window.open(res.data.file_url),
          err => this.toaster.pop('error', 'Factura de venta', 'Ha ocurrido un error al descargar la factura')
        );
  }

  confirmate( id ) {
    this.API.custom('get', `api/${MODEL}/${id}/confirmation`)
        .subscribe(
          res => {
            if ( res.success ) this.toaster.pop('success', 'Factura de venta', 'La factura ha sido confirmada');
            else this.toaster.pop('error', 'Factura de venta', res.errors);
          }, err => this.toaster.pop('error', 'Factura de venta', 'Ha ocurrido un error al descargar la factura')
        );
  }
}
