import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { Observable } from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import {FormControl, FormBuilder, FormGroup, Validators} from '@angular/forms';

import * as moment from 'moment';

import DateGMT from 'app/classes/date-gmt';
import {CustomValidators} from "ng2-validation";

const MODEL = "saleinvoices";

@Component({
  selector: 'az-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class SalesComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fileInput') fileInput;
  @ViewChild(MatProgressBarModule) bar: MatProgressBarModule;

  columns = ['customer_id', 'number', 'issue_date', 'gross_amount', 'state', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  importForm: FormGroup;
  filtersForm1: FormGroup;
  filtersForm2: FormGroup;
  customerCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  loadingImport: boolean = false;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  customers;
  customerSub;
	lastYears: Array<any> = [];
  types = [ { value: 33, name: 'FACTURA ELECTRONICA' }, { value: 56, name: 'NOTA DE DEBITO ELECTRONICA' }, { value: 2, name: 'NOTA DE CREDITO ELECTRONICA' }, { value: 39, name: 'BOLETA ELECTRONICA' } ];
  states = [ { value: 0, name: 'NO PAGADA' }, { value: 1, name: 'PAGADA' }, { value: 2, name: 'PAGADA PARCIALMENTE ENTREGADO' }];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
  ) {
    this.createFiltersForm1();
    this.createFiltersForm2();
		this.getLastYears();
		this.createImportForm();
  }

	getLastYears() {
		let params = new HttpParams();
		params = params.append('p', `1`);
		params = params.append('sort_dir', `asc`);

		this.API.index(MODEL, params)
        .subscribe(
          res => {
						let gasto = res.data[0];
						if ( gasto ) {
							this.setLastYears(gasto.date)
						} else this.setLastYears();
          }, err => {
						this.setLastYears();
          })
	}

	setLastYears( date? ) {

		let start = new Date(date || "2010-01-02");
		let end = new Date();
		let years = moment(end).diff(start, 'years');
		for ( let year = 0; year < years +1; year++ )
			this.lastYears.push(start.getFullYear() + year);

		this.createFiltersForm1();
	}

	createImportForm() {
    this.importForm = this.fb.group({
      folio: [ '', [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
      movement: [ '', [ Validators.required] ],
    });
  }

  createFiltersForm1() {
    this.filtersForm1 = this.fb.group({
      month: new Date().getMonth(),
      year: this.lastYears[this.lastYears.length - 1] || new Date().getFullYear()
    });
  }

  createFiltersForm2() {
    this.customerCtrl = new FormControl("");
    this.customerSub = this.customerCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'customers', 'providers'));

    this.filtersForm2 = this.fb.group({
      customer_id: '',
      issue_date_from: '',
      issue_date_to: '',
      due_date_from: '',
      due_date_to: '',
      type: '',
      state: '',
    });
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model ) {
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    if ( model == "providers" ) params = params.append('is_customer', "true");
    return this.API.index(model, params).subscribe(data => {
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Venta', 'La venta ha sido eliminado exitosamente');
          }, err => {
            this.toaster.pop('error', 'Venta', 'Ha ocurrido un error al eliminar la venta');
          })
  }

  download( id ) {
    this.API.custom('get', `api/${MODEL}/${id}/download`)
        .subscribe(
          res => window.open(res.data.file_url),
          err => this.toaster.pop('error', 'Factura de venta', 'Ha ocurrido un error al descargar la factura')
        );
  }

  toggleFilters() {
    jQuery("#filters").slideToggle();
  }

  resetFilters() {
    this.createFiltersForm2();
    this.reload();
  }

  buildFilters(value, form) {
    this.reload();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          if ( this.filtersForm1.value.month && this.filtersForm1.value.year ) {
            let date = new Date( this.filtersForm1.value.year, this.filtersForm1.value.month ),
                from = new Date(date.getFullYear(), date.getMonth(), 1),
                to = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            to.setHours(23);
            to.setMinutes(59);
            to.setSeconds(59);
            params = params.append('issue_date_from', this.dateTransform.transform(from, "y-MM-dd"));
            params = params.append('issue_date_to', this.dateTransform.transform(to, "y-MM-dd"));
          }

          if (  this.filtersForm2.value.customer_id || this.filtersForm2.value.issue_date_from || this.filtersForm2.value.issue_date_to ||
                this.filtersForm2.value.due_date_from || this.filtersForm2.value.due_date_to || this.filtersForm2.value.type || this.filtersForm2.value.state ) {

            params = params.delete("issue_date_from");
            params = params.delete("issue_date_to");

            if ( this.filtersForm2.value.customer_id ) params = params.append('customer_id', this.filtersForm2.value.customer_id);
            if ( this.filtersForm2.value.issue_date_from ) params = params.append('issue_date_from', this.dateTransform.transform(this.filtersForm2.value.issue_date_from, "y-MM-dd"));
            if ( this.filtersForm2.value.issue_date_to ) params = params.append('issue_date_to', this.dateTransform.transform(this.filtersForm2.value.issue_date_to, "y-MM-dd"));
            if ( this.filtersForm2.value.due_date_from ) params = params.append('due_date_from', this.dateTransform.transform(this.filtersForm2.value.due_date_from, "y-MM-dd"));
            if ( this.filtersForm2.value.due_date_to ) params = params.append('due_date_to', this.dateTransform.transform(this.filtersForm2.value.due_date_to, "y-MM-dd"));
            if ( this.filtersForm2.value.type ) params = params.append('type', this.filtersForm2.value.type);
            if ( this.filtersForm2.value.state ) params = params.append('state', this.filtersForm2.value.state);
          }

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }

  import() {
    this.loadingImport = true;
    let params = new HttpParams();
    params = params.append('folio', this.importForm.value.folio);
    params = params.append('movement', this.importForm.value.movement);
    this.API.custom('get', `api/saleinvoices/import`, {params: params})
      .subscribe(
        res => {
          if (res.success) this.toaster.pop('success', 'Libro de Ventas', 'La importación se ha realizado con éxito.');
          if (!res.success) this.toaster.pop('warning', 'Libro de Ventas', 'La importación ha fallado');
          this.loadingImport = false;
          jQuery("#import-modal").modal('hide');
          this.importForm.reset();
          this.paginator.page.emit();
        },
        err => {
          this.toaster.pop('error', 'Libro de Ventas', 'Ha ocurrido un error durante la importación');
          this.loadingImport = false;
        }
      );
  }
}
