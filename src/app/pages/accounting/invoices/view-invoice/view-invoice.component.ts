import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppConfig } from './../../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../../services/api.service';

const MODEL = "invoices";

@Component({
  selector: 'az-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.scss']
})
export class ViewInvoicesComponent {
  
  id: number;
  source: any = {};
  loadingResults: boolean = true;
  haveError: boolean = false;
  types = [ { value: 0, name: 'FACTURA COMPRA' }, { value: 1, name: 'FACTURA VENTA' }, { value: 2, name: 'NOTA CREDITO' }, { value: 3, name: 'NOTA DEBITO' }, { value: 4, name: 'FACTURA COMPRA SP' }, { value: 5, name: 'BOLETA ELECTRONICA' } ];
  emision = [ { id: 0, name: 'MANUAL' }, { id: 1, name: 'ELECTRONICA' }];
  states = [ { value: 0, name: 'NO PAGADA' }, { value: 1, name: 'PAGADA' }, { value: 2, name: 'PAGADA PARCIALMENTE ENTREGADO' } ];
  currencies = [ { value: 0, name: 'PESOS' }, { value: 1, name: 'USD' } ];
  booleans = [ { value: 0, name: 'NO' }, { value: 1, name: 'SI' } ];
  tax = { 0: 19, 1: 19, 2: 19, 3: 19, 4: 19, 5: 19 };

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private route: ActivatedRoute,
  ) {
    route.params.subscribe( params => {
      this.id = params.id;
      this.load();
    });
  }
  
  getTotal() {
    let total = parseFloat(this.source.gross_amount);
    if ( this.source.exempt_amount ) total += parseFloat(this.source.exempt_amount);
    total += parseFloat(this.source.tax_amount);
    if ( this.source.specific_tax_amount ) total += parseFloat(this.source.specific_tax_amount);
    return total;
  }

  getTotalDollarAmount() {
    let total: any = parseFloat(this.source.gross_amount);
    if ( this.source.exempt_amount ) total += parseFloat(this.source.exempt_amount);
    total += parseFloat(this.source.tax_amount);
    if ( this.source.specific_tax_amount ) total += parseFloat(this.source.specific_tax_amount);

    total = (total/this.source.exchange_rate).toFixed(2);
    return total;
  }
  
  load() {
    this.API.show(MODEL, this.id)
        .subscribe(
          data => {
            this.loadingResults = false;
            this.haveError = false;
            this.source = data;
          },
          err => {
            this.loadingResults = false;
            this.haveError = true;
          }
        );
  }
  
  download( id ) {
    this.API.custom('get', `api/${MODEL}/${id}/download`)
        .subscribe(
          res => window.open(res.data.file_url),
          err => this.toaster.pop('error', 'Factura de compra', 'Ha ocurrido un error al descargar la factura')
        );
  }
}
