import {Component, OnDestroy, ViewChild} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {DatePipe} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import {ToasterService} from 'angular2-toaster';
import {ApiService} from './../../../../services/api.service';
import {TranslateService} from './../../../../services/translate.service';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';

import DateGMT from 'app/classes/date-gmt';
import {Subject} from 'rxjs/Subject';
import {ISubscription} from 'rxjs/Subscription';
import {ExpenseService} from '../../../../services/api/expense/expense.service';
import {Expense} from '../../../../models/expense';
import {Pagination} from '../../../../services/api/pagination';

const MODEL = "invoices";

@Component({
  selector: 'az-form-invoice',
  templateUrl: './form-invoice.component.html',
  styleUrls: ['./form-invoice.component.scss']
})
export class FormInvoicesComponent implements OnDestroy {
  @ViewChild('fileInput') fileInput;

  id: number;
  source: any = {};
  form: FormGroup;
  productForm: FormGroup;
  costCenterCtrl: FormControl = new FormControl("");
  providerCtrl: FormControl = new FormControl("");
  seasonCtrl: FormControl = new FormControl("");
  productCtrl: FormControl = new FormControl("");
  productsCtrls: any = {};
  loadingResults: boolean = true;
  haveError: boolean = false;
  errorMessages;
  costCenters;
  costCenterSub;
  providers;
  providerSub;
  seasons;
  seasonSub;
  products;
  productSub;
	selectedCostCenters: Array<any> = [];
	selectedCostCentersAll = {};
  types = [ { value: 0, name: 'FACTURA COMPRA' }, { value: 1, name: 'FACTURA VENTA' }, { value: 2, name: 'NOTA CREDITO' }, { value: 3, name: 'NOTA DEBITO' }, { value: 4, name: 'FACTURA COMPRA SP' }, { value: 5, name: 'BOLETA ELECTRONICA' } ];
  states = [ { value: 0, name: 'NO PAGADA' }, { value: 1, name: 'PAGADA' }, { value: 2, name: 'PAGADA PARCIALMENTE ENTREGADO' } ];
  emision = [ { id: 0, name: 'MANUAL' }, { id: 1, name: 'ELECTRONICA' }];
  currencies = [ { value: 0, name: 'PESOS' }, { value: 1, name: 'USD' } ];
  booleans = [ { value: 0, name: 'NO' }, { value: 1, name: 'SI' } ];
  tax = { 0: 19, 1: 19, 2: 19, 3: 19, 4: 19, 5: 19 };

  // Gastos
  readonly newExpenseObject  = { detail: 'Crear nuevo gasto', id: 0 };
  gastoChanged =  new Subject<string>();
  subscriptionGasto: ISubscription;
  expenses = [];
  expensesLoading = false;

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    private route: ActivatedRoute,
    private router: Router,
    private expenseService: ExpenseService
  ) {
    route.params.subscribe( params => {
      this.id = params.id;
      if ( this.id ) this.load();
      else this.createForm();
    });
  }

  createForm() {
    if ( this.fileInput ) this.fileInput.nativeElement.value = '';
    this.errorMessages = undefined;
    this.costCenterCtrl = new FormControl(this.source.costcenter ? this.source.costcenter.name : "");
    this.costCenterSub = this.costCenterCtrl.valueChanges.startWith(this.source.costcenter ? this.source.costcenter.name : "").subscribe(q => this.searchAutoComplete(q, 'costCenters', 'costcenters'));
    this.providerCtrl = new FormControl(this.source.provider ? this.source.provider.name : "");
    this.providerSub = this.providerCtrl.valueChanges.startWith(this.source.provider ? this.source.provider.name : "").subscribe(q => this.searchAutoComplete(q, 'providers', 'providers'));
    this.seasonCtrl = new FormControl(this.source.season ? this.source.season : "");
    this.seasonSub = this.seasonCtrl.valueChanges.startWith(this.source.season ? this.source.season : "").subscribe(q => this.searchAutoComplete(q, 'seasons', 'seasons'));

    this.form = this.fb.group({
      number: [ this.source.number || '', [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
      specific_tax_amount: [ this.source.specific_tax_amount || 0, [ Validators.required, CustomValidators.number, CustomValidators.min(0) ] ],
      exempt_amount: [ this.source.exempt_amount || 0, [ Validators.required, CustomValidators.number, CustomValidators.min(0) ] ],
      gross_amount: [ this.source.gross_amount || 0, [ Validators.required, CustomValidators.number, CustomValidators.min(0) ] ],
      tax_amount: [ this.source.tax_amount || 0, [ Validators.required, CustomValidators.number, CustomValidators.min(0) ] ],
      dollar_amount: [ this.source.dollar_amount || false, [ ] ],
      exchange_rate: [ this.source.exchange_rate || false, [ ] ],
      type: [ this.source.type || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 5]) ] ],
      state: [ this.source.state || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 2]) ] ],
      issue_date: [ this.source.issue_date ? DateGMT(this.source.issue_date) : '', [ ] ],
      due_date: [ this.source.due_date ? DateGMT(this.source.due_date) : '', [ ] ],
      provider_id: [ this.source.provider_id || '', [ Validators.required, CustomValidators.number ] ],
      // season_id: [ this.source.season || '', [ CustomValidators.number ] ],
      cost_centers: [ [], [ ] ],
      accounting: [ this.source.accounting || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      is_expense: [ this.source.is_expense ? 1 : 0, [] ],
      expense: [ this.source.expense || null, []],
      tax_payed: [ this.source.tax_payed || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      currency: [ this.source.currency || 0, [ Validators.required ] ],
      products: this.fb.array(this.initProducts()),
      file: [ '', this.source.id ? [ ] : [ Validators.required ] ],
      notes: [ this.source.notes || '', [ Validators.maxLength(255) ] ],
      type_create: [ this.source.type_create || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 5]) ] ],
    });

    console.log(this.form);
    this.initCostCenters();
    this.createProductForm();
    this.loadingResults = false;
  }

  createProductForm() {
    this.productCtrl = new FormControl("");
    this.productSub = this.productCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'products', 'products'));

    this.productForm = this.fb.group({
      product_id: [ '', [ Validators.required, CustomValidators.number ] ],
      product: [ '', [ ] ],
      quantity: [ 1, [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
      price: [ 1, [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
      notes: [ '', [ Validators.maxLength(255) ] ],
    });
  }

  initProducts() {
    let all = [];
    if ( this.source.products ) {
      this.source.products.forEach( ( product, index ) => {
        this.productsCtrls[index] = {
          ctrl: new FormControl(""),
          values: []
        }
        this.productsCtrls[index].sub = this.productsCtrls[index].ctrl.valueChanges.startWith(product.product ? product.product.name : "").subscribe(q => this.searchAutoComplete(q, 'productsCtrls', 'products', index))
        product = this.fb.group({
          product_id: [ product.id, [ Validators.required, CustomValidators.number ] ],
          product: [ product, [ ] ],
          quantity: [ product.pivot.quantity, [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
          price: [ product.pivot.price, [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ],
          notes: [ product.pivot.notes, [ Validators.maxLength(255) ] ],
        });
        all.push(product);
      })
    }
    return all;
  }

  initCostCenters() {
    if ( this.source.costcenters ) {
      this.source.costcenters.forEach( ( cost_center, index ) => {
        if ( !this.selectedCostCentersAll[cost_center.id] ) {
          this.selectedCostCentersAll[cost_center.id] = true;
          let costCenter = {
            name: cost_center.name,
            id: cost_center.id
          };
          this.form.controls['cost_centers'].setValue(Object.keys(this.selectedCostCentersAll));
          this.selectedCostCenters.push(costCenter);
        }
      })
    }
  }

  addProduct( productForm ) {
    const products = <FormArray>this.form.controls['products'];
    if (products.value.length < 1) this.form.controls['gross_amount'].setValue(0);
    products.push(productForm);

    this.createProductForm();

    let gross_amount = this.form.controls['gross_amount'];
    gross_amount.setValue( parseFloat(gross_amount.value) + ( parseFloat(productForm.value.price) * productForm.value.quantity ) );
    this.calculateTax( parseFloat(gross_amount.value) );

    let index = products.length;
    this.productsCtrls[index] = {
      ctrl: new FormControl(""),
      values: []
    }
    this.productsCtrls[index].sub = this.productsCtrls[index].ctrl.valueChanges.startWith(productForm.value.product ? productForm.value.product.name : "").subscribe(q => this.searchAutoComplete(q, 'productsCtrls', 'products', index))
  }

  removeProduct( product, index ) {
    const products = <FormArray>this.form.controls['products'];
    products.removeAt(index);

    let gross_amount = this.form.controls['gross_amount'];
    gross_amount.setValue( gross_amount.value - ( product.price * product.quantity ) );
    this.calculateTax(gross_amount.value);

    delete this.productsCtrls[index];
  }

  calculateTax( amount ) {
    let percent = this.tax[this.form.controls['type'].value];
    this.form.controls['tax_amount'].setValue( Math.round(( percent * amount ) / 100) );
  }

  setDollarAmount( currency ) {
    if ( currency == 1 && this.form.controls['gross_amount'].value ) {
      this.form.controls['dollar_amount'].setValue( parseFloat(this.form.controls['gross_amount'].value) );
      this.form.controls['exchange_rate'].setValue( 1 );
    } else {
      this.form.controls['dollar_amount'].setValue( false );
      this.form.controls['exchange_rate'].setValue( false );
    }
  }

  addedFile( $event ) {
    if ( $event.target.files[0].type != "application/pdf" ) {
      $event.target.value = '';
      this.form.controls['file'].setValue(null);
      return this.form.controls['file'].setErrors({ 'type': true });
    }
    this.form.controls['file'].setErrors({ 'type': null });
    this.form.controls['file'].setValue($event.target.files[0]);
  }

  searchAutoComplete( q, variable, model, index? ) {
    this.loadingResults = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    if ( model == "providers" ) params = params.append('is_provider', "true");
    return this.API.index(model, params).subscribe(data => {
      this.loadingResults = false;
      if ( index && variable == 'productsCtrls' ) this[variable][index].values = data.data;
      else this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    if ( form == "selectedCostCenters" ) {
      this.costCenterCtrl.setValue("");
      if ( !this.selectedCostCentersAll[row.id] ) {
        this.selectedCostCentersAll[row.id] = true;
        let costCenter = {
          name: row.name,
          id: row.id
        };
        this.form.controls['cost_centers'].setValue(Object.keys(this.selectedCostCentersAll));
        this[form].push(row);
      }
    } else if ( form == "productForm" ) {
      this[form].controls[model+"_id"].setValue(row.id);
      this[form].controls[model].setValue(row);
    } else this[form].controls[model+"_id"].setValue(row.id);
  }

  removeCostCenter( id, index ) {
    delete this.selectedCostCentersAll[''+id];
    this.selectedCostCenters.splice(index, 1);
    this.form.controls['cost_centers'].setValue(Object.keys(this.selectedCostCentersAll));
  }

  save(formParam) {
    // Es necesario hacer una copia
    const form = <any>Object.assign({}, formParam);
    if ( !this.form.valid ) return;
    this.loadingResults = true;
    this.errorMessages = undefined;
    let method = "save";

    const formData = new FormData();

    if ( this.source.id ) method = "update";

    if ( method == "update" && !form.file ) delete form.file;

    if ( form.issue_date ) {
      form.issue_date = DateGMT(form.issue_date);
      form.issue_date = this.dateTransform.transform(form.issue_date, "y-MM-dd");
    } else delete form.issue_date;

    if ( form.due_date ) {
      form.due_date = DateGMT(form.due_date);
      form.due_date = this.dateTransform.transform(form.due_date, "y-MM-dd");
    } else delete form.due_date;

    if ( form.type == 4 ) {
      form.products = [];
      // Forzamos el parametro para quitar los productos.
      formData.append('products', '[]');
      if (form.is_expense) {
        formData.append('expense_id', form.expense.id || 0);
      }
      // Quitamos el expense (no es necesario para el request.
      delete form.expense;
    } else {
      // is expense se borra porque no es una factura SP
      delete form.is_expense;
      delete form.expense;
    }

    if ( !form.products.length ) formData.append('products', JSON.stringify([]));
    else {
      for ( let i = 0; i < form.products.length; i++ ) {
        if ( form.products[i].product_id ) form.products[i].id = form.products[i].product_id;
      }
      formData.append('products', JSON.stringify(form.products));
    }

    if ( !form.cost_centers.length && method == "update" ) delete form.cost_centers;
    else if ( !form.cost_centers.length && method == "save" ) {
      formData.append('cost_centers[]', '');
    } else {
      for ( let i = 0; i < form.cost_centers.length; i++ ) formData.append('cost_centers[]', form.cost_centers[i]);
    }

    if ( form.dollar_amount == false ) delete form.dollar_amount;
    if ( form.exchange_rate == false ) delete form.exchange_rate;

    delete form.products;
    delete form.cost_centers;

    // if ( method == "save" ) delete form.season;

    for ( let key in form ) formData.append(key, form[key] );

    if ( method == "update" ) formData.append('_method', 'PUT');

    this.API[method](MODEL, formData, this.source.id, "post")
        .subscribe(
          res => {
						this.toaster.pop('success', 'Factura de compra', `La factura de compra ha sido ${ this.source.id ? "actualizada" : "creado" } exitosamente`);
            this.loadingResults = false;
            if ( !this.source.id ) this.router.navigate([`/pages/contabilidad/compras/${res.id}`]);
          }, err => {
						this.toaster.pop('error', 'Factura de compra', `Ha ocurrido un error al ${ this.source.id ? "actualizar" : "crear" } la factura de compra`);
            this.loadingResults = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
						  if ( [ "products", "cost_centers" ].indexOf(name) == -1 ) this.form.controls[name].setValue('');
						}
          })
  }

  getTotal() {
    let total = parseFloat(this.form.controls['gross_amount'].value);
    if ( this.form.controls['exempt_amount'].value ) total += parseFloat(this.form.controls['exempt_amount'].value);
    total += parseFloat(this.form.controls['tax_amount'].value);
    if ( this.form.controls['specific_tax_amount'].value ) total += parseFloat(this.form.controls['specific_tax_amount'].value);
    return total || 0;
  }

  getDollarAmount() {
    let total: any = parseFloat(this.form.controls['gross_amount'].value);
    if ( this.form.controls['exempt_amount'].value ) total += parseFloat(this.form.controls['exempt_amount'].value);
    total += parseFloat(this.form.controls['tax_amount'].value);
    if ( this.form.controls['specific_tax_amount'].value ) total += parseFloat(this.form.controls['specific_tax_amount'].value);
    
    total = (total/this.form.controls['exchange_rate'].value).toFixed(2);
    return total || 0;
  }

  load() {
    this.API.show(MODEL, this.id)
        .subscribe(
          data => {
            this.haveError = false;
            this.source = data;
            this.createForm();
          },
          err => {
            this.loadingResults = false;
            this.haveError = true;
          }
        );
  }

  download( id ) {
    this.API.custom('get', `api/${MODEL}/${id}/download`)
        .subscribe(
          res => window.open(res.data.file_url),
          err => this.toaster.pop('error', 'Factura de compra', 'Ha ocurrido un error al descargar la factura')
        );
  }

  initInputGasto(value) {
    if (+value === 1 && !this.subscriptionGasto) {
      this.form.controls['expense'].setValidators([Validators.required]);
      this.subscriptionGasto =
        this.gastoChanged
          .debounceTime(300)
          .distinctUntilChanged()
          .subscribe((text) => {
            this.expensesLoading = true;
            this.expenseService.findAll({q: text})
              .finally(() => {
                this.expensesLoading = false;
              })
              .subscribe((data: Expense[]) => {
                this.expenses = data;
              });
          });
    } else {
      this.form.controls['expense'] = null;
      this.form.controls['expense'].setValidators([]);
    }
  }

  gastoInputChanged(text: string): void {
    this.gastoChanged.next(text);
  }

  ngOnDestroy() {
    if (this.subscriptionGasto) {
      this.subscriptionGasto.unsubscribe();
    }
  }

  expenseDetail(expense: Expense) {
    return expense ? expense.detail : '';
  }
  setNewExpense() {
    this.form.controls['expense'].setValue(this.newExpenseObject);
  }
}
