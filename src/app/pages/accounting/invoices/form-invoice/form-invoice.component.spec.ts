import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInvoicesComponent } from './form-invoices.component';

describe('FormInvoicesComponent', () => {
  let component: FormInvoicesComponent;
  let fixture: ComponentFixture<FormInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
