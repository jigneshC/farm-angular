import {Component, AfterViewInit, ViewChild, ViewEncapsulation, ElementRef, OnInit} from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import {FormControl, FormBuilder, FormGroup, Validators} from '@angular/forms';

import * as moment from 'moment';
import {SelectionModel} from '@angular/cdk/collections';
import {InvoiceService} from '../../../services/api/invoice/invoice.service';
import {isEmpty} from '../../../utils/utils';
import {CostCenterService} from '../../../services/api/cost-center/cost-center.service';
import {ProviderService} from '../../../services/api/provider/provider.service';
import {ActivatedRoute} from '@angular/router';

const MODEL = "invoices";

@Component({
  selector: 'az-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class InvoicesComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fileInput') fileInput;

  @ViewChild('inputProveedor') inputProveedor: ElementRef;

  columns = ['select', 'provider_id', 'rut', 'number', 'issue_date', 'gross_amount', 'costcenters', 'type', 'state', 'actions'];
  columnsViewProducts = ["name", "quantity", "price", "total"];
  source = new MatTableDataSource();
  invoicesProducts = new MatTableDataSource();

  selection = new SelectionModel(true, []);
  form: FormGroup;
  filtersForm1: FormGroup;
  filtersForm2: FormGroup;
  stateForm: FormGroup;
  costCenterCtrl: FormControl = new FormControl("");
  providerCtrl: FormControl = new FormControl("");
  seasonCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any = {};
  errorMessages;
  costCenters;
  costCenterSub;
  providers;
  providerSub;
  seasons;
  seasonSub;
	lastYears: Array<any> = [];
	lastSeasons: Array<any> = [];
  selectedCostCenters: Array<any> = [];
  allCostCenters: Array<any> = [];
  allProviders: Array<any> = [];
	selectedCostCentersAll = {};
  types = [ { value: 0, name: 'FACTURA COMPRA' }, { value: 1, name: 'FACTURA VENTA' }, { value: 2, name: 'NOTA CREDITO' }, { value: 3, name: 'NOTA DEBITO' }, { value: 4, name: 'FACTURA COMPRA SP' }, { value: 5, name: 'BOLETA ELECTRONICA' } ];
  states = [ { value: 0, name: 'NO PAGADA' }, { value: 1, name: 'PAGADA' }, { value: 2, name: 'PAGADA PARCIALMENTE ENTREGADO' }];
  currencies = [ { value: 0, name: 'PESOS' }, { value: 1, name: 'USD' } ];
  booleans = [ { value: 0, name: 'NO' }, { value: 1, name: 'SI' } ];
  accounting = [ { value: 0, name: 'NO CONTABILIZADA' }, { value: 1, name: 'CONTABILIZADA' } ];

  constructor(
    private invoiceService: InvoiceService,
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    private costCenterService: CostCenterService,
    private providerService: ProviderService,
    protected route: ActivatedRoute
  ) {

  }

  ngOnInit(): void {
    const urlParams = this.route.snapshot.queryParamMap;
    if (!urlParams.get('filters')) {
      this.resetStoredFilters();
    }
    this.getCostCenterInfo();
    this.getProvidersInfo();
    this.createFiltersForm1();
    this.createFiltersForm2();
    this.createStateForm();
    this.getLastYears();
    this.getLastSeasons();
  }

  getType(val) {
    // Puede ser 0
    if (val === undefined || val === '') {
      return '';
    } else if (!this.types[val]) {
      return val;
    }
    return this.types[val].name;
  }

  createStateForm() {
    this.stateForm = this.fb.group({
      state: [null, []],
      accounting: [null, []],
      tax_payed: [null, []]
    });
  }

  viewProducts(row){
    this.invoicesProducts.data = [];
    let myArray = [];
    row.products.forEach(element => {
      myArray.push({
        name: element.name,
        quantity: element.pivot.quantity,
        price: element.pivot.price,
        total: (element.pivot.quantity * element.pivot.price)
      });
    });
    this.invoicesProducts.data = myArray;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.source.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.source.data.forEach(row => this.selection.select(row));
  }

  updateItem(form, method) {
    const formData = new FormData();
    if (form.type === 4) {
      form.products = [];
    }
    if ( (!form.costcenters || !form.costcenters.length) && method === 'update' ) {
      delete form.costcenters;
    } else {
      for (let i = 0; i < form.costcenters.length; i++) {
        formData.append('cost_centers[]', form.costcenters[i].id);
      }
    }
    if (!form.dollar_amount) {
      delete form.dollar_amount;
    }
    if (!form.exchange_rate) {
      delete form.exchange_rate;
    }
    for (const key in form ) {
      if (key !== 'costcenters' && key !== 'products') {
        formData.append(key, form[key] );
      }
    }
    formData.append('_method', 'PUT');
    return this.API[method](MODEL, formData, form.id, 'post');
  }

  callUpdate(update, item, method) {
    return this.API[method](MODEL, update, item.id, 'put')
  }

  updateState() {
    const formValue = this.stateForm.value;
    let method:string = 'update';
    if (this.selection.selected && ( !isEmpty(formValue.state) || !isEmpty(formValue.accounting) || !isEmpty(formValue.tax_payed))) {
      const observables: Array<Observable<any>> = [];
      this.loadingResults = true;
      this.selection.selected.forEach(item => {
        const stateValue = formValue.state;
        const accountingValue = formValue.accounting;
        const taxValue = formValue.tax_payed;
        let update : any = {};
        if (!isEmpty(stateValue)) {
          update.state = stateValue;
        }
        if (!isEmpty(accountingValue)) {
          update.accounting = accountingValue;
        }
        if (!isEmpty(taxValue)) {
          update.tax_payed = taxValue;
        }
        observables.push(this.callUpdate(update, item, 'update'));
      });
      Observable.forkJoin(observables)
        .subscribe(
          res => {
            this.toaster.pop('success', 'Factura de compra', `La facturas de compra han sido actualizadas exitosamente`);
            this.loadingResults = false;
            this.reload();
          }, err => {
            this.toaster.pop('error', 'Factura de compra', `Ha ocurrido un error al actualizar las facturas de compra`);
            this.loadingResults = false;
          }
        );
      this.selection.clear()
    }
  }

	getLastSeasons() {
		let params = new HttpParams();
		params = params.append('p', `1`);
		params = params.append('sort_by', `issue_date`);
		params = params.append('sort_dir', `asc`);

		this.API.index(MODEL, params)
        .subscribe(
          res => {
						let invoice = res.data[0];
						if ( invoice ) {
							this.setLastSeasons(invoice.season)
						} else this.setLastSeasons();
          }, err => {
						this.setLastSeasons();
          })
	}

	setLastSeasons( season? ) {
		let start = new Date(season+"-01-02" || "2010-01-02");
		let end = new Date();
		let years = moment(end).diff(start, 'years');
		for ( let year = 0; year < years +1; year++ )
			this.lastSeasons.push(start.getFullYear() + year);
	}

	getLastYears() {
		let params = new HttpParams();
		params = params.append('p', `1`);
		params = params.append('sort_dir', `asc`);

		this.API.index(MODEL, params)
        .subscribe(
          res => {
						let gasto = res.data[0];
						if ( gasto ) {
							this.setLastYears(gasto.date)
						} else this.setLastYears();
          }, err => {
						this.setLastYears();
          })
	}

	setLastYears( date? ) {
		let start = new Date(date || "2010-01-02");
		let end = new Date();
		let years = moment(end).diff(start, 'years');
		for ( let year = 0; year < years +1; year++ )
			this.lastYears.push(start.getFullYear() + year);

		this.createFiltersForm1();
	}

  createFiltersForm1() {
    this.filtersForm1 = this.fb.group({
      month: new Date().getMonth(),
      year: this.lastYears[this.lastYears.length - 1] || new Date().getFullYear()
    });
    this.getFiltersInStorage(true);
  }

  createFiltersForm2() {
    const filterStorage = JSON.parse(localStorage.getItem('comprasAdvanceFilter'));
    this.costCenterCtrl = new FormControl("");
    this.costCenterSub = this.costCenterCtrl.valueChanges.startWith("").subscribe(q => this.searchAllAutoComplete(q, 'costCenters', 'allCostCenters'));
    this.providerCtrl = new FormControl("");
    this.providerSub = this.providerCtrl.valueChanges.startWith("").subscribe(q => this.searchAllAutoComplete(q, 'providers', 'allProviders'));

    this.seasonCtrl = new FormControl("");
    this.seasonSub = this.seasonCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'seasons', 'seasons'));

    this.filtersForm2 = this.fb.group({
      number: '',
      provider_id: '',
      cost_center: '',
      issue_date_from: '',
      issue_date_to: '',
      due_date_from: '',
      due_date_to: '',
      type: '',
      season: '',
      currency: '',
      tax_payed: '',
      state: '',
      accounting: '',
      is_expense: '',
      is_created_automatic: '',
      is_file_uploaded: ''
    });
    this.getFiltersInStorage(false);
  }

  openAdvanceFilter(){
    this.toggleFilters();
    let localFilters = JSON.parse(localStorage.getItem("invoiceDetail"));
    this.filtersForm2.controls["provider_id"].setValue(localFilters.provider);
    this.filtersForm2.controls["issue_date_from"].setValue(localFilters.from);
    this.filtersForm2.controls["issue_date_to"].setValue(localFilters.to);
    localStorage.removeItem("invoiceDetail");

    this.allProviders.forEach(element => {
      if (parseInt(element.id) === parseInt(localFilters.provider)){
        this.selectRow(element, 'provider', 'filtersForm2');
      }
    });
    this.reload();
  }

  getCostCenterInfo(){
    let params = {
      p: -1
    }
    this.API.index('costcenters', params).subscribe(res => {
      this.allCostCenters = res.data;
    });
  }

  getProvidersInfo(){
    let params = {
      p: -1
    }
    this.API.index('providers', params).subscribe(res => {
      this.allProviders = res.data;
      if (localStorage.getItem("invoiceDetail")) this.openAdvanceFilter();
    });
  }

  displayFn(val: any) {
    return val ? val.name : val;
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model ) {
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    if ( model == "providers" ) params = params.append('is_provider', "true");
    return this.API.index(model, params).subscribe(data => {
      this[variable] = data.data;
    });
  }

  searchAllAutoComplete( q, variable, model ) {
    if (typeof q === 'string'){
      this[variable] = this[model].filter(element =>
        element.name.toLowerCase().indexOf(q.toLowerCase()) === 0);

      return this[variable];
    }
  }

  selectRow( row, model, form ) {
    if ( form == "selectedCostCenters" ) {
      this.costCenterCtrl.setValue("");
      if ( !this.selectedCostCentersAll[row.id] ) {
        this.selectedCostCentersAll[''+row.id] = true;
        let costCenter = {
          name: row.name,
          id: row.id
        };
        this[form].push(row);
      }
    } else if ( model == "season" ) {
      this[form].controls[model].setValue(row.year);
    } else this[form].controls[model+"_id"].setValue(row.id);
  }

  removeCostCenter( id, index ) {
    delete this.selectedCostCentersAll[''+id];
    this.selectedCostCenters.splice(index, 1)
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Compra', 'La compra ha sido eliminado exitosamente');
          }, err => {
            this.toaster.pop('error', 'Compra', 'Ha ocurrido un error al eliminar la compra');
          })
  }

  download( id ) {
    this.API.custom('get', `api/${MODEL}/${id}/download`)
        .subscribe(
          res => window.open(res.data.file_url),
          err => this.toaster.pop('error', 'Factura de compra', 'Ha ocurrido un error al descargar la factura')
        );
  }

  toggleFilters() {
    jQuery('#filters').slideToggle(400, () => {
      if (this.isFormAdvancedToogled()) {
        localStorage.setItem('isFilterToggled', 'true');
      } else {
        localStorage.setItem('isFilterToggled', 'false');
      }
    });
  }

  resetFilters() {
    localStorage.setItem('comprasAdvanceFilter', null);
    this.selectedCostCenters = [];
    this.selectedCostCentersAll = {};
    this.filtersForm2.reset();
    this.reload();
  }

  resetStoredFilters() {
    localStorage.removeItem('comprasAdvanceFilter');
    localStorage.removeItem('comprasBasicFilter');
    localStorage.removeItem('isFilterToggled');
  }

  saveFiltersInStorage() {
    localStorage.setItem('comprasBasicFilter', JSON.stringify(this.filtersForm1.value));
    localStorage.setItem('comprasAdvanceFilter', JSON.stringify(this.filtersForm2.value));
  }

  getFiltersInStorage(basic: boolean) {
    const basicFilterStorage = localStorage.getItem('comprasBasicFilter');
    const advanceFilterStorage = localStorage.getItem('comprasAdvanceFilter');
    if (basicFilterStorage) {
      this.filtersForm1.patchValue(JSON.parse(basicFilterStorage));
    }
    if (advanceFilterStorage && advanceFilterStorage && advanceFilterStorage !== 'null' && !basic) {
      const formValue = JSON.parse(advanceFilterStorage);
      this.filtersForm2.patchValue(formValue);
      // Hay que agregar los centros de costo manualmente
      if (formValue.cost_center && formValue.cost_center.length > 0) {
        formValue.cost_center.forEach(  id => {
          this.costCenterService.findOne(id)
            .subscribe( costCenter => {
              // Si no existe el cc entonces se agrega a la lista.
              if (!this.selectedCostCenters.some( c => c.id == id)) {
                this.selectedCostCenters.push(costCenter);
              }
            })
        });
      }
      if (formValue.provider_id) {
        this.providerService.findOne(formValue.provider_id)
          .subscribe(res => {
            this.inputProveedor.nativeElement.value = res.name;
          })
      }
    }
  }

  buildFilters(value, form) {
    if ( form == 'filtersForm2' ) this[form].controls["cost_center"].setValue( Object.keys(this.selectedCostCentersAll) );
    this.saveFiltersInStorage();
    this.reload();
  }

  ngAfterViewInit() {
    if (localStorage.getItem('isFilterToggled') === 'true') {
      this.toggleFilters();
    }
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          if ( this.filtersForm1.value.month && this.filtersForm1.value.year ) {
            const date = new Date( this.filtersForm1.value.year, this.filtersForm1.value.month );
            const startOfMonth = moment(date).startOf('month').format('YYYY-MM-DD');
            const endOfMonth   = moment(date).endOf('month').format('YYYY-MM-DD');
            params = params.append('issue_date_from', startOfMonth);
            params = params.append('issue_date_to', endOfMonth);
          }

          if ( this.isFormAdvancedToogled() ) {

            params = params.delete("issue_date_from");
            params = params.delete("issue_date_to");

            if ( this.filtersForm2.value.provider_id ) params = params.append('provider_id', this.filtersForm2.value.provider_id);
            if ( this.filtersForm2.value.cost_center ) {
              for ( let costCenter of this.filtersForm2.value.cost_center ) {
                params = params.append('cost_center[]', costCenter);
              }
            }

            if ( this.filtersForm2.value.number ) params = params.append('number', this.filtersForm2.value.number);
            if ( this.filtersForm2.value.issue_date_from ) params = params.append('issue_date_from', this.dateTransform.transform(this.filtersForm2.value.issue_date_from, "y-MM-dd"));
            if ( this.filtersForm2.value.issue_date_to ) params = params.append('issue_date_to', this.dateTransform.transform(this.filtersForm2.value.issue_date_to, "y-MM-dd"));
            if ( this.filtersForm2.value.due_date_from ) params = params.append('due_date_from', this.dateTransform.transform(this.filtersForm2.value.due_date_from, "y-MM-dd"));
            if ( this.filtersForm2.value.due_date_to ) params = params.append('due_date_to', this.dateTransform.transform(this.filtersForm2.value.due_date_to, "y-MM-dd"));
            if ( this.filtersForm2.value.type ) params = params.append('type', this.filtersForm2.value.type);
            if ( this.filtersForm2.value.season ) params = params.append('season', this.filtersForm2.value.season);
            if ( this.filtersForm2.value.currency ) params = params.append('currency', this.filtersForm2.value.currency);
            if ( this.filtersForm2.value.tax_payed ) params = params.append('tax_payed', this.filtersForm2.value.tax_payed);
            if ( this.filtersForm2.value.state ) params = params.append('state', this.filtersForm2.value.state);
            if ( this.filtersForm2.value.accounting ) params = params.append('accounting', this.filtersForm2.value.accounting);
            if ( this.filtersForm2.value.is_expense ) params = params.append('is_expense', this.filtersForm2.value.is_expense);
            if ( this.filtersForm2.value.is_created_automatic ) params = params.append('is_created_automatic', this.filtersForm2.value.is_created_automatic);
            if ( this.filtersForm2.value.is_file_uploaded ) params = params.append('is_file_uploaded', this.filtersForm2.value.is_file_uploaded);
          }

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }

  generarReporte(): string {
    const params = this.buildQueryParams();
    return this.invoiceService.generateLinkReport(params);
  }

  private buildQueryParams(): URLSearchParams {
    const params = new URLSearchParams();
    if (this.filtersForm2.value.provider_id || this.filtersForm2.value.cost_center || this.filtersForm2.value.issue_date_from ||
      this.filtersForm2.value.issue_date_to || this.filtersForm2.value.due_date_from || this.filtersForm2.value.due_date_to ||
      this.filtersForm2.value.type || this.filtersForm2.value.season || this.filtersForm2.value.currency ||
      this.filtersForm2.value.tax_payed || this.filtersForm2.value.state || this.filtersForm2.value.accounting) {

      if (this.filtersForm2.value.provider_id) {
        params.append('provider_id', this.filtersForm2.value.provider_id);
      }
      if (this.filtersForm2.value.cost_center) {
        for (const costCenter of this.filtersForm2.value.cost_center) {
          params.append('cost_center[]', costCenter);
        }
      }
      if (this.filtersForm2.value.issue_date_from) {
        params.append('issue_date_from', this.dateTransform.transform(this.filtersForm2.value.issue_date_from, 'y-MM-dd'));
      }
      if (this.filtersForm2.value.issue_date_to) {
        params.append('issue_date_to', this.dateTransform.transform(this.filtersForm2.value.issue_date_to, 'y-MM-dd'));
      }
      if (this.filtersForm2.value.due_date_from) {
        params.append('due_date_from', this.dateTransform.transform(this.filtersForm2.value.due_date_from, 'y-MM-dd'));
      }
      if (this.filtersForm2.value.due_date_to) {
        params.append('due_date_to', this.dateTransform.transform(this.filtersForm2.value.due_date_to, 'y-MM-dd'));
      }
      if (this.filtersForm2.value.type) {
        params.append('type', this.filtersForm2.value.type);
      }
      if (this.filtersForm2.value.season) {
        params.append('season', this.filtersForm2.value.season);
      }
      if (this.filtersForm2.value.currency) {
        params.append('currency', this.filtersForm2.value.currency);
      }
      if (this.filtersForm2.value.tax_payed) {
        params.append('tax_payed', this.filtersForm2.value.tax_payed);
      }
      if (this.filtersForm2.value.state) {
        params.append('state', this.filtersForm2.value.state);
      }
      if (this.filtersForm2.value.accounting) {
        params.append('accounting', this.filtersForm2.value.accounting);
      }
      if (this.filtersForm2.value.number) {
        params.append('number', this.filtersForm2.value.number);
      }

      if (this.filtersForm2.value.is_expense) {
        params.append('is_expense', this.filtersForm2.value.is_expense);
      }

      if (this.filtersForm2.value.is_created_automatic) {
        params.append('is_created_automatic', this.filtersForm2.value.is_created_automatic);
      }
    }
    return params;
  }

  isFormAdvancedToogled() {
    return jQuery('#filters').is(':visible');
  }

  public getRowClass(row): string {
    return !row.is_balance ? 'not-balance' : row.is_created_automatic ? 'autocreated' : row.is_expense ? 'expense' : ''
  }

  // Es necesario quitar el proveedor cuando es vacio por la forma que esta hecho no lo hace automatico.
  inputProveedorChanged($event): void {
    if( $event === '') {
      this.filtersForm2.patchValue({
        provider_id : ''
      })
    }
  }

  generateExportLink(output) {
    const params = this.buildQueryParams();
    params.append('output', output)
    window.open(this.invoiceService.generateExportLink(params), '_blank');
  }

}
