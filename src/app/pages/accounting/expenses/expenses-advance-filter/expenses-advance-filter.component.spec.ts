import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpensesAdvanceFilterComponent } from './expenses-advance-filter.component';

describe('ExpensesAdvanceFilterComponent', () => {
  let component: ExpensesAdvanceFilterComponent;
  let fixture: ComponentFixture<ExpensesAdvanceFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpensesAdvanceFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpensesAdvanceFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
