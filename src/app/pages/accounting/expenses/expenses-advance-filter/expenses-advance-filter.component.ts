import { Component } from '@angular/core';
import { AbstractCrudFilterComponent } from '../../../../classes/components/crud/abstract-crud-filter-component';
import { ApiService } from '../../../../services/api.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { TranslateService } from '../../../../services/translate.service';
import {ExpenseService} from '../../../../services/api/expense/expense.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'az-expenses-advance-filter',
  templateUrl: './expenses-advance-filter.component.html',
  styleUrls: ['./expenses-advance-filter.component.scss']
})
export class ExpensesAdvanceFilterComponent extends AbstractCrudFilterComponent {

  costCenterCtrl: FormControl = new FormControl("");
  costCenterSub;

  costcenters: any[] = [];

  constructor(
    API: ApiService,
    private fb: FormBuilder,
    private dateTransform: DatePipe,
    private expenseService: ExpenseService,
    public translate: TranslateService
  ) {
    super(API)
    this.createForm();
  }

  createForm() {
    this.costCenterCtrl = new FormControl("");
    this.costCenterSub = this.costCenterCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'costcenters', 'costcenters'));

    this.form = this.fb.group({
      costcenter_id: '',
      from: '',
      to: '',
      has_invoice: -1,
      state: ''
    });
  }

  resetFilters() {
    this.createForm();
    this.runFiltering();
  }

  buildReport() {
    const query = new URLSearchParams();

    if (this.form.value) {
      if (this.form.value.costcenter_id) {
        query.append('cost_center', this.form.value.costcenter_id);
      }
      if (this.form.value.has_invoice && this.form.value.has_invoice  > -1) {
        query.append('has_invoice', this.form.value.has_invoice);
      }
      if (this.form.value.from) {
        query.append('from', this.dateTransform.transform(this.form.value.from, 'y-MM-dd'))
      }
      if (this.form.value.to) {
        query.append('to', this.dateTransform.transform(this.form.value.to, 'y-MM-dd'))
      }
      if (this.form.value.state) {
        query.append('state', this.form.value.state)
      }
    }
    const url = this.expenseService.generateLinkReport(query);
    window.open(url, '_blank');
  }

}
