import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import {AppConfig} from '../../../app.config';

import * as moment from 'moment';
import { TimeCalculatorService } from "../../../services/time-calculator.service";

import DateGMT from 'app/classes/date-gmt';
import {SelectionModel} from '@angular/cdk/collections';
import {isEmpty} from '../../../utils/utils';
import {ExpenseService} from "../../../services/api/expense/expense.service";

const MODEL = "expenses";

@Component({
  selector: 'az-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class ExpensesComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fileInput') fileInput;

  selection = new SelectionModel(true, []);

  private simpleFilterFormValue: any = {
    year: new Date().getFullYear(),
    month: new Date().getMonth()
  };
  private advanceFilterFormValue: any;

  columns = ['select', 'expense_date', 'costcenter_id', 'detail', 'state', 'amount', 'notes', 'file', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  filtersForm: FormGroup;
  stateForm: FormGroup;
  costCenterCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  costCenters;
  costCenterSub;
  lastYears: Array<any> = [];
  allCostCenters: Array<any> = [];
  states = [ { value: 0, name: 'NO PAGADO' }, { value: 1, name: 'PAGADO' } ];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    private expenseService: ExpenseService,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public timeService: TimeCalculatorService,
    public appConfig: AppConfig
  ) {
    this.getCostCenterInfo();
    this.createForm();
    this.createFiltersForm();
    this.createStateForm();
		this.getLastYears();
  }

  addedFile( $event ) {
    if ( $event.target.files[0].type != "application/pdf" ) {
      $event.target.value = '';
      this.form.controls['file'].setValue(null);
      return this.form.controls['file'].setErrors({ 'type': true });
    }
    this.form.controls['file'].setErrors(null);
    this.form.controls['file'].setValue($event.target.files[0]);
  }

  getAverageHumity( row ) {
    return ( ( row.humity_at_30 + row.humity_at_60 + row.humity_at_90 ) / 3 ).toFixed(2);
  }

	getLastYears() {
		let params = new HttpParams();
		params = params.append('p', `1`);
		params = params.append('sort_dir', `asc`);

		this.API.index(MODEL, params)
        .subscribe(
          res => {
						let gasto = res.data[0];
						if ( gasto ) {
							this.setLastYears(gasto.date)
						} else this.setLastYears();
          }, err => {
						this.setLastYears();
          })
	}

	setLastYears( date? ) {
		let start = new Date(date || "2010-01-02");
		let end = new Date();
		let years = moment(end).diff(start, 'years');
		for( let year = 0; year < years +1; year++ )
			this.lastYears.push(start.getFullYear() + year);

		this.createFiltersForm();
	}

  createStateForm() {
    this.stateForm = this.fb.group({
      state: [null, []]
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.source.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.source.data.forEach(row => this.selection.select(row));
  }

  createForm() {
    if ( this.fileInput ) this.fileInput.nativeElement.value = '';
    this.errorMessages = undefined;
    this.costCenterCtrl = new FormControl(this.current.costcenter ? this.current.costcenter.name : "");
    this.costCenterSub = this.costCenterCtrl.valueChanges.startWith(this.current.costcenter ? this.current.costcenter.name : "").subscribe(q => this.searchAutoComplete(q, 'costCenters', 'costcenters'));

    this.form = this.fb.group({
      expense_date: [ this.current.expense_date || '', [ Validators.required, CustomValidators.date ] ],
      detail: [ this.current.detail || '', [ Validators.required, Validators.maxLength(255) ] ],
      amount: [ this.current.amount || '', [ Validators.required, CustomValidators.number ] ],
      notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
      state: [ this.current.state || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      costcenter_id: [ this.current.costcenter_id || '', [ CustomValidators.number ] ],
      invoice_id: [ this.current.invoice_id || '', [ CustomValidators.number ] ],
      file: [ '', this.current.id ? [ ] : [ Validators.required ] ],
    });
  }

  getCostCenterInfo(){
    let params = {};
    this.API.index('costcenters', params).subscribe(res => {
      this.allCostCenters = res.data;
    });
  }

  searchAllAutoComplete( q, variable, model ) {
    return this[variable] = this[model];
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      month: new Date().getMonth(),
      year: this.lastYears[this.lastYears.length - 1] || "2017"
    });
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    row.expense_date = DateGMT(row.expense_date);
    this.current = row;
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( model == "costCenters" && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model == "costCenters" && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  updateItem(form, method) {

    const formData = new FormData();
    if ( !form.file ) { delete form.file }
    if ( !form.invoice_id ) { delete form.invoice_id }

    for ( const key in form ) {
      formData.append(key, form[key] );
    }

    formData.append('_method', 'PUT');
    return this.API[method](MODEL, formData, form.id, 'post');
  }

  updateState() {
    const formValue = this.stateForm.value;
    if (this.selection.selected && ( !isEmpty(formValue.state) )) {
      const observables: Array<Observable<any>> = [];
      this.loadingResults = true;
      this.selection.selected.forEach(item => {
        const stateValue = formValue.state;
        if (!isEmpty(stateValue)) {
          item.state = stateValue;
        }
        observables.push(this.updateItem(item, 'update'));
      });
      Observable.forkJoin(observables)
        .subscribe(
          res => {
            this.toaster.pop('success', 'Gastos', `Los gastos han sido actualizados exitosamente`);
            this.loadingResults = false;
          }, err => {
            this.toaster.pop('error', 'Gastos', `Ha ocurrido un error al actualizar los gastos`);
            this.loadingResults = false;
          }
        );
      this.selection.clear()
    }
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    if ( !form.invoice_id ) delete form.invoice_id;

    if ( method == "update" && !form.file ) delete form.file;

    form.expense_date = DateGMT(form.expense_date);

    form.expense_date = this.dateTransform.transform(form.expense_date, "y-MM-dd");

    const formData = new FormData();

    for ( let key in form ) formData.append(key, form[key] );

    if ( method == "update" ) formData.append('_method', 'PUT');

    this.API[method](MODEL, formData, this.current.id, "post")
        .subscribe(
          res => {
						this.toaster.pop('success', 'Gasto', `El gasto ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						jQuery('#form-modal').modal('toggle');
          }, err => {
						this.toaster.pop('error', 'Gasto', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el gasto`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Gasto', 'El gasto ha sido eliminado exitosamente');
          }, err => {
            this.toaster.pop('error', 'Gasto', 'Ha ocurrido un error al eliminar el gasto');
          })
  }

  download( id, model? ) {
    this.API.custom('get', `api/${ model || MODEL }/${id}/download`)
        .subscribe(
          res => window.open(res.data.file_url),
          err => this.toaster.pop('error', 'Comprobante', 'Ha ocurrido un error al descargar el comprobante')
        );
  }

  toggleFilters(): Promise<void> {
    return new Promise<void>((resolve) => {
      jQuery('#filters').slideToggle(400, () => {
        resolve();
      });
    });
  }

  isAdvancedFilterVisible(): boolean {
    return jQuery('#filters').is(':visible');
  }

  resetFilters() {
    this.createFiltersForm();
    this.reload();
  }

  buildFilters(form) {
    this.reload();
  }

  onSimpleFiltering(form) {
    if (this.isAdvancedFilterVisible()) {
      this.toggleFilters().then(() => {
          this.simpleFilterFormValue = form;
          this.reload();
        }
      );
    } else {
      this.simpleFilterFormValue = form;
      this.reload();
    }
  }

  onAdvanceFiltering(form) {
    this.advanceFilterFormValue = form;
    this.reload();
  }
  buildQueryParams() {
    let query = new URLSearchParams();
    if (this.isAdvancedFilterVisible() && this.advanceFilterFormValue && this.advanceFilterFormValue.costcenter_id) {
      query.append('cost_center', this.advanceFilterFormValue.costcenter_id);
    }
    if (this.isAdvancedFilterVisible() && this.advanceFilterFormValue && this.advanceFilterFormValue.has_invoice && this.advanceFilterFormValue.has_invoice > -1) {
      query.append('has_invoice', this.advanceFilterFormValue.has_invoice);
    }
    if (this.isAdvancedFilterVisible() && this.advanceFilterFormValue && this.advanceFilterFormValue.from) {
      query.append('from', this.dateTransform.transform(this.advanceFilterFormValue.from, 'y-MM-dd'))
    }
    if (this.isAdvancedFilterVisible() && this.advanceFilterFormValue && this.advanceFilterFormValue.to) {
      query.append('to', this.dateTransform.transform(this.advanceFilterFormValue.to, 'y-MM-dd'))
    }
    if (this.isAdvancedFilterVisible() && this.advanceFilterFormValue && this.advanceFilterFormValue.state) {
      query.append('state', this.advanceFilterFormValue.state)
    }
    return query;
  }

  filterReport(output: string) {
    let query = this.buildQueryParams();
    query.append('output', output);
    return query;
  }

  generateExportLink(output: string) {
    let query = this.filterReport(output);
    var url = `${this.appConfig.config.API_URL}reports/expenses?${query.toString()}`;
    window.open(url, '_blank');
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          if (!this.isAdvancedFilterVisible()) {
            let date = new Date( this.simpleFilterFormValue.year, this.simpleFilterFormValue.month ),
              from = new Date(date.getFullYear(), date.getMonth(), 1),
              to = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            params = params.append('from', this.dateTransform.transform(from, "y-MM-dd"));
            params = params.append('to', this.dateTransform.transform(to, "y-MM-dd"));
          } else {

            if (this.advanceFilterFormValue.costcenter_id)
              params = params.append('cost_center', this.advanceFilterFormValue.costcenter_id);
            if (this.advanceFilterFormValue.has_invoice && this.advanceFilterFormValue.has_invoice > -1)
              params = params.append('has_invoice', this.advanceFilterFormValue.has_invoice);
            if (this.advanceFilterFormValue.state)
              params = params.append('state', this.advanceFilterFormValue.state);
              if (this.advanceFilterFormValue.from)
              params = params.append('from', this.dateTransform.transform(this.advanceFilterFormValue.from, 'y-MM-dd'));
            if (this.advanceFilterFormValue.to)
              params = params.append('to', this.dateTransform.transform(this.advanceFilterFormValue.to, 'y-MM-dd'));
          }

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;
          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }

  public getRowClass(row): string {
    if (row) {
      return row.state ? '' : 'unpaid';
    }
  }

}
