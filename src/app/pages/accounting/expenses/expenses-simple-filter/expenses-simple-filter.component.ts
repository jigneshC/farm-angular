import { Component } from '@angular/core';
import { AbstractCrudFilterComponent } from '../../../../classes/components/crud/abstract-crud-filter-component';
import { ApiService } from '../../../../services/api.service';
import { FormBuilder } from '@angular/forms';

import * as moment from 'moment';

@Component({
  selector: 'az-expenses-simple-filter',
  templateUrl: './expenses-simple-filter.component.html',
  styleUrls: ['./expenses-simple-filter.component.scss']
})
export class ExpensesSimpleFilterComponent extends AbstractCrudFilterComponent {

  lastYears: Array<any> = [];

  constructor(
    API: ApiService,
    private fb: FormBuilder
  ) {
    super(API);
    this.createForm();
    this.getLastYears();
  }

  getLastYears( date? ) {
    let start = new Date(date || "2010-01-02");
    let end = new Date();
    let years = moment(end).diff(start, 'years');
    for ( let year = 0; year < years +1; year++ )
      this.lastYears.push(start.getFullYear() + year);
  }

  createForm() {
    this.form = this.fb.group({
      year: new Date().getFullYear(),
      month: new Date().getMonth()
    });
  }
}
