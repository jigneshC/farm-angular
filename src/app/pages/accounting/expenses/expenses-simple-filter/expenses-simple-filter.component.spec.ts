import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpensesSimpleFilterComponent } from './expenses-simple-filter.component';

describe('ExpensesSimpleFilterComponent', () => {
  let component: ExpensesSimpleFilterComponent;
  let fixture: ComponentFixture<ExpensesSimpleFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpensesSimpleFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpensesSimpleFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
