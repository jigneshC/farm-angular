import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import DateGMT from 'app/classes/date-gmt';

const MODEL = "bankloans";

@Component({
  selector: 'az-bankloans',
  templateUrl: './bankloans.component.html',
  styleUrls: ['./bankloans.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class BankloansComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['bank_id', 'currency', 'amount', 'reason', 'rate', 'dues', 'curse_date', 'notes', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  formDue: FormGroup;
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  currentDue: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  banks = [ { value: 0, name: 'BANCO SANTANDER' }, { value: 1, name: 'BANCO DE CHILE' }, { value: 2, name: 'BANCO BCI' } ];
  currencies = [ { value: 0, name: 'PESOS' }, { value: 1, name: 'USD' } ];
  reasons = [ { value: 0, name: 'CAPITAL DE TRABAJO' }, { value: 1, name: 'SOLUCION PROBLEMA LIQUIDEZ' }, { value: 2, name: 'INVERSION' } ];
  states = [ { value: 0, name: 'SIN PAGAR' }, { value: 1, name: 'PAGADO' } ];
  tab: string = "details";
  dues: FormArray;

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
  ) {
    this.createForm();
    this.createFormDue();
  }

  createForm() {
    this.errorMessages = undefined;

    this.form = this.fb.group({
      bank_id: [ this.current.bank_id || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 2]) ] ],
      curse_date: [ this.current.curse_date || '', [ CustomValidators.date ] ],
      currency: [ this.current.currency || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      amount: [ this.current.amount || '', [ Validators.required, CustomValidators.number ] ],
      rate: [ this.current.rate || '', [ Validators.required, CustomValidators.number ] ],
      reason: [ this.current.reason || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 2]) ] ],
      notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
      dues: this.fb.array([])
    });
  }

  get formDues() {
    return <FormArray>this.form.get('dues');
  }

  createFormDue() {
    this.errorMessages = undefined;

    this.formDue = this.fb.group({
      bank_loan_id: [ this.current.id ],
      due_date: [ this.currentDue.due_date ? DateGMT(this.currentDue.due_date) : '', [ Validators.required, CustomValidators.date ] ],
      paid_date: [ this.currentDue.paid_date ? DateGMT(this.currentDue.paid_date) : '', [ CustomValidators.date ] ],
      status: [ this.currentDue.status || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      amount: [ this.currentDue.amount || '', [ Validators.required, CustomValidators.number ] ]
    });
  }

  next() {
    jQuery('#todues0').click();
    this.tab = 'dues';
    this.dues = <FormArray>this.form.controls['dues'];
    for ( let i = 0; i < this.current.totaldues; i++ ) {
      let due = this.fb.group({
          amount: [ "", [ Validators.required, CustomValidators.number ] ],
          due_date: [ "", [ Validators.required, CustomValidators.date ] ],
      });
      this.dues.push(due);
    }
  }

  prev() {
    jQuery('#todetails0').click();
    this.tab = 'details';
    while ( this.dues.length !== 0 ) {
      this.dues.removeAt(0)
    }
  }

  create() {
    jQuery('#todetails0').click();
    this.tab = 'details';
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    jQuery('#todetails2').click();
    this.tab = 'details';
    this.current = row;
    this.current.curse_date = DateGMT(row.curse_date);
    this.createForm();
    this.createFormDue();
  }

  show( row: any ) {
    jQuery('#todetails1').click();
    this.tab = 'details';
    this.current = row;
    this.current.curse_date = DateGMT(row.curse_date);
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    if ( form.curse_date ) form.curse_date = this.dateTransform.transform(form.curse_date, "y-MM-dd");
    else delete form.curse_date;

    if ( form.dues && form.dues.length ) {
      for ( let due of form.dues ) {
        due.due_date = this.dateTransform.transform(due.due_date, "y-MM-dd");
      }
    }

    this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Préstamo bancario', `El préstamo bancario ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;

						if ( this.current.id ) {
              jQuery('#todues2').click();
              this.tab = 'dues';
						} else {
						  jQuery('#create-modal').modal('toggle');
						  this.paginator.pageIndex = 0;
						}

						this.paginator.page.emit();
          }, err => {
						this.toaster.pop('error', 'Préstamo bancario', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el préstamo bancario`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
						  if ( name != "dues" ) this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Préstamo bancario', 'El préstamo bancario ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Préstamo bancario', 'Ha ocurrido un error al eliminar el préstamo bancario');
          })
  }

  resetFormDue() {
    this.currentDue = {};
    this.createFormDue();
  }

  saveDue(form) {
    if ( !this.formDue.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.currentDue.id ) method = "update";

    if ( form.paid_date ) form.paid_date = this.dateTransform.transform(form.paid_date, "y-MM-dd");
    else delete form.paid_date;

    form.due_date = this.dateTransform.transform(form.due_date, "y-MM-dd");

    this.API[method]('bankloandues', form, this.currentDue.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Pago', `El pago ha sido ${ this.currentDue.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						delete res.bank_loan;

						if ( this.currentDue.id ) {
						  this.current.dues[this.currentDue.index] = res;
						} else this.current.dues.unshift(res);

						this.currentDue = {};
						this.createFormDue();
          }, err => {
						this.toaster.pop('error', 'Pago', `Ha ocurrido un error al ${ this.currentDue.id ? "actualizar" : "crear" } el pago`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
						  this.formDue.controls[name].setValue('');
						}
          })
  }

  editDue( due, index ) {
    this.errorMessages = undefined;
    due.due_date = DateGMT(due.due_date);
    if ( due.paid_date ) due.paid_date = DateGMT(due.paid_date);
    this.currentDue = due;
    this.currentDue.index = index;
    this.createFormDue();
  }

  deleteDue( due, index ) {
    this.current.requesting = true;
    this.API.delete('bankloandues', due.id)
        .subscribe(
          res => {
            this.current.requesting = false;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Pago', 'El pago ha sido eliminado exitosamente');
            this.current.dues.splice(index, 1);
          }, err => {
            this.current.requesting = false;
            this.toaster.pop('error', 'Pago', 'Ha ocurrido un error al eliminar el pago');
          })
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
