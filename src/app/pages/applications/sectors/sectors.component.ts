import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';
import { TimeCalculatorService } from "../../../services/time-calculator.service";

import {SelectionModel} from '@angular/cdk/collections';
import {isEmpty} from '../../../utils/utils';
import { forEach } from '@angular/router/src/utils/collection';

const MODEL = "applications/bysector";

@Component({
  selector: 'az-applications-sectors',
  templateUrl: './sectors.component.html',
  styleUrls: ['./sectors.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ApplicationsSectorsComponent implements AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fileInput') fileInput;

  selection = new SelectionModel(true, []);

  private simpleFilterFormValue: any = {
    year: new Date().getFullYear(),
    month: new Date().getMonth()
  };
  private advanceFilterFormValue: any;

  columns = [];
  displayedColumns = [];
  source = new MatTableDataSource([]);
  form: FormGroup;
  filtersForm: FormGroup;
  stateForm: FormGroup;
  costCenterCtrl: FormControl = new FormControl("");
  npksCtrl: FormControl = new FormControl("");
  productsCtrl: FormControl = new FormControl("");
  filteredOptionsNpks: Observable<any[]>;
  filteredOptionsProducts: Observable<any[]>;
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  costCenters;
  costCenterSub;
  lastYears: Array<any> = [];
  selectedNpks: Array<any> = [];
  selectedProducts: Array<any> = [];
  npks: Array<any> = [];
  sectors: Array<any> = [];
  products: Array<any> = [];
  resultsNpksProducts: Array<any> = [];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public timeService: TimeCalculatorService,
  ) {
    this.getSectorsInfo();
    this.createFiltersForm();
    this.createStateForm();
  }

  getSectorsInfo(){
    let params = {
      p: -1
    }
    this.API.index('sectors', params).subscribe(res => {
      this.sectors = res.data;

      this.displayedColumns.push("Producto")
      this.sectors.forEach((ele, i) =>{
        let formattedName = ele.name.toString().trim();
        this.displayedColumns.push(formattedName);
        this.columns.push({
          columnDef: formattedName,
          header: formattedName,
          cell: (row) => `${row[formattedName]}`
        });
      });
    });
  }

  getAverageHumity( row ) {
    return ( ( row.humity_at_30 + row.humity_at_60 + row.humity_at_90 ) / 3 ).toFixed(2);
  }

	setLastYears( date? ) {
		let start = new Date(date || "2010-01-02");
		let end = new Date();
		let years = moment(end).diff(start, 'years');
		for( let year = 0; year < years +1; year++ )
			this.lastYears.push(start.getFullYear() + year);

		this.createFiltersForm();
	}

  createStateForm() {
    this.stateForm = this.fb.group({
      state: [null, []]
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.source.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.source.data.forEach(row => this.selection.select(row));
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      date_from: moment().startOf('month').toDate(),
      date_to: moment().endOf('month').toDate(),
    });

    this.productsCtrl = new FormControl("");
    this.filteredOptionsProducts = this.productsCtrl.valueChanges
    .pipe(
      startWith(''),
      map(val => this.searchFilterProducts(val))
    )
  }

  runFiltering(){
    this.loadingResults = true;
    this.source.data = [];
    let params = new HttpParams();

    if ( this.filtersForm.value.date_from ) params = params.append('from', this.dateTransform.transform(this.filtersForm.value.date_from, "y-MM-dd"));
    if ( this.filtersForm.value.date_to ) params = params.append('to', this.dateTransform.transform(this.filtersForm.value.date_to, "y-MM-dd"));
    if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);


    if (this.selectedProducts.length){
      this.selectedProducts.forEach(res => {
        params = params.append('products[]', res.id);
      });
    } else {
      this.products.forEach(res => {
        params = params.append('products[]', res.id);
      });
    }

    this.API.index(MODEL, params).subscribe(res => {
      this.loadingResults = false;
      let mySectors = this.sectors;
      let myServerResult = res.data.results;
      let myLocalResult = [];

      this.selectedProducts.forEach(ele => {
        let mySectorsKeys = Object.keys(myServerResult[ele.id]);
        let innerSectors = [];
        mySectorsKeys.forEach(sectorid => {
          let name = null;
          let val = myServerResult[ele.id][sectorid];
          mySectors.forEach(serversector => {
            if (parseInt(serversector.id) === parseInt(sectorid)){
              name = serversector.name.toString().trim();
              innerSectors[name] = val;
            }
          });
        });

        myLocalResult.push({
          Producto: ele.name,
          ...innerSectors
        });
      });

      this.source.data = myLocalResult;
    });
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  selectRowNpks(row) {
    if ( !this.selectedNpks[row.id] ) {
      let npks = {
        name: row.name,
        id: row.id
      };
      this.selectedNpks.push(row);
    }

    this.npksCtrl.setValue("");
  }

  selectRowProducts(row) {
    if ( !this.selectedProducts[row.id] ) {
      let npks = {
        name: row.name,
        id: row.id
      };
      this.selectedProducts.push(row);
    }
    this.productsCtrl.setValue("");
  }

  removeNpk(id, index){
    this.selectedNpks.splice(index, 1);
  }

  removeProduct(id, index){
    this.selectedProducts.splice(index, 1);
  }

  updateItem(form, method) {

    const formData = new FormData();
    if ( !form.file ) { delete form.file }
    if ( !form.invoice_id ) { delete form.invoice_id }

    for ( const key in form ) {
      formData.append(key, form[key] );
    }

    formData.append('_method', 'PUT');
    return this.API[method](MODEL, formData, form.id, 'post');
  }

  toggleFilters(): Promise<void> {
    return new Promise<void>((resolve) => {
      jQuery('#filters').slideToggle(400, () => {
        resolve();
      });
    });
  }

  isAdvancedFilterVisible(): boolean {
    return jQuery('#filters').is(':visible');
  }

  resetFilters() {
    this.createFiltersForm();
    this.reload();
  }

  buildFilters(form) {
    this.reload();
  }

  onSimpleFiltering(form) {
    if (this.isAdvancedFilterVisible()) {
      this.toggleFilters().then(() => {
          this.simpleFilterFormValue = form;
          this.reload();
        }
      );
    } else {
      this.simpleFilterFormValue = form;
      this.reload();
    }
  }

  searchFilterNpks(val: any) {
    return this.npks.filter(option =>
      option.name.toLowerCase().indexOf((val) ? val.toLowerCase() : val) === 0);
  }

  searchFilterProducts(val: any) {
    return this.products.filter(option =>
      option.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  onAdvanceFiltering(form) {
    this.advanceFilterFormValue = form;
    this.reload();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );
    let myParams = {};
    let products = [];
    this.API.index('products', myParams).subscribe(res => {
      products = res.data;
      Observable.merge(this.sort.sortChange, this.paginator.page)
          .startWith(null)
          .switchMap(() => {
            this.loadingResults = true;
            let params = new HttpParams();

            if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);
            if (products.length){
              products.forEach(res => {
                params = params.append('products[]', res.id);
              });
            }

            if (!this.isAdvancedFilterVisible()) {
              let date = new Date( this.simpleFilterFormValue.year, this.simpleFilterFormValue.month ),
                from = new Date(date.getFullYear(), date.getMonth(), 1),
                to = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                params = params.append('from', this.dateTransform.transform(from, "y-MM-dd"));
                params = params.append('to', this.dateTransform.transform(to, "y-MM-dd"));
            } else {

              if (this.advanceFilterFormValue.from)
                params = params.append('from', this.dateTransform.transform(this.advanceFilterFormValue.from, 'y-MM-dd'));
              if (this.advanceFilterFormValue.to)
                params = params.append('to', this.dateTransform.transform(this.advanceFilterFormValue.to, 'y-MM-dd'));
            }

            return this.API.index(MODEL, params);
          })
          .map(data => {
            this.loadingResults = false;
            this.haveError = false;
            this.total = data.total;
            this.limit = data.per_page;
            this.products = products;
            let mySectors = this.sectors;
            let myServerResult = data.data.results;
            let myLocalResult = [];

            this.products.forEach(ele => {
              let mySectorsKeys = Object.keys(myServerResult[ele.id]);
              let innerSectors = [];
              mySectorsKeys.forEach(sectorid => {
                let name = null;
                let val = myServerResult[ele.id][sectorid];
                mySectors.forEach(serversector => {
                  if (parseInt(serversector.id) === parseInt(sectorid)){
                    name = serversector.name.toString().trim();
                    innerSectors[name] = val;
                  }
                });
              });

              myLocalResult.push({
                Producto: ele.name,
                ...innerSectors
              });
            });

            this.source.data = myLocalResult;
            return data.data;
          })
          .catch( err => {
            this.loadingResults = false;
            this.haveError = true;
            return Observable.of([]);
          })
          .subscribe(data => {});
      });
    }

}
