import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {CustomFormsModule} from 'ng2-validation';
import {
  MatAutocompleteModule, MatDialogModule, MatInputModule, MatPaginatorIntl, MatPaginatorModule, MatProgressSpinnerModule,
  MatSelectModule, MatSortModule, MatTableModule, MatTooltipModule
} from '@angular/material';
import {DirectivesModule} from '../../theme/directives/directives.module';
import {PipesModule} from '../../theme/pipes/pipes.module';
import {CrudComponentsModule} from './../../theme/components/crud-components/crud-components.module';
import {ApiService} from './../../services/api.service';
import {SelectHelperService} from './../../services/select-helper.service';
import {MatPaginatorLabels} from './../../services/mat-paginator-labels.service';
import {ApplicationsComponent} from './applications/applications.component';
import {TechnicalReferenceComponent} from './technical-reference/technical-reference.component';
import {TechnicalReferenceService} from '../../services/api/technical-reference/technical-reference.service';
import {SectorService} from '../../services/api/sector/sector.service';
import {TechnicalReferenceFormComponent} from './technical-reference/technical-reference-form/technical-reference-form.component';
import {ProductsFormComponent} from './applications/products-form/products-form.component';
import {ProductService} from '../../services/api/product/product.service';
import {SectorFormComponent} from './applications/sector-form/sector-form.component';
import {AdvanceFilterFormComponent} from './technical-reference/advance-filter-form/advance-filter-form.component';
import {AppAdvanceFilterFormComponent} from './applications/app-advance-filter-form/app-advance-filter-form.component';
import {AgronomistService} from '../../services/api/agronomist/agronomist.service';
import {ConfirmFormComponent} from './applications/confirm-form/confirm-form.component';
import {VarietyService} from '../../services/api/variety/variety.service';
import {TractorService} from '../../services/api/tractor/tractor.service';
import {MachineService} from '../../services/api/machine/machine.service';
import {FilterFormModule} from './technical-reference/filter-form/filter-form.module';
import {ApplicationService} from '../../services/api/application/application.service';
import {ApplicationReportService} from '../../services/api/application/application-report.service';
import {ApplicationReportComponent} from './applications/reports/application-report/application-report.component';
import {ApplicationsSectorsComponent} from './sectors/sectors.component';
import {ApplicationReportFilterComponent} from './applications/reports/application-report/filter/application-report-filter/application-report-filter.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {ApplicationReportParser} from './applications/reports/application-report/parsers/parser.service';
import {NpkService} from '../../services/api/application/npk.service';

export const routes = [
  { path: '', redirectTo: 'aplicaciones', pathMatch: 'full' },
  { path: 'aplicaciones', component: ApplicationsComponent, data: { breadcrumb: 'Aplicaciones' } },
  { path: 'referencia-tecnica', component: TechnicalReferenceComponent, data: { breadcrumb: 'Referencias Técnicas' } },
  { path: 'sectores', component: ApplicationsSectorsComponent, data: {breadcrumb: 'Aplicaciones por Sector', reportType: 'bysector'}},
  { path: 'reportes/npk', component: ApplicationReportComponent, data: {breadcrumb: 'Aplicaciones por NPK', reportType: 'bynpks'}},
  { path: 'reportes/hectarea', component: ApplicationReportComponent, data: {breadcrumb: 'Aplicaciones por Hectarea', reportType: 'byhas'}}
];

@NgModule({
  entryComponents: [
    TechnicalReferenceFormComponent,
    ProductsFormComponent,
    SectorFormComponent,
    AdvanceFilterFormComponent,
    AppAdvanceFilterFormComponent,
    ConfirmFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    CrudComponentsModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatDialogModule,
    DateTimePickerModule,
    CustomFormsModule,
    RouterModule.forChild(routes),
    FilterFormModule,
    NgxChartsModule
  ],
  declarations: [
    ApplicationsComponent,
    TechnicalReferenceComponent,
    TechnicalReferenceFormComponent,
    ProductsFormComponent,
    SectorFormComponent,
    AdvanceFilterFormComponent,
    ConfirmFormComponent,
    AppAdvanceFilterFormComponent,
    ApplicationReportComponent,
    ApplicationsSectorsComponent,
    ApplicationReportFilterComponent
  ],
  providers: [
    ApiService,
    SelectHelperService,
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorLabels
    },
    ApplicationReportService,
    ApplicationService,
    DatePipe,
    TechnicalReferenceService,
    SectorService,
    ProductService,
    NpkService,
    AgronomistService,
    VarietyService,
    TractorService,
    MachineService,
    ApplicationReportParser
  ]
})

export class ApplicationsModule { }
