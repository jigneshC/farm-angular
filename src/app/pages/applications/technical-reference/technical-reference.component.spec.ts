import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalReferenceComponent } from './technical-reference.component';

describe('TechnicalReferenceComponent', () => {
  let component: TechnicalReferenceComponent;
  let fixture: ComponentFixture<TechnicalReferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicalReferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalReferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
