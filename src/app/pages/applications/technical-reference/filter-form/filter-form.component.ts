import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'az-filter-form',
  templateUrl: './filter-form.component.html',
  styleUrls: ['./filter-form.component.scss']
})
export class FilterFormComponent implements OnInit {

  @Output() confirmFilters = new EventEmitter<string>();

  lastYears = ['2015', '2016', '2017', '2018', '2019', '2020'];

  filtersForm: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.createFiltersForm();
  }

  search(form: any) {
    form.month++;
    const auxMonth = form.month < 10 ? '0' + form.month : form.month;
    this.confirmFilters.emit(form.year + '-' + auxMonth + '-01');
    form.month--;
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      month: new Date().getMonth(),
      year: new Date().getFullYear()
    });
  }

}
