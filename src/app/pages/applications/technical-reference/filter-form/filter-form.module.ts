import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FilterFormComponent} from './filter-form.component';

@NgModule({
  entryComponents: [
    FilterFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    FilterFormComponent
  ],
  exports: [
    FilterFormComponent
  ],
  providers: []
})
export class FilterFormModule { }
