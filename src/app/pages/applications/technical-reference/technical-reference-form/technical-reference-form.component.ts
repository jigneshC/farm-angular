import {Component, EventEmitter, Inject, OnInit, Output, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToasterService} from 'angular2-toaster';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TechnicalReferenceService} from '../../../../services/api/technical-reference/technical-reference.service';
import {TechnicalReference} from '../../../../models/technical-reference';
import {CustomValidators} from 'ng2-validation';
import {TranslateService} from '../../../../services/translate.service';
import {DatePipe} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import {AgronomistService} from '../../../../services/api/agronomist/agronomist.service';
import {Agronomist} from '../../../../models/agronomist';
import {Variety} from '../../../../models/variety';
import {VarietyService} from '../../../../services/api/variety/variety.service';

@Component({
  selector: 'app-technical-reference-form',
  templateUrl: './technical-reference-form.component.html',
  styleUrls: ['./technical-reference-form.component.scss']
})
export class TechnicalReferenceFormComponent implements OnInit {

  @ViewChild('fileInput') fileInput;

  @Output() afterSave = new EventEmitter<string>(true);

  // TODO: Connect to service
  agronomists: Agronomist[] = [
    { id: 'José Guiñez', name: 'José Guiñez'},
    { id: 'José Torres', name: 'José Torres'},
    { id: 'Mauricio Alvarado', name: 'Mauricio Alvarado'},
    { id: 'Claudio Hernandez', name: 'Claudio Hernandez'},
  ];

  public varieties: any = [];

  seasons = ['2015', '2016', '2017', '2018', '2019', '2020'];
  public title: string;
  public errorMessages = [];
  public form: FormGroup;
  public isLoading = false;
  public isEdit = false;
  public varietyInput: any;

  technicalRef: TechnicalReference = new TechnicalReference();

  constructor(protected dialogRef: MatDialogRef<TechnicalReferenceFormComponent>,
              protected formBuilder: FormBuilder,
              protected technicalReferenceService: TechnicalReferenceService,
              protected agronomistService: AgronomistService,
              protected varietyService: VarietyService,
              protected toaster: ToasterService,
              public translate: TranslateService,
              private dateTransform: DatePipe,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    if (this.data.technicalRef) {
      this.isEdit = true;
      this.title = 'Editar Referencia Técnica';
      this.isLoading = true;
      Observable.forkJoin(
        this.technicalReferenceService.findOne(this.data.technicalRef.id),
        this.agronomistService.findAll(),
        this.varietyService.findAll()
      ).subscribe(data => {
        this.isLoading = false;
        this.technicalRef = data[0];
        // this.agronomists = data[1];
        this.varieties = data[2];
        this.varietyInput = data[0].variety.name;
        this.form.patchValue(data[0]);
      }, error => {
          this.errorMessages = ['Ha ocurrido un error al obtener la referencia técnica.'];
          this.isLoading = false;
      });
    } else {
      this.isEdit = false;
      this.title = 'Crear Referencia Técnica';
    }
    this.createForm();
/*    this.agronomistService.findAll()
      .subscribe(agronomists => {
        this.agronomists = agronomists;
      });*/
    this.varietyService.findAll()
      .subscribe(varieties => {
        this.varieties = varieties;
      });
  }

  createForm() {
    if ( this.fileInput ) { this.fileInput.nativeElement.value = '' };
    this.form = this.formBuilder.group({
      agronomist: [ '', [Validators.required, Validators.maxLength(75)]],
      number: [ 0, [Validators.required, Validators.maxLength(10)]],
      date: [ '', [ Validators.required, CustomValidators.date ] ],
      notes: [ '', [Validators.maxLength(255)]],
      season: [ '', [Validators.required, Validators.maxLength(6)]],
      variety_id: [ '', [Validators.required]],
      file: [ '', this.data.technicalRef ? [] : [ Validators.required ] ],
    });
  }

  addVariety(variety: Variety) {
    this.form.get('variety_id').patchValue(variety.id);
  }

  addedFile( $event ) {
    if ( $event.target.files[0].type !== 'application/pdf' ) {
      $event.target.value = '';
      this.form.controls['file'].setValue(null);
      return this.form.controls['file'].setErrors({ 'type': true });
    }
    this.form.controls['file'].setErrors({ 'type': null });
    this.form.controls['file'].setValue($event.target.files[0]);
  }

  close(): void {
    this.dialogRef.close();
  }

  parseForm(form): any {
    const formData = new FormData();
    for ( const key in form ) { if (key) {formData.append(key, form[key] )} }
    if (this.isEdit) { formData.append('_method', 'PUT'); }
    return formData;
  }

  save(form): void {
    this.form.value.date = this.dateTransform.transform(this.form.value.date, 'y-MM-dd');
    if (this.isEdit) {
      if (!form.file) { delete form.file }
      this.technicalReferenceService.update_post(String(this.technicalRef.id), this.parseForm(form))
        .subscribe(
          res => {
            this.afterSave.emit('success');
            this.dialogRef.afterClosed().subscribe(() => {
              this.toaster.pop('success', 'Referencia', 'Referencia editada exitosamente.');
            });
            this.dialogRef.close();
          },
          error => {
            this.errorMessages.push('Ha ocurrido un error al intentar guardar.');
          })
    } else {
      this.technicalReferenceService.create(this.parseForm(form))
        .subscribe(res => {
            this.afterSave.emit('success');
            this.dialogRef.afterClosed().subscribe(() => {
              this.toaster.pop('success', 'Referencia', 'Referencia guardada exitosamente.');
            });
            this.dialogRef.close();
          },
          error => {
            this.errorMessages.push('Ha ocurrido un error al intentar guardar.');
          });
    }
  }

}
