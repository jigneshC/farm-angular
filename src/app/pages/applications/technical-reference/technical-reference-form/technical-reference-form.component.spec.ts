import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnicalReferenceFormComponent } from './technical-reference-form.component';

describe('TechnicalReferenceFormComponent', () => {
  let component: TechnicalReferenceFormComponent;
  let fixture: ComponentFixture<TechnicalReferenceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechnicalReferenceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnicalReferenceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
