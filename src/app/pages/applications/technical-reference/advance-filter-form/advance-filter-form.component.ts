import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TranslateService} from '../../../../services/translate.service';
import {VarietyService} from '../../../../services/api/variety/variety.service';

@Component({
  selector: 'az-advance-filter-form',
  templateUrl: './advance-filter-form.component.html',
  styleUrls: ['./advance-filter-form.component.scss']
})
export class AdvanceFilterFormComponent implements OnInit {

  // TODO: DUMMY DATA
  agronomists: any = [
    { id: 'José Guiñez', name: 'José Guiñez'},
    { id: 'José Torres', name: 'José Torres'},
    { id: 'Mauricio Alvarado', name: 'Mauricio Alvarado'},
    { id: 'Claudio Hernandez', name: 'Claudio Hernandez'},
  ];

  varieties: any = [];

  seasons = ['2015', '2016', '2017', '2018', '2019', '2020'];

  @Output() confirmFilters = new EventEmitter<boolean>();

  filtersForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public translate: TranslateService,
    private varietyService: VarietyService
  ) { }

  ngOnInit() {
    this.varietyService.findAll()
      .subscribe(varieties => {
        this.varieties = varieties;
      });
    this.createFiltersForm();
  }

  search(form: any) {
    this.confirmFilters.emit(form);
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      start: [ '' ],
      end: [ '' ],
      agronomist: [ '' ],
      season: [ '' ],
      variety_id: [ 0 ],
    });
  }

  resetFilters() {
    this.filtersForm.reset();
  }

}
