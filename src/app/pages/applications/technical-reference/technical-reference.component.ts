import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Subscription} from 'rxjs/Subscription';
import {ToasterService} from 'angular2-toaster';
import {Subject} from 'rxjs/Subject';
import {TechnicalReferenceService} from '../../../services/api/technical-reference/technical-reference.service';
import {TechnicalReference} from '../../../models/technical-reference';
import {Observable} from 'rxjs/Observable';
import {Pagination} from '../../../services/api/pagination';
import {HttpParams} from '@angular/common/http';
import {TechnicalReferenceFormComponent} from './technical-reference-form/technical-reference-form.component';
import {DatePipe} from '@angular/common';
import {ApiService} from '../../../services/api.service';

export class AdvanceFilter {
  start: string;
  end: string;
  agronomist: string;
  season: string;
  variety_id: number;
}

@Component({
  selector: 'app-technical-reference',
  templateUrl: './technical-reference.component.html',
  styleUrls: ['./technical-reference.component.scss']
})

export class TechnicalReferenceComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public columns = ['date', 'agronomist', 'number', 'season', 'variety_id', 'notes', 'actions'];
  public source = new MatTableDataSource();
  public loadingResults = true;
  public error: string;
  haveError: boolean = false;
  public filter: string;
  public basicFilter: string = (new Date().getFullYear()) + "-" + (new Date().getMonth()+1);
  private isBasicFilter: boolean = true;
  public advanceFilter: AdvanceFilter;
  timeout: any;
  total: number = 0;
  limit: number = 10;
  public tecnhicalRef: TechnicalReference;

  public filterChanged = new Subject<string>();
  public filterSubscription = new Subscription();

  protected formResultSubscription: Subscription;

  constructor(
    private technicalReferenceService: TechnicalReferenceService,
    private API: ApiService,
    private dateTransform: DatePipe,
    private toaster: ToasterService,
    protected  dialog: MatDialog) {

  }

  toggleFilters() {
    jQuery('#filters').slideToggle();
  }

  reload() {
    if ( this.timeout ) { clearTimeout(this.timeout) };
    this.source.data = [];
    this.filter = '';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  searchWithFilter(filters: any) {
    this.isBasicFilter = true;
    this.basicFilter = filters;
    this.search();
  }

  searchWithAdvanceFilter(filters: any) {
    this.isBasicFilter = false;
    this.advanceFilter = filters;
    this.search();
  }

  getTechnicalReferences(): void {
    this.loadingResults = true;
    const params = this.buildSearchParams();

    this.technicalReferenceService.findPaginated(params)
      .catch(error => {
        this.error = error;
        return Observable.of([]);
      })
      .finally(() => {
        this.loadingResults = false;
      })
      .map((data: Pagination<TechnicalReference>) => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .subscribe((data: TechnicalReference[]) => {
          this.source.data = data;
        }
      );
  }

  download( id ) {
    this.API.custom('get', `api/${ 'technicalreferences' }/${id}/download`)
      .subscribe(
        res => window.open(res.data.file_url),
        err => this.toaster.pop('error', 'Informe', 'Ha ocurrido un error al descargar el informe')
      );
  }

  createOrEdit(row?: TechnicalReference): void {
    this.tecnhicalRef = row;
    const data = this.tecnhicalRef ? {technicalRef: this.tecnhicalRef} : {};
    const form = this.dialog.open(TechnicalReferenceFormComponent, {
      width: '800px',
      data
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.getTechnicalReferences();
      }
    })
  }

  delete() {
    this.technicalReferenceService.delete(String(this.tecnhicalRef.id))
      .subscribe(
        res => {
          if ( this.source.data.length === 1 && this.paginator.pageIndex > 0 ) { this.paginator.pageIndex-- };
          this.paginator.page.emit();
          this.toaster.pop('success', 'Referencia Técnica', 'Referencia Técnica eliminada exitosamente.');
        },
        error => {
          this.toaster.pop('error', 'Referencia Técnica', 'Ha ocurrido un error al eliminar la Referencia Técnica.');
        }
      );
  }

  ngOnInit() {
    // this.source = new MatTableDataSource();
    // this.getTechnicalReferences();
  }

  buildSearchParams(): HttpParams {
    let params = new HttpParams();

    if ( this.paginator.pageIndex >= 0 ) { params = params.append('page', `${ this.paginator.pageIndex + 1 }`) }
    if ( this.paginator.pageSize ) {params = params.append('p', `${this.paginator.pageSize}`) }
    if ( this.filter ) { params = params.append('q', this.filter) }
    if ( this.sort.active ) { params = params.append('sort_by', this.sort.active) }
    if ( this.sort.direction ) { params = params.append('sort_dir', this.sort.direction) }

    // basic filter
    if (this.basicFilter && this.isBasicFilter) {
      const since = this.basicFilter;
      const until = new Date(+this.basicFilter.split('-')[0], +this.basicFilter.split('-')[1], 0);
      params = params.append('from', this.dateTransform.transform(since, 'yyyy-MM-dd'));
      params = params.append('to', this.dateTransform.transform(until, 'yyyy-MM-dd'));
    }
    // advance filter
    if (this.advanceFilter && !this.isBasicFilter) {
      if (this.advanceFilter.start) {
        params = params.append('from', this.dateTransform.transform(this.advanceFilter.start, 'yyyy-MM-dd'));
      }
      if (this.advanceFilter.end) {
        params = params.append('to', this.dateTransform.transform(this.advanceFilter.end, 'yyyy-MM-dd'));
      }
      if (this.advanceFilter.agronomist) { params = params.append('agronomist', this.advanceFilter.agronomist) };
      if (this.advanceFilter.variety_id) { params = params.append('variety', String(this.advanceFilter.variety_id)) };
      if (this.advanceFilter.season) { params = params.append('season', String(this.advanceFilter.season)) };
    }

    return params;
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        const params = this.buildSearchParams();

        return this.technicalReferenceService.findPaginated(params);
      })
      .map((data: Pagination<TechnicalReference>) => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .catch( err => {
        this.loadingResults = false;
        this.haveError = true;
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data);
  }

  ngOnDestroy() {
    // this.formResultSubscription.unsubscribe();
  }

  keyupChange(): void {
    this.filterChanged.next(this.filter);
  }

  search() {
    if ( this.timeout ) { clearTimeout(this.timeout) };
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }

}
