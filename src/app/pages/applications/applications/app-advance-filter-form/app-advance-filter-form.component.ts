import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '../../../../services/translate.service';
import {Sector} from '../../../../models/sector';
import {SectorService} from '../../../../services/api/sector/sector.service';
import {ApplicationService} from '../../../../services/api/application/application.service';
import {AppConfig} from '../../../../app.config';
import { ApiService } from '../../../../services/api.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-advance-filter-form',
  templateUrl: './app-advance-filter-form.component.html',
  styleUrls: ['./app-advance-filter-form.component.scss']
})
export class AppAdvanceFilterFormComponent implements OnInit {

  // TODO: quitar dummy
  types: any = [
    { id: 0, type: 'Aerea'},
    { id: 1, type: 'Vía Riego'},
    { id: 2, type: 'Foliar'},
    { id: 3, type: 'Manual'},
  ];

  dosagetypes: any = [
    { id: 'porha', value: 'Por Hectárea'},
    { id: '100 litros', value: 'Por 100 litros'},
  ];

  @Input()
  tractors: any = [];
  @Input()
  machines: any = [];
  @Input()
  technicalreferences: any = [];
  @Input()
  phenologicalstages: any = [];

  aplicators = [];
  allNpks: Array<any> = [];
  products: Array<any> = [];

  @Output() confirmFilters = new EventEmitter<boolean>();

  filtersForm: FormGroup;

  sectors: Sector[];

  constructor(
    private fb: FormBuilder,
    public translate: TranslateService,
    protected sectorService: SectorService,
    protected applicationService: ApplicationService,
    public appConfig: AppConfig,
    private API: ApiService,
    private dateTransform: DatePipe
  ) {
    this.getNpksInfo();
    this.getProducts();
  }

  ngOnInit() {
    this.aplicators = this.applicationService.getApplicators();
    this.createFiltersForm();
  }

  getProducts() {
    let params = {
      p: -1
    }
    this.API.index('products', params).subscribe( res => {
      this.products = res.data;
    });
  }

  getNpksInfo(){
    let params = {
    }
    this.API.index('products/npks', params).subscribe(res => {
      this.allNpks = res["npks"];
    });
  }

  search(form: any) {
    this.confirmFilters.emit(form);
  }

  createFiltersForm() {
    this.sectorService.findAll()
      .subscribe(sectors => {
        this.sectors = sectors;
      });
    this.filtersForm = this.fb.group({
      start: [ '' ],
      end: [ '' ],
      type: [ null, []],
      sectors: [ 0],
      phenologicalstage: [ '', []],
      tractor_id: [ 0 , []],
      machine_id: [ 0, []],
      dosificador: [ '', []],
      dosagetype: [ 0, []],
      aplicador: [ '', []],
      npk: [ 0, []],
      product_type: [ null, []],
      product: [null, []]
    });
  }

  filterReport(form: any, output: string = '') {
    let query = new URLSearchParams();
    if (form.start) {
      query.append('from', this.dateTransform.transform(form.start, "y-MM-dd"));
    }
    if (form.end) {
      query.append('to', this.dateTransform.transform(form.end, "y-MM-dd"));
    }
    if (form.type) {
      query.append('type', form.type)
    }
    if (form.sectors) {
      query.append('sector', form.sectors);
    }
    if (form.phenologicalstage) {
      query.append('phenologicalstage', form.phenologicalstage);
    }
    if (form.tractor_id) {
      query.append('tractor_id', form.tractor_id);
    }
    if (form.machine_id) {
      query.append('machine_id', form.machine_id);
    }
    if (form.dosificador) {
      query.append('dosificador', form.dosificador);
    }
    if (form.dosagetype) {
      query.append('dosagetype', form.dosagetype);
    }
    if (form.aplicador) {
      query.append('aplicador', form.aplicador);
    }
    if (form.npk) {
      query.append('npk', form.npk);
    }
    if (form.product_type) {
      query.append('product_type', form.product_type);
    }
    if (form.product) {
      query.append('product[]', form.product);
    }
    return query;
  }

  report() {
    let form = this.filtersForm.value;
    let query = this.filterReport(form);
    var url = `${this.appConfig.config.API_URL}applications/reports/phytosanataries-products?${query.toString()}`;
    window.open(url, '_blank');
  }

  reportFertilizers() {
    let form = this.filtersForm.value;
    let query = this.filterReport(form);
    var url = `${this.appConfig.config.API_URL}applications/reports/fertilizers-products?${query.toString()}`;
    window.open(url, '_blank');
  }

  patchData(data: any) {
    this.filtersForm.patchValue(data);
  }

  resetFilters() {
    this.filtersForm.reset();
  }

}
