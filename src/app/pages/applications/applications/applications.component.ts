import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../services/api.service';
import {ToasterService} from 'angular2-toaster';
import {TranslateService} from '../../../services/translate.service';
import {TimeCalculatorService} from '../../../services/time-calculator.service';
import {DatePipe} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import {HttpParams} from '@angular/common/http';
import {CustomValidators} from 'ng2-validation';

import DateGMT from '../../../classes/date-gmt';
import {Subscription} from 'rxjs/Subscription';
import {ConfirmFormComponent} from './confirm-form/confirm-form.component';
import {TractorService} from '../../../services/api/tractor/tractor.service';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {MachineService} from '../../../services/api/machine/machine.service';
import {TechnicalReferenceService} from '../../../services/api/technical-reference/technical-reference.service';
import {ApplicationService} from '../../../services/api/application/application.service';
import { AppConfig } from './../../../app.config';
import {ActivatedRoute} from '@angular/router';
import {AppAdvanceFilterFormComponent} from './app-advance-filter-form/app-advance-filter-form.component';
import {SectorFormComponent} from './sector-form/sector-form.component';

const MODEL = 'applications';

export class AdvanceFilter {
  start: string;
  end: string;
  type: string;
  sectors: string;
  phenologicalstage: string;
  tractor_id: string;
  machine_id: string;
  dosificador: string;
  dosagetype: string;
  aplicador: string;
  npk: string;
  product_type: string;
  product: string;
}

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements AfterViewInit, OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(AppAdvanceFilterFormComponent) appAdvanceFilterFormComponent: AppAdvanceFilterFormComponent;
  @ViewChild(SectorFormComponent) sectorForm: SectorFormComponent;

  // TODO: quitar dummy
  types: any = [
    {id: 0, type: 'Aerea'},
    {id: 1, type: 'Vía Riego'},
    {id: 2, type: 'Foliar'},
    {id: 3, type: 'Manual'},
  ];

  phenologicalstages: any = [];

  dosagetypes: any = [
    {id: 'porha', value: 'Por Hectárea'},
    {id: '100 litros', value: 'Por 100 litros'},
  ];

  tractors: any = [];
  machines: any = [];
  technicalreferences: any = [];

  aplicators = [];

  public packages = ['Litros', 'Kilos'];

  columns = ['id', 'date', 'status', 'start', 'end', 'sectors', 'type', 'phenologicalstage', 'appliedhas', 'mojamientoporha', 'products',
    'tractor', 'machine', 'dosificador', 'dosagetype', 'aplicador', 'technicalreference_id', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  filtersForm: FormGroup;
  errors = {sinceDate: '', untilDate: ''};
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  public basicFilter: string = (new Date().getFullYear()) + '-' + (new Date().getMonth() + 1);
  private isBasicFilter: boolean = true;
  public advanceFilter: AdvanceFilter;
  timeout: any;
  errorMessages: any;
  sectors: Array<{ id: number; name: string; appliedhas: number; }>;
  products: Array<{ id: number; name: string; quantity: number; total_quantity: number; package: string; }> = [];
  isFoliar = false;
  filteredTractors: Observable<string[]>;
  tractorCtrl: FormControl = new FormControl();
  filteredMachines: Observable<string[]>;
  machineCtrl: FormControl = new FormControl();
  filteredReferences: Observable<string[]>;
  referencesCtrl: FormControl = new FormControl();

  protected formResultSubscription: Subscription;

  constructor(private toaster: ToasterService,
              private API: ApiService,
              private applicationService: ApplicationService,
              private tractorService: TractorService,
              private machineService: MachineService,
              private technicalReferenceServices: TechnicalReferenceService,
              private fb: FormBuilder,
              public translate: TranslateService,
              private dateTransform: DatePipe,
              public appConfig: AppConfig,
              public timeService: TimeCalculatorService,
              protected dialog: MatDialog,
              protected route: ActivatedRoute) {
    this.aplicators = this.applicationService.getApplicators();
  }

  public getPackageName(index) {
    if (index > 1) {
      return 'Kilos'
    }
    return this.packages[index];
  }

  createForm() {
    this.errorMessages = undefined;

    if (this.current.type) {
      this.isFoliar = +this.current.type === 2;
    } else {
      this.isFoliar = false;
    }
    if (this.current.sectors) {
      this.current.appliedhas = this.current.sectors.map(s => {
        return +s.pivot.appliedhas
      }).reduce((total, suma) => {
        return total + suma;
      }, 0);

      this.sectors = this.current.sectors.map(sector => {
        return {
          id: sector.id,
          name: sector.name,
          appliedhas: +sector.pivot.appliedhas
        };
      });
    } else {
      this.sectors = [];
    }

    if (this.current.products) {
      this.products = this.current.products.map(p => {
        return {
          id: p.id,
          name: p.name,
          quantity: p.pivot.quantity,
          total_quantity: +this.current.appliedhas,
          package: this.getPackageName(p.package)
        }
      }, []);
    } else {
      this.products = [];
    }

    this.form = this.fb.group({
      date: [this.current.date || '', [Validators.required, CustomValidators.date]],
      start: [this.current.start || '', [Validators.required, CustomValidators.date]],
      end: [this.current.end || '', [Validators.required, CustomValidators.date]],
      type: [this.current.type, [Validators.required]],
      products: [this.current.products || [], []],
      phenologicalstage: [this.current.phenologicalstage || '', [Validators.required]],
      mojamientoporha: [this.current.mojamientoporha || 0, [Validators.maxLength(8)]],
      tractor_id: [this.current.tractor_id || null, []],
      machine_id: [this.current.machine_id || null, []],
      dosificador: [this.current.dosificador || '', [Validators.required]],
      dosagetype: [this.current.dosagetype || 0, [Validators.required]],
      aplicador: [this.current.aplicador || '', [Validators.required]],
      technicalreference_id: [this.current.technicalreference_id || null, [Validators.required]],
      notes: [this.current.notes || '', [Validators.maxLength(255)]],
      observations: [this.current.observations || '', [Validators.maxLength(255)]],
      status: [this.current.status || 0],
      goal: [this.current.goal || '', [Validators.required, Validators.maxLength(150)]],
    });

    this.initializeAutoCompletes();
    this.searchPhenologicalstages();
    if (localStorage.getItem("applicationDetail")) this.openAdvanceFilter();
  }

  initializeAutoCompletes() {
    this.tractorCtrl = new FormControl(this.current.tractor ? (this.current.tractor.brand + this.current.tractor.model + this.current.tractor.year) : null);
    this.machineCtrl = new FormControl(this.current.machine ? (this.current.machine.brand + this.current.machine.model + this.current.machine.year) : null);
    this.referencesCtrl = new FormControl(
      this.current.technical_reference
        ? this.current.technical_reference.agronomist + ' N° ' + this.current.technical_reference.number : null);
    this.filteredTractors = this.tractorCtrl.valueChanges
      .pipe(
        startWith(this.current.tractor ? this.current.tractor.identifier : ''),
        map(val => this.filterTractors(val))
      );
    this.filteredMachines = this.machineCtrl.valueChanges
      .pipe(
        startWith(this.current.machine ? this.current.machine.identifier : ''),
        map(val => this.filterMachines(val))
      );
    this.filteredReferences = this.referencesCtrl.valueChanges
      .pipe(
        startWith(
          this.current.technical_reference
            ? this.current.technical_reference.agronomist + ' N° ' + this.current.technical_reference.number : ''),
        map(val => this.filterReferences(val))
      );
  }

  addTractor(tractor: any) {
    this.form.get('tractor_id').patchValue(tractor.id);
  }

  addMachine(machine: any) {
    this.form.get('machine_id').patchValue(machine.id);
  }

  addReference(ref: any) {
    this.form.get('technicalreference_id').patchValue(ref.id);
  }

  filterTractors(val: string): string[] {
    return this.tractors.filter(tractor =>
      tractor.identifier.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  filterMachines(val: string): string[] {
    return this.machines.filter(machine =>
      machine.identifier.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  filterReferences(val: string): string[] {
    return this.technicalreferences.filter(reference =>
      reference.agronomist.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  typeChange(typeId) {
    if (+typeId !== 2) {
      this.isFoliar = false;
    } else {
      this.isFoliar = true;
    }
  }

  changeAutoComplete($type, $event){
    let val = $event.target.value;
    if (!val){
      if ($type === 'tractor'){
        this.form.get('tractor_id').patchValue('');
      }else {
        this.form.get('machine_id').patchValue('');
      }
    }
  }

  getTypeName(id: number) {
    if (id > 3) {
      return 'Foliar'
    }
    return this.types.filter(type => {
      return type.id === id
    })[0].type;
  }

  getTechnicalReferenceName(id: number) {
    const list = this.technicalreferences.filter(t => {
      if (t.id === id) {
        return t;
      }
    }, []);
    if (list.length === 0) {
      return {name: 'No'}
    } else {
      return list[0]
    }
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  saveSector(sector: { id: number; name: string; appliedhas: number; }) {
    this.sectors.push(sector);

    const appliedhas = this.sectors.map(s => {
      return s.appliedhas
    }).reduce((total, suma) => {
      return total + suma;
    }, 0);
    // this.form.controls['appliedhas'].patchValue(appliedhas);
  }

  deleteSector(index: number) {
    this.sectors.splice(index, 1);
  }

  saveProduct(product: { id: number; name: string; quantity: number; total_quantity: number; package: string; }) {
    this.products.push(product);
    this.form.controls['products'].patchValue(this.products);
  }

  deleteProduct(index: number) {
    this.products.splice(index, 1);
  }

  buildFilters(form) {
    if (this.errors.sinceDate || this.errors.untilDate) {
      return
    }
    ;
    this.reload();
  }

  toggleFilters() {
    jQuery('#filters').slideToggle();
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit(row: any) {
    row.start = DateGMT(row.start);
    row.end = DateGMT(row.end);
    this.current = row;
    this.createForm();
  }

  duplicate(row: any) {
    row.start = DateGMT(row.start);
    row.end = DateGMT(row.end);
    this.current = row;
    delete this.current.id;
    this.createForm();
  }

  save(form) {
    if (!this.form.valid) {
      return
    }
    ;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = 'save';

    if (this.current.id) {
      method = 'update'
    }
    ;

    form.date = this.dateTransform.transform(form.date, 'y-MM-dd');
    form.start = this.dateTransform.transform(form.start, 'y-MM-dd') + ' 00:00:00';
    form.end = this.dateTransform.transform(form.end, 'y-MM-dd') + ' 00:00:00';

    // missing field
    form.dosis = '2';
    // type selection change
    if (!this.isFoliar) {
      delete form.tractor_id;
      delete form.machine_id;
      delete form.mojamientoporha;
    } else {
      if (!form.tractor_id) delete form.tractor_id;
      if (!form.machine_id) delete form.machine_id;
    }
    // calculate appliedhas
    form.appliedhas = this.sectors.map(s => {
      return +s.appliedhas
    }).reduce((total, suma) => {
      return total + suma;
    }, 0);
    form.sectors = this.sectors.map((sector) => {
      return {
        id: sector.id,
        appliedhas: +sector.appliedhas
      }
    }, []);
    form.products = this.products.map(product => {
      return {
        id: product.id,
        quantity: product.quantity,
        total_quantity: product.total_quantity
      }
    }, []);

    this.API[method](MODEL, form, this.current.id)
      .subscribe(
        res => {
          this.toaster.pop('success', 'Aplicación', `La aplicación ha sido ${ this.current.id ? 'actualizado' : 'creado' } exitosamente`);
          this.current.requesting = false;
          if (!this.current.id) {
            this.paginator.pageIndex = 0
          }
          ;
          this.paginator.page.emit();
          jQuery('#form-modal').modal('toggle');
        }, err => {
          this.toaster.pop('error', 'Aplicación', `Ha ocurrido un error al ${ this.current.id ? 'actualizar' : 'crear' } la aplicación!`);
          this.current.requesting = false;
          this.errorMessages = err.error.errors;
          for (const name of this.errorMessages) {
            this.form.controls[name].setValue('');
          }
        })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
      .subscribe(
        res => {
          if (this.source.data.length === 1 && this.paginator.pageIndex > 0) {
            this.paginator.pageIndex--
          }
          ;
          this.paginator.page.emit();
          this.toaster.pop('success', 'Aplicación', 'La aplicación se ha sido eliminado exitosamente');
          this.current = {};
        }, err => {
          this.toaster.pop('error', 'Aplicación', 'Ha ocurrido un error al eliminar la aplicación');
        })
  }

  reload() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.source.data = [];
    this.filter = '';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  searchAutoComplete(q, variable, model) {
    let params = new HttpParams();
    if (q) {
      params = params.append('q', q);
    }
    return this.API.index(model, params).subscribe(data => {
      this.current.requesting = false;
    });
  }

  searchWithFilter(filters: any) {
    this.isBasicFilter = true;
    this.basicFilter = filters;
    this.search();
  }

  searchWithAdvanceFilter(filters: any) {
    this.isBasicFilter = false;
    this.advanceFilter = filters;
    this.search();
  }

  search() {
    if ( this.timeout ) { clearTimeout(this.timeout); }
    this.timeout = setTimeout(() => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }

  selectRow(row, model, form) {
    this[form].controls[model + '_id'].setValue(row.id);
  }

  ngOnInit() {
    Observable.forkJoin(
      this.tractorService.findAll(),
      this.machineService.findAll(),
      this.technicalReferenceServices.findAll()
    ).subscribe(data => {
      this.tractors = data[0];
      this.machines = data[1];
      this.technicalreferences = data[2];
    });
    this.createForm();
  }

  buildSearchParams(): HttpParams {
    let params = new HttpParams();

    if ( this.paginator.pageIndex >= 0 ) { params = params.append('page', `${ this.paginator.pageIndex + 1 }`) }
    if ( this.paginator.pageSize ) {params = params.append('p', `${this.paginator.pageSize}`) }
    if ( this.filter ) { params = params.append('q', this.filter) }
    if ( this.sort.active ) { params = params.append('sort_by', this.sort.active) }
    if ( this.sort.direction ) { params = params.append('sort_dir', this.sort.direction) }

    // basic filter
    if (this.basicFilter && this.isBasicFilter) {
      const since = this.basicFilter;
      const until = new Date(+this.basicFilter.split('-')[0], +this.basicFilter.split('-')[1], 0);
      params = params.append('from', this.dateTransform.transform(since, 'yyyy-MM-dd'));
      params = params.append('to', this.dateTransform.transform(until, 'yyyy-MM-dd'));
    }
    // advance filter
    if (this.advanceFilter && !this.isBasicFilter) {
      if (this.advanceFilter.start) {
        params = params.append('from', this.dateTransform.transform(this.advanceFilter.start, 'yyyy-MM-dd'));
      }
      if (this.advanceFilter.end) {
        params = params.append('to', this.dateTransform.transform(this.advanceFilter.end, 'yyyy-MM-dd'));
      }
      if (this.advanceFilter.type) {
        params = params.append('type', this.advanceFilter.type)
      }
      if (this.advanceFilter.sectors) {
        params = params.append('sector', String(this.advanceFilter.sectors))
      }
      if (this.advanceFilter.phenologicalstage) {
        params = params.append('phenological_stage', String(this.advanceFilter.phenologicalstage))
      }
      if (this.advanceFilter.tractor_id) {
        params = params.append('tractor_id', String(this.advanceFilter.tractor_id))
      }
      if (this.advanceFilter.machine_id) {
        params = params.append('machine_id', String(this.advanceFilter.machine_id))
      }
      if (this.advanceFilter.dosificador) {
        params = params.append('dosificador', String(this.advanceFilter.dosificador))
      }
      if (this.advanceFilter.aplicador) {
        params = params.append('aplicador', String(this.advanceFilter.aplicador))
      }
      if (this.advanceFilter.dosagetype) {
        params = params.append('dosagetype', String(this.advanceFilter.dosagetype))
      }
      if (this.advanceFilter.npk) {
        params = params.append('npk', String(this.advanceFilter.npk))
      }
      if (this.advanceFilter.product_type) {
        params = params.append('product_classification', String(this.advanceFilter.product_type))
      }
      if (this.advanceFilter.product) {
        params = params.append('product', this.advanceFilter.product)
      }
      if (this.route.snapshot.paramMap.get('isFromApplicationReportHectarea') && this.advanceFilter.product) {
        params = params.append('product', this.advanceFilter.product);
        // Borramos el parametro una vez utilizado
        this.advanceFilter.product = null;
      }
    }

    return params;
  }

  filterReport(output: string = '') {
    let query = new URLSearchParams();
    let advanceFilter = this.appAdvanceFilterFormComponent.filtersForm;

    // advance filter
      if (advanceFilter.controls["start"].value) {
        query.append('from', this.dateTransform.transform(advanceFilter.controls["start"].value, 'yyyy-MM-dd'));
      }
      if (advanceFilter.controls["end"].value) {
        query.append('to', this.dateTransform.transform(advanceFilter.controls["end"].value, 'yyyy-MM-dd'));
      }
      if (advanceFilter.controls["type"].value) {
        query.append('type', advanceFilter.controls["type"].value)
      }
      if (advanceFilter.controls["sectors"].value) {
        query.append('sector', String(advanceFilter.controls["sectors"].value))
      }
      if (advanceFilter.controls["phenologicalstage"].value) {
        query.append('phenological_stage', String(advanceFilter.controls["phenologicalstage"].value))
      }
      if (advanceFilter.controls["tractor_id"].value) {
        query.append('tractor_id', String(advanceFilter.controls["tractor_id"].value))
      }
      if (advanceFilter.controls["machine_id"].value) {
        query.append('machine_id', String(advanceFilter.controls["machine_id"].value))
      }
      if (advanceFilter.controls["dosificador"].value) {
        query.append('dosificador', String(advanceFilter.controls["dosificador"].value))
      }
      if (advanceFilter.controls["aplicador"].value) {
        query.append('aplicador', String(advanceFilter.controls["aplicador"].value))
      }
      if (advanceFilter.controls["dosagetype"].value) {
        query.append('dosagetype', String(advanceFilter.controls["dosagetype"].value))
      }
      if (advanceFilter.controls["npk"].value) {
        query.append('npk', String(advanceFilter.controls["npk"].value))
      }
      if (advanceFilter.controls["product_type"].value) {
        query.append('product_classification', String(advanceFilter.controls["product_type"].value))
      }

    query.append('output', output);
    return query;
  }

  generateExportLink(output: string) {
    let query = this.filterReport(output);
    var url = `${this.appConfig.config.API_URL}applications/reports?${query.toString()}`;
    window.open(url, '_blank');
  }

  processQueryParams(): void {
    const urlParams = this.route.snapshot.paramMap;
    if (urlParams) {
      if (urlParams.get('isFromApplicationReportNPK')) {
        const filters = {
          sectors: [+urlParams.get('sector_id')],
          npk: [+urlParams.get('product_id')],
          start: urlParams.get('fechaDesde'),
          end: urlParams.get('fechaHasta')
        };
        this.appAdvanceFilterFormComponent.patchData(filters);
        this.advanceFilter = this.appAdvanceFilterFormComponent.filtersForm.value;
        this.isBasicFilter = false;
        this.toggleFilters();
      } else if (urlParams.get('isFromApplicationReportHectarea')) {
        const filters = {
          sectors: [+urlParams.get('sector_id')],
          start: urlParams.get('fechaDesde'),
          end: urlParams.get('fechaHasta'),
          product: urlParams.get('product_id')
        };
        this.appAdvanceFilterFormComponent.patchData(filters);
        this.advanceFilter = <any> filters;
        this.isBasicFilter = false;
        this.toggleFilters();
      }
    }
  }

  searchPhenologicalstages(){
    let params = {
    }
    this.API.index('varieties/phenologicalstages', params).subscribe(res => {
      let myArray = [];
      let arrayOfKeys = Object.keys(res);
      arrayOfKeys.forEach(element =>{
        myArray.push({
          id: element,
          value: res[element]
        })
      });
      this.phenologicalstages = myArray;
    });
  }

  selectMySector(sector){
    let params = {
      variety_id: sector.variety.id
    }
    this.API.index('varieties/phenologicalstages', params).subscribe(res => {
      if (res.length){
        let myArray = [];
        res.forEach(element => {
          myArray.push({
            id: element,
            value: element
          });
        });
        this.phenologicalstages = myArray;
      }
    });
  }

  ngAfterViewInit() {
    this.processQueryParams();
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        const params = this.buildSearchParams();

        return this.API.index(MODEL, params);
      })
      .map(data => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .catch(err => {
        this.loadingResults = false;
        this.haveError = true;
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data);
  }

  getLinkPDF(rowId: any) {
    return this.applicationService.getLnkPDF(String(rowId));
  }

  goToConfirm(row?: any): void {
    const data = row;
    const form = this.dialog.open(ConfirmFormComponent, {
      width: '800px',
      data
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.paginator.page.emit();
      }
    })
  }

  openAdvanceFilter(){
    this.toggleFilters();
    let localFilters = JSON.parse(localStorage.getItem("applicationDetail"));
    this.appAdvanceFilterFormComponent.filtersForm.controls["start"].setValue(localFilters.from);
    this.appAdvanceFilterFormComponent.filtersForm.controls["end"].setValue(localFilters.to);
    localStorage.removeItem("applicationDetail");

    this.reload();
  }
}
