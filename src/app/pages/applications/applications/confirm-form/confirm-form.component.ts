import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {DatePipe} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '../../../../services/translate.service';
import {ApiService} from '../../../../services/api.service';
import {CustomValidators} from 'ng2-validation';
import {TimeKeyInputService} from '../../../../services/time-key-input.service';
import * as moment from "moment";

@Component({
  selector: 'az-confirm-form',
  templateUrl: './confirm-form.component.html',
  styleUrls: ['./confirm-form.component.scss']
})
export class ConfirmFormComponent implements OnInit {

  @Output() afterSave = new EventEmitter<string>(true);

  public title = 'Confirmar Aplicación';
  public errorMessages = [];
  public form: FormGroup;
  public isLoading = false;
  private app: any;

  constructor(protected dialogRef: MatDialogRef<ConfirmFormComponent>,
              protected formBuilder: FormBuilder,
              protected API: ApiService,
              protected toaster: ToasterService,
              public translate: TranslateService,
              protected dateTransform: DatePipe,
              public timeKeyService: TimeKeyInputService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.createForm();
  }

  ngOnInit() {
    if (this.data.id) {
      this.isLoading = true;
      this.API.show('applications', this.data.id)
        .subscribe(
          res => {
            this.app = res;
            this.form.get('start').patchValue(res.start);
            this.form.get('end').patchValue(res.end);
            this.form.get('startHour').patchValue(moment(res.start).format('HH:mm'));
            this.form.get('endHour').patchValue(moment(res.end).format('HH:mm'));
            this.form.setValue({
              start: res.start,
              end: res.end,
              startHour: moment(res.start).format('HH:mm'),
              endHour: moment(res.end).format('HH:mm'),
              realmojamientoporha: res.realmojamientoporha ? res.realmojamientoporha : 0,
              excess: res.excess,
              place_excess_drop: res.place_excess_drop,
              dilution: res.dilution,
              notes: res.notes
            });
            this.isLoading = false;
          }, err => {
            this.toaster.pop('error', 'Aplicación', `Ha ocurrido un error al obtener la aplicación!`);
            this.errorMessages = err.error.errors;
            this.isLoading = false;
            for ( const name of this.errorMessages ) {
              this.form.controls[name].setValue('');
            }
          })
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  createForm() {
    this.form = this.formBuilder.group({
      start: [ '', [CustomValidators.date]],
      startHour: [ '', [ Validators.required ] ],
      end: [ '', [CustomValidators.date]],
      endHour: [ '', [ Validators.required ] ],
      excess: [ 0, [ CustomValidators.number ] ],
      place_excess_drop: [ '', [Validators.maxLength(80)]],
      dilution: [ true, []],
      realmojamientoporha: [ 0, [CustomValidators.number]],
      notes: [ '', [Validators.maxLength(255)]],
    });
  }

  confirm(form: any) {
    const start = moment(form.start).format('YYYY-MM-DD') + ' ' + form.startHour;
    const end = moment(form.end).format('YYYY-MM-DD') + ' ' + form.endHour;
    if (moment(end).isBefore(start)) {
      this.form.controls['end'].setErrors({'menors': true});
      return;
    }
    this.isLoading = true;
    form.start = moment(start).format('YYYY-MM-DD HH:mm:ss');
    form.end = moment(end).format('YYYY-MM-DD HH:mm:ss');
    this.API.save(`applications/${this.data.id}/confirmation`, form)
      .subscribe(
        res => {
          this.isLoading = false;
          this.afterSave.emit('success');
          this.dialogRef.afterClosed().subscribe(() => {
            this.toaster.pop('success', 'Aplicación', `La aplicación ha sido confirmada exitosamente`);
          });
          this.dialogRef.close();
        }, err => {
          this.isLoading = false;
          this.toaster.pop('error', 'Aplicación', `Ha ocurrido un error al confirmar la aplicación!`);
          this.errorMessages = err.error.errors;
          for ( const name of this.errorMessages ) {
            this.form.controls[name].setValue('');
          }
        })
  }

}
