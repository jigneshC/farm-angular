import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApplicationReportService, ApplicationReportType} from '../../../../../services/api/application/application-report.service';
import {ProductService} from '../../../../../services/api/product/product.service';
import {ApplicationReportFilterComponent} from './filter/application-report-filter/application-report-filter.component';
import {Observable} from 'rxjs/Observable';
import {ApplicationReportModel} from '../../../../../models/application-report';
import {ApplicationReportParser} from './parsers/parser.service';
import {StackedBarModel} from '../../../../../utils/parsers/graph.interfaces';
import * as moment from 'moment';
import {NpkService} from '../../../../../services/api/application/npk.service';
import {RestService} from '../../../../../services/api/rest.service';
import {Product} from '../../../../../models/product';

@Component({
  selector: 'az-application-report',
  templateUrl: './application-report.component.html',
  styleUrls: ['./application-report.component.scss']
})
export class ApplicationReportComponent implements OnInit {

  @ViewChild('filter') filter: ApplicationReportFilterComponent;

  public reportConfig = {
    xLabel: 'Sectores',
    yLabel: 'Cantidades',
    colorScheme: {
      domain: ['#265a88', '#419641', '#2aabd2', '#eb9316', '#c12e2a', '#3c763d', '#31708f', '#8a6d3b', '#a94442']
    }
  };
  public isLoading = false;
  public error: string;
  public title = '';
  public reportType = '';
  public data: StackedBarModel[] = [];
  public products = [];
  public productRestService: RestService<Product>;

  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected reportService: ApplicationReportService,
              protected productService: ProductService,
              protected npkService: NpkService,
              protected parser: ApplicationReportParser) {
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.reportType = data.reportType;
      this.initPageType();
      this.isLoading = true;
      Observable.forkJoin(this.productRestService.findAll(), this.reportService.get(this.reportType, this.buildQueryParams()))
        .subscribe(resultArray => {
            this.isLoading = false;
            this.products = resultArray[0];
            this.data = this.parser.parse(<ApplicationReportModel[]> resultArray[1]);
          },
          error => {
            this.isLoading = false;
            console.error(error);
            this.error = 'Ha ocurrido un error al obtener los datos'
          });
    });
  }

  protected buildQueryParams(): any {
    return this.filter.toParams();
  }

  submitFilter(): void {
    this.error = null;
    this.isLoading = true;
    this.reportService.get(this.reportType, this.buildQueryParams())
      .subscribe( (data: ApplicationReportModel[]) => {
          this.isLoading = false;
          this.data = this.parser.parse(data);
        },
        error => {
          this.isLoading = false;
          console.error(error);
          this.error = 'Ha ocurrido un error al obtener los datos'
        });
  }

  onSelect($event): void {
    const dateFilters: any = {};
    const fechaDesde = this.filter.filtersForm.get('fechaDesde').value;
    const fechaHasta = this.filter.filtersForm.get('fechaHasta').value;
    if (fechaDesde) {
      dateFilters.fechaDesde = moment(fechaDesde).format('YYYY-MM-DD');
    }
    if (fechaHasta) {
      dateFilters.fechaHasta = moment(fechaHasta).format('YYYY-MM-DD');
    }
    if (this.reportType === ApplicationReportType.NPK) {
      const data = Object.assign({isFromApplicationReportNPK: true}, $event.data, dateFilters);
      this.router.navigate(['pages', 'aplicaciones', 'aplicaciones', data]);
    } else if (this.reportType === ApplicationReportType.HECTAREA) {
      const data = Object.assign({isFromApplicationReportHectarea: true}, $event.data, dateFilters);
      this.router.navigate(['pages', 'aplicaciones', 'aplicaciones', data]);
    }
  }

  initPageType(): void {
    switch (this.reportType) {
      case ApplicationReportType.NPK:
        this.title = 'Aplicaciones por NPK';
        this.productRestService = this.npkService;
        break;
      case ApplicationReportType.HECTAREA:
        this.title = 'Aplicaciones por Hectárea';
        this.productRestService = this.productService;
        break;
      case ApplicationReportType.SECTOR:
        this.title = 'Aplicaciones por Sector';
        this.productRestService = this.productService;
        break;
    }
  }
}


