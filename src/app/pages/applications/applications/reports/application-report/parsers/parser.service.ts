import {StackedBarModel, StackedBarParser} from '../../../../../../utils/parsers/graph.interfaces';
import {ApplicationReportModel} from '../../../../../../models/application-report';
import {Injectable} from '@angular/core';

@Injectable()
export class ApplicationReportParser implements StackedBarParser<ApplicationReportModel> {

  parse(modelArray: ApplicationReportModel[]): StackedBarModel[] {
    const map = new Map<string, StackedBarModel>();
    for (const model of modelArray) {
      for (const sector of model.sectors) {
        if (map.has(sector.sector_name)) {
          const pkg = (model.product.package) ? ' ' + model.product.package : '';
          const name: string =  model.product.name + pkg;
          map.get(sector.sector_name).series.push(
            {name: name, value: +sector.quantity, data: {product_id: model.product.id, sector_id: sector.sector_id}}
          );
        } else {
          const stackedModel = new StackedBarModel();
          stackedModel.name = sector.sector_name;
          stackedModel.series = [{
            name: model.product.name,
            value: +sector.quantity,
            data: {product_id: model.product.id, sector_id: sector.sector_id}
          }];
          map.set(sector.sector_name, stackedModel);
        }
      }
    }
    return Array.from(map.values());
  }

}
