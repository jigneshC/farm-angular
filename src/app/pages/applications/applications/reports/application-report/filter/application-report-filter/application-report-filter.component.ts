import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../../../../../../../models/product';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DateValidators} from '../../../../../../../utils/validators';
import {TranslateService} from '../../../../../../../services/translate.service';
import * as moment from 'moment';
import {ApplicationReportType} from '../../../../../../../services/api/application/application-report.service';

@Component({
  selector: 'az-application-report-filter',
  templateUrl: './application-report-filter.component.html',
  styleUrls: ['./application-report-filter.component.scss']
})
export class ApplicationReportFilterComponent implements OnInit {

  @Input() products: Product[];
  @Output() onSubmit = new EventEmitter<any>();
  @Input() filterType = ApplicationReportType.HECTAREA;

  public comboLabel: string;
  public filtersForm: FormGroup;

  protected comboParamString: string;

  constructor(protected fb: FormBuilder, public translate: TranslateService) {
    const fechaHasta = moment().endOf('month').toDate();
    const fechaDesde = moment().startOf('month').toDate();
    this.filtersForm = this.fb.group({
      products: ['', []],
      fechaDesde: [fechaDesde, [Validators.required]],
      fechaHasta: [fechaHasta, [Validators.required]],
      searchInput: ['', [Validators.maxLength(50)]]
    }, {
      validator: Validators.compose([
        DateValidators.dateLessThan('fechaDesde', 'fechaHasta', {'dateLess': true})
      ])
    });
  }

  ngOnInit() {
    switch(this.filterType) {
      case ApplicationReportType.NPK:
        this.comboLabel = 'NPKS';
        this.comboParamString = 'npks[]';
        break;
      case ApplicationReportType.HECTAREA:
      case ApplicationReportType.SECTOR:
        this.comboLabel = 'Productos';
        this.comboParamString = 'products[]';
        break;
      default:
        break;
    }
  }

  toParams(): any {
    const formValue = this.filtersForm.value;
    const fechaDesde = formValue.fechaDesde;
    const fechaHasta = formValue.fechaHasta;
    const products = formValue.products;
    const searchFilter = formValue.searchInput;
    const result = {};
    if (fechaDesde) {
      result['from'] = moment(fechaDesde).format('YYYY-MM-DD');
    }
    if (fechaHasta) {
      result['to'] = moment(fechaHasta).format('YYYY-MM-DD');
    }
    if (products) {
      result[this.comboParamString] = products.map( p =>  p.id)
    }
    if (searchFilter) {
      result['q'] = searchFilter;
    }
    return result;
  }

  resetFilters() {
    const fechaHasta = moment().endOf('month').toDate();
    const fechaDesde = moment().startOf('month').toDate();
    this.filtersForm.patchValue({fechaDesde: fechaDesde, fechaHasta: fechaHasta, products: [''], searchInput: ''})
  }


}
