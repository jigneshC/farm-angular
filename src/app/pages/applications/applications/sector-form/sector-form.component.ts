import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SectorService} from '../../../../services/api/sector/sector.service';
import {Sector} from '../../../../models/sector';

@Component({
  selector: 'az-sector-form',
  templateUrl: './sector-form.component.html',
  styleUrls: ['./sector-form.component.scss']
})
export class SectorFormComponent implements OnInit {

  @Input() selectedSectors: Array<{ id: number; name: string; appliedhas: number; }> = [];
  @Output() confirmSectors = new EventEmitter<{ id: number; name: string; appliedhas: number; }>();
  @Output() confirmDeleteSector = new EventEmitter<number>();
  @Output() selectSector = new EventEmitter<any>();

  public form: FormGroup;
  public sectors: Sector[];

  constructor(private sectorsService: SectorService,
              protected formBuilder: FormBuilder) {

  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  confirm(sector: { id: number; name: string; appliedhas: number; }) {
    this.confirmSectors.emit(sector);
  }

  changeSector(event: any) {
    const sector = event.value;
    if (sector) {
      this.selectSector.emit(sector);
      this.form.patchValue(sector);
      this.form.patchValue({appliedhas: sector.size});
    }
  }

  addSector(form: any) {
    const p = {
      id: form.sector.id,
      name: form.sector.name,
      appliedhas: form.appliedhas
    };
    // this.selectedSectors.push(p);
    this.form.reset();
    this.confirm(p);
  }

  deleteSector(index: number) {
    this.confirmDeleteSector.emit(index);
    // this.selectedSectors.splice(index, 1);
  }

/*  createForm(sector: any) {
    this.form = this.formBuilder.group({
      sector: [ sector, [Validators.required]],
      appliedhas: [sector.appliedhas !== 0 ? sector.appliedhas : sector.size, [Validators.required, Validators.maxLength(10)]],
    });
  }*/

  ngOnInit() {
    this.form = this.formBuilder.group({
      sector: [ [], [Validators.required]],
      appliedhas: [0, [Validators.required, Validators.maxLength(10)]],
    });

    this.sectorsService.findAll()
      .subscribe(sectors => {
        this.sectors = sectors;
      });
  }

}
