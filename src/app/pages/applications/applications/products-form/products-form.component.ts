import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductService} from '../../../../services/api/product/product.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Product} from '../../../../models/product';

@Component({
  selector: 'az-products-form',
  templateUrl: './products-form.component.html',
  styleUrls: ['./products-form.component.scss']
})
export class ProductsFormComponent implements OnInit {

  @Input() selectedSectors: Array<{ id: number; name: string; appliedhas: number; }> = [];
  @Input() selectedProducts: Array<{ id: number; name: string; quantity: number; total_quantity: number; package: string; }> = [];
  @Output() confirmProducts = new EventEmitter<{ id: number; name: string; quantity: number; total_quantity: number; package: string; }>();
  @Output() confirmDeleteProduct = new EventEmitter<number>();

  public productForm: FormGroup;
  public products: Product[];

  public packages = ['Litros', 'Kilos'];

  constructor(private productsService: ProductService,
              protected formBuilder: FormBuilder) {

  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  public getPackageName(index) {
    if (index > 1) { return 'Kilos' }
    return this.packages[index];
  }

  addProduct(form: any, product: any) {
    const p = {
      id: form.product.id,
      name: form.product.name,
      quantity: form.quantity,
      total_quantity: form.quantity,
      package: this.getPackageName(product.package)
    };
    p.total_quantity = this.selectedSectors.map(sector => {
      return sector.appliedhas;
    }, []).reduce((total, suma) => {
      return total + suma;
    });
    // this.selectedProducts.push(p);
    this.confirmProducts.emit(p);
    this.productForm.reset();
  }

  deleteProduct(index: number) {
    this.confirmDeleteProduct.emit(index);
    // this.selectedProducts.splice(index, 1);
  }

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      product: [ [], []],
      quantity: [0, [ Validators.maxLength(10)]],
    });

    this.productsService.findAll()
      .subscribe(products => {
        this.products = products;
      });
  }

}
