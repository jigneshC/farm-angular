import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalicatasComponent } from './calicatas.component';

describe('CalicatasComponent', () => {
  let component: CalicatasComponent;
  let fixture: ComponentFixture<CalicatasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalicatasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalicatasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
