import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';
import { TimeCalculatorService } from "../../../services/time-calculator.service";
import { TimeKeyInputService } from "../../../services/time-key-input.service";

import DateGMT from 'app/classes/date-gmt';
import {Marker, MarkerOptions} from "@agm/core/services/google-maps-types";
import {LatLng} from "@agm/core";
import { MouseEvent as AGMMouseEvent } from '@agm/core';

const MODEL = "calicatas";

@Component({
  selector: 'az-calicatas',
  templateUrl: './calicatas.component.html',
  styleUrls: ['./calicatas.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class CalicatasComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['date', 'time_since_last_irrigation', 'sector_id', 'type_soil', 'place', 'humity_at_30', 'humity_at_60', 'humity_at_90', 'humity_avg', 'quality_fruit', 'quality_foliage', 'quality_flower', 'quality_cuaja', 'condition', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  filtersForm1: FormGroup;
  filtersForm2: FormGroup;
  sectorCtrl: FormControl = new FormControl("");
  filterSectorCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  sectors;
  sectorSub;
  filterSectors;
  filterSectorSub;
	lastYears: Array<any> = [];
  typeSoils = [ { value: 0, name: 'FRANCO' }, { value: 1, name: 'ARCILLOSO' }, { value: 2, name: 'ARENOSO' }, { value: 3, name: 'LIMOSO' }, { value: 4, name: 'FRANCO/ARCILLOSO' }, { value: 5, name: 'PEDREGOSO' } ];
  places = [ { value: 0, name: 'MISMA HILERA SONDA' }, { value: 1, name: 'MISMO SECTOR ALEATORIO' }, { value: 2, name: 'SECTOR SIN SONDA' } ];
  plantQualities = [ { value: 0, name: 'NO APLICA' }, { value: 1, name: 'POCA' }, { value: 2, name: 'ABUNDANTE' } ];
  humityValues = [1, 1.5, 2, 2.5, 3, 3.5, 4];
  advanceFilterEnable: boolean = false;
  title: string = 'Calicata';
  lat: any = -30.714546;
  lng: any = -70.905587;
  mapTypeId: string = 'satellite';
  zoom: number = 18;
  mark: MarkerOptions = {
    position: {
      lat: -30.714546,
      lng: -70.905587
    },
    clickable: true,
    draggable: true,
    title: 'Calicata',
    label: 'Calicata'
  };

  constructor(
    private toaster: ToasterService,
    public appConfig: AppConfig,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public timeService: TimeCalculatorService,
    public timeKeyService: TimeKeyInputService
  ) {
    this.mark.position.lng = this.lng;
    this.mark.position.lat = this.lat;
    this.createForm();
    this.createFiltersForm1();
    this.createFiltersForm2();
		this.getLastYears();
  }

  getAverageHumity( row ) {
    return ( ( row.humity_at_30 + row.humity_at_60 + row.humity_at_90 ) / 3 ).toFixed(2);
  }

	getLastYears() {
		let params = new HttpParams();
		params = params.append('p', `1`);
		params = params.append('sort_dir', `asc`);

		this.API.index(MODEL, params)
        .subscribe(
          res => {
						let calicata = res.data[0];
						if ( calicata ) {
							this.setLastYears(calicata.date)
						} else this.setLastYears();
          }, err => {
						this.setLastYears();
          })
	}

	setLastYears( date? ) {
		let start = moment(new Date(date || "2010-01-02"));
    let end = moment();
    do {
        this.lastYears.push(start.year());
        start.add(1, 'y');
    } while (start.year() <= end.year());

		this.createFiltersForm1();
	}

  createForm() {
    this.errorMessages = undefined;
    this.sectorCtrl = new FormControl(this.current.sector ? this.current.sector.name : "");
    this.sectorSub = this.sectorCtrl.valueChanges.startWith(this.current.sector ? this.current.sector.name : "").subscribe(q => this.searchAutoComplete(q, 'sectors', 'sectors'));

    this.form = this.fb.group({
      date: [ this.current.date || '', [ Validators.required, CustomValidators.date ] ],
      hour: [ this.current.hour ? moment(this.current.hour).format("HH:mm") : '', [ Validators.required ] ],
      sector_id: [ this.current.sector_id || '', [ Validators.required, CustomValidators.number ] ],
      type_soil: [ this.current.type_soil || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 5]) ] ],
      place: [ this.current.place || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 2]) ] ],
      humity_at_30: [ this.current.humity_at_30 || '', [ Validators.required, CustomValidators.number, CustomValidators.range([1, 4]) ] ],
      humity_at_60: [ this.current.humity_at_60 || '', [ Validators.required, CustomValidators.number, CustomValidators.range([1, 4]) ] ],
      humity_at_90: [ this.current.humity_at_90 || '', [ Validators.required, CustomValidators.number, CustomValidators.range([1, 4]) ] ],
      quality_fruit: [ this.current.quality_fruit || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 2]) ] ],
      quality_foliage: [ this.current.quality_foliage || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 2]) ] ],
      quality_flower: [ this.current.quality_flower || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 2]) ] ],
      quality_cuaja: [ this.current.quality_cuaja || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 2]) ] ],
      condition: [ this.current.condition || '', [ ] ]
    });
  }

  createFiltersForm1() {
    this.filtersForm1 = this.fb.group({
      month: new Date().getMonth(),
      year: this.lastYears[this.lastYears.length - 1] || new Date().getFullYear()
    });
  }

  createFiltersForm2() {
    this.filterSectorCtrl = new FormControl("");
    this.filterSectorSub = this.filterSectorCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'filterSectors', 'sectors'));

    this.filtersForm2 = this.fb.group({
      sector_id: '',
      from: '',
      to: '',
      humity30: '',
      humity60: '',
      humity90: '',
    });
  }

  create() {
    this.current = {};
    this.createForm();
    this.mark.position.lng = this.lng;
    this.mark.position.lat = this.lat;
  }

  edit( row: any ) {
    this.current = row;
    this.mark.position.lng = row.lng ? row.lng : this.lng;
    this.mark.position.lat = row.lat ? row.lat : this.lat;
    this.lng = this.mark.position.lng;
    this.lat = this.mark.position.lat;

    if ( typeof row.hour == "string" ) {
      let ar = row.hour.split(":");
      let hour = DateGMT(row.date);
      hour.setHours( ar[0] );
      hour.setMinutes( ar[1] );
      this.current.hour = hour;
    }
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  resetAutocomplete( form, control, ctrl, variable, model ) {
    this[ctrl] = new FormControl("");
    this[ctrl].valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, variable, model));
    this[form].controls[control].setValue('');
  }

  changeSectorValue($event){
    if (!$event.target.value){
      this["filtersForm2"].controls["sector_id"].setValue('');
    }
  }

  searchAutoComplete( q, variable, model ) {
    if ( model == "sectors" && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model == "sectors" && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    if ( typeof form.hour == "string" ) {
      let ar = form.hour.split(":");
      let hour = DateGMT(form.date);
      hour.setHours( ar[0] );
      hour.setMinutes( ar[1] );
      form.hour = hour;
    }

    form.date = this.dateTransform.transform(form.date, "y-MM-dd");
    form.hour = this.dateTransform.transform(form.hour, "HH:mm");
    form.lat  = this.mark.position.lat;
    form.lng  = this.mark.position.lng;

    this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Calicata', `La calicata ha sido ${ this.current.id ? "actualizada" : "creada" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						jQuery('#form-modal').modal('toggle');
          }, err => {
						this.toaster.pop('error', 'Calicata', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } la calicata`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Calicata', 'La calicata ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Calicata', 'Ha ocurrido un error al eliminar la calicata');
          })
  }

  toggleFilters() {
    jQuery("#filters").slideToggle();
    this.advanceFilterEnable = !this.advanceFilterEnable;
  }

  resetFilters() {
    this.createFiltersForm2();
    this.reload();
  }

  buildFilters(value, form) {
    this.reload();
  }

  filterReport(output: string = '') {
    let query = new URLSearchParams();
    if ( this.filtersForm2.value.sector_id ) query.append('by_sector', this.filtersForm2.value.sector_id);
    if ( this.filtersForm2.value.from ) query.append('from', this.dateTransform.transform(this.filtersForm2.value.from, "y-MM-dd"));
    if ( this.filtersForm2.value.to ) query.append('to', this.dateTransform.transform(this.filtersForm2.value.to, "y-MM-dd"));
    if (output) query.append('output', output);
    return query;
  }

  generateExportLink(output: string) {
    let query = this.filterReport(output);
    var url = `${this.appConfig.config.API_URL}calicatas/reports?${query.toString()}`;
    window.open(url, '_blank');
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          if ( !this.advanceFilterEnable ) {
            let date = new Date( this.filtersForm1.value.year, this.filtersForm1.value.month ),
                from = new Date(date.getFullYear(), date.getMonth(), 1),
                to = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            params = params.append('from', this.dateTransform.transform(from, "y-MM-dd"));
            params = params.append('to', this.dateTransform.transform(to, "y-MM-dd"));
          }

          if ( this.advanceFilterEnable ) {

            if ( this.filtersForm2.value.sector_id ) params = params.append('by_sector', this.filtersForm2.value.sector_id);
            if ( this.filtersForm2.value.from ) params = params.append('from', this.dateTransform.transform(this.filtersForm2.value.from, "y-MM-dd"));
            if ( this.filtersForm2.value.to ) params = params.append('to', this.dateTransform.transform(this.filtersForm2.value.to, "y-MM-dd"));
            if ( this.filtersForm2.value.humity30 ) params = params.append('humity30', this.filtersForm2.value.humity30);
            if ( this.filtersForm2.value.humity60 ) params = params.append('humity60', this.filtersForm2.value.humity60);
            if ( this.filtersForm2.value.humity90 ) params = params.append('humity90', this.filtersForm2.value.humity90);

          }

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }

  mapClicked($event: AGMMouseEvent) {
    this.mark.position.lat = $event.coords.lat;
    this.mark.position.lng = $event.coords.lng;
  }
}
