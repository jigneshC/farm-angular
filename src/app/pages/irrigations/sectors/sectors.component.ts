import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';
import {IrrigationService} from '../../../services/api/irrigation/irrigation';

const MODEL = "sectors";

@Component({
  selector: 'az-sectors',
  templateUrl: './sectors.component.html',
  styleUrls: ['./sectors.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SectorsComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['name', 'variety_id', 'size', 'distance', 'precipitation', 'plantation_year', 'lt_second', 'm3_hour', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  filterSectorCtrl: FormControl;
  varietyCtrl: FormControl = new FormControl('');
  deviceCtrl: FormControl = new FormControl('');
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  ha1:any='';
  ha2:any='';
  ha3:any='';
  ha4:any='';
  errorMessages;
  varieties;
  varietySub;
  devicesSub;
  filterSectors;
  filterSectorsSub;
  lastYears:Array<any> = [];
  enables = [ { value: 1, name: 'SI' }, { value: 0, name: 'NO' } ];

  devices: any = [];

  constructor(
    private irrigationService: IrrigationService,
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
  ) {
    this.loadVarieties();
    this.setLastYears();
    this.createForm();
  }

  loadVarieties(){
    this.API.index('varieties')
      .subscribe(
        res => {
          this.ha1 = res.data[0];
          this.ha2 = res.data[1];
          this.ha3 = res.data[2];
          this.ha4 = res.data[3];
        }
      )
  }

  setLastYears( date? ) {
    let start = new Date("1950-01-02");
    let end = new Date();
    let years = moment(end).diff(start, 'years');
    for( let year = 0; year < years +1; year++ )
      this.lastYears.push(start.getFullYear() + year);
  }

  createForm() {
    this.errorMessages = undefined;
    this.filterSectorCtrl = new FormControl(this.current.associated_sector_id ? this.current.associate.name : "");
    this.filterSectorsSub = this.filterSectorCtrl.valueChanges.startWith(this.current.associated_sector_id ? this.current.associate.name : "").subscribe(q => this.searchAutoComplete(q, 'filterSectors', 'sectors'));
    this.varietyCtrl = new FormControl(this.current.variety ? this.current.variety.name : "");
    this.varietySub = this.varietyCtrl.valueChanges.startWith(this.current.variety ? this.current.variety.name : "").subscribe(q => this.searchAutoComplete(q, 'varieties', 'varieties'));
    this.deviceCtrl = new FormControl(this.current.irrigation_device_id ? this.current.irrigation_device.name : null);
    this.devicesSub = this.deviceCtrl.valueChanges.startWith(this.current.irrigation_device_id ? this.current.irrigation_device.name : "").subscribe(q => this.searchAutoComplete(q, 'devices', 'irrigationdevices'));

    this.form = this.fb.group({
      name: [ this.current.name || '', [ Validators.required, Validators.maxLength(255) ] ],
      variety_id: [ this.current.variety_id || '', [ Validators.required, CustomValidators.number ] ],
      size: [ this.current.size || '', [ Validators.required, CustomValidators.number ] ],
      distance: [ this.current.distance || '', [ Validators.required, Validators.maxLength(7) ] ],
      precipitation: [ this.current.precipitation || '', [ Validators.required, CustomValidators.number ] ],
      plantation_year: [ this.current.plantation_year || '', [ Validators.required, CustomValidators.number, CustomValidators.min(1950) ] ],
      m3_hour: [ this.current.m3_hour || '', [ CustomValidators.number, CustomValidators.range([0, 99999.99]) ] ],
      lt_second: [ this.current.lt_second || '', [ CustomValidators.number, CustomValidators.range([0, 99999.99]) ] ],
      mask: [ this.current.mask || '', [ CustomValidators.number, CustomValidators.range([0, 99999.99]) ] ],
      irrigation_device_id: [ this.current.irrigation_device_id || '', [ CustomValidators.number, CustomValidators.range([0, 9999999]) ] ],
      associated_sector_id: [ this.current.associated_sector_id || '', [ CustomValidators.number ] ],
      enabled: [ this.current.enabled || 1]
    });
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    this.current = row;
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( model == "varieties" && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model == "varieties" && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    if (form === 'sectors'){
      this.form.controls['associated_sector_id'].setValue(row.id);
    } else {
      this[form].controls[model+"_id"].setValue(row.id);
    }
  }

  selectDevice(device: any) {
    this.form.get('irrigation_device_id').patchValue(device.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    if ( !form.m3_hour ) delete form.m3_hour;
    if ( !form.lt_second ) delete form.lt_second;

    this.API[method](MODEL, form, this.current.id)
      .subscribe(
        res => {
          this.toaster.pop('success', 'Sector', `El sector ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
          this.current.requesting = false;
          if ( !this.current.id ) this.paginator.pageIndex = 0;
          this.paginator.page.emit();
          jQuery('#form-modal').modal('toggle');
        }, err => {
          this.toaster.pop('error', 'Sector', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el sector`);
          this.current.requesting = false;
          this.errorMessages = err.error.errors;
          for ( let name in this.errorMessages ) {
            this.form.controls[name].setValue('');
          }
        })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
      .subscribe(
        res => {
          if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
          this.paginator.page.emit();
          this.toaster.pop('success', 'Sector', 'El sector ha sido eliminado exitosamente');
          this.current = {};
        }, err => {
          this.toaster.pop('error', 'Sector', 'Ha ocurrido un error al eliminar el sector');
        })
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        let params = new HttpParams();

        if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
        if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
        if ( this.filter ) params = params.append('q', this.filter);
        if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
        if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

        return this.API.index(MODEL, params);
      })
      .map(data => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;
        let newArray = [];
        data.data.forEach(element => {
          if (element.enabled) newArray.push(element);
        });

        this.source.data = newArray;
        return data.data;
      })
      .catch( err => {
        this.loadingResults = false;
        this.haveError = true;
        return Observable.of([]);
      })
      .subscribe(data => {});
  }
}
