export const Chartparams: any[] = [
    {
        id: 1,
        name: 'inTemp',
        color: '#FFA500',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v1",gid:"g1",viewminimum:0,viewmaximum:0,parent_id:1,
        position: "left", glabel:"inTemp", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"°C",unitname:"inTemp (°C)",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 2,
        name: 'outTemp',
        color: '#A52A2A',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v2",gid:"g2",viewminimum:0,viewmaximum:0,parent_id:1,
        position: "left", glabel:"outTemp", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"outTemp (°C)",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 3,
        name: 'extraTemp1',
        color: '#7FFF00',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v3",gid:"g3",viewminimum:0,viewmaximum:0,parent_id:1,
        position: "left", glabel:"extraTemp1", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"extraTemp1 (°C)",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 4,
        name: 'extraTemp2',
        color: '#FF7F50',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v4",gid:"g4",viewminimum:0,viewmaximum:0,parent_id:1,
        position: "left", glabel:"extraTemp2", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"extraTemp2 (°C)",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 5,
        name: 'extraTemp3',
        color: '#00FFFF',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v5",gid:"g5",viewminimum:0,viewmaximum:0,parent_id:1,
        position: "left", glabel:"extraTemp3", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"extraTemp3 (°C)",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 6,
        name: 'heatindex',
        color: '#A0522D',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v6",gid:"g6",viewminimum:0,viewmaximum:0,parent_id:1,
        position: "left", glabel:"heatindex", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"heatindex",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 7,
        name: 'windSpeed',
        color: '#0000FF',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v7",gid:"g7",viewminimum:0,viewmaximum:0,parent_id:7,
        position: "left", glabel:"windSpeed", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"Wind speed (km/hr)",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 8,
        name: 'windGust',
        color: '#00008B',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v8",gid:"g8",viewminimum:0,viewmaximum:0,parent_id:7,
        position: "left", glabel:"windGust", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"windGust",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 9,
        name: 'windchill',
        color: '#A9A9A9',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v9",gid:"g9",viewminimum:0,viewmaximum:0,parent_id:7,
        position: "left", glabel:"windchill", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"windchill",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 10,
        name: 'windDir',
        color: '#8A2BE2',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v10",gid:"g10",viewminimum:0,viewmaximum:0,parent_id:10,
        position: "left", glabel:"windDir", offset :0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"windDir",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    
    {
        id: 11,
        name: 'windGustDir',
        color: '#B8860B',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v11",gid:"g11",viewminimum:0,viewmaximum:0,parent_id:10,
        position: "left", glabel:"windGustDir", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"windGustDir",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 12,
        name: 'consBatteryVoltage',
        color: '#8B008B',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v12",gid:"g12",viewminimum:0,viewmaximum:0,parent_id:12,
        position: "left", glabel:"consBatteryVoltage", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"consBatteryVoltage",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 13,
        name: 'UV',
        color: '#4B0082',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v13",gid:"g13",viewminimum:0,viewmaximum:0,parent_id:12,
        position: "left", glabel:"UV", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"UV",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 14,
        name: 'dewpoint',
        color: '#9ACD32',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v14",gid:"g14",viewminimum:0,viewmaximum:0,parent_id:12,
        position: "left", glabel:"dewpoint", offset : 0, bullet:"round",linetype:"smoothedLine", unitlable:"",unitname:"dewpoint",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 15,
        name: 'pressure',
        color: '#2F4F4F',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v15",gid:"g15",viewminimum:0,viewmaximum:0,parent_id:15,
        position: "right", glabel:"pressure", offset : 0, bullet:"round",linetype:"line", unitlable:"",unitname:"pressure (Pa)",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 16,
        name: 'altimeter',
        color: '#0c3c0a',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v16",gid:"g16",viewminimum:0,viewmaximum:0,parent_id:15,
        position: "right", glabel:"altimeter", offset : 0, bullet:"round",linetype:"line", unitlable:"",unitname:"altimeter",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 17,
        name: 'inHumidity',
        color: '#FFD700',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v17",gid:"g17",viewminimum:0,viewmaximum:0,parent_id:17,
        position: "right", glabel:"inHumidity", offset : 0, bullet:"round",linetype:"line", unitlable:"",unitname:"inHumidity",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 18,
        name: 'outHumidity',
        color: '#DAA520',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v18",gid:"g18",viewminimum:0,viewmaximum:0,parent_id:17,
        position: "right", glabel:"outHumidity", offset : 0, bullet:"round",linetype:"line", unitlable:"",unitname:"outHumidity",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 19,
        name: 'extraHumid1',
        color: '#ADFF2F',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v19",gid:"g19",viewminimum:0,viewmaximum:0,parent_id:17,
        position: "right", glabel:"extraHumid1", offset : 0, bullet:"round",linetype:"line", unitlable:"",unitname:"extraHumid1",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 20,
        name: 'extraHumid2',
        color: '#CD5C5C',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v20",gid:"g20",viewminimum:0,viewmaximum:0,parent_id:17,
        position: "right", glabel:"extraHumid2", offset : 0, bullet:"round",linetype:"line", unitlable:"",unitname:"extraHumid2",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 21,
        name: 'rxCheckPercent',
        color: '#BC8F8F',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v21",gid:"g21",viewminimum:0,viewmaximum:0,parent_id:17,
        position: "right", glabel:"rxCheckPercent", offset :0, bullet:"round",linetype:"line", unitlable:"",unitname:"rxCheckPercent (%)",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },

    {
        id: 22,
        name: 'radiation',
        color: '#ff0000',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v22",gid:"g22",viewminimum:0,viewmaximum:0,parent_id:22,
        position: "right", glabel:"radiation", offset :0, bullet:"round",linetype:"line", unitlable:"",unitname:"radiation",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 23,
        name: 'txBatteryStatus',
        color: '#FF8C00',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v23",gid:"g23",viewminimum:0,viewmaximum:0,parent_id:23,
        position: "right", glabel:"txBatteryStatus", offset :0, bullet:"round",linetype:"line", unitlable:"",unitname:"BatteryStatus",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 25,
        name: 'rainRate',
        color: '#E9967A',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v25",gid:"g25",viewminimum:0,viewmaximum:0,parent_id:23,
        position: "right", glabel:"rainRate", offset : 0, bullet:"round",linetype:"line", unitlable:"",unitname:"rainRate",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 26,
        name: 'barometer',
        color: '#FF1493',
        min: null,fillAlphas:0,
        max: null,includeHidden:false,
        vid:"v26",gid:"g26",viewminimum:0,viewmaximum:0,parent_id:23,
        position: "right", glabel:"barometer", offset : 0, bullet:"round",linetype:"line", unitlable:"",unitname:"barometer",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
   
    {
        id: 27,
        name: 'rain',
        color: '#8B0000',
        min: null,fillAlphas:0.5,
        max: null,includeHidden:false,
        vid:"v27",gid:"g27",viewminimum:0,viewmaximum:0,parent_id:23,
        position: "right", glabel:"rain", offset : 0, bullet:"round",linetype:"column", unitlable:"",unitname:"Rain",
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    },
    {
        id: 28,
        name: 'ET',
        color: '#6A5ACD',
        min: null,fillAlphas:0.5,
        max: null,includeHidden:false,
        vid:"v28",gid:"g28",viewminimum:0,viewmaximum:0,parent_id:28,
        position: "right", glabel:"ET", offset : 0, bullet:"round",linetype:"column", unitlable:"",unitname:"ET",        
        recordcount: 0, sumofvalue: 0 ,
        avg: 0, isSelected: false
    }
    
    


];   


export const station: any[] = [
    {name:"Newyork",id:1},
    {name:"London",id:2},
];   
