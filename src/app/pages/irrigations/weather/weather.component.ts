import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';

import { Chartparams } from "./chartparams";
import { station } from "./chartparams";
import { AppConfig } from "../../../app.config";
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import * as moment from 'moment';

const MODEL = "weather";

@Component({
  selector: 'az-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class WeatherComponent {

  charParm: any[] = [];
  stationList: any[] = [];
  public selectedStation: string = "";

  chartData: any[];
  valueAxes: any[] = [];
  graphsValue: any[] = [];
  fieldMappings: any[] = [];
  private chart: AmChart;

  chartDatat: any[];
  valueAxest: any[] = [];
  graphsValuet: any[] = [];
  fieldMappingst: any[] = [];
  enableChart2: boolean = false;
 
 private charts: AmChart;

  public fromDateGraph: string = "";
  public toDateGraph: string = "";
  public graph_validation_error: string = "";
  public graphLoading: boolean = false;

  public config: any;
  public configFn: any;
  public self: any;

  loadingResults: boolean = true;
  haveError: boolean = false;





  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private _appConfig: AppConfig,
    public AmCharts: AmChartsService
  ) {

    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
    var self = this;
    //  this.stationList = station;

  }


  ngOnInit() {
    this.getStation();
    this.getGraphData("", "", "");
  }
  getStation() {
    this.API.searchModel('weather/stations', null).subscribe(
      (res) => {
        this.stationList = res.json().data;
      })
  }
  refreshGraph() {


    if (this.fromDateGraph == "") {
      this.graph_validation_error = "*Require  From Date";
      return false;
    }
    if (this.toDateGraph) {
      var dateto = moment(this.toDateGraph);
      var datefrom = moment(this.fromDateGraph);

      if (dateto < datefrom) {
        this.graph_validation_error = "*Please enter valid date range";
        return false;
      }
    }
    this.graph_validation_error = "";

    this.getGraphData(this.fromDateGraph, this.toDateGraph, this.selectedStation);
  }

  getByStation() {
    this.getGraphData(this.fromDateGraph, this.toDateGraph, this.selectedStation);
  }

  getGraphData(fromd, tod, station) {

    let today = new Date();
    var chartData = [];

    if (fromd == "") {
      fromd = moment().subtract(2, "days").format('YYYY-MM-DD');
    }
    if (tod == "") {
      //  tod = moment().format('YYYY-MM-DD');
    }
    this.graphLoading = true;


    this.graph_validation_error = "";

    // Weather Graph setp 1
    this.API.getWeatherGraph('weather/graph', {
      from: fromd,
      to: tod,
      station: station,
    }).subscribe(
      (res) => {
        var chartDatar = res.json().data.reverse();
        this.setChartData(chartDatar);
      },
      (err) => {
        console.log('error', err);
      });
  }
  setChartData(chartDatar) {


    this.valueAxes = [];
    this.graphsValue = [];
    this.chartData = [];
    this.fieldMappings = [];

    this.valueAxest = [];
    this.graphsValuet = [];
    this.chartDatat = [];
    this.fieldMappingst = [];

    var chartData = [];
    for (let i = 0; i < chartDatar.length; i++) {
      var isvalid = 1;

      var idate = moment.unix(chartDatar[i].dateTime).format('YYYY-MM-DD HH:mm:ss');

      if (isvalid) {
        chartData.push({
          date: moment.unix(chartDatar[i].dateTime).toDate(),

          inTemp: (chartDatar[i].inTemp !=null) ? chartDatar[i].inTemp.toFixed(2) : null,
          outTemp: (chartDatar[i].outTemp !=null) ? chartDatar[i].outTemp.toFixed(2) : null,
          extraTemp1: (chartDatar[i].extraTemp1 !=null) ? chartDatar[i].extraTemp1.toFixed(2) : null,
          extraTemp2: (chartDatar[i].extraTemp2 !=null) ? chartDatar[i].extraTemp2.toFixed(2) : null,
          extraTemp3: (chartDatar[i].extraTemp3 !=null) ? chartDatar[i].extraTemp3.toFixed(2) : null,


          windSpeed: (chartDatar[i].windSpeed !=null) ? chartDatar[i].windSpeed.toFixed(2) : null,

          windDir: (chartDatar[i].windDir !=null) ? chartDatar[i].windDir.toFixed(2) : null,
          windGust: (chartDatar[i].windGust !=null) ? chartDatar[i].windGust.toFixed(2) : null,
          windGustDir: (chartDatar[i].windGustDir !=null) ? chartDatar[i].windGustDir.toFixed(2) : null,
          windchill: (chartDatar[i].windchill !=null) ? chartDatar[i].windchill.toFixed(2) : null,


          consBatteryVoltage: (chartDatar[i].consBatteryVoltage !=null) ? chartDatar[i].consBatteryVoltage.toFixed(2) : null,
          txBatteryStatus: (chartDatar[i].txBatteryStatus !=null) ? chartDatar[i].txBatteryStatus.toFixed(2) : null,
         
          rainRate: (chartDatar[i].rainRate !=null) ? chartDatar[i].rainRate.toFixed(2) : null,

          pressure: (chartDatar[i].pressure !=null) ? chartDatar[i].pressure.toFixed(2) : null,
          barometer: (chartDatar[i].barometer !=null) ? chartDatar[i].barometer.toFixed(2) : null,

          inHumidity: (chartDatar[i].inHumidity !=null) ? chartDatar[i].inHumidity.toFixed(2) : null,
          outHumidity: (chartDatar[i].outHumidity !=null) ? chartDatar[i].outHumidity.toFixed(2) : null,
          extraHumid1: (chartDatar[i].extraHumid1 !=null) ? chartDatar[i].extraHumid1.toFixed(2) : null,
          extraHumid2: (chartDatar[i].extraHumid2 !=null) ? chartDatar[i].extraHumid2.toFixed(2) : null,

          UV: (chartDatar[i].UV !=null) ? chartDatar[i].UV.toFixed(2) : null,

          radiation: (chartDatar[i].radiation !=null) ? chartDatar[i].radiation.toFixed(2) : null,

          heatindex: (chartDatar[i].heatindex !=null) ? chartDatar[i].heatindex.toFixed(2) : null,

          altimeter: (chartDatar[i].altimeter !=null) ? chartDatar[i].altimeter.toFixed(2) : null,
          dewpoint: (chartDatar[i].dewpoint !=null) ? chartDatar[i].dewpoint.toFixed(2) : null,

          rxCheckPercent: (chartDatar[i].rxCheckPercent !=null) ? chartDatar[i].rxCheckPercent.toFixed(2) : null,

          ET: (chartDatar[i].eto !=null) ? chartDatar[i].eto.toFixed(2) : null,
          rain: (chartDatar[i].rain !=null) ? chartDatar[i].rain.toFixed(2) : null,


        });
      }
    }

    this.calcMinMax(chartData);

    this.getDefaultAxesGraph();

    this.chartData = chartData;
    this.chartDatat = chartData;

    if (this.chart) {
      this.AmCharts.destroyChart(this.chart);
    }
    this.initGraph();
  }

  calcMinMax(chartData) {
    let alldatalength = 0;
    let etocount = 0;
    let raincount = 0;
    if (chartData.length <= 0) return false;

    for (let i = 0; i < chartData.length; i++) {
      alldatalength++;
      for (let j = 0; j < Chartparams.length; j++) {
        if(i==0){
          Chartparams[j].min = null;
          Chartparams[j].max = null;
          Chartparams[j].sumofvalue = 0;
        }
        if (chartData[i][Chartparams[j].name] != null) {
          let dvalue = parseFloat(chartData[i][Chartparams[j].name]);
          Chartparams[j].sumofvalue = parseFloat(Chartparams[j].sumofvalue) + dvalue;
          if (Chartparams[j].name == "ET") {
            etocount++;
          }
          if (Chartparams[j].name == "rain") {
            raincount++;
          }
          if (Chartparams[j].min > dvalue || Chartparams[j].min == null) {
            Chartparams[j].min = dvalue;
          }
          if (Chartparams[j].max < dvalue ||  Chartparams[j].max == null) {
            Chartparams[j].max = dvalue;
          }
        }
      }
    }

    // Set average of each y axis , and set minimum and maximum view limit of only parent field
    for (let j = 0; j < Chartparams.length; j++) {
      Chartparams[j].recordcount = alldatalength;
      if (Chartparams[j].name == "ET" && etocount > 0) {
        Chartparams[j].avg = (Chartparams[j].sumofvalue / etocount).toFixed(2);
      }else if(Chartparams[j].name == "rain" && raincount > 0){
        Chartparams[j].avg = (Chartparams[j].sumofvalue / raincount).toFixed(2);
      } else {
        Chartparams[j].avg = (Chartparams[j].sumofvalue / alldatalength).toFixed(2);
      }


      /*
            let id = Chartparams.findIndex(x => x.id == Chartparams[j].parent_id);
      
      
            if (id >= 0 && Chartparams[id] && (Chartparams[id].viewminimum > Chartparams[j].min) || Chartparams[id].viewminimum == 0) {
              Chartparams[id].viewminimum = Chartparams[j].min;
            }
            if (id >= 0 && Chartparams[id] && (Chartparams[id].viewmaximum < Chartparams[j].max) || Chartparams[id].viewmaximum == 0) {
              Chartparams[id].viewmaximum = Chartparams[j].max;
      
            }*/
    }

    // Set average of each y axis , and set minimum and maximum view to chiled
    /*   for (let j = 0; j < Chartparams.length; j++) {
         let id = Chartparams.findIndex(x => x.id == Chartparams[j].parent_id);
         Chartparams[j].viewminimum = Chartparams[id].viewminimum;
         Chartparams[j].viewmaximum = Chartparams[id].viewmaximum;
       }*/

    this.charParm = Chartparams;

  }
  getDefaultAxesGraph() {

    for (let j = 0; j < Chartparams.length; j++) {
      this.getvalueAxes(Chartparams[j].vid, Chartparams[j].position, Chartparams[j].unitname, Chartparams[j].unitlable, Chartparams[j].color, Chartparams[j].offset, Chartparams[j].includeHidden, Chartparams[j].viewmaximum, Chartparams[j].viewminimum);
      this.getGraph(Chartparams[j].gid, Chartparams[j].vid, Chartparams[j].glabel, Chartparams[j].name, Chartparams[j].unitlable, Chartparams[j].color, Chartparams[j].bullet, Chartparams[j].linetype, Chartparams[j].isSelected, Chartparams[j].fillAlphas);
    }
  }


  initGraph() {
    var exportData = this.chartData;
    
    this.graphLoading = false;
    this.chart = this.AmCharts.makeChart("chartdiv", {
      "type": "stock",
      "theme": "light",
      "categoryAxesSettings": {
        "minPeriod": "mm",
        maxSeries: 2000,
      },
      "dataSets": [{
        "fieldMappings": this.fieldMappings,
        "dataProvider": this.chartData,
        "categoryField": "date"

      }],

      "panels": [{
        "stockGraphs": this.graphsValue,
        "valueAxes": this.valueAxes,
        plotAreaBorderAlpha: 1,
        marginBottom: 10,
        marginTop: 10,
        plotAreaBorderColor: "#000000",
        "stockLegend": { "valueAlign": "right" }
      }],
      "panelsSettings": {
        "marginLeft": 40, // inside: false requires that to gain some space
        "marginRight": 40,
      },

      "valueAxesSettings": {
        "axisThickness": 2,
        "gridAlpha": 0,
        "axisAlpha": 1,
        "inside": true
      },
      "chartScrollbarSettings": { "position": "top", "color": "#000000" },
      "chartCursorSettings": {
        "valueBalloonsEnabled": true,
        "fullWidth": true,
        categoryBalloonDateFormats: { period: "MM", format: "MMM, YYYY" },
        "valueLineEnabled": false,
        "valueLineBalloonEnabled": false,
        showNextAvailable: false
      },
      "legendSettings": {
        "align": "center",
        "equalWidths": true,
        "position": "bottom",
        "textClickEnabled": true,
        "listeners": [{
          "event": "clickMarker",
          "method": this.handleLegendClick.bind(this)
        }]
      },
      "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
      },
      "periodSelector": {
        "position": "top",
        "dateFormat": "YYYY-MM-DD",
        "inputFieldWidth": 150,
        "inputFieldsEnabled": false,
        "periods": [{
          "period": "hh",
          "count": 1,
          "label": "1 hour"
        },
        {
          "period": "hh",
          "count": 4,
          "label": "4 hours"
        }, {
          "period": "hh",
          "count": 8,
          "label": "8 hours"
        }, {
          "period": "hh",
          "count": 12,
          "label": "12 hours"
        },
        {
          "period": "DD",
          "count": 1,
          "label": "1 day"
        },
        {
          "period": "DD",
          "count": 3,
          "label": "3 days"
        },
        {
          "period": "DD",
          "count": 7,
          "label": "1 week"
        },
        {
          "period": "MM",
          "count": 1,
          "label": "1 month"
        }, {
          "period": "YYYY",
          "count": 1,
          "label": "1 year"
        }, {
          "period": "MAX",
          "label": "MAX",
          selected: true,
        }],
        "listeners": [{
          "event": "changed",
          "method": function (event) {

          }
        }]
      },
     "export": {
        "enabled": true,
        "position": "bottom-right",
        "menu": [{
          "class": "export-main",
          "menu": [{
            "label": "Download as ..",
            "menu": ["PNG", "JPG", "SVG", "PDF"]
          },
          {
            "label": "Save as ..",
            "menu": [

              {
                label: "XLSX",

                click: function () {
                  this.toXLSX({
                    data: exportData
                  }, function (data) {
                    this.download(data, this.defaults.formats.XLSX.mimeType, "amCharts.xlsx");
                  });
                }


              },
              {
                label: "CSV",
                click: function () {
                  this.toCSV({
                    data: exportData
                  }, function (data) {
                    this.download(data, this.defaults.formats.CSV.mimeType, "amCharts.csv");
                  });
                }
              },

              {
                label: "JSON",

                click: function () {
                  this.toJSON({
                    data: exportData
                  }, function (data) {
                    console.log(data);
                    this.download(data, this.defaults.formats.JSON.mimeType, "amCharts.json");
                  });
                }

              },
            ]
          },
          {
            "label": "Annotate",
            "action": "draw"
          },
            "PRINT"
          ]
        }]
      },
      "responsive": {
        "enabled": true
      }
    });



  }
  cloneGraph() {
    this.enableChart2 = true;
    this.charts = this.AmCharts.makeChart("chartdiv2", {
      "type": "stock",
      "theme": "light",
      "categoryAxesSettings": {
        "minPeriod": "mm",
        maxSeries: 2000,
      },
      "dataSets": [{
        "fieldMappings": this.fieldMappingst,
        "dataProvider": this.chartDatat,
        "categoryField": "date"

      }],

      "panels": [{
        "stockGraphs": this.graphsValuet,
        "valueAxes": this.valueAxest,
        plotAreaBorderAlpha: 1,
        marginBottom: 10,
        marginTop: 10,
        plotAreaBorderColor: "#000000",
        "stockLegend": { "valueAlign": "right" }
      }],
      "panelsSettings": {
        "marginLeft": 40, // inside: false requires that to gain some space
        "marginRight": 40,
      },

      "valueAxesSettings": {
        "axisThickness": 1,
        "gridAlpha": 0,
        "axisAlpha": 1,
        "inside": true
      },
      "chartScrollbarSettings": { "position": "top" },
      "chartCursorSettings": {
        "valueLineEnabled": false,
        "valueLineBalloonEnabled": false,
        valueBalloonsEnabled: false,
        showNextAvailable: false
      },
      "legendSettings": {
        "align": "center",
        "equalWidths": true,
        "position": "bottom",
        "textClickEnabled": true,
        "listeners": [{
          "event": "clickMarker",
          "method": this.handleLegendClickchartt.bind(this)
        }]
      },
      "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
      },
      "periodSelector": {
        "position": "top",
        "dateFormat": "YYYY-MM-DD",
        "inputFieldWidth": 150,
        "inputFieldsEnabled": false,
        "periods": [{
          "period": "hh",
          "count": 1,
          "label": "1 hour"
        },
        {
          "period": "hh",
          "count": 4,
          "label": "4 hours"
        }, {
          "period": "hh",
          "count": 8,
          "label": "8 hours"
        }, {
          "period": "hh",
          "count": 12,
          "label": "12 hours"
        },
        {
          "period": "DD",
          "count": 1,
          "label": "1 day"
        },
        {
          "period": "DD",
          "count": 3,
          "label": "3 days"
        },
        {
          "period": "DD",
          "count": 7,
          "label": "1 week"
        },
        {
          "period": "MM",
          "count": 1,
          "label": "1 month"
        }, {
          "period": "YYYY",
          "count": 1,
          "label": "1 year"
        }, {
          "period": "MAX",
          "label": "MAX"
        }]
      },
      "responsive": {
        "enabled": true
      }
    });



  }


  getvalueAxes(id, pos, title, unit, coler, offset, includeHidden, maximum, minimum) {
    let valueax = {
      "id": id,
      "title": title,
      "labelRotation": 0,
      "position": pos,
      "axisColor": coler,
      "titleColor": coler,
      "offset": offset,
      "includeHidden": includeHidden,
      "autoGridCount": false,
      "labelFunction": function (value) {
        if (title == "") {
          return "";
        } else {
          return value;
        }
      }
    };
    this.valueAxes.push(valueax);

    let valueaxt = {
      "id": id + "t",
      "title": title,
      "labelRotation": 0,
      "position": pos,
      "axisColor": coler,
      "titleColor": coler,
      "offset": offset,
      "includeHidden": includeHidden,
      "autoGridCount": false,
      "labelFunction": function (value) {
        if (title == "") {
          return "";
        } else {
          return value;
        }
      }
    };
    this.valueAxest.push(valueaxt);

  }
  getGraph(id, vid, title, valuefield, unit, color, bullet, type, ishidden, fillAlphas) {
    ishidden = !ishidden;

    let gdata = {
      "id": id,
      "hidden": ishidden,
      "valueAxis": vid,
      "lineColor": color,
      "type": type,
      "bullet": bullet,
      "title": title,
      "valueField": vid,
      "fillColors": color,
      "fillAlphas": fillAlphas,
      "gapPeriod": 0,
      "connect": false,
      "bulletSize": 5,
      "hideBulletsCount": 50,
      "lineThickness": 1,
      "clustered": false,
      fixedColumnWidth: 30,
      "columnWidth": 0.5,
      "legendValueText": "[[value]]" + unit,
      "balloonText": "[[category]]<br><b><span style='font-size:14px;'>" + valuefield + " : [[value]] " + unit + "</span></b>",
      "useDataSetColors": false
    };

    let mappingdata = {
      "fromField": valuefield,
      "toField": vid
    };

    this.graphsValue.push(gdata);
    this.fieldMappings.push(mappingdata);

    let gdatat = {
      "id": id + "t",
      "hidden": ishidden,
      "valueAxis": vid + "t",
      "lineColor": color,
      "type": type,
      "bullet": bullet,
      "title": title,
      "valueField": vid + "t",
      "fillColors": color,
      "fillAlphas": fillAlphas,
      "gapPeriod": 0,
      "connect": false,
      "bulletSize": 5,
      "hideBulletsCount": 50,
      "lineThickness": 1,
      "clustered": false,
      fixedColumnWidth: 30,
      "columnWidth": 0.5,
      "legendValueText": "[[value]] " + unit,
      "balloonText": "[[category]]<br><b><span style='font-size:14px;'>" + valuefield + " : [[value]] " + unit + "</span></b>",
      "useDataSetColors": false
    };

    let mappingdatat = {
      "fromField": valuefield,
      "toField": vid + "t"
    };

    this.graphsValuet.push(gdatat);
    this.fieldMappingst.push(mappingdatat);
  }

  cleareGraph() {
    if (!this.chart) {
      return false;
    }

    this.fromDateGraph = "";
    this.toDateGraph = "";
    this.valueAxes = [];
    this.graphsValue = [];

    this.valueAxest = [];
    this.graphsValuet = [];

    for (let j = 0; j < Chartparams.length; j++) {
      Chartparams[j].isSelected = false;
    }

    this.getDefaultAxesGraph();
    if (this.chart) {
      this.AmCharts.destroyChart(this.chart);
    }
    this.initGraph();

    if (this.charts) {
      //this.enableChart2 = false;
      this.AmCharts.destroyChart(this.charts);
      this.cloneGraph();
    }

    /* for( var i = 0; i < this.chart.graphs.length; i++ ) {
       if(this.selected_left_legend = this.chart.graphs[i].valueField || this.selected_right_legend == this.chart.graphs[i].valueField){
         this.chart.hideGraph(this.chart.graphs[i]);
       }
     }*/

  }

  handleLegendClick(graph) {

    var chart = graph.chart;
    var pos = graph.dataItem.valueAxis.position;
    var field = graph.dataItem.title;

    
    let id = this.charParm.findIndex(x => x.name == field);
    if (graph.dataItem.hidden) {
      this.charParm[id].isSelected = false;
    } else {
      this.charParm[id].isSelected = true;

      for (let j = 0; j < this.charParm.length; j++) {
        let tgraph = chart.getGraphById(this.charParm[j].gid);
        if(id != j && pos==tgraph.valueAxis.position){
          if(!tgraph.hidden){
            chart.hideGraph(tgraph);
            this.charParm[j].isSelected = false;
          }
        }
        if(id!=j){
          let nextpos = "right";
          if(pos == "right") { nextpos = "left"; }
          tgraph.valueAxis.position = nextpos;
        }
      }
    }
    
    /* for (let j = 0; j < this.charParm.length; j++) {
      if(this.charParm[id].isSelected && this.charParm[j].isSelected && id != j && pos==this.charParm[j].position){
        chart.hideGraph(chart.getGraphById(this.charParm[j].gid));
        this.charParm[j].isSelected = false;
      }
    }*/
  }
  

  handleLegendClickchartt(graph) {

    var chart = graph.chart;
    var pos = graph.dataItem.valueAxis.position;
    var field = graph.dataItem.title;

    let id = this.charParm.findIndex(x => x.name == field);
    if (!graph.dataItem.hidden) {
      for (let j = 0; j < this.charParm.length; j++) {
        let tgraph = chart.getGraphById(this.charParm[j].gid+"t");
        if(id != j && pos==tgraph.valueAxis.position){
            if(!tgraph.hidden){
              chart.hideGraph(tgraph);
            }
        }
        if(id!=j){
          let nextpos = "right";
          if(pos == "right") { nextpos = "left"; }
          tgraph.valueAxis.position = nextpos;
        }
      }
    } 
  }
  
  
  /*
  zoomgraph(zoomlevel){
    
    
    if(zoomlevel==1){
    	this.chart.zoomToIndexes(this.chart.dataProvider.length - 40, this.chart.dataProvider.length - 1);
      
		}else if(zoomlevel==3){
			this.chart.zoomToIndexes(this.chart.dataProvider.length - 80, this.chart.dataProvider.length - 1);
		}
		else if(zoomlevel==6){
			this.chart.zoomToIndexes(this.chart.dataProvider.length - 200, this.chart.dataProvider.length - 1);
		}
		else if(zoomlevel==12){
			this.chart.zoomToIndexes(this.chart.dataProvider.length - 400, this.chart.dataProvider.length - 1);
		}
		else {
			this.chart.zoomToIndexes(0, this.chart.dataProvider.length - 1);
    }
  }*/
  /*
    refreshGraphBySlote(range){
  
    
      if(this.fromDateGraph == ""){
        if(range=='week'){
          this.fromDateGraph = moment().subtract(1, 'week').format("YYYY-MM-DD");
          this.toDateGraph = moment().format('YYYY-MM-DD');
        }
        if(range=='month'){
          this.fromDateGraph = moment().subtract(1, 'month').format("YYYY-MM-DD");
          this.toDateGraph = moment().format('YYYY-MM-DD');
        }
        if(range=='today'){
          this.fromDateGraph = moment().format('YYYY-MM-DD');
          this.toDateGraph = moment().format('YYYY-MM-DD');
        }
      }else{
        if(range=='week'){
          this.toDateGraph = moment(this.fromDateGraph).add(1, 'week').format("YYYY-MM-DD");
        }
        if(range=='month'){
          this.toDateGraph = moment(this.fromDateGraph).add(1, 'month').format("YYYY-MM-DD");
        }
        if(range=='day'){
          this.toDateGraph = this.fromDateGraph;
        }
      }
      this.getGraphData(this.fromDateGraph,this.toDateGraph);
    }*/


}