import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammingIrrigationsFormComponent } from './programming-irrigations-form.component';

describe('ProgrammingIrrigationsFormComponent', () => {
  let component: ProgrammingIrrigationsFormComponent;
  let fixture: ComponentFixture<ProgrammingIrrigationsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgrammingIrrigationsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammingIrrigationsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
