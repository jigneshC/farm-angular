import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {TranslateService} from '../../../../services/translate.service';
import {Sector} from '../../../../models/sector';
import * as moment from 'moment';
import {CustomValidators} from 'ng2-validation';
import {SectorService} from '../../../../services/api/sector/sector.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {IrrigationService} from '../../../../services/api/irrigation/irrigation';
import {ProgrammingIrrigationService} from '../../../../services/api/irrigation/programming-irrigation';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'az-programming-irrigations-form',
  templateUrl: './programming-irrigations-form.component.html',
  styleUrls: ['./programming-irrigations-form.component.scss']
})
export class ProgrammingIrrigationsFormComponent implements OnInit {

  @Output() afterSave = new EventEmitter<string>(true);

  public title: string = 'Programación rápida de riego';
  public errorMessages = [];
  public isLoading = false;
  form: FormGroup;
  sectors: any;
  sectorsCtrl: FormControl = new FormControl();
  deviceCtrl: FormControl = new FormControl('');
  devicesSub;
  filteredSectors: Observable<any[]>;
  devices: any = [];
  isStop: boolean = false;

  constructor(
    protected programmingIrrigationService: ProgrammingIrrigationService,
    protected irrigationService: IrrigationService,
    protected sectorService: SectorService,
    protected dialogRef: MatDialogRef<ProgrammingIrrigationsFormComponent>,
    protected formBuilder: FormBuilder,
    protected toaster: ToasterService,
    public translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data === 'stop') {
      this.isStop = true;
    }
    this.sectorService.findAllProgrammable()
      .subscribe(sectors => {
        this.sectors = sectors;
        this.initAutoCompletes();
      });
    this.createForm();
  }

  searchDevices(query?: string) {
    this.irrigationService.getDevices()
      .subscribe(devices => {
        this.devices = devices.data;
      });
  }

  initAutoCompletes() {
    this.sectorsCtrl = new FormControl('');
    this.filteredSectors = this.sectorsCtrl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filterSectors(val))
      );
  }

  filterSectors(val: string): any[] {
    return this.sectors.filter(sector =>
      sector.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  selectSector(sector: any) {
    this.form.patchValue(sector);
  }

  selectDevice(device: any) {
    this.form.get('device').patchValue(device.id);
  }

  createForm() {
    this.form = this.formBuilder.group({
      ids: [ null, [ Validators.required, CustomValidators.number ] ],
      mask: [ null, [ CustomValidators.number, CustomValidators.range([0, 99999.99]) ] ],
      device: [ null, [ CustomValidators.number, CustomValidators.range([0, 9999999]) ] ],
    });
  }

  save(form: any) {
    this.isLoading = true;

    if (!this.isStop) {
      this.programmingIrrigationService.start(form.mask, form.device)
        .subscribe(() => {
          this.isLoading = false;
          this.afterSave.emit('success');
          this.dialogRef.afterClosed().subscribe(() => {
            this.toaster.pop('success', 'Programación de Riego', `Programación de Riego iniciada exitosamente.`);
          });
          this.dialogRef.close();
        }, error => {
          this.isLoading = false;
          this.errorMessages.push(`Ha ocurrido un error al intentar iniciar la Programación de Riego.`);
        });
    } else {
      this.programmingIrrigationService.stop(form.device)
        .subscribe(() => {
          this.isLoading = false;
          this.afterSave.emit('success');
          this.dialogRef.afterClosed().subscribe(() => {
            this.toaster.pop('success', 'Programación de Riego', `Programación de Riego detenida exitosamente.`);
          });
          this.dialogRef.close();
        }, error => {
          this.isLoading = false;
          this.errorMessages.push(`Ha ocurrido un error al intentar detener la Programación de Riego.`);
        });
    }
  }

  close(): void {
    this.dialogRef.close();
  }

}
