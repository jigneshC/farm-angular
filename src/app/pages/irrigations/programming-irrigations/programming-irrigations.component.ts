import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';

import DateGMT from 'app/classes/date-gmt';
import {IrrigationBasicFormComponent} from '../irrigations/irrigation-basic-form/irrigation-basic-form.component';
import {Subscription} from 'rxjs/Subscription';
import {ProgrammingIrrigationsFormComponent} from './programming-irrigations-form/programming-irrigations-form.component';

const MODEL = "programmingirrigations";

@Component({
  selector: 'az-programming-irrigations',
  templateUrl: './programming-irrigations.component.html',
  styleUrls: ['./programming-irrigations.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class ProgrammingIrrigationsComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['sector_id', 'start', 'end', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  sectorCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  errorMessages;
  sectors;
  sectorSub;
  filter: string;
  timeout: any;

  protected formResultSubscription: Subscription;

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    protected  dialog: MatDialog
  ) {
    this.createForm();
  }

  createForm() {
    this.errorMessages = undefined;
    this.sectorCtrl = new FormControl(this.current.sectors ? this.current.sectors[0].name : '');
    this.sectorSub = this.sectorCtrl.valueChanges.startWith(this.current.sectors ? this.current.sectors[0].name : '')
      .subscribe(q => this.searchAutoComplete(q, 'sectors', 'sectors/programmable'));

    this.form = this.fb.group({
      start: [ this.current.start || '', [ Validators.required, CustomValidators.date ] ],
      end: [ this.current.end || '', [ Validators.required, CustomValidators.date ] ],
      sectors: [ this.current.sectors || '', [ Validators.required ] ]
    }, { validator: this.dateLessThan('start', 'end') });
  }

  dateLessThan(from: string, to: string) {
    return ( group: FormGroup ): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];
      if ( new Date(t.value) < new Date(f.value) ) {
        return {
          end: true
        };
      }
      return {};
    }
  }

  stop() {
    const data = 'stop';
    const form = this.dialog.open(ProgrammingIrrigationsFormComponent, {
      width: '800px',
      data: data
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.search();
      }
    })
  }

  start() {
    const form = this.dialog.open(ProgrammingIrrigationsFormComponent, {
      width: '800px'
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.search();
      }
    })
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    this.current = row;
    row.start = DateGMT(row.start);
    row.end = DateGMT(row.end);
    this.createForm();
  }

  reload() {
    this.source.data = [];
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( model == "sectors/programmable" && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model == "sectors/programmable" && this.current ) this.current.requesting = false;
      if ( model === "sectors/programmable" ) {
        this[variable] = data;
      } else {
        this[variable] = data.data;
      }
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  selectSector(sector) {
    this.form.get('sectors').patchValue(sector.ids);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    form.start = this.dateTransform.transform(form.start, "y-MM-dd HH:mm:ss");
    form.end = this.dateTransform.transform(form.end, "y-MM-dd HH:mm:ss");

    this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Programación de riego', `La programación de riego ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						jQuery('#form-modal').modal('toggle');
          }, err => {
						this.toaster.pop('error', 'Programación de riego', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } la programación de riego`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Programación de riego', 'La programación de riego ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Programación de riego', 'Ha ocurrido un error al eliminar la programación de riego');
          })
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
