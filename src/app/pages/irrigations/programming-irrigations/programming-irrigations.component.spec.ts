import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammingProgrammingIrrigationsComponent } from './programming-irrigations.component';

describe('ProgrammingProgrammingIrrigationsComponent', () => {
  let component: ProgrammingIrrigationsComponent;
  let fixture: ComponentFixture<ProgrammingIrrigationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgrammingIrrigationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammingIrrigationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
