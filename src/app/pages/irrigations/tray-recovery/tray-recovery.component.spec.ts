import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrayRecoveryComponent } from './tray-recovery.component';

describe('TrayRecoveryComponent', () => {
  let component: TrayRecoveryComponent;
  let fixture: ComponentFixture<TrayRecoveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrayRecoveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrayRecoveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
