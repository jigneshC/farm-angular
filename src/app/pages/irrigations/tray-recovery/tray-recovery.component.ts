import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';
import { TimeCalculatorService } from "../../../services/time-calculator.service";

import DateGMT from 'app/classes/date-gmt';

const MODEL = "etoperiod";

@Component({
  selector: 'az-tray-recovery',
  templateUrl: './tray-recovery.component.html',
  styleUrls: ['./tray-recovery.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class TrayRecoveryComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fileInput') fileInput;
  
  columns = ['sector', 'hours', 'mm', 'trayrecovery'];
  source = new MatTableDataSource();
  form: FormGroup;
  filtersForm: FormGroup;
  etoperiod: number = 0;
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public timeService: TimeCalculatorService,
  ) {
    this.createFiltersForm();
  }
  
  createFiltersForm() {
    let date = new Date(),
        from = new Date( date.getFullYear(), date.getMonth(), 1 );
        
    this.filtersForm = this.fb.group({
      from: from,
      to: date
    });
  }
  
  reload() {
    this.index();
  }
  
  toggleFilters() {
    jQuery("#filters").slideToggle();
  }
  
  buildFilters(form) {
    this.reload();
  }
  
  index() {
    this.loadingResults = true;
    let params = new HttpParams();
    
    if ( this.filtersForm.value.from ) params = params.append('from', `${this.dateTransform.transform(this.filtersForm.value.from, "y-MM-dd")}`);
    if ( this.filtersForm.value.to ) params = params.append('to', `${this.dateTransform.transform(this.filtersForm.value.to, "y-MM-dd")}`);
    
    this.API.index(MODEL, params)
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.data.sectors.length;
          
          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => {
          this.source.data = data.sectors;
          this.etoperiod = data.eto_period;
        });
  }
  
  ngAfterViewInit() {
    this.index();
  }
}