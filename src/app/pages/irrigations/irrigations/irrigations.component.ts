import { Component, AfterViewInit, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Router } from '@angular/router';

import * as moment from 'moment';
import { TimeCalculatorService } from "../../../services/time-calculator.service";
import { TimeKeyInputService } from "../../../services/time-key-input.service";

import DateGMT from 'app/classes/date-gmt';
import {TechnicalReferenceFormComponent} from '../../applications/technical-reference/technical-reference-form/technical-reference-form.component';
import {Subscription} from 'rxjs/Subscription';
import {IrrigationBasicFormComponent} from './irrigation-basic-form/irrigation-basic-form.component';

const MODEL = "irrigations";

@Component({
  selector: 'az-irrigations',
  templateUrl: './irrigations.component.html',
  styleUrls: ['./irrigations.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IrrigationsComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['date', 'sector_id', 'start', 'end', 'time', 'milimeters', 'timeFromLast', 'evo', 'notes', 'actions'];
  columnsTrayRecovery = ['sector', 'date', 'percentage'];
  source = new MatTableDataSource();
  trayRecovery = new MatTableDataSource([]);
  form: FormGroup;
  filtersForm: FormGroup;
  simpleFiltersForm: FormGroup;
  isASimpleFiltering: boolean = true;
  isAnAdvancedFiltering: boolean = false;
  errors = { sinceDate: "", untilDate: "" };
  sectorCtrl: FormControl = new FormControl("");
  filterSectorCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  loadingResultsTray: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  eto: number;
  etoMin: number;
  etoMax: number;
  etoAvg: number;
  rain: number;
  selectedSectors: Array<any> = [];
	selectedSectorsAll = {};
  errorMessages;
  sectors;
  formSectors: any = [];
  sectorSub;
  filterSectors;
  filterSectorsSub;

  lastYears: Array<any> = [];

  protected formResultSubscription: Subscription;

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public timeService: TimeCalculatorService,
    private router: Router,
    public appConfig: AppConfig,
    protected  dialog: MatDialog,
    public timeKeyService: TimeKeyInputService
  ) {
    this.createForm();
    this.createSimpleFiltersForm();
    this.createFiltersForm();
    this.setLastYears();
  }

  minLength(length: number) {
    return (control: FormControl) => {
      return control.value.length >= length ? null : {
        minLength: true
      };
    }
  }

  createForm() {
    this.errorMessages = undefined;
    this.sectorCtrl = new FormControl(this.current.sector ? this.current.sector.name : "");
    this.sectorSub = this.sectorCtrl.valueChanges.startWith(this.current.sector ? this.current.sector.name : "").subscribe(q => this.searchAutoComplete(q, 'sectors', 'sectors'));

    this.form = this.fb.group({
      start: [ (this.current.start) ? moment(this.current.start).format("HH:mm") : '', [ Validators.required ] ],
      end: [ (this.current.end) ? moment(this.current.end).format("HH:mm") : '', [ Validators.required ] ],
      notes: [ this.current.notes || ''],
      chlorination: [ this.current.chlorination || false ],
      tail: [ this.current.tail || false ],
      acidification: [ this.current.acidification || false ],
      date: [ this.current.date || '', [ Validators.required, CustomValidators.date ] ],
      sector_id: [ this.current.sector_id || '', [ Validators.required, CustomValidators.number ] ]
    });
  }

  setLastYears( date? ) {
    let start = new Date(date || "2010-01-02");
    let end = new Date();
    let years = moment(end).diff(start, 'years');
    for( let year = 0; year < years +1; year++ )
      this.lastYears.push(start.getFullYear() + year);

    this.createFiltersForm();
  }

  createFiltersForm() {
    this.filterSectorCtrl = new FormControl("");
    this.filterSectorsSub = this.filterSectorCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'filterSectors', 'sectors'));

    this.filtersForm = this.fb.group({
      sector_id: '',
      sinceDate: '',
      untilDate: ''
    });

    this.subscribeFiltersForm();
  }

  buildSimpleFilters(form) {
    if (this.isFilterVisible()) {
      this.toggleFilters();
    }
    this.isASimpleFiltering = true;
    this.isAnAdvancedFiltering = false;
    this.reload();
  }

  createSimpleFiltersForm() {
    this.simpleFiltersForm = this.fb.group({
      month: new Date().getMonth(),
      year: this.lastYears[this.lastYears.length - 1] || new Date().getFullYear()
    });
  }

  subscribeFiltersForm() {
    this.filtersForm.valueChanges.subscribe(form => {
      if ( form.sinceDate && form.untilDate && ( form.sinceDate > form.untilDate ) ) {
        this.errors.sinceDate = "Selecciona una fecha anterior a la final";
        this.errors.untilDate = "Selecciona una fecha mayor a la inicial";
      } else {
        delete this.errors.sinceDate;
        delete this.errors.untilDate;
      }
    });
  }

  createFast() {
    const form = this.dialog.open(IrrigationBasicFormComponent, {
      width: '800px'
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.search();
      }
    })
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    this.current = row;
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.trayRecovery.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( model == "sectors" && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model == "sectors" && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form) {
    this[form].controls[model+"_id"].setValue(row.id);
  }


  selectMultipleRow( row, model, form) {
    if ( !this.selectedSectorsAll[row.id] ) {
      this.selectedSectorsAll[row.id] = true;
      let sector = {
        name: row.name,
        id: row.id
      };
      this.formSectors = Object.keys(this.selectedSectorsAll).toString();
      this.selectedSectors.push(row);
    }
    this.filterSectorCtrl.setValue("");
  }

  removeSector( id, index ) {
    delete this.selectedSectorsAll[''+id];
    this.selectedSectors.splice(index, 1);
    this.formSectors = Object.keys(this.selectedSectorsAll).toString();
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    form.date = this.dateTransform.transform(form.date, 'y-MM-dd');
    form.start = moment(form.date + ' ' + form.start).format('YYYY-MM-DD HH:mm:ss');
    form.end = moment(form.date + ' ' + form.end).format('YYYY-MM-DD HH:mm:ss');

    if (form.end < form.start) {
      form.end = moment(form.end).add(1, 'd').format('YYYY-MM-DD HH:mm:ss');
    }

    this.API[method](MODEL, form, this.current.id)
      .subscribe(
        res => {
          this.toaster.pop('success', 'Riego', `El riego ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
          this.current.requesting = false;
          if ( !this.current.id ) this.paginator.pageIndex = 0;
          this.paginator.page.emit();
          jQuery('#form-modal').modal('toggle');
        }, err => {
          this.toaster.pop('error', 'Riego', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el riego`);
          this.current.requesting = false;
          this.errorMessages = err.error.errors;
          for ( let name in this.errorMessages ) {
            this.form.controls[name].setValue('');
          }
        })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
      .subscribe(
        res => {
          if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
          this.paginator.page.emit();
          this.toaster.pop('success', 'Riego', 'El riego ha sido eliminado exitosamente');
          this.current = {};
        }, err => {
          this.toaster.pop('error', 'Riego', 'Ha ocurrido un error al eliminar el riego');
        })
  }

  toggleFilters() {
    jQuery('#filters').slideToggle(400, () => {
      if (this.isFilterVisible()) {
        this.isAnAdvancedFiltering = true;
        this.isASimpleFiltering = false;
      } else {
        this.isAnAdvancedFiltering = false;
        this.isASimpleFiltering = true;
      }
    });
  }

  resetFilters() {
    this.createFiltersForm();
    this.createSimpleFiltersForm();
    this.reload();
  }

  filterReport(output: string) {
    let query = new URLSearchParams();

    if (this.formSectors && this.formSectors.length > 0) {
      query.append('sectors', this.formSectors)
    }
    if (this.filtersForm.value.sinceDate) {
      query.append('from', this.dateTransform.transform(this.filtersForm.value.sinceDate, "y-MM-dd"))
    }
    if (this.filtersForm.value.untilDate) {
      query.append('to', this.dateTransform.transform(this.filtersForm.value.untilDate, "y-MM-dd"))
    }

    query.append('output', output);
    return query;
  }

  generateExportLink(output: string) {
    let query = this.filterReport(output);
    var url = `${this.appConfig.config.API_URL}irrigations/report?${query.toString()}`;
    window.open(url, '_blank');
  }

  buildFilters(form) {
    this.isAnAdvancedFiltering = true;
    this.isASimpleFiltering = false;
    if ( this.errors.sinceDate || this.errors.untilDate ) return;
    this.reload();
  }

  getEtoData(params = null) {
    let today = new Date();
    this.API.specificAction('days','eto', params).subscribe( res => this.eto = res.json().eto );
    this.API.specificAction('days','etomax', params).subscribe( res => this.etoMax = res.json().max );
    this.API.specificAction('days','etomin', params).subscribe( res => this.etoMin = res.json().min );
    this.API.specificAction('days','etoavg', params).subscribe( res => this.etoAvg = res.json().avg );

    let myRainParams = {
      ...params,
      start : moment(today).subtract(30).format('YYYY-MM-DD'),
      end: moment(today).format('YYYY-MM-DD')
    }
    this.API.getRain('days', myRainParams).subscribe( res => this.rain = res.json().rain )
  }

  goToTrayRecovery(){
    this.router.navigate(["/pages/suelo-clima/reposicion-bandeja"])
  }

  ngOnInit() {
  }

  isFilterVisible(): boolean {
    return jQuery('#filters').is(':visible');
  }

  getApplicationLink(row){
    return `${this.appConfig.config.API_URL}applications/download/${row.application.id}`;
  }

  formatHour(evt, type = "start"){
    let deleteCode = 8;
    let backspaceCode = 46;
    let input = evt.target.value;

    let cond = false;
    var charCode = evt.keyCode;
    if (charCode == 8 || charCode == 46 || (charCode >= 96 && charCode <= 105)) cond = true;
    if (!cond){
      if ((evt.shiftKey || (charCode < 48 || charCode > 57)) && (charCode < 96 || charCode > 105)) {
        this.form.controls[type].setValue(input.slice(0, (input.length - 1)));
        return false;
      }
    }

     this.form.controls[type].setValue(input.replace( /\[\d+\]/g, ''));
      if (input && input.length > 5) { this.form.controls[type].setValue(input.slice(0, (input.length - 1))); return false; }
      if ((input && (input.length > 0 && input.length < 6))){
        let date = input.trim();
        if( charCode != 8 && charCode != 46){
          if (date.length === 2 && date.indexOf(':') === -1){
            if (parseInt(date) > 24) date = 24;
            this.form.controls[type].setValue(date+':');

          } else if (date.length === 4){
            let splitted = date.split(':');
            if (parseInt(splitted[1]) >= 6) this.form.controls[type].setValue(splitted[0]+':'+6);
            if (parseInt(splitted[1]) < 5) this.form.controls[type].setValue(splitted[0]+':'+splitted[1]);
          }
        } else if (charCode === 8 || charCode === 46){
          if (date.length > 2 && date.indexOf(':') === -1){
            this.form.controls[type].setValue(date.substr(0, 2)+':'+date.substr(2));
          } else if (date.length < 3 && date.indexOf(':') != -1){
            this.form.controls[type].setValue(date.replace(/:/g, ''));
          }
        }
      }
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );
    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        let params = new HttpParams();

        if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
        if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
        if ( this.filter ) params = params.append('q', this.filter);
        if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
        if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

        if(this.isASimpleFiltering){
          let date = new Date( this.simpleFiltersForm.value.year, this.simpleFiltersForm.value.month ),
            start = new Date(date.getFullYear(), date.getMonth(), 1),
            end = new Date(date.getFullYear(), date.getMonth() + 1, 0);

          params = params.append('from', this.dateTransform.transform(start, "y-MM-dd"));
          params = params.append('to', this.dateTransform.transform(end, "y-MM-dd"));

          this.getEtoData({
            month: parseInt(this.simpleFiltersForm.value.month) + 1,
            year: this.simpleFiltersForm.value.year
          });

        } else {
          if ( this.formSectors.length > 0 ) params = params.append('sectors', this.formSectors);
          if ( this.filtersForm.value.sinceDate ) params = params.append('from', this.dateTransform.transform(this.filtersForm.value.sinceDate, "y-MM-dd"));
          if ( this.filtersForm.value.untilDate ) params = params.append('to', this.dateTransform.transform(this.filtersForm.value.untilDate, "y-MM-dd"));

        }

        return this.API.index(MODEL, params);
      })
      .map(data => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;
        this.trayRecovery.data = (data.tray_recovery && data.tray_recovery.length) ? data.tray_recovery : [];

        return data.data;
      })
      .catch( err => {
        this.loadingResults = false;
        this.haveError = true;
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data );
  }
}
