import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {SectorService} from '../../../../services/api/sector/sector.service';
import {IrrigationService} from '../../../../services/api/irrigation/irrigation';
import {Observable} from 'rxjs/Observable';
import {Sector} from '../../../../models/sector';
import {map} from 'rxjs/operators/map';
import {startWith} from 'rxjs/operators/startWith';
import {TranslateService} from '../../../../services/translate.service';
import {ToasterService} from 'angular2-toaster';

import * as moment from 'moment';

@Component({
  selector: 'az-irrigation-basic-form',
  templateUrl: './irrigation-basic-form.component.html',
  styleUrls: ['./irrigation-basic-form.component.scss']
})
export class IrrigationBasicFormComponent implements OnInit {

  @Output() afterSave = new EventEmitter<string>(true);

  public title: string = 'Programación rápida de riego';
  public errorMessages = [];
  public isLoading = false;
  form: FormGroup;
  sectors: any;
  sectorsCtrl: FormControl = new FormControl();
  filteredSectors: Observable<Sector[]>;

  constructor(
    protected irrigationService: IrrigationService,
    protected sectorService: SectorService,
    protected dialogRef: MatDialogRef<IrrigationBasicFormComponent>,
    protected formBuilder: FormBuilder,
    protected toaster: ToasterService,
    public translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.sectorService.findAll()
      .subscribe(sectors => {
        this.sectors = sectors;
        this.initAutoCompletes();
    });
    this.createForm();
  }

  initAutoCompletes() {
    this.sectorsCtrl = new FormControl('');
    this.filteredSectors = this.sectorsCtrl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filterSectors(val))
      );
  }

  filterSectors(val: string): Sector[] {
    return this.sectors.filter(sector =>
      sector.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  selectSector(id: number) {
    this.form.get('sector_id').patchValue(id);
  }

  createForm() {
    this.form = this.formBuilder.group({
      start: [moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'), [ CustomValidators.date ] ],
      end: [ '', [ Validators.required, CustomValidators.date ] ],
      notes: [ '' ],
      chlorination: [ false ],
      tail: [ false ],
      acidification: [ false ],
      date: [ moment(Date.now()).format('YYYY-MM-DD'), [ CustomValidators.date ] ],
      sector_id: [ '', [ Validators.required, CustomValidators.number ] ]
    });
  }

  save(form: any) {
    this.isLoading = true;
    form.start = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
    form.end = moment(this.form.get('end').value).format('YYYY-MM-DD HH:mm:ss');

    this.irrigationService.create(form)
      .subscribe(() => {
        this.isLoading = false;
        this.afterSave.emit('success');
        this.dialogRef.afterClosed().subscribe(() => {
          this.toaster.pop('success', 'Programación de Riego', 'Programación de Riego creada exitosamente.');
        });
        this.dialogRef.close();
      }, error => {
        this.isLoading = false;
        this.errorMessages.push('Ha ocurrido un error al intentar crear la Programación de Riego.');
      });
  }

  close(): void {
    this.dialogRef.close();
  }

}
