import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IrrigationBasicFormComponent } from './irrigation-basic-form.component';

describe('IrrigationBasicFormComponent', () => {
  let component: IrrigationBasicFormComponent;
  let fixture: ComponentFixture<IrrigationBasicFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IrrigationBasicFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IrrigationBasicFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
