import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';
import { TimeCalculatorService } from "../../../services/time-calculator.service";

import DateGMT from 'app/classes/date-gmt';

const MODEL = "days";

@Component({
  selector: 'az-days',
  templateUrl: './days.component.html',
  styleUrls: ['./days.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class DaysComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['date', 'evotranspiration', 'rain', 'tray_recovery', 'notes', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  filtersForm: FormGroup;
  sectorCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
	sectors: Array<any> = [];
	trayRecovery: Array<any> = [];
	lastYears: Array<any> = [];
	eto: number = 0;
	etoMin: number = 0;
	etoMax: number = 0;
	etoAvg: number = 0;
	rain: number = 0;
  errorMessages;

  isAdvanceFilter: boolean = false;
  advanceFilter: any;

  // Immutable
  public readonly ROLES = Object.freeze({
    SAVE: 'suelo-clima:dias:save',
    UPDATE: 'suelo-clima:dias:update',
    REMOVE: 'suelo-clima:dias:remove'
  });

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    public appConfig: AppConfig,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public timeService: TimeCalculatorService,
  ) {
    this.createForm();
    this.getSectors();
    this.createFiltersForm();
		this.getLastYears();
  }

  createForm() {
    this.errorMessages = undefined;

    this.form = this.fb.group({
      date: [ this.current.date || '', [ Validators.required, CustomValidators.date ] ],
      evotranspiration: [ this.current.evotranspiration || '', [ Validators.required, CustomValidators.number, CustomValidators.max(10) ] ],
      rain: [ this.current.rain || '', [ Validators.required, CustomValidators.number ] ],
      notes: [ this.current.notes || '', [ ] ],
      tray_recovery: [ this.current.tray_recovery || [], [ ] ],
    });
  }

	getSectors() {
		let params = new HttpParams();
		params = params.append('p', `-1`);

    this.API.index('sectors', params)
        .subscribe(
          res => {
            this.sectors = res.data;
            for ( let sector of this.sectors ) {
              let trayRecovery = {
                name: sector.name,
                sector_id: sector.id,
                tray_recovery_percentage: '',
              };
              this.trayRecovery.push(trayRecovery);
            }
          }
        )
	}

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      month: new Date().getMonth(),
      year: new Date().getFullYear()
    });
  }

	getLastYears() {
		let params = new HttpParams();
		params = params.append('p', `1`);
		params = params.append('sort_dir', `asc`);

		this.API.index(MODEL, params)
        .subscribe(
          res => {
						let first = res.data[0];
						if ( first ) {
							this.setLastYears(first.date)
						} else this.setLastYears();
          }, err => {
						this.setLastYears();
          })
	}

	setLastYears( date? ) {
		let start = moment(new Date(date || "2010-01-02"));
		let end = moment();
		do {
            this.lastYears.push(start.year());
            start.add(1, 'y');
        } while (start.year() <= end.year());
		this.createFiltersForm();
		this.loadEto()
	}

	loadEto() {
    let params = new HttpParams();

		let today = new Date();

    if ( this.filtersForm.value.month && this.filtersForm.value.year ) {
      let date = new Date( this.filtersForm.value.year, this.filtersForm.value.month ),
          start = new Date(date.getFullYear(), date.getMonth(), 1),
          end = new Date(date.getFullYear(), date.getMonth() + 1, 0);

      params = params.append('start', this.dateTransform.transform(start, "y-MM-dd"));
      params = params.append('end', this.dateTransform.transform(end, "y-MM-dd"));
      params = params.append('month', (parseInt(this.filtersForm.value.month) + 1) + '');
      params = params.append('year', this.filtersForm.value.year + '');
    }

		this.API.custom('get', 'api/days/eto', { params: params })
				.subscribe(
					res => this.eto = res.eto
				);

		this.API.custom('get', 'api/days/etomax', { params: params })
				.subscribe(
					res => this.etoMax = res.max
				);

		this.API.custom('get', 'api/days/etomin', { params: params })
				.subscribe(
					res => this.etoMin = res.min
				);

		this.API.custom('get', 'api/days/etoavg', { params: params })
				.subscribe(
					res => this.etoAvg = res.avg
				);

		this.API.custom('get', 'api/days/rain', { params: params })
				.subscribe(
					res => this.rain = res.rain
				);
	}

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    this.current = row;
    this.current.date = DateGMT(row.date);
    for ( let trayRecovery of this.trayRecovery ) {
      if ( this.current.sectors ) {
        for ( let sector of this.current.sectors ) {
          if ( sector.id == trayRecovery.sector_id ) {
            trayRecovery.tray_recovery_percentage = sector.pivot.tray_recovery_percentage * 100 + '';
            break;
          }
        }
      }
    }
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchWithAdvanceFilter(filters: any) {
    this.isAdvanceFilter = true;
    this.advanceFilter = filters;
    this.search();
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    form.date = this.dateTransform.transform(form.date, "y-MM-dd");

		for ( let trayRecovery of this.trayRecovery ) {
			if ( trayRecovery.tray_recovery_percentage ) {
				let temp = Object.assign({}, trayRecovery);
				delete temp.name;
				form.tray_recovery.push(temp);
			}
		}

		if ( !form.notes ) delete form.notes;

    this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Día', `El día ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						jQuery('#form-modal').modal('toggle');
          }, err => {
						this.toaster.pop('error', 'Día', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el día`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Día', 'El día ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Día', 'Ha ocurrido un error al eliminar el día');
          })
  }

  toggleFilters() {
    jQuery('#filters').slideToggle();
  }

  buildFilters(value) {
    this.isAdvanceFilter = false;
    this.reload();
  }

  filterReport(output: string) {
    let query = new URLSearchParams();
    if (this.isAdvanceFilter && this.advanceFilter.start) {
      query.append('from', this.dateTransform.transform(this.advanceFilter.start, 'y-MM-dd'))
    }
    if (this.isAdvanceFilter && this.advanceFilter.end) {
      query.append('to', this.dateTransform.transform(this.advanceFilter.end, 'y-MM-dd'))
    }

    query.append('output', output);
    return query;
  }

  generateExportLink(output: string): String {
    let query = this.filterReport(output);
    var url = `${this.appConfig.config.API_URL}days/reports?${query.toString()}`;
    return url;
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          if ( !this.isAdvanceFilter && this.filtersForm.value.month && this.filtersForm.value.year ) {
            let date = new Date( this.filtersForm.value.year, this.filtersForm.value.month ),
                from = new Date(date.getFullYear(), date.getMonth(), 1),
                to = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            params = params.append('from', this.dateTransform.transform(from, "y-MM-dd"));
            params = params.append('to', this.dateTransform.transform(to, "y-MM-dd"));

            this.loadEto();
          } else if (this.isAdvanceFilter) {
            params = params.append('from', this.dateTransform.transform(this.advanceFilter.start, 'y-MM-dd'));
            params = params.append('to', this.dateTransform.transform(this.advanceFilter.end, 'y-MM-dd'));
          }

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
