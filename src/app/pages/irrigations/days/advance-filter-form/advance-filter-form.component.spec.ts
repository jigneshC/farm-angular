import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvanceFilterFormComponent } from './advance-filter-form.component';

describe('AdvanceFilterFormComponent', () => {
  let component: AdvanceFilterFormComponent;
  let fixture: ComponentFixture<AdvanceFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvanceFilterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvanceFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
