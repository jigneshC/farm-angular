import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {TranslateService} from '../../../../services/translate.service';
import {CustomValidators} from 'ng2-validation';

@Component({
  selector: 'az-days-advance-filter-form',
  templateUrl: './advance-filter-form.component.html',
  styleUrls: ['./advance-filter-form.component.scss']
})
export class AdvanceFilterFormComponent implements OnInit {

  @Output() confirmFilters = new EventEmitter<any>();

  filtersForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dateTransform: DatePipe,
    public translate: TranslateService,
  ) { }

  ngOnInit() {
    this.createFiltersForm();
  }

  search(form: any) {
    form.start = this.dateTransform.transform(this.filtersForm.get('start').value, 'yyyy-MM-dd');
    form.end = this.dateTransform.transform(this.filtersForm.get('end').value, 'yyyy-MM-dd');
    this.confirmFilters.emit(form);
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      start: [ '', [CustomValidators.date]],
      end: [ '', [CustomValidators.date]],
    });
  }

  resetFilters() {
    this.filtersForm.reset();
  }

}
