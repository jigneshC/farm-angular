import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { CustomFormsModule } from 'ng2-validation'
import {
  MatTooltipModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule, MatInputModule, MatPaginatorIntl,
  MatAutocompleteModule, MatDialogModule
} from '@angular/material';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { CrudComponentsModule } from './../../theme/components/crud-components/crud-components.module';
import { DaysComponent } from './days/days.component';
import { SectorsComponent } from './sectors/sectors.component';
import { IrrigationsComponent } from './irrigations/irrigations.component';
import { ProgrammingIrrigationsComponent } from './programming-irrigations/programming-irrigations.component';
import { TrayRecoveryComponent } from './tray-recovery/tray-recovery.component';
import { CalicatasComponent } from './calicatas/calicatas.component';
import { WeatherComponent } from './weather/weather.component';
import { ApiService } from './../../services/api.service';
import { SelectHelperService } from './../../services/select-helper.service';
import { MatPaginatorLabels } from './../../services/mat-paginator-labels.service';
import { AdvanceFilterFormComponent } from './days/advance-filter-form/advance-filter-form.component';
import { IrrigationBasicFormComponent } from './irrigations/irrigation-basic-form/irrigation-basic-form.component';
import {IrrigationService} from '../../services/api/irrigation/irrigation';
import {SectorService} from '../../services/api/sector/sector.service';
import {
  ProgrammingIrrigationsFormComponent
} from './programming-irrigations/programming-irrigations-form/programming-irrigations-form.component';
import {ProgrammingIrrigationService} from '../../services/api/irrigation/programming-irrigation';
import {PermissionGuardService} from '../../services/auth/permission-guard.service';
import {AgmCoreModule} from "@agm/core";

export const routes = [
  { path: '', redirectTo: 'riegos', pathMatch: 'full' },
  { path: 'dias', component: DaysComponent, data: { roles: ['suelo-clima:dias'], breadcrumb: 'Días' }, canActivate: [PermissionGuardService] },
  { path: 'sectores', component: SectorsComponent, data: { breadcrumb: 'Sectores' } },
  { path: 'riegos', component: IrrigationsComponent, data: { breadcrumb: 'Riegos' } },
  { path: 'programacion-riegos', component: ProgrammingIrrigationsComponent, data: { breadcrumb: 'Programación de riegos' } },
  { path: 'reposicion-bandeja', component: TrayRecoveryComponent, data: { breadcrumb: 'Reposición de bandeja' } },
  { path: 'calicatas', component: CalicatasComponent, data: { breadcrumb: 'Calicatas' } },
  { path: 'metereologia', component: WeatherComponent, data: { breadcrumb: 'Metereología' } },
];

@NgModule({
  entryComponents: [
    IrrigationBasicFormComponent,
    ProgrammingIrrigationsFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    CrudComponentsModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDialogModule,
    DateTimePickerModule,
    CustomFormsModule,
    RouterModule.forChild(routes),
    AgmCoreModule
  ],
  declarations: [
    DaysComponent,
    SectorsComponent,
    IrrigationsComponent,
    ProgrammingIrrigationsComponent,
    TrayRecoveryComponent,
    CalicatasComponent,
    WeatherComponent,
    AdvanceFilterFormComponent,
    IrrigationBasicFormComponent,
    ProgrammingIrrigationsFormComponent,
  ],
  providers:[
    ApiService,
    SelectHelperService,
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorLabels
    },
    DatePipe,
    IrrigationService,
    SectorService,
    ProgrammingIrrigationService
  ]
})

export class IrrigationsModule { }
