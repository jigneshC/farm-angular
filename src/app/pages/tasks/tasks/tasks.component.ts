import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';

import DateGMT from 'app/classes/date-gmt';

const MODEL = "tasks";

@Component({
  selector: 'az-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class TasksComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['date', 'category_id', 'season_id', 'sectors', 'contractors', 'due_date', 'completed', 'notes', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  sectorCtrl: FormControl = new FormControl("");
  contractorCtrl: FormControl = new FormControl("");
  categoryCtrl: FormControl = new FormControl("");
  seasonCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  sectors;
  sectorSub;
  contractors;
  contractorSub;
  categories;
  categorySub;
  seasons;
  seasonSub;
  contractor = { object: null, price: null };
  errors = { contractor: false, price: false };
  tab: string = "form";

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    private dateTransform: DatePipe,
    public translate: TranslateService,
  ) {
    this.createForm();
  }

  createForm() {
    this.errorMessages = undefined;
    this.categoryCtrl = new FormControl(this.current.category_id ? this.current.category.name : "");
    this.categorySub = this.categoryCtrl.valueChanges.startWith(this.current.category_id ? this.current.category.name : "").subscribe(q => this.searchAutoComplete(q, 'categories', 'taskcategories'));
    this.seasonCtrl = new FormControl(this.current.season ? this.current.season.year : "");
    this.seasonSub = this.seasonCtrl.valueChanges.startWith(this.current.season ? this.current.season.year : "").subscribe(q => this.searchAutoComplete(q, 'seasons', 'seasons'));

    this.tab = 'form';
    this.form = this.fb.group({
      due_date: [ this.current.due_date || '', [ CustomValidators.date ] ],
      date: [ this.current.date || '', [ CustomValidators.date ] ],
      completed: [ this.current.completed || false ],
      notes: [ this.current.type || '', [ Validators.required ] ],
      day_id: [ this.current.day_id || '', [ CustomValidators.number ] ],
      category_id: [ this.current.category_id || '', [ Validators.required, CustomValidators.number ] ],
      season_id: [ this.current.season_id || '', [ Validators.required, CustomValidators.number ] ]
    }, { validator: this.dateLessThan('date', 'due_date') });
  }

  dateLessThan(from: string, to: string) {
    return ( group: FormGroup ): { [key: string]: any } => {
      let f = group.controls[from];
      let t = group.controls[to];
      if ( new Date(t.value) < new Date(f.value) ) {
        return {
          due_date: true
        };
      }
      return {};
    }
  }

  create() {
    jQuery('#toform').click();
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    jQuery('#toform').click();
    row.date = DateGMT(row.date);
    row.due_date = DateGMT(row.due_date);
    this.current = row;
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( ["sectors", "contractors", "seasons"].indexOf(model) > -1 && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( ["sectors", "contractors", "seasons"].indexOf(model) > -1 && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    if ( form.date ) form.date = this.dateTransform.transform(form.date, "y-MM-dd");
    else delete form.date;

    if ( form.due_date ) form.due_date = this.dateTransform.transform(form.due_date, "y-MM-dd");
    else delete form.due_date;

    if ( !form.day_id ) delete form.day_id;

    this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.current = res;
					  jQuery('#tosectors').click();
						this.changeTab('sectors');
						this.toaster.pop('success', 'Labor', `El labor ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
						this.paginator.page.emit();
          }, err => {
						this.toaster.pop('error', 'Labor', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el labor`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Labor', 'El labor ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Labor', 'Ha ocurrido un error al eliminar el labor');
          })
  }

  changeTab( tab ) {
    this.tab = tab;
    if ( tab == 'sectors' ) {
      if ( !this.sectors || !this.sectors.length ) {
        this.sectorCtrl = new FormControl("");
        this.sectorSub = this.sectorCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'sectors', 'sectors'));
      }
    } else if ( tab == 'contractors' ) {
      if ( !this.contractors || !this.contractors.length ) {
        this.contractorCtrl = new FormControl("");
        this.contractorSub = this.contractorCtrl.valueChanges.startWith("").subscribe(q => {
          if ( this.contractor.object && this.contractor.object.name != q ) this.contractor.object = null;
          this.searchAutoComplete(q, 'contractors', 'contractors')
        });
      }
    }
  }

  addSector( sector ) {
    if ( !this.current.id ) {
      this.toaster.pop('error', 'Labor', `Debe crear primero el labor para asociar sectores`);
      return false;
    }

    this.current.requesting = true;
    this.API.custom('post', `api/tasks/${ this.current.id }/sectors/${ sector.id }`)
        .subscribe(
          res => {
            this.sectorCtrl = new FormControl("");
            this.sectorSub = this.sectorCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'sectors', 'sectors'));
            this.current = res;
						this.toaster.pop('success', 'Contratista', `El sector ha sido asociado exitosamente`);
						this.paginator.page.emit();
          }, err => {
						this.toaster.pop('error', 'Contratista', `Ha ocurrido un error al intentar asociar el sector`);
            this.current.requesting = false;
          })
  }

  removeSector( sector, index ) {
    if ( !sector ) {
      this.toaster.pop('error', 'Contratista', `Elige un sector para remover`);
      return false;
    }
    if ( !this.current.id ) {
      this.toaster.pop('error', 'Labor', `Debes seleccionar un labor para continuar`);
      return false;
    }

    this.current.requesting = true;
    this.API.custom('delete', `api/tasks/${ this.current.id }/sectors/${ sector.id }`)
        .subscribe(
          res => {
            this.current = res;
						this.toaster.pop('success', 'Contratista', `El sector ha sido removido exitosamente`);
            this.current.requesting = false;
						this.paginator.page.emit();
          }, err => {
						this.toaster.pop('error', 'Contratista', `Ha ocurrido un error al intentar remover el sector`);
            this.current.requesting = false;
          })
  }

  addContractor() {
    if ( !this.contractor.object ) {
      this.errors.contractor = true;
    } else this.errors.contractor = false;

    if ( !this.contractor.price ) {
      this.errors.price = true;
    } else this.errors.price = false;

    if ( this.errors.contractor || this.errors.price ) return false;

    this.current.requesting = true;
    this.API.custom('post', `api/tasks/${ this.current.id }/contractors/${ this.contractor.object.id }`, { price: this.contractor.price })
        .subscribe(
          res => {
            this.contractor = { object: null, price: null };
            this.contractorCtrl = new FormControl("");
            this.contractorSub = this.contractorCtrl.valueChanges.startWith("").subscribe(q => {
              if ( this.contractor.object && this.contractor.object.name != q ) this.contractor.object = null;
              this.searchAutoComplete(q, 'contractors', 'contractors')
            });
            this.current = res;
						this.toaster.pop('success', 'Contratista', `El contratista ha sido asociado exitosamente`);
						this.paginator.page.emit();
          }, err => {
						this.toaster.pop('error', 'Contratista', `Ha ocurrido un error al intentar asociar el contratista`);
            this.current.requesting = false;
          })
  }

  removeContractor( contractor, index ) {
    if ( !contractor ) {
      this.toaster.pop('error', 'Contratista', `Elige un contratista para remover`);
      return false;
    }
    if ( !this.current.id ) {
      this.toaster.pop('error', 'Labor', `Debes seleccionar un labor para continuar`);
      return false;
    }

    this.current.requesting = true;
    this.API.custom('delete', `api/tasks/${ this.current.id }/contractors/${ contractor.id }`)
        .subscribe(
          res => {
            this.current = res;
						this.toaster.pop('success', 'Contratista', `El contratista ha sido removido exitosamente`);
            this.current.requesting = false;
						this.paginator.page.emit();
          }, err => {
						this.toaster.pop('error', 'Contratista', `Ha ocurrido un error al intentar remover el contratista`);
            this.current.requesting = false;
          })
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
