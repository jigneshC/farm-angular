import { CrudComponentsModule } from './../../theme/components/crud-components/crud-components.module';
import { ApiService } from './../../services/api.service';
import { MatPaginatorLabels } from './../../services/mat-paginator-labels.service';
import { ContractorsComponent } from './contractors/contractors.component';
import { TasksComponent } from './tasks/tasks.component';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { CustomFormsModule } from 'ng2-validation'
import { MatTooltipModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule, MatInputModule, MatPaginatorIntl, MatAutocompleteModule } from '@angular/material';

export const routes = [
  { path: '', redirectTo: 'lista-labores', pathMatch: 'full' },
  { path: 'contratistas', component: ContractorsComponent, data: { breadcrumb: 'Contratistas' } },
  { path: 'lista-labores', component: TasksComponent, data: { breadcrumb: 'Lista de labores' } },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    CrudComponentsModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatAutocompleteModule,
    DateTimePickerModule,
    CustomFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ContractorsComponent,
    TasksComponent,
  ],
  providers:[
    ApiService,
    {
      provide: MatPaginatorIntl, 
      useClass: MatPaginatorLabels
    },
    DatePipe,
  ]
})
export class TasksModule{ }