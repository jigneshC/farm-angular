import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AppConfig } from './../../../app.config';

import * as moment from 'moment';
import { TimeCalculatorService } from "../../../services/time-calculator.service";

import DateGMT from 'app/classes/date-gmt';
import {SelectionModel} from '@angular/cdk/collections';
import {isEmpty} from '../../../utils/utils';

const MODEL = "products/summary";

@Component({
  selector: 'az-reports-by-date',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class ReportsByDateComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fileInput') fileInput;

  selection = new SelectionModel(true, []);

  private simpleFilterFormValue: any = {
    year: new Date().getFullYear(),
    month: new Date().getMonth()
  };
  private advanceFilterFormValue: any;

  columns = ['name', 'purchases', 'applications', 'variations'];
  source = new MatTableDataSource([]);
  form: FormGroup;
  filtersForm: FormGroup;
  stateForm: FormGroup;
  costCenterCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  costCenters;
  costCenterSub;
  lastYears: Array<any> = [];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    public appConfig: AppConfig,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public timeService: TimeCalculatorService,
  ) {
    this.createForm();
    this.createFiltersForm();
    this.createStateForm();
  }

  getAverageHumity( row ) {
    return ( ( row.humity_at_30 + row.humity_at_60 + row.humity_at_90 ) / 3 ).toFixed(2);
  }

  createStateForm() {
    this.stateForm = this.fb.group({
      state: [null, []]
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.source.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.source.data.forEach(row => this.selection.select(row));
  }

  createForm() {
    this.errorMessages = undefined;
    this.form = this.fb.group({
    });
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      date_from: moment().startOf('month').toDate(),
      date_to: moment().endOf('month').toDate(),
    });
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    row.expense_date = DateGMT(row.expense_date);
    this.current = row;
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  updateItem(form, method) {

    const formData = new FormData();
    if ( !form.file ) { delete form.file }
    if ( !form.invoice_id ) { delete form.invoice_id }

    for ( const key in form ) {
      formData.append(key, form[key] );
    }

    formData.append('_method', 'PUT');
    return this.API[method](MODEL, formData, form.id, 'post');
  }

  toggleFilters(): Promise<void> {
    return new Promise<void>((resolve) => {
      jQuery('#filters').slideToggle(400, () => {
        resolve();
      });
    });
  }

  isAdvancedFilterVisible(): boolean {
    return jQuery('#filters').is(':visible');
  }

  resetFilters() {
    this.createFiltersForm();
    this.reload();
  }

  buildFilters(form) {
    this.reload();
  }

  onSimpleFiltering(form) {
    if (this.isAdvancedFilterVisible()) {
      this.toggleFilters().then(() => {
          this.simpleFilterFormValue = form;
          this.reload();
        }
      );
    } else {
      this.simpleFilterFormValue = form;
      this.reload();
    }
  }

  onAdvanceFiltering(form) {
    this.advanceFilterFormValue = form;
  }

  goToInvoices(row){
    let from = null;
    let to = null;
    if ( this.filtersForm.value.date_from ) from = this.dateTransform.transform(this.filtersForm.value.date_from, "y-MM-dd");
    if ( this.filtersForm.value.date_to ) to = this.dateTransform.transform(this.filtersForm.value.date_to, "y-MM-dd");
    localStorage.setItem("invoiceDetail", JSON.stringify({ provider: '', from: from, to: to }))
    window.open('pages/contabilidad/compras', '_blank');
  }

  goToApplications(row){
    let from = null;
    let to = null;
    if ( this.filtersForm.value.date_from ) from = this.dateTransform.transform(this.filtersForm.value.date_from, "y-MM-dd");
    if ( this.filtersForm.value.date_to ) to = this.dateTransform.transform(this.filtersForm.value.date_to, "y-MM-dd");
    localStorage.setItem("applicationDetail", JSON.stringify({ product: '', from: from, to: to }))
    window.open('pages/aplicaciones/aplicaciones', '_blank');
  }

  filterReport(output: string = '') {
    let query = new URLSearchParams();
    if ( this.filtersForm.value.date_from ) query.append('from', this.dateTransform.transform(this.filtersForm.value.date_from, "y-MM-dd"));
    if ( this.filtersForm.value.date_to ) query.append('to', this.dateTransform.transform(this.filtersForm.value.date_to, "y-MM-dd"));
    if (output) query.append('output', output);
    return query;
  }

  generateExportLink(output: string) {
    let query = this.filterReport(output);
    //var url = `${this.appConfig.config.API_URL}purchasebook/report?${query.toString()}`;
    //window.open(url, '_blank');
  }

  runFiltering(){
    this.loadingResults = true;
    this.source.data = [];
    let params = new HttpParams();

    if ( this.filtersForm.value.date_from ) params = params.append('from', this.dateTransform.transform(this.filtersForm.value.date_from, "y-MM-dd"));
    if ( this.filtersForm.value.date_to ) params = params.append('to', this.dateTransform.transform(this.filtersForm.value.date_to, "y-MM-dd"));
    if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);
    if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
    if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
    if ( this.sort.active ) params = params.append('sort_by', this.sort.active);


    this.API.index(MODEL, params).subscribe(res => {
      this.loadingResults = false;
      this.source.data = res.products;
    });
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();
          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          if ( this.filtersForm.value.date_from ) params = params.append('from', this.dateTransform.transform(this.filtersForm.value.date_from, "y-MM-dd"));
          if ( this.filtersForm.value.date_to ) params = params.append('to', this.dateTransform.transform(this.filtersForm.value.date_to, "y-MM-dd"));

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.products;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data =>{
          this.source.data = data;
        });
  }
}
