import { Component } from '@angular/core';
import { AbstractCrudFilterComponent } from '../../../../classes/components/crud/abstract-crud-filter-component';
import { ApiService } from '../../../../services/api.service';
import { FormBuilder } from '@angular/forms';
import {AppConfig} from '../../../../app.config';
import { ToasterService } from 'angular2-toaster';

import * as moment from 'moment';

@Component({
  selector: 'az-reports-by-date-filter',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsByDateFilterComponent extends AbstractCrudFilterComponent {

  lastYears: Array<any> = [];

  constructor(
    API: ApiService,
    public myAPI: ApiService,
    private fb: FormBuilder,
    public appConfig: AppConfig,
    private toaster: ToasterService
  ) {
    super(API);
    this.createForm();
    this.getLastYears();
  }

  getLastYears( date? ) {
    let start = new Date(date || "2010-01-02");
    let end = new Date();
    let years = moment(end).diff(start, 'years');
    for ( let year = 0; year < years +1; year++ )
      this.lastYears.push(start.getFullYear() + year);
  }

  createForm() {
    this.form = this.fb.group({
      year: new Date().getFullYear(),
      month: new Date().getMonth()
    });
  }

  importInvoices(){
    //let url = `${this.appConfig.config.API_URL}api/invoices/import`;
    //window.open(url,'_blank');

    this.myAPI.custom('get', `api/invoices/import`)
    .subscribe(
      res => { if (res.success) this.toaster.pop('success', 'Libro de Compras', 'La importación se ha realizado con éxito.') },
      err => this.toaster.pop('error', 'Libro de Compras', 'Ha ocurrido un error durante la importación')
    );
  }
}
