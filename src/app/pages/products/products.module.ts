import { CrudComponentsModule } from './../../theme/components/crud-components/crud-components.module';
import { ApiService } from './../../services/api.service';
import { MatPaginatorLabels } from './../../services/mat-paginator-labels.service';
import { ProductsComponent } from './products/products.component';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { CustomFormsModule } from 'ng2-validation'
import {
  MatTooltipModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule, MatInputModule, MatPaginatorIntl,
  MatAutocompleteModule, MatDialogModule
} from '@angular/material';
import { DoseComponent } from './dose/dose.component';
import { ShortcomingsComponent } from './shortcomings/shortcomings.component';
import { NpkComponent } from './npk/npk.component';
import { ReportsByDateComponent } from './reportsbydate/reports.component';
import { ReportsByDateFilterComponent } from './reportsbydate/reports-filter/reports.component';
import { InventoryAdjustmentsComponent } from './inventory/inventory-adjustments.component';
import {InventoryAdjustmentService} from '../../services/api/product/inventory-adjustments.service';
import { InventoryAdjustmentsFormComponent } from './inventory/inventory-adjustments-form/inventory-adjustments-form.component';
import {ProductService} from '../../services/api/product/product.service';
import {ReportsByPurchasesComponent} from './reportsbypurchases/reportsbypurchases.component';

export const routes = [
  { path: '', redirectTo: 'lista-productos', pathMatch: 'full' },
  { path: 'lista-productos', component: ProductsComponent, data: { breadcrumb: 'Lista de productos' } },
  { path: 'dosis', component: DoseComponent, data: { breadcrumb: 'Dosis' } },
  { path: 'carencias', component: ShortcomingsComponent, data: { breadcrumb: 'Carencias' } },
  { path: 'npk', component: NpkComponent, data: { breadcrumb: 'Npk por producto' } },
  { path: 'reportes-fechas', component: ReportsByDateComponent, data: { breadcrumb: 'Reporte por fechas' } },
  { path: 'reporte-compras', component: ReportsByPurchasesComponent, data: { breadcrumb: 'Reporte Productos por Compras' } },
  { path: 'ajuste-inventario', component: InventoryAdjustmentsComponent, data: { breadcrumb: 'Ajuste de Inventario' } }
];

@NgModule({
  entryComponents: [
    InventoryAdjustmentsFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    CrudComponentsModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDialogModule,
    DateTimePickerModule,
    CustomFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ProductsComponent,
    DoseComponent,
    ShortcomingsComponent,
    NpkComponent,
    ReportsByDateComponent,
    ReportsByDateFilterComponent,
    InventoryAdjustmentsComponent,
    InventoryAdjustmentsFormComponent,
    ReportsByPurchasesComponent
  ],
  providers: [
    ApiService,
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorLabels
    },
    DatePipe,
    InventoryAdjustmentService,
    ProductService
  ]
})
export class ProductsModule{ }
