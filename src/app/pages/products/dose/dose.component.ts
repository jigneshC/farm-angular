import { Component } from '@angular/core';
import { AbstractCrudComponent, IMessagesConfig } from '../../../classes/components/crud/abstract-crud-component';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from '../../../services/api.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'az-dose',
  templateUrl: './dose.component.html',
  styleUrls: ['./dose.component.scss']
})
export class DoseComponent extends AbstractCrudComponent {

  public static readonly MODEL = 'doses';
  public static readonly COLUMNS = ['product_id', 'variety_id', 'min_dose_100_liters', 'max_dose_100_liters', 'min_dose_ha', 'max_dose_ha', 'actions'];
  public static readonly MESSAGES_CONFIG: IMessagesConfig = {
    title: 'Dosis',
    successDeleteBody: 'La dosis ha sido eliminado exitosamente',
    errorDeleteBody: 'Ha ocurrido un error al eliminar la dosis'
  };

  varietySub;
  varietyCtrl: FormControl = new FormControl("");

  varieties: any[] = [];

  products: any[] = [];

  productSub;
  productCtrl: FormControl = new FormControl("");

  constructor(
    toaster: ToasterService,
    API: ApiService,
    private fb: FormBuilder
  ) {
    super(DoseComponent.MODEL, DoseComponent.COLUMNS,DoseComponent.MESSAGES_CONFIG,toaster,API);
  }

  protected createForm(): FormGroup {

    this.varietyCtrl = new FormControl(this.current.variety ? this.current.variety.name : "");
    this.varietySub = this.varietyCtrl.valueChanges.startWith(this.current.variety ? this.current.variety.name : "").subscribe(q => this.searchAutoComplete(q, 'varieties', 'varieties'));
    this.productCtrl = new FormControl(this.current.product ? this.current.product.name : "");
    this.productSub = this.productCtrl.valueChanges.startWith(this.current.product ? this.current.product.name : "").subscribe(q => this.searchAutoComplete(q, 'products', 'products'));

    return this.fb.group({
      variety_id: [ this._current.variety_id || '', [ Validators.required, CustomValidators.number ] ],
      product_id: [ this._current.product_id || '', [ Validators.required, CustomValidators.number ] ],
      min_dose_100_liters: [ this._current.min_dose_100_liters || '', [ Validators.required, CustomValidators.number, CustomValidators.range([0, 99999.99]) ] ],
      max_dose_100_liters: [ this._current.max_dose_100_liters || '', [ Validators.required, CustomValidators.number, CustomValidators.range([0, 99999.99]) ] ],
      min_dose_ha: [ this._current.min_dose_ha || '', [ Validators.required, CustomValidators.number, CustomValidators.range([0, 99999.99]) ] ],
      max_dose_ha: [ this._current.max_dose_ha || '', [ Validators.required, CustomValidators.number, CustomValidators.range([0, 99999.99]) ] ]
    });
  }


  protected cleanForm() {



  }

  protected onAfterViewInit(): void {

  }
}
