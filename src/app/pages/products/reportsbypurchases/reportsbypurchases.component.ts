import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppConfig } from './../../../app.config';
import * as moment from 'moment';
import {SelectionModel} from '@angular/cdk/collections';
import {ProductService} from "../../../services/api/product/product.service";

const MODEL = "products/purchases";

@Component({
  selector: 'az-reports-by-purchases',
  templateUrl: './reportsbypurchases.component.html',
  styleUrls: ['./reportsbypurchases.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class ReportsByPurchasesComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fileInput') fileInput;

  private advanceFilterFormValue: any;

  columns = ['name', 'costcenter', 'quantity', 'package', 'total_quantity', 'currency', 'average', 'weightedaverage', 'std', 'variance', 'total_buy', 'total_invoices'];
  source = new MatTableDataSource([]);
  filtersForm: FormGroup;
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  packages: any = [];
  currencies: any = [];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    public appConfig: AppConfig,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public product: ProductService
  ) {
    this.createFiltersForm();
    this.packages = product.getPackages();
    this.currencies = API.getCurrencies();
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      date_from: moment().startOf('month').toDate(),
      date_to: moment().endOf('month').toDate(),
    });
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  toggleFilters(): Promise<void> {
    return new Promise<void>((resolve) => {
      jQuery('#filters').slideToggle(400, () => {
        resolve();
      });
    });
  }

  resetFilters() {
    this.createFiltersForm();
    this.reload();
  }

  buildFilters(form) {
    this.reload();
  }

  runFiltering(){
    this.loadingResults = true;
    this.source.data = [];
    let params = new HttpParams();

    if ( this.filtersForm.value.date_from ) params = params.append('from', this.dateTransform.transform(this.filtersForm.value.date_from, "y-MM-dd"));
    if ( this.filtersForm.value.date_to ) params = params.append('to', this.dateTransform.transform(this.filtersForm.value.date_to, "y-MM-dd"));
    if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);
    if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
    if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
    if ( this.sort.active ) params = params.append('sort_by', this.sort.active);


    this.API.index(MODEL, params).subscribe(res => {
      this.loadingResults = false;
      this.source.data = res.data;
    });
  }

  ngAfterViewInit() {
    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          if ( this.filtersForm.value.date_from ) params = params.append('from', this.dateTransform.transform(this.filtersForm.value.date_from, "y-MM-dd"));
          if ( this.filtersForm.value.date_to ) params = params.append('to', this.dateTransform.transform(this.filtersForm.value.date_to, "y-MM-dd"));

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.total;
          this.source.data = data.data;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => {

          this.source.data = data
        });
  }
}
