import {Component, EventEmitter, Inject, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {TranslateService} from '../../../../services/translate.service';
import {ToasterService} from 'angular2-toaster';
import {CustomValidators} from 'ng2-validation';
import {InventoryAdjustmentService} from '../../../../services/api/product/inventory-adjustments.service';
import {ProductService} from '../../../../services/api/product/product.service';
import {Product} from '../../../../models/product';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';

@Component({
  selector: 'az-inventory-adjustments-form',
  templateUrl: './inventory-adjustments-form.component.html',
  styleUrls: ['./inventory-adjustments-form.component.scss']
})
export class InventoryAdjustmentsFormComponent implements OnInit {

  @Output() afterSave = new EventEmitter<string>(true);

  public title: string;
  public errorMessages = [];
  public form: FormGroup;
  public productForm: FormGroup;
  public isLoading = false;
  public isEdit = false;

  products: Product[];
  selectedProducts: { id: number; name: string, quantity: number }[] = [];
  selectedProduct: { id: number; name: string, quantity: number };
  inventoryId: number;

  productsCtrl: FormControl = new FormControl();
  filteredProducts: Observable<Product[]>;

  constructor(
    protected inventoryService: InventoryAdjustmentService,
    protected productService: ProductService,
    protected dialogRef: MatDialogRef<InventoryAdjustmentsFormComponent>,
    protected formBuilder: FormBuilder,
    protected toaster: ToasterService,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.isLoading = true;
    if (this.data.id) {
      this.initForEdition();
    } else {
      this.initForCreation();
    }
    this.createForm();
  }

  createForm() {
    this.form = this.formBuilder.group({
      date: [ '', [ Validators.required, CustomValidators.date ] ],
      notes: [ '', [Validators.maxLength(255)]],
      products: [ [], [Validators.required]],
    });
    this.productForm = this.formBuilder.group({
      quantity: [ 0, [Validators.required]],
      product: [ null, [Validators.required]]
    })
  }

  initAutoCompletes() {
    this.productsCtrl = new FormControl(null);
    this.filteredProducts = this.productsCtrl.valueChanges
      .pipe(
        startWith(''),
        map(val => this.filterProducts(val))
      );
  }

  initForCreation() {
    this.isEdit = false;
    this.title = 'Crear Ajuste de Inventario';
    Observable.forkJoin(
      this.productService.findAll()
    ).subscribe(data => {
      this.isLoading = false;
      this.products = data[0];
      this.initAutoCompletes();
    }, error => {
      this.errorMessages = ['Ha ocurrido un error al inicializar el Ajuste de Inventario.'];
      this.isLoading = false;
    });
  }

  initForEdition() {
    this.isEdit = true;
    this.inventoryId = this.data.id;
    this.title = 'Editar Ajuste de Inventario';
    Observable.forkJoin(
      this.productService.findAll(),
      this.inventoryService.findOne(String(this.inventoryId)),
    ).subscribe(data => {
      this.isLoading = false;
      this.products = data[0];
      this.form.patchValue(data[1]);
      this.selectedProducts = this.form.get('products').value.map(p => {
        return {
          id: p.id,
          name: p.name,
          quantity: p.pivot.inventory_quantity
        }
      });
      this.initAutoCompletes();
    }, error => {
      this.errorMessages = ['Ha ocurrido un error al inicializar el Ajuste de Inventario.'];
      this.isLoading = false;
    });
  }

  filterProducts(val: string): Product[] {
    return this.products.filter(product =>
      product.name.toLowerCase().indexOf(val.toLowerCase()) === 0);
  }

  selectProduct(product: any) {
    this.selectedProduct = {
      id: product.id,
      name: product.name,
      quantity: 0
    };
    this.productForm.get('product').patchValue(this.selectedProduct);
  }

  addProduct(product: any) {
    this.selectedProduct.quantity = product.quantity;
    this.selectedProducts.push(this.selectedProduct);
    this.form.get('products').patchValue(this.selectedProducts);
    this.productForm.reset();
    this.productsCtrl.patchValue('');
    this.productForm.get('product').setValidators([]);
    this.productForm.get('product').updateValueAndValidity();
    // this.initAutoCompletes();
  }

  save(form: any) {
    this.isLoading = true;
    form.date = this.dateTransform.transform(this.form.value.date, 'y-MM-dd');
    form.products = this.selectedProducts.map(p => {
      return {
        product_id: p.id,
        quantity: p.quantity
      }
    });

    if (this.isEdit) {
      this.inventoryService.update(String(this.inventoryId), form)
        .subscribe(data => {
            this.afterSave.emit('success');
            this.dialogRef.afterClosed().subscribe(() => {
              this.isLoading = false;
              this.toaster.pop('success', 'Ajuste de Inventario', 'Ajuste de Inventario editado exitosamente.');
            });
            this.dialogRef.close();
          }, error => {
            this.isLoading = false;
            this.errorMessages.push('Ha ocurrido un error al intentar guardar el Inventario.');
        });
    } else {
      this.inventoryService.create(form)
        .subscribe(data => {
          this.afterSave.emit('success');
          this.dialogRef.afterClosed().subscribe(() => {
            this.isLoading = false;
            this.toaster.pop('success', 'Ajuste de Inventario', 'Ajuste de Inventario guardada exitosamente.');
          });
          this.dialogRef.close();
        }, error => {
          this.isLoading = false;
          this.errorMessages.push('Ha ocurrido un error al intentar crear el Inventario.');
        });
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  deleteProduct(index: number) {
    this.selectedProducts.splice(index, 1);
    this.form.get('products').patchValue(this.selectedProducts);
    if (this.selectedProducts.length === 0) {
      this.productForm.get('product').setValidators([Validators.required]);
      this.productForm.get('product').updateValueAndValidity();
    }
  }

}
