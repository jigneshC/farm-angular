import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryAdjustmentsFormComponent } from './inventory-adjustments-form.component';

describe('InventoryAdjustmentsFormComponent', () => {
  let component: InventoryAdjustmentsFormComponent;
  let fixture: ComponentFixture<InventoryAdjustmentsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryAdjustmentsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryAdjustmentsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
