import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {Pagination} from '../../../services/api/pagination';
import {DatePipe} from '@angular/common';
import {Router} from '@angular/router';
import {ToasterService} from 'angular2-toaster';
import {HttpParams} from '@angular/common/http';
import {InventoryAdjustmentService} from '../../../services/api/product/inventory-adjustments.service';
import {InventoryAdjustment} from '../../../models/inventory-adjustment';
import {Subscription} from 'rxjs/Subscription';
import {InventoryAdjustmentsFormComponent} from './inventory-adjustments-form/inventory-adjustments-form.component';

@Component({
  selector: 'az-inventory-adjustments',
  templateUrl: './inventory-adjustments.component.html',
  styleUrls: ['./inventory-adjustments.component.scss']
})
export class InventoryAdjustmentsComponent implements OnInit , AfterViewInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public columns = ['id', 'date', 'product', 'adjust', 'last_inventory', 'notas', 'actions'];
  public source = new MatTableDataSource();
  public loadingResults = true;
  public error: string;
  haveError: boolean = false;
  public filter: string;
  public basicFilter: string;
  private isBasicFilter: boolean = false;
  public advanceFilter: any;
  timeout: any;
  total: number = 0;
  limit: number = 10;

  inventory: InventoryAdjustment;

  protected formResultSubscription: Subscription;

  constructor(
    private router: Router,
    private inventoryService: InventoryAdjustmentService,
    private dateTransform: DatePipe,
    private toaster: ToasterService,
    protected  dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  createOrEdit(row?: InventoryAdjustment) {
    const data = row ? {id: row.id} : {};
    const form = this.dialog.open(InventoryAdjustmentsFormComponent, {
      width: '800px',
      data
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.getInventoryAdjustments();
      }
    })
  }

  reload() {
    if ( this.timeout ) { clearTimeout(this.timeout) };
    this.source.data = [];
    this.filter = '';
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  buildSearchParams(): HttpParams {
    let params = new HttpParams();

    if ( this.paginator.pageIndex >= 0 ) { params = params.append('page', `${ this.paginator.pageIndex + 1 }`) }
    if ( this.paginator.pageSize ) {params = params.append('p', `${this.paginator.pageSize}`) }
    if ( this.filter ) { params = params.append('q', this.filter) }
    if ( this.sort.active ) { params = params.append('sort_by', this.sort.active) }
    if ( this.sort.direction ) { params = params.append('sort_dir', this.sort.direction) }

    return params;
  }

  getInventoryAdjustments() {
    this.loadingResults = true;
    const params = this.buildSearchParams();

    this.inventoryService.findPaginated(params)
      .catch(error => {
        this.error = error;
        return Observable.of([]);
      })
      .finally(() => {
        this.loadingResults = false;
      })
      .map((data: Pagination<InventoryAdjustment>) => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .subscribe((data: InventoryAdjustment[]) => {
          this.source.data = data;
        }
      );
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        const params = this.buildSearchParams();

        return this.inventoryService.findPaginated(params);
      })
      .map((data: Pagination<InventoryAdjustment>) => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .catch( error => {
        this.loadingResults = false;
        this.haveError = true;
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data);
  }

  search() {
    if ( this.timeout ) { clearTimeout(this.timeout) }
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }

  ngOnDestroy() {
    // this.formResultSubscription.unsubscribe();
  }

  delete() {
    this.inventoryService.delete(String(this.inventory.id))
      .subscribe(
        res => {
          if ( this.source.data.length === 1 && this.paginator.pageIndex > 0 ) { this.paginator.pageIndex-- };
          this.paginator.page.emit();
          this.toaster.pop('success', 'Ajuste de Inventario', 'Ajuste de Inventario eliminado exitosamente.');
        },
        error => {
          this.toaster.pop('error', 'Ajuste de Inventario', 'Ha ocurrido un error al eliminar el Ajuste de Inventario.');
        }
      );
  }

}
