import { Component } from '@angular/core';
import {AbstractCrudComponent, IMessagesConfig} from '../../../classes/components/crud/abstract-crud-component';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../services/api.service';

@Component({
  selector: 'az-shortcomings',
  templateUrl: './shortcomings.component.html',
  styleUrls: ['./shortcomings.component.scss']
})
export class ShortcomingsComponent extends AbstractCrudComponent{

  public static readonly MODEL = 'shortcomings';
  public static readonly COLUMNS = ['product_id', 'variety_id', 'destination', 'days', 'type', 'actions'];
  public static readonly MESSAGES_CONFIG: IMessagesConfig = {
    title: 'Carencia',

    successDeleteBody: 'La carencia ha sido eliminado exitosamente',

    errorDeleteBody: 'Ha ocurrido un error al eliminar la carencia'
  };

  varietySub;
  varietyCtrl: FormControl = new FormControl("");

  products: any[] = [];
  varieties: any[] = [];

  productSub;
  productCtrl: FormControl = new FormControl("");

  readonly types: any[] = [ { value: 0, name: 'ETIQUETA' }, { value: 1, name: 'MINISTERIO DE AGRICULTURA' } ];
  readonly destinations: any[] = [
    { value: 'USA', name: 'USA'},
    { value: 'China', name: 'China'},
    { value: 'Brasil', name: 'Brasil'},
    { value: 'Canada', name: 'Canada'},
    { value: 'Corea', name: 'Corea'},
    { value: 'Taiwan', name: 'Taiwan'},
    { value: 'Latinoamerica', name: 'Latinoamerica'},
    { value: 'Rusia', name: 'Rusia'},
    { value: 'Japon', name: 'Japon'}
  ];

  constructor(
    toaster: ToasterService,
    API: ApiService,
    private fb: FormBuilder
  ) {
    super(ShortcomingsComponent.MODEL,ShortcomingsComponent.COLUMNS,ShortcomingsComponent.MESSAGES_CONFIG,toaster,API);
  }

  protected createForm(): FormGroup {

    this.varietyCtrl = new FormControl(this.current.variety ? this.current.variety.name : "");
    this.varietySub = this.varietyCtrl.valueChanges.startWith(this.current.variety ? this.current.variety.name : "").subscribe(q => this.searchAutoComplete(q, 'varieties', 'varieties'));
    this.productCtrl = new FormControl(this.current.product ? this.current.product.name : "");
    this.productSub = this.productCtrl.valueChanges.startWith(this.current.product ? this.current.product.name : "").subscribe(q => this.searchAutoComplete(q, 'products', 'products'));

    return this.fb.group({
      variety_id: [ this._current.variety_id || '', [ Validators.required, CustomValidators.number ] ],
      product_id: [ this._current.product_id || '', [ Validators.required, CustomValidators.number ] ],
      days: [ this._current.days || '', [ Validators.required, CustomValidators.number ] ],
      type: [ this._current.type || 0, [ Validators.required ] ],
      destination: [ this._current.destination || '', [ Validators.required ] ],
    });
  }

  public filterType(type: number): string {

    let result =this.types.filter((item) => {
      return item.value == type;
    });

    return result.length >= 1 ? result[0].name : 'N/A';
  }

  protected cleanForm() {



  }

  protected onAfterViewInit(): void {

  }
}
