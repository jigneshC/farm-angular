import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortcomingsComponent } from './shortcomings.component';

describe('ShortcomingsComponent', () => {
  let component: ShortcomingsComponent;
  let fixture: ComponentFixture<ShortcomingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortcomingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortcomingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
