import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NpkComponent } from './npk.component';

describe('NpkComponent', () => {
  let component: NpkComponent;
  let fixture: ComponentFixture<NpkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NpkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NpkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
