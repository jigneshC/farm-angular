import {AfterViewInit, Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {DatePipe} from '@angular/common';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import {ToasterService} from 'angular2-toaster';
import {ApiService} from './../../../services/api.service';
import {TranslateService} from './../../../services/translate.service';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import {isNumeric} from 'rxjs/util/isNumeric';
import {ProductService} from "../../../services/api/product/product.service";

const MODEL = "products";

@Component({
  selector: 'az-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class ProductsComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fileInput') fileInput;

  columns = ['name', 'package', 'quantity_per_package', 'origin', 'formula', 'classification', 'npks', 'varieties', 'label_shortcoming', 'reentry', 'inventory', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  formNPK: FormGroup;
  formIngredient: FormGroup;
  varietyCtrl: FormControl = new FormControl("");
  npkCtrl: FormControl = new FormControl("");
  npksCtrls: any = {};
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  currentNPK: any = {};
  currentIngredient: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  varieties;
  npks;
	selectedVarieties: Array<any> = [];
	selectedVarietiesAll = {};
  classifications = [ { value: 0, name: 'FERTILIZANTE' }, { value: 1, name: 'HERBICIDA' }, { value: 2, name: 'FITOSANITARIO' }, { value: 3, name: 'FERTILIZANTE FOLIAR' }, { value: 4, name: 'OTROS' } ];
  packages = [ { value: 0, name: 'LITROS' }, { value: 1, name: 'KILOS' } ];
  origins = [ { value: 0, name: 'QUÍMICO' }, { value: 1, name: 'ORGÁNICO' } ];
  tab: string = "form";

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    private dateTransform: DatePipe,
    public translate: TranslateService,
    protected productsService: ProductService
  ) {
    this.createForm();
    this.createFormNPK();
    this.createFormIngredient();
  }

  minLength(length: number) {
    return (control: FormControl) => {
      return control.value.length >= length ? null : {
        minLength: true
      };
    }
  }

  createForm(tab?) {
    if ( this.fileInput ) this.fileInput.nativeElement.value = '';
    this.errorMessages = undefined;
    this.varietyCtrl = new FormControl("");
    this.varietyCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'varieties', 'varieties'));

    this.tab =  tab? tab : 'form';
    this.form = this.fb.group({
      name: [ this.current.name || '', [ Validators.required, Validators.maxLength(255) ] ],
      package: [ this.current.package || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      quantity_per_package: [ this.current.quantity_per_package || '', [ Validators.required, CustomValidators.number, CustomValidators.min(0), CustomValidators.max(999999.99999) ] ],
      origin: [ this.current.origin || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      formula: [ this.current.formula || '', [ Validators.required, Validators.maxLength(255) ] ],
      classification: [ this.current.classification || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 4]) ] ],
      reentry: [ this.current.reentry || '', [ CustomValidators.number, CustomValidators.min(0), CustomValidators.max(99999.99) ] ],
      objective: [ this.current.objective || '', [ Validators.maxLength(255) ] ],
      adjust_inventory: [ this.current.adjust_inventory || '', [ CustomValidators.number, CustomValidators.min(0), CustomValidators.max(999999.99999) ] ],
      notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
      npks: this.fb.array(this.initNPKs()),
      active_ingredients: this.fb.array(this.initIngredients()),
      file: [ '' ],
      varieties: [ [], [ this.minLength(1) ] ],
    });
    this.initVarieties();
    this.createFormNPK();

    this.form.valueChanges.subscribe( form => {
      let fileCtrl = this.form.controls['file'];

      if ( !form.file && !this.current.id ) fileCtrl.setErrors({'required': true});
      else fileCtrl.setErrors(null);

      if ( form.classification == 1 || form.classification == 2 ) this.form.controls['reentry'].setValidators(Validators.compose([ Validators.required, CustomValidators.number, CustomValidators.min(0), CustomValidators.max(99999.99) ]));
      else this.form.controls['reentry'].setValidators(Validators.compose([ CustomValidators.number, CustomValidators.min(0), CustomValidators.max(99999.99) ]));
    });
  }

  createFormNPK() {
    this.errorMessages = undefined;
    this.npkCtrl = new FormControl(this.currentNPK.npk ? this.currentNPK.npk.name : "");
    this.npkCtrl.valueChanges.startWith(this.currentNPK.npk ? this.currentNPK.npk.name : "").subscribe(q => this.searchAutoComplete(q, 'npks', 'npks'));

    this.formNPK = this.fb.group({
      npk_id: [ this.currentNPK.npk_id || '', [ Validators.required, CustomValidators.number ] ],
      quantity: [ this.currentNPK.pivot ? this.currentNPK.pivot.quantity : this.currentNPK.quantity ? this.currentNPK.quantity : '', [ Validators.required, CustomValidators.number ] ]
    });
  }

  initNPKs() {
    let all = [];
    if ( this.current.npks ) {
      this.current.npks.forEach( ( npk, index ) => {
        this.npksCtrls[index] = {
          ctrl: new FormControl(""),
          values: []
        }
        this.npksCtrls[index].sub = this.npksCtrls[index].ctrl.valueChanges.startWith(npk.npk ? npk.npk.name : "").subscribe(q => this.searchAutoComplete(q, 'npksCtrls', 'npks', index))
        npk = this.fb.group({
            npk_id: [ npk.id, [ Validators.required, CustomValidators.number ] ],
            npk: [ npk, [ ] ],
            quantity: [ npk.pivot.quantity, [ Validators.required, CustomValidators.number, CustomValidators.min(1) ] ]
        });
        all.push(npk);
      })
    }
    return all;
  }

  initIngredients() {
    let all = [];
    if ( this.current.active_ingredients ) {
      this.current.active_ingredients.forEach( ( ingredient, index ) => {
        ingredient = this.fb.group({
            name: [ ingredient.name, [ Validators.required ] ]
        });
        all.push(ingredient);
      })
    }
    return all;
  }

  createFormIngredient() {
    this.errorMessages = undefined;

    this.formIngredient = this.fb.group({
      name: [ '', [ Validators.required ] ],
    });
  }

  initVarieties() {
    this.selectedVarietiesAll = {};
    this.selectedVarieties = [];
    if ( this.current.varieties ) {
      this.current.varieties.forEach( ( variety, index ) => {
        if ( !this.selectedVarietiesAll[variety.id] ) {
          this.selectedVarietiesAll[variety.id] = true;
          variety = {
            name: variety.name,
            id: variety.id
          };
          this.form.controls['varieties'].setValue(Object.keys(this.selectedVarietiesAll));
          this.selectedVarieties.push(variety);
        }
      })
    }
  }

  addedFile( $event, control ) {
    if ( $event.target.files[0].type != "application/pdf" ) {
      $event.target.value = '';
      this.form.controls[control].setValue(null);
      return this.form.controls[control].setErrors({ 'type': true });
    }
    this.form.controls[control].setErrors(null);
    this.form.controls[control].setValue($event.target.files[0]);
  }

  create() {
    jQuery('#toform').click();
    this.tab = "form";
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    jQuery('#toform').click();
    this.tab = "form";
    this.current = row;
    this.createForm();
    this.createFormNPK();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model, index? ) {
    if ( ["varieties", "npks"].indexOf(model) > -1 && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(
      data => {
        if (['varieties', 'npks'].indexOf(model) > -1 && this.current) this.current.requesting = false;
        if (index && variable === 'npksCtrls') {
          this[variable][index].values = data.data;
        }
        else {
          this[variable] = data.data;
        }
      });
  }

  selectRow( row, model, form ) {
    if ( form == "selectedVarieties" ) {
      this.varietyCtrl.setValue("");
      if ( !this.selectedVarietiesAll[row.id] ) {
        this.selectedVarietiesAll[row.id] = true;
        let variety = {
          name: row.name,
          id: row.id
        };
        this.form.controls['varieties'].setValue(Object.keys(this.selectedVarietiesAll));
        this[form].push(row);
      }
    } else this[form].controls[model+"_id"].setValue(row.id);
  }

  removeVariety( id, index ) {
    delete this.selectedVarietiesAll[''+id];
    this.selectedVarieties.splice(index, 1);
    this.form.controls['varieties'].setValue(Object.keys(this.selectedVarietiesAll));
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    const formData = new FormData();

    if ( this.current.id ) method = "update";

    if ( !form.objective ) delete form.objective;
    if ( !form.adjust_inventory ) delete form.adjust_inventory;
    if ( !form.reentry ) delete form.reentry;
    if ( !form.file ) delete form.file;

    for ( let i = 0; i < form.varieties.length; i++ ) {
      formData.append('varieties[]', form.varieties[i]);
    }
    /*
    for ( let i = 0; i < form.npks.length; i++ ) formData.append('npks[]', form.npks[i]);
    for ( let i = 0; i < form.active_ingredients.length; i++ ) formData.append('active_ingredients', form.active_ingredients[i]);
      */

    for ( let key in form ) {
      if ( ["varieties", "npks", "active_ingredients"].indexOf(key) == -1 ) formData.append(key, form[key] );
    }

    if ( method == "update" ) formData.append('_method', 'PUT');

    this.API[method](MODEL, formData, this.current.id, "post")
        .subscribe(
          res => {
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.current = res;
					  jQuery('#tonpks').click();
						this.changeTab('npks');
						this.toaster.pop('success', 'Producto', `El producto ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
						this.paginator.page.emit();
          }, err => {
						this.toaster.pop('error', 'Producto', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el producto`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
							if ( name == 'file' && this.fileInput ) this.fileInput.nativeElement.value = '';
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Producto', 'El producto ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Producto', 'Ha ocurrido un error al eliminar el producto');
          })
  }

  resetFormNPK() {
    this.currentNPK = {};
    this.createFormNPK();
  }

  saveNPK(formNPK, action? ) {
    const form = this.form.value;
    this.errorMessages = undefined;

    const formData = new FormData();

    if ( !form.objective ) delete form.objective;
    if ( !form.adjust_inventory ) delete form.adjust_inventory;
    if ( !form.reentry ) delete form.reentry;
    if ( !form.file ) delete form.file;

    // Agregamos npk del formulario NPK
    if (action !== 'delete') {
      const npkAlta = this.formNPK.value;
      if (npkAlta.npk_id) {
        if (form.npks.filter((el) => el.npk_id === npkAlta.npk_id).length === 0) {
          form.npks.push({
            id: npkAlta.npk_id,
            quantity: npkAlta.quantity
          });
        } else {
          this.formNPK.reset();
          this.formNPK.controls['npk_id'].setErrors({'exists': true});
          return;
        }
      }
    }
    formData.append('npks', JSON.stringify(form.npks
      .map(npk => {
        return {id: npk.id ? npk.id : npk.npk_id, quantity: npk.quantity};
      })));
    for ( let i = 0; i < form.varieties.length; i++ ) formData.append('varieties[]', form.varieties[i]);
    for ( let i = 0; i < form.active_ingredients.length; i++ ) formData.append('active_ingredients[]', form.active_ingredients[i]);

    for ( let key in form ) {
      if ( ["varieties", "npks", "active_ingredients"].indexOf(key) == -1 ) formData.append(key, form[key] );
    }

    formData.append('_method', 'PUT');
    this.current.requesting = true;
    this.API.update(MODEL, formData, this.current.id, 'post')
      .subscribe(
        res => {
          if (action === 'delete') {
            this.toaster.pop('success', 'NPK', 'El NPK ha sido eliminado exitosamente');
          } else {
            this.toaster.pop('success', 'NPK', `El NPK ha sido ${ this.currentNPK.id ? 'actualizado' : 'creado' } exitosamente`);
          }
          this.current.requesting = false;
          this.current = res;
          this.createForm('npks');
          this.npkCtrl.setValue('');
          this.currentNPK = {};

          this.paginator.page.emit();
        }, err => {
          if (action === 'delete') {
            this.toaster.pop('error', 'NPK', 'Ha ocurrido un error al eliminar el NPK');
          } else {
            this.toaster.pop('error', 'NPK', `Ha ocurrido un error al ${ this.currentNPK.id ? 'actualizar' : 'crear' } el NPK`);
          }

          this.current.requesting = false;
          this.errorMessages = err.error.errors;
          for (let name in this.errorMessages) {
            this.formNPK.controls[name].setValue('');
          }
        })
  }

  editNPK(npkToEdit) {
    const form = this.form.value;
    this.errorMessages = undefined;
    const formData = new FormData();
    if ( !form.objective ) delete form.objective;
    if ( !form.adjust_inventory ) delete form.adjust_inventory;
    if ( !form.reentry ) delete form.reentry;
    if ( !form.file ) delete form.file;

    const npkForm = form.npks.find( n => n.npk_id === npkToEdit.id);
    npkForm.quantity = npkToEdit.pivot.quantity;
    formData.append('npks', JSON.stringify(form.npks
      .map(npk => {
        return {id: npk.id ? npk.id : npk.npk_id, quantity: npk.pivot ? npk.pivot.quantity : npk.quantity};
      })));
    for ( let i = 0; i < form.varieties.length; i++ ) formData.append('varieties[]', form.varieties[i]);
    for ( let i = 0; i < form.active_ingredients.length; i++ ) formData.append('active_ingredients[]', form.active_ingredients[i]);

    for ( const key in form ) {
      if ( ['varieties', 'npks', 'active_ingredients'].indexOf(key) === -1 ) {
        formData.append(key, form[key] );
      }
    }

    formData.append('_method', 'PUT');
    this.API.update(MODEL, formData, this.current.id, 'post')
      .subscribe(
        () => {
        }, err => {
          this.toaster.pop('error', 'NPK', `Hubo un error al actualizar el NPK`);
          this.current.requesting = false;
          this.errorMessages = err.error.errors;
          for (let name in this.errorMessages) {
            this.formNPK.controls[name].setValue('');
          }
        })
  }

  deleteNPK( npk, index ) {
    this.current.requesting = true;
    const npks = <FormArray>this.form.controls['npks'];
    npks.removeAt(index);
    this.saveNPK( this.form.value, "delete" );
  }

  resetFormIngredient() {
    this.currentIngredient = {};
    this.createFormIngredient();
  }

  saveIngredient( formIngredients, action? ) {
    const form = this.form.value;
    this.errorMessages = undefined;
    this.current.requesting = true;

    const formData = new FormData();
    const ingredientes = [];

    if ( !form.objective ) { delete form.objective; }
    if ( !form.adjust_inventory ) { delete form.adjust_inventory; }
    if ( !form.reentry ) { delete form.reentry; }
    if ( !form.file ) { delete form.file; }

    for ( let i = 0; i < form.varieties.length; i++ ) {
      formData.append('varieties[]', form.varieties[i]);
    }
    formData.append('npks', '[]');
    if (action !== 'delete') {
      // Agrego nuevo ingrediente
      ingredientes.push(formIngredients.name);
    }
    for ( let i = 0; i < form.active_ingredients.length; i++ ) {
      ingredientes.push(form.active_ingredients[i].name);
    }

    formData.append('active_ingredients', JSON.stringify(ingredientes));

    if (ingredientes.length === 0) {
      formData.append('active_ingredients', '[]');
    }

    for ( const key in form ) {
      if ( ['varieties', 'npks', 'active_ingredients'].indexOf(key) === -1 ) {
        formData.append(key, form[key] );
      }
    }

    formData.append('_method', 'PUT');

    this.API.update(MODEL, formData, this.current.id, 'post')
        .subscribe(
          res => {
            if ( action === 'delete' ) {
              this.toaster.pop('success', 'Ingrediente activo', 'El ingrediente activo ha sido eliminado exitosamente');
            } else {
              this.toaster.pop('success', 'Ingrediente activo', `El ingrediente activo ha sido creado exitosamente`);
            }
            this.current.requesting = false;

            this.current = res;
						this.createForm('npks');
					  this.currentIngredient = {};
						this.formIngredient.reset();
          }, err => {
            if ( action === 'delete' ) {
              this.toaster.pop('error', 'Ingrediente activo', 'Ha ocurrido un error al eliminar el ingrediente activo');
            } else {
              this.toaster.pop('error', 'Ingrediente activo', `Ha ocurrido un error al crear } el ingrediente activo`);
            }

            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
						  this.formIngredient.controls[name].setValue('');
						}
          })
  }

  editIngredient( ingredient, index ) {
    this.errorMessages = undefined;
    this.currentIngredient = ingredient;
    this.currentIngredient.index = index;
    this.createFormIngredient();
  }

  deleteIngredient( index ) {
    this.current.requesting = true;
    const ingredients = <FormArray>this.form.controls['active_ingredients'];
    ingredients.removeAt(index);
    this.saveIngredient( this.form.value, 'delete' );
  }

  changeTab( tab ) {
    this.tab = tab;
  }

  download( id ) {
    this.API.custom('get', `api/${MODEL}/${id}/download`)
        .subscribe(
          res => window.open(res.data.file_url),
          err => this.toaster.pop('error', 'Producto', 'Ha ocurrido un error al descargar el archivo')
        );
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }

  updateQuantity(npk, value): void {
    if (isNumeric(value) && npk.pivot) {
      npk.pivot.quantity = value;
    }
  }

  generateExportLink(output: string): string {
    return this.productsService.linkReportLink(output);
  }
}
