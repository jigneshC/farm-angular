import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './../../services/auth/auth.service';

@Component({
  selector: 'az-recover-password',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent {
  public form: FormGroup;
  public email: AbstractControl;
  public message: boolean = false;
  public error: boolean = false;

  constructor(
    private fb: FormBuilder,
    private auth: AuthService
  ) {
    this.form = fb.group({
        'email': ['', Validators.compose([Validators.required, emailValidator])]
    });
    
    this.email = this.form.controls['email'];
  }

  public onSubmit( values:Object ): void {     
    this.error = false;
    this.message = false;
    
    this.auth.recover( this.form.value.email )
    .subscribe(
      response => this.message = true,
      error => this.error = true
    );
  }
}

export function emailValidator(control: FormControl): {[key: string]: any} {
  var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;    
  if (control.value && !emailRegexp.test(control.value)) {
    return { invalidEmail: true };
  }
}
