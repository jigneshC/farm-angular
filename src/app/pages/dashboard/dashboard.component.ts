import {ApiService} from './../../services/api.service';
import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {AppConfig} from "../../app.config";
import {DashboardService} from './dashboard.service';
import * as moment from 'moment';

import DateGMT from 'app/classes/date-gmt';
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'az-dashboard',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DashboardService]
})
export class DashboardComponent implements OnInit {
  public config: any;
  public configFn: any;
  public bgColor: any;
  public date = new Date();
  public weatherData: any;
  public eto: number;
  public etoMax: number;
  public etoMin: number;
  public etoAvg: number;
  public today: any;
  public rain: any;
  public currentDay: any;
  public indexDay: any;
  public daysTranslate = {
    "Sunday": "DOM",
    "Monday": "LUN",
    "Tuesday": "MAR",
    "Wednesday": "MIE",
    "Thursday": "JUE",
    "Friday": "VIE",
    "Saturday": "SAB"
  };
  public iconsTranslate = {
    "WIND": "Viento",
    "CLEAR-DAY": "Día claro",
    "PARTLY-CLOUDY-DAY": "Parcialmente nublado",
    "CLOUDY": "Nublado",
    "RAIN": "Lluvia",
    "SLEET": "Lluvia y nieve",
    "SNOW": "Nieve",
    "FOG": "Niebla"
  };
  public weather$: Observable<any>;
  public weather: any = {
    outTemp: 0
  };

  constructor(private _appConfig: AppConfig, private _dashboardService: DashboardService, private API: ApiService) {
    this.config = this._appConfig.config;
    this.configFn = this._appConfig;
    this.currentDay = _dashboardService.weatherData[0];
  }

  ngOnInit() {
    let today = new Date();
    this.API.specificAction('days', 'eto').subscribe(res => this.eto = res.json().eto);
    this.API.specificAction('days', 'etomax').subscribe((res) => {
      this.etoMax = res.json().max;
    });
    this.API.specificAction('days', 'etomin').subscribe((res) => {
      this.etoMin = res.json().min;
    });
    this.API.specificAction('days', 'etoavg').subscribe((res) => {
      this.etoAvg = res.json().avg;
    });
    this.API.getRain('days', {
      start: moment(today).subtract(30).format('YYYY-MM-DD'),
      end: moment(today).format('YYYY-MM-DD')
    }).subscribe(
      (res) => {
        this.rain = res.json().rain;
      },
      (err) => {
        console.log('error', err);
      });
    this.weather$ = this._dashboardService.getWeatherData();
    this.weather$.subscribe((res) => {
        this.weather = res;
      },
      (err) => {
        console.log('error', err);
      });
  }
}
