import { Injectable } from '@angular/core';
import {WeatherService} from "../../services/api/weather/weather.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class DashboardService {

  constructor(private weather: WeatherService) {
  }

  public weatherData = [
      { day: 'Sunday', icon: 'CLEAR-DAY', degree: '18° / 22°'},
      { day: 'Monday', icon: 'PARTLY-CLOUDY-DAY', degree: '14° / 16°'},
      { day: 'Tuesday', icon: 'CLOUDY', degree: '8° / 12°'},
      { day: 'Wednesday', icon: 'RAIN', degree: '4° / 6°'},
      { day: 'Thursday', icon: 'SLEET', degree: '-1° / 3°'},
      { day: 'Friday', icon: 'SNOW', degree: '-3° / -1°'},
      { day: 'Saturday', icon: 'FOG', degree: '-1° / 2°'}
  ];

  public getWeatherData(): Observable<any> {
    return this.weather.getLastUpdate();
  }
}
