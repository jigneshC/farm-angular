import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';
import { TimeCalculatorService } from "../../../services/time-calculator.service";

import DateGMT from 'app/classes/date-gmt';

const MODEL = "overtimehours";

@Component({
  selector: 'az-overtimehours',
  templateUrl: './overtimehours.component.html',
  styleUrls: ['./overtimehours.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class OvertimehoursComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['employee_id', 'costcenter_id', 'from', 'quantity', 'notes', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  filtersForm1: FormGroup;
  filtersForm2: FormGroup;
  employeeCtrl: FormControl = new FormControl("");
  costCenterCtrl: FormControl = new FormControl("");
  filterEmployeeCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  employees;
  employeeSub;
  costCenters;
  costCenterSub;
  filterEmployees;
  filterEmployeeSub;
	lastYears: Array<any> = [];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public timeService: TimeCalculatorService,
  ) {
    this.createForm();
    this.createFiltersForm1();
    this.createFiltersForm2();
		this.getLastYears();
  }

	getLastYears() {
		let params = new HttpParams();
		params = params.append('p', `1`);
		params = params.append('sort_dir', `asc`);

		this.API.index(MODEL, params)
        .subscribe(
          res => {
						let first = res.data[0];
						if ( first ) {
							this.setLastYears(first.from)
						} else this.setLastYears();
          }, err => {
						this.setLastYears();
          })
	}

	setLastYears( date? ) {
		let start = moment(new Date(date || "2010-01-02"));
    let end = moment();
    do {
        this.lastYears.push(start.year());
        start.add(1, 'y');
    } while (start.year() <= end.year());

		this.createFiltersForm1();
	}

  createForm() {
    this.errorMessages = undefined;
    this.employeeCtrl = new FormControl(this.current.employee ? this.current.employee.name : "");
    this.employeeSub = this.employeeCtrl.valueChanges.startWith(this.current.employee ? this.current.employee.name : "").subscribe(q => this.searchAutoComplete(q, 'employees', 'employees'));
    this.costCenterCtrl = new FormControl(this.current.costcenter ? this.current.costcenter.name : "");
    this.costCenterSub = this.costCenterCtrl.valueChanges.startWith(this.current.costcenter ? this.current.costcenter.name : "").subscribe(q => this.searchAutoComplete(q, 'costCenters', 'costcenters'));

    this.form = this.fb.group({
      employee_id: [ this.current.employee_id || '', [ Validators.required, CustomValidators.number ] ],
      costcenter_id: [ this.current.costcenter_id || '', [ CustomValidators.number ] ],
      from: [ this.current.from || '', [ Validators.required, CustomValidators.date ] ],
      quantity: [ this.current.quantity || '', [ Validators.required, CustomValidators.number ] ],
      notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
    });
  }

  createFiltersForm1() {
    this.filtersForm1 = this.fb.group({
      month: new Date().getMonth(),
      year: this.lastYears[this.lastYears.length - 1] || "2017"
    });
  }

  createFiltersForm2() {
    this.filterEmployeeCtrl = new FormControl("");
    this.filterEmployeeSub = this.filterEmployeeCtrl.valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, 'filterEmployees', 'employees'));

    this.filtersForm2 = this.fb.group({
      employee_id: '',
      from: '',
      to: '',
    });
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    this.current = row;
    this.current.from = DateGMT(row.from);
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  resetAutocomplete( form, control, ctrl, variable, model ) {
    this[ctrl] = new FormControl("");
    this[ctrl].valueChanges.startWith("").subscribe(q => this.searchAutoComplete(q, variable, model));
    this[form].controls[control].setValue('');
  }

  searchAutoComplete( q, variable, model ) {
    if ( [ "employees", "costcenters" ].indexOf(model) > -1 && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( [ "employees", "costcenters" ].indexOf(model) > -1 && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    form.from = this.dateTransform.transform(form.from, "y-MM-dd");

    if ( !form.costcenter_id ) delete form.costcenter_id;

    this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Horas Extra', `Las horas extra ha sido ${ this.current.id ? "actualizado" : "creada" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						jQuery('#form-modal').modal('toggle');
          }, err => {
						this.toaster.pop('error', 'Horas Extra', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } las horas extra`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Horas Extras', 'Las horas extra ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Horas Extras', 'Ha ocurrido un error al eliminar las horas extra');
          })
  }

  toggleFilters() {
    jQuery("#filters").slideToggle();
  }

  resetFilters() {
    this.createFiltersForm2();
    this.reload();
  }

  buildFilters(value, form?) {
    this.reload();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          if ( this.filtersForm1.value.month && this.filtersForm1.value.year ) {
            let date = new Date( this.filtersForm1.value.year, this.filtersForm1.value.month ),
                from = new Date(date.getFullYear(), date.getMonth(), 1),
                to = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            params = params.append('from', this.dateTransform.transform(from, "y-MM-dd"));
            params = params.append('to', this.dateTransform.transform(to, "y-MM-dd"));
          }

          if ( this.filtersForm2.value.employee_id || this.filtersForm2.value.from || this.filtersForm2.value.to ) {

            params = params.delete("from");
            params = params.delete("to");

            if ( this.filtersForm2.value.employee_id ) params = params.append('by_employee', this.filtersForm2.value.employee_id);
            if ( this.filtersForm2.value.from ) params = params.append('from', this.dateTransform.transform(this.filtersForm2.value.from, "y-MM-dd"));
            if ( this.filtersForm2.value.to ) params = params.append('to', this.dateTransform.transform(this.filtersForm2.value.to, "y-MM-dd"));
          }

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
