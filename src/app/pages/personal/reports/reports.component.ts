import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

import * as moment from 'moment';
import {EmployeeService} from '../../../services/api/personel/employee.service';

@Component({
  selector: 'az-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  years = [];
  months = ['january', 'febrary', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

  filtersForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private employeeService: EmployeeService
  ) { }

  ngOnInit() {
    const date = new Date();
    const currentYear = date.getFullYear();
    for (let i = 0; i <= 5; i++) {
      this.years.push(currentYear - i);
    }
    this.createFiltersForm();
  }

  generarReporte(form: any): string {
    const year = form.year;
    const month = form.month;
    return this.employeeService.generateLinkReport(month, year);
  }

  generarReporteNow(): string {
    const date = new Date();
    const year = date.getFullYear();
    const month = this.months[date.getMonth()];
    return this.employeeService.generateLinkReport(month, year.toString());
  }

  createFiltersForm() {
    this.filtersForm = this.fb.group({
      month: this.months[new Date().getMonth()],
      year: new Date().getFullYear()
    });
  }

}
