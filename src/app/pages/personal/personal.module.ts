import {CrudComponentsModule} from './../../theme/components/crud-components/crud-components.module';
import {SelectHelperService} from './../../services/select-helper.service';
import {ApiService} from './../../services/api.service';
import {EmployeesComponent} from './employees/employees.component';
import {SalariesComponent} from './salaries/salaries.component';
import {OvertimehoursComponent} from './overtimehours/overtimehours.component';
import {AbsencesComponent} from './absences/absences.component';
import {LoansComponent} from './loans/loans.component';
import {VacationsComponent} from './vacations/vacations.component';
import {MatPaginatorLabels} from './../../services/mat-paginator-labels.service';
import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DirectivesModule} from '../../theme/directives/directives.module';
import {PipesModule} from '../../theme/pipes/pipes.module';
import {DateTimePickerModule} from 'ng-pick-datetime';
import {CustomFormsModule} from 'ng2-validation'
import {
  MatAutocompleteModule, MatDialogModule, MatInputModule, MatPaginatorIntl, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule, MatTooltipModule
} from '@angular/material';
import {LiquidationsComponent} from './liquidations/liquidations.component';
import {LiquidationService} from '../../services/api/personel/liquidation.service';
import {LiquidationFormComponent} from './liquidations/liquidation-form/liquidation-form.component';
import {LiquidationsFilterComponent} from './liquidations/liquidations-filter/liquidations-filter.component';
import {EmployeeService} from '../../services/api/personel/employee.service';
import {CostCenterService} from '../../services/api/personel/cost-center.service';
import {ComponentsModule} from '../../theme/components/components.module';
import { LiquidationGenerateFormComponent } from './liquidations/liquidation-generate-form/liquidation-generate-form/liquidation-generate-form.component';
import { ReportsComponent } from './reports/reports.component';

export const routes = [
  { path: 'empleados', component: EmployeesComponent, data: { breadcrumb: 'Empleados' } },
  { path: 'sueldos', component: SalariesComponent, data: { breadcrumb: 'Sueldos' } },
  { path: 'horas-extras', component: OvertimehoursComponent, data: { breadcrumb: 'Horas extras' } },
  { path: 'ausencias', component: AbsencesComponent, data: { breadcrumb: 'Ausencias' } },
  { path: 'prestamos', component: LoansComponent, data: { breadcrumb: 'Préstamos' } },
  { path: 'vaciones', component: VacationsComponent, data: { breadcrumb: 'Vaciones' } },
  { path: 'liquidaciones', component: LiquidationsComponent, data: { breadcrumb: 'Liquidaciones' } },
  { path: 'reportes', component: ReportsComponent, data: { breadcrumb: 'Reportes' } }
];

@NgModule({
  entryComponents: [LiquidationFormComponent, LiquidationGenerateFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    ComponentsModule,
    PipesModule,
    FormsModule,
    ReactiveFormsModule,
    CrudComponentsModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDialogModule,
    DateTimePickerModule,
    CustomFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
   EmployeesComponent,
   SalariesComponent,
   OvertimehoursComponent,
   AbsencesComponent,
   LoansComponent,
   VacationsComponent,
   LiquidationsComponent,
   LiquidationFormComponent,
   LiquidationsFilterComponent,
   LiquidationGenerateFormComponent,
   ReportsComponent,
  ],
  providers: [
    ApiService,
    SelectHelperService,
    EmployeeService,
    CostCenterService,
    LiquidationService,
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorLabels
    },
    DatePipe,
  ]
})

export class PersonalModule { }
