import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import DateGMT from 'app/classes/date-gmt';

const MODEL = "salaries";

@Component({
  selector: 'az-salaries',
  templateUrl: './salaries.component.html',
  styleUrls: ['./salaries.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class SalariesComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['employee_id', 'date', 'base_salary', 'net_salary', 'family_responsibilities', 'advance_amount', 'notes', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  employeeCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  employees;
  employeeSub;
  types = [ { value: 0, name: 'PLAZO FIJO' }, { value: 1, name: 'INDEFINIDO' } ];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
  ) {
    this.createForm();
  }

  createForm() {
    this.errorMessages = undefined;
    this.employeeCtrl = new FormControl(this.current.employee ? this.current.employee.name : "");
    this.employeeSub = this.employeeCtrl.valueChanges.startWith(this.current.employee ? this.current.employee.name : "").subscribe(q => this.searchAutoComplete(q, 'employees', 'employees'));

    this.form = this.fb.group({
      employee_id: [ this.current.employee_id || '', [ Validators.required, CustomValidators.number ] ],
      date: [ this.current.date || '', [ Validators.required, CustomValidators.date ] ],
      net_salary: [ this.current.net_salary || '', [ Validators.required, CustomValidators.number ] ],
      base_salary: [ this.current.base_salary || '', [ Validators.required, CustomValidators.number ] ],
      family_responsibilities: [ this.current.family_responsibilities || '', [ Validators.required, CustomValidators.number ] ],
      advance_amount: [ this.current.advance_amount || '', [ CustomValidators.number ] ],
      mobilization_amount: [ this.current.mobilization_amount || '', [ CustomValidators.number ] ],
      collaction_amount: [ this.current.collaction_amount || '', [ CustomValidators.number ] ],
      notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
      type: [ this.current.type || 0, [ Validators.required, CustomValidators.number ] ],
    });
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    this.current = row;
    this.current.date = DateGMT(row.date);
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( model == "employees" && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model == "employees" && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    form.date = this.dateTransform.transform(form.date, "y-MM-dd");

    if ( !form.advance_amount ) delete form.advance_amount;
    if ( !form.mobilization_amount ) delete form.mobilization_amount;
    if ( !form.collaction_amount ) delete form.collaction_amount;

    this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Sueldo', `El sueldo ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						jQuery('#form-modal').modal('toggle');
          }, err => {
						this.toaster.pop('error', 'Sueldo', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el sueldo`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Sueldo', 'El sueldo ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Sueldo', 'Ha ocurrido un error al eliminar el sueldo');
          })
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
