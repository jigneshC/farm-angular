import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material';
import {LiquidationService} from '../../../../../services/api/personel/liquidation.service';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'az-liquidation-generate-form',
  templateUrl: './liquidation-generate-form.component.html',
  styleUrls: ['./liquidation-generate-form.component.scss']
})
export class LiquidationGenerateFormComponent implements OnInit {

  public form: FormGroup;
  public years: number[]= [];
  public isLoading = false;
  public errorMessages = [];

  constructor(protected toaster: ToasterService, protected liquidationService: LiquidationService, protected formBuilder: FormBuilder, protected dialogRef: MatDialogRef<LiquidationGenerateFormComponent>) {
  }

  ngOnInit() {
    const date = new Date();
    const currentYear = date.getFullYear();
    for (let i = 0; i < 3; i++) {
      this.years.push(currentYear - i);
    }
    this.form = this.formBuilder.group({
      selectedMonth: [date.getMonth() + 1, [Validators.required]],
      selectedYear: [date.getFullYear(), [Validators.required]]
    });
  }

  generateLink(): string {
    return this.liquidationService.linkGenerateLiquidation(this.form.get('selectedMonth').value, this.form.get('selectedYear').value);
  }

  close() {
    this.dialogRef.close();
  }

}
