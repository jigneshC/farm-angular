import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {EmployeeService} from '../../../../services/api/personel/employee.service';
import {Employee} from '../../../../models/liquidation';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DateValidators} from '../../../../utils/validators';
import {TranslateService} from '../../../../services/translate.service';
import * as moment from 'moment';

@Component({
  selector: 'az-liquidations-filter',
  templateUrl: './liquidations-filter.component.html',
  styleUrls: ['./liquidations-filter.component.scss']
})
export class LiquidationsFilterComponent implements OnInit {

  @Output()
  public onSubmit = new EventEmitter<any>();

  public filtersForm: FormGroup;
  public employees: Employee[] = [];

  constructor(protected restService: EmployeeService, protected fb: FormBuilder, public translate: TranslateService) {
    this.filtersForm = this.fb.group({
      employee: ['', []],
      fechaDesde: null,
      fechaHasta: null,
      status: null,
    }, {
      validator: Validators.compose([
        DateValidators.dateLessThan('fechaDesde', 'fechaHasta', {'dateLess': true}),
        // En rango de 360 dias.
        DateValidators.dateInRangeOf('fechaDesde', 'fechaHasta', 360, {'dateRange': true})
      ])
    });
  }

  ngOnInit() {
    this.restService.findAll()
      .subscribe( emp => {
        this.employees = emp;
      })
  }

  employeeName(employee: Employee) {
    return employee ? employee.name : '';
  }

  resetFilters() {
    this.filtersForm.reset();
  }

  /**
   * Construye los parametros para el pedido http.
   * @returns {any}
   */
  toParams(): any {
    const formValue = this.filtersForm.value;
    const fechaDesde = formValue.fechaDesde;
    const fechaHasta = formValue.fechaHasta;
    const employee = formValue.employee;
    const active = formValue.status;
    const result = {};
    if (fechaDesde) {
      result['from'] = moment(fechaDesde).format('YYYY-MM-DD');
    }
    if (fechaHasta) {
      result['to'] = moment(fechaHasta).format('YYYY-MM-DD');
    }
    if (employee) {
      result['employee'] = employee.id;
    }
    if (active != null) {
      result['status'] = active;
    }
    return result;
  }

}
