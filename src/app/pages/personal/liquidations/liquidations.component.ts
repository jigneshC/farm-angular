import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {LiquidationService} from '../../../services/api/personel/liquidation.service';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {Employee, Liquidation, LiquidationModel} from '../../../models/liquidation';
import {ToasterService} from 'angular2-toaster';
import {ISubscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/mergeAll';
import {LiquidationFormComponent} from './liquidation-form/liquidation-form.component';
import * as moment from 'moment';
import {LiquidationsFilterComponent} from './liquidations-filter/liquidations-filter.component';
import {
  EmployeeDialogComponent,
  EmployeeDialogModel
} from '../../../theme/components/employee-dialog/employee-dialog/employee-dialog.component';
import {CostCenterService} from '../../../services/api/personel/cost-center.service';
import {LiquidationGenerateFormComponent} from './liquidation-generate-form/liquidation-generate-form/liquidation-generate-form.component';
@Component({
  selector: 'az-liquidations',
  templateUrl: './liquidations.component.html',
  styleUrls: ['./liquidations.component.scss']
})
export class LiquidationsComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(LiquidationsFilterComponent)
  public filters: LiquidationsFilterComponent;
  @ViewChild(MatSort)
  public sort: MatSort;
  public tableData: MatTableDataSource<any>;

  public isLoading = false;
  public filter: string;
  public error: string;
  public displayedColumns: Array<string> = [];
  public subscriptions: ISubscription[] = [];

  constructor(protected costCenterService: CostCenterService,  protected restService: LiquidationService, protected toaster: ToasterService, protected  dialog: MatDialog) { }

  ngOnInit() {
    this.tableData = new MatTableDataSource<any>();
    this.getData();
  }

  public getData(includeParams?: boolean) {
    this.liquidationsQuery(includeParams ? this.buildQueryParams() : null)
      .subscribe((data: any) => {
          this.isLoading = false;
          this.updateData(data);
        }, error => {
          this.isLoading = false;
          this.toaster.pop('error', 'Liquidaciones', `Ha ocurrido un error al obtener las liquidaciones.`)
        }
      )
  }

  protected updateData(result: LiquidationModel[]) {
    this.displayedColumns = [];
    if (result.length > 0) {
      this.displayedColumns = [];
      const model = result[0];
      // Mapeamos las columnas viendo el primer elemento como ejemplo.
      this.displayedColumns.push('name');
      this.displayedColumns.push('status');
      for (const liquidation of model.liquidations) {
        this.displayedColumns.push(liquidation.date);
      }
      this.tableData.data = result;
    } else {
      this.tableData.data = [];
    }
  }

  protected buildQueryParams(): any {
    const params = <any> {};
    if (this.sort.active) {
      params.sort_by = this.sort.active;
    }
    if (this.sort.direction) {
      params.sort_dir = this.sort.direction;
    }
    if (this.isFilterVisible()) {
      const extraParameters = this.filters.toParams();
      // Merge...
      Object.assign(params, extraParameters);
    }
    return params;
  }

  uploadLiquidation(employee: Employee, liquidation: Liquidation): any {
    const fecha = moment(liquidation.date).toDate();
    const data = {
      employee: employee,
      year: fecha.getFullYear(),
      month: fecha.getMonth() + 1
    };
    const form = this.dialog.open(LiquidationFormComponent, {
      width: '800px',
      data
    });
    this.subscriptions.push(form.componentInstance.afterSave.subscribe((result) => {
        if (result === 'success') {
          this.getData(true);
        }
      })
    );
  }

  downloadLiquidation(employee: Employee, liquidation: Liquidation): void {
    const fecha = moment(liquidation.date).toDate();
    this.restService.getFile(String(employee.id), String(fecha.getMonth() + 1), String(fecha.getFullYear()))
      .subscribe(
        res => window.open(res.data.file_url).opener = null,
        errr => this.toaster.pop('error', 'Liquidaciones', `Ha ocurrido un error al descargar la liquidación.`)
      )
  }

  liquidationsQuery(params?): Observable<any> {
    this.isLoading = true;
    return this.restService
      .findAll(params)
  }

  employeeDialog(employee: Employee) {
    const model = {
      employee: employee,
      costCenter: this.costCenterService.findOne(String(employee.costcenter_id))
    };
    this.dialog.open(EmployeeDialogComponent, {
      width: '600px',
      data: model
    });
  }

  ngAfterViewInit(): void {
    this.subscriptions.push(this.sort.sortChange
      .switchMap(() => {
        return this.liquidationsQuery(this.buildQueryParams());
      })
      .subscribe((result) => {
          this.isLoading = false;
          this.updateData(result);
        },
        err => {
          this.isLoading = false;
          this.toaster.pop('error', 'Liquidaciones', `Ha ocurrido un error al buscar las liquidaciones.`);
        }));
  }

  isFilterVisible() {
    return jQuery('#filters').is(':visible');
  }

  toggleFiltersIfVisible() {
    if (this.isFilterVisible()) {
      jQuery('#filters').slideToggle();
    }
  }

  toggleFilters() {
    jQuery('#filters').slideToggle();
  }

  ngOnDestroy(): void {
    for (const subscription of this.subscriptions) {
      if (subscription && !subscription.closed) {
        subscription.unsubscribe();
      }
    }
  }

  generarLiquidacion() {
    this.dialog.open(LiquidationGenerateFormComponent, {
      width: '500px'
    });
  }

}
