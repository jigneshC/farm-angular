import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {capitalize, isExtensionImage, toFormData} from '../../../../utils/utils';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToasterService} from 'angular2-toaster';
import {LiquidationService} from '../../../../services/api/personel/liquidation.service';
import {Employee} from '../../../../models/liquidation';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'az-liquidation-form',
  templateUrl: './liquidation-form.component.html',
  styleUrls: ['./liquidation-form.component.scss']
})
export class LiquidationFormComponent implements OnInit {

  public title: string;
  public form: FormGroup;
  public errors: any = {};
  public years: number[] = [];
  public errorMessages = [];
  public isLoading = false;

  @Output() afterSave = new EventEmitter<string>(true);

  protected employee: Employee;

  constructor(protected dialogRef: MatDialogRef<any>,
              protected formBuilder: FormBuilder,
              protected liquidationService: LiquidationService,
              protected toaster: ToasterService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.employee = this.data.employee;
    this.title = `LIQUIDACIÓN FIRMADA PARA ${this.employee.name}`;
  }

  ngOnInit() {
    this.initYearData();
    this.form = this.formBuilder.group({
      file: ['', [Validators.required]],
      selectedMonth: [this.data.month || 1, [Validators.required]],
      selectedYear: [this.data.year || 1, [Validators.required]]
    });
  }

  submit() {
    this.isLoading = true;
    const year = this.form.get('selectedYear').value;
    const month = this.form.get('selectedMonth').value;
    this.liquidationService.postFile(this.buildEntityForSubmit(),
      String(this.employee.id), month, year)
      .subscribe(
        res => {
          this.isLoading = false;
          this.afterSave.emit('success');
          this.dialogRef.afterClosed().subscribe(() => {
            this.toaster.pop('success', '', 'Liquidación subida exitosamente.');
          });
          this.dialogRef.close();
        },
        error => {
          this.isLoading = false;
          this.handleError(error);
        });
  }

  protected buildEntityForSubmit(): any {
    const entity = {file: this.form.get('file').value};
    return toFormData(entity);
  }

  protected initYearData(): void {
    const currentYear = new Date().getFullYear();
    for (let i = 0; i < 3; i++) {
      this.years.push(currentYear - i);
    }
  }

  protected handleError(error: HttpErrorResponse) {
    if (error) {
      const errorObj = error.error;
      this.errorMessages = ['Ha ocurrido un error al intentar guardar.'];
      if (errorObj.errors) {
        this.errors = errorObj.errors;
      }
    }
  }

  public addedFile($event): void {
    const formControl = this.form.get('file');
    formControl.setErrors(null);
    formControl.setValue($event.target.files[0]);
  }

  close(): void {
    this.dialogRef.close();
  }

}
