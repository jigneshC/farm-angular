import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import {DateValidators} from '../../../utils/validators';

import DateGMT from 'app/classes/date-gmt';

const MODEL = "employees";

@Component({
  selector: 'az-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class EmployeesComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('contractFileInput') contractFileInput;
  @ViewChild('firedFileInput') firedFileInput;

  columns = ['name', 'status', 'tax_id', 'birth_date', 'contract_date', 'fired_date', 'labor', 'costcenter_id', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  costCenterCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  AFP: any;
  ISAPRE: any;
  isapreInfo;
  errorMessages;
  afpInfo;
  costCenters;
  costCenterSub;
  states = [ { value: 0, name: 'INACTIVO' }, { value: 1, name: 'ACTIVO' } ];

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
  ) {
    this.createForm();
  }

  createForm() {
    if ( this.contractFileInput ) this.contractFileInput.nativeElement.value = '';
    if ( this.firedFileInput ) this.firedFileInput.nativeElement.value = '';
    this.errorMessages = undefined;
    this.costCenterCtrl = new FormControl(this.current.costcenter ? this.current.costcenter.name : "");
    this.costCenterSub = this.costCenterCtrl.valueChanges.startWith(this.current.costcenter ? this.current.costcenter.name : "").subscribe(q => this.searchAutoComplete(q, 'costCenters', 'costcenters'));

    this.form = this.fb.group({
      name: [ this.current.name || '', [ Validators.required, Validators.maxLength(255) ] ],
      status: [ this.current.status == 0 ? 0 : 1, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      tax_id: [ this.current.tax_id || '', [ Validators.required, DateValidators.validateRut ] ],
      birth_date: [ this.current.birth_date || '', [ Validators.required, CustomValidators.date ] ],
      contract_date: [ this.current.contract_date || '', [ Validators.required, CustomValidators.date ] ],
      fired_date: [ this.current.fired_date || '', [ CustomValidators.date ] ],
      labor: [ this.current.labor || '', [ Validators.required, Validators.maxLength(255) ] ],
      costcenter_id: [ this.current.costcenter_id || '', [ Validators.required, CustomValidators.number ] ],
      afp: [ this.current.afp || '', [ Validators.required, CustomValidators.number ] ],
      isapre: [ this.current.isapre || '', [ Validators.required, CustomValidators.number ] ],
      contract_file: [ '', this.current.id ? [ ] : [ Validators.required ] ],
      fired_file: [ '' ],
    });

    this.form.valueChanges.subscribe( form => {
      let firedDateCtrl = this.form.controls['fired_date'];
      let firedFileCtrl = this.form.controls['fired_file'];
      if ( form.status == 0 ) {
        if ( !form.fired_date && this.current.id ) firedDateCtrl.setErrors({'required': true});
        if ( !form.fired_file && this.current.id && !this.current.fired_date ) firedFileCtrl.setErrors({'required': true});
      }

      if ( form.status > 0 ) {
        let err = null
        if ( firedDateCtrl.errors && firedDateCtrl.errors.date ) err = { date: firedDateCtrl.errors.date };
        firedDateCtrl.setErrors(err);
        firedFileCtrl.setErrors(null);
      }
    });

    this.getAFPInfo();
    this.getISAPREInfo();
  }

  getAFPInfo(){
    let params = {
      name: 'AFP'
    }
    this.API.index('configs', params).subscribe(res => {
      this.AFP = JSON.parse(res.data[0].value);
    });
  }

  getISAPREInfo(){
    let params = {
      name: 'ISAPRE'
    }
    this.API.index('configs', params).subscribe(res => {
      this.ISAPRE = JSON.parse(res.data[0].value);
    });
  }

  addedFile( $event, control ) {
    if ( $event.target.files[0].type != "application/pdf" ) {
      $event.target.value = '';
      this.form.controls[control].setValue(null);
      return this.form.controls[control].setErrors({ 'type': true });
    }
    this.form.controls[control].setErrors(null);
    this.form.controls[control].setValue($event.target.files[0]);
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    this.current = row;
    row.birth_date = DateGMT(row.birth_date);
    row.contract_date = DateGMT(row.contract_date);
    if ( row.fired_date ) row.fired_date = DateGMT(row.fired_date);
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( model == "costcenters" && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model == "costcenters" && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    form.birth_date = this.dateTransform.transform(form.birth_date, "y-MM-dd");
    form.contract_date = this.dateTransform.transform(form.contract_date, "y-MM-dd");
    if ( form.fired_date ) form.fired_date = this.dateTransform.transform(form.fired_date, "y-MM-dd");

    if ( method == "save" ) {
      delete form.fired_file;
      delete form.fired_date;
    }

    if ( method == "update" ) {
      if ( !form.contract_file ) delete form.contract_file;
      if ( form.status == 1 ) {
        delete form.fired_date;
        delete form.fired_file;
      }
    }

    const formData = new FormData();

    for ( let key in form ) formData.append(key, form[key] );

    if ( method == "update" ) formData.append('_method', 'PUT');

    this.API[method](MODEL, formData, this.current.id, 'post')
        .subscribe(
          res => {
						this.toaster.pop('success', 'Empleado', `El empleado ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						jQuery('#form-modal').modal('toggle');
          }, err => {
						this.toaster.pop('error', 'Empleado', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el empleado`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
							if ( name == 'fired_file' && this.firedFileInput ) this.firedFileInput.nativeElement.value = '';
							if ( name == 'contract_file' && this.contractFileInput ) this.contractFileInput.nativeElement.value = '';
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Empleado', 'El empleado ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Empleado', 'Ha ocurrido un error al eliminar el empleado');
          })
  }

  download( id, file ) {
    this.API.custom('get', `api/${MODEL}/${id}/${file}`)
        .subscribe(
          res => window.open(res.data.file_url),
          err => this.toaster.pop('error', 'Empleado', 'Ha ocurrido un error al descargar el archivo')
        );
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
