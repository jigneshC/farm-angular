import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import DateGMT from 'app/classes/date-gmt';

const MODEL = "loans";

@Component({
  selector: 'az-loans',
  templateUrl: './loans.component.html',
  styleUrls: ['./loans.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class LoansComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  columns = ['employee_id', 'loan_date', 'amount', 'payments', 'notes', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  formPayment: FormGroup;
  employeeCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  currentPayment: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  employees;
  employeeSub;
  banks = [ { value: 0, name: 'BANCO SANTANDER' }, { value: 1, name: 'BANCO DE CHILE' }, { value: 2, name: 'BANCO BCI' } ];
  currencies = [ { value: 0, name: 'PESOS' }, { value: 1, name: 'USD' } ];
  reasons = [ { value: 0, name: 'CAPITAL DE TRABAJO' }, { value: 1, name: 'SOLUCION PROBLEMA LIQUIDEZ' }, { value: 2, name: 'INVERSION' } ];
  states = [ { value: 0, name: 'SIN PAGAR' }, { value: 1, name: 'PAGADO' } ];
  tab: string = "details";
  payments: FormArray;

  constructor(
    private toaster: ToasterService,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
  ) {
    this.createForm();
    this.createFormPayment();
  }

  createForm() {
    this.errorMessages = undefined;
    this.employeeCtrl = new FormControl(this.current.employee ? this.current.employee.name : "");
    this.employeeSub = this.employeeCtrl.valueChanges.startWith(this.current.employee ? this.current.employee.name : "").subscribe(q => this.searchAutoComplete(q, 'employees', 'employees'));

    this.form = this.fb.group({
      employee_id: [ this.current.employee_id || '', [ Validators.required, CustomValidators.number ] ],
      loan_date: [ this.current.loan_date || '', [ Validators.required, CustomValidators.date ] ],
      amount: [ this.current.amount || '', [ Validators.required, CustomValidators.number, CustomValidators.max(999999999) ] ],
      notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
      payments: this.fb.array([])
    });
  }

  get formPayments() {
    return <FormArray>this.form.get('payments');
  }

  createFormPayment() {
    this.errorMessages = undefined;

    this.formPayment = this.fb.group({
      loan_id: [ this.current.id, [ Validators.required, CustomValidators.number ] ],
      due_date: [ this.currentPayment.due_date ? DateGMT(this.currentPayment.due_date) : '', [ Validators.required, CustomValidators.date ] ],
      paid_date: [ this.currentPayment.paid_date ? DateGMT(this.currentPayment.paid_date) : '', [ CustomValidators.date ] ],
      state: [ this.currentPayment.state || 0, [ Validators.required, CustomValidators.number, CustomValidators.range([0, 1]) ] ],
      amount: [ this.currentPayment.amount || '', [ Validators.required, CustomValidators.number ] ],
      notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
    });
  }

  next() {
    jQuery('#topayments0').click();
    this.tab = 'payments';
    this.payments = <FormArray>this.form.controls['payments'];
    for ( let i = 0; i < this.current.totalpayments; i++ ) {
      let payment = this.fb.group({
          amount: [ "", [ Validators.required, CustomValidators.number ] ],
          due_date: [ "", [ Validators.required, CustomValidators.date ] ],
      });
      this.payments.push(payment);
    }
  }

  prev() {
    jQuery('#todetails0').click();
    this.tab = 'details';
    while ( this.payments.length !== 0 ) {
      this.payments.removeAt(0)
    }
  }

  create() {
    jQuery('#todetails0').click();
    this.tab = 'details';
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    jQuery('#todetails2').click();
    this.tab = 'details';
    this.current = row;
    this.current.loan_date = DateGMT(row.loan_date);
    this.createForm();
    this.createFormPayment();
  }

  show( row: any ) {
    jQuery('#todetails1').click();
    this.tab = 'details';
    this.current = row;
    this.current.loan_date = DateGMT(row.loan_date);
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( model == "employees" && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model == "employees" && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    if ( form.loan_date ) form.loan_date = this.dateTransform.transform(form.loan_date, "y-MM-dd");
    else delete form.loan_date;

    if ( form.payments && form.payments.length ) {
      for ( let payment of form.payments ) {
        payment.due_date = this.dateTransform.transform(payment.due_date, "y-MM-dd");
      }
    }

    console.log(form.notes)

    this.API[method](MODEL, form, this.current.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Préstamo', `El préstamo ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;

						if ( this.current.id ) {
              jQuery('#topayments2').click();
              this.tab = 'payments';
						} else {
						  jQuery('#create-modal').modal('toggle');
						  this.paginator.pageIndex = 0;
						}

						this.paginator.page.emit();
          }, err => {
						this.toaster.pop('error', 'Préstamo', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el préstamo`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
						  if ( name != "payments" ) this.form.controls[name].setValue('');
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Préstamo', 'El préstamo ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Préstamo', 'Ha ocurrido un error al eliminar el préstamo');
          })
  }

  resetFormPayment() {
    this.currentPayment = {};
    this.createFormPayment();
  }

  savePayment(form) {
    if ( !this.formPayment.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.currentPayment.id ) method = "update";

    if ( form.paid_date ) form.paid_date = this.dateTransform.transform(form.paid_date, "y-MM-dd");
    else delete form.paid_date;

    form.due_date = this.dateTransform.transform(form.due_date, "y-MM-dd");

    this.API[method]('loanpayments', form, this.currentPayment.id)
        .subscribe(
          res => {
						this.toaster.pop('success', 'Pago', `El pago ha sido ${ this.currentPayment.id ? "actualizado" : "creado" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						delete res.bank_loan;

						if ( this.currentPayment.id ) {
						  this.current.payments[this.currentPayment.index] = res;
						} else this.current.payments.unshift(res);

						this.currentPayment = {};
						this.createFormPayment();
          }, err => {
						this.toaster.pop('error', 'Pago', `Ha ocurrido un error al ${ this.currentPayment.id ? "actualizar" : "crear" } el pago`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
						  this.formPayment.controls[name].setValue('');
						}
          })
  }

  editPayment( payment, index ) {
    this.errorMessages = undefined;
    payment.due_date = DateGMT(payment.due_date);
    if ( payment.paid_date ) payment.paid_date = DateGMT(payment.paid_date);
    this.currentPayment = payment;
    this.currentPayment.index = index;
    this.createFormPayment();
  }

  deletePayment( payment, index ) {
    this.current.requesting = true;
    this.API.delete('loanpayments', payment.id)
        .subscribe(
          res => {
            this.current.requesting = false;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Pago', 'El pago ha sido eliminado exitosamente');
            this.current.payments.splice(index, 1);
          }, err => {
            this.current.requesting = false;
            this.toaster.pop('error', 'Pago', 'Ha ocurrido un error al eliminar el pago');
          })
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
