import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';
import { ApiService } from './../../../services/api.service';
import { TranslateService } from './../../../services/translate.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

import * as moment from 'moment';
import { TimeCalculatorService } from "../../../services/time-calculator.service";

import DateGMT from 'app/classes/date-gmt';

const MODEL = "vacations";

@Component({
  selector: 'az-vacations',
  templateUrl: './vacations.component.html',
  styleUrls: ['./vacations.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class VacationsComponent implements AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('fileInput') fileInput;

  columns = ['employee_id', 'date', 'number_of_days', 'amount', 'season_id', 'notes', 'actions'];
  source = new MatTableDataSource();
  form: FormGroup;
  employeeCtrl: FormControl = new FormControl("");
  seasonCtrl: FormControl = new FormControl("");
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  current: any = {};
  filter: string;
  timeout: any;
  errorMessages;
  employees;
  employeeSub;
  seasons;
  seasonSub;

  constructor(
    private toaster: ToasterService,
    public appConfig: AppConfig,
    private API: ApiService,
    private fb: FormBuilder,
    public translate: TranslateService,
    private dateTransform: DatePipe,
    public timeService: TimeCalculatorService,
  ) {
    this.createForm();
  }

  createForm() {
    if ( this.fileInput ) this.fileInput.nativeElement.value = '';
    this.errorMessages = undefined;
    this.employeeCtrl = new FormControl(this.current.employee ? this.current.employee.name : "");
    this.employeeSub = this.employeeCtrl.valueChanges.startWith(this.current.employee ? this.current.employee.name : "").subscribe(q => this.searchAutoComplete(q, 'employees', 'employees'));
    this.seasonCtrl = new FormControl(this.current.season ? this.current.season.year : "");
    this.seasonSub = this.seasonCtrl.valueChanges.startWith(this.current.season ? this.current.season.year : "").subscribe(q => this.searchAutoComplete(q, 'seasons', 'seasons'));

    this.form = this.fb.group({
      employee_id: [ this.current.employee_id || '', [ Validators.required, CustomValidators.number ] ],
      date: [ this.current.date || '', [ Validators.required, CustomValidators.date ] ],
      number_of_days: [ this.current.number_of_days || '', [ Validators.required, CustomValidators.number ] ],
      amount: [ this.current.amount || '', [ Validators.required, CustomValidators.number ] ],
      season_id: [ this.current.season_id || '', [ Validators.required, CustomValidators.number ] ],
      notes: [ this.current.notes || '', [ Validators.maxLength(255) ] ],
      file: [ '', this.current.id ? [ ] : [ Validators.required ] ],
    });
  }

  addedFile( $event ) {
    if ( $event.target.files[0].type != "application/pdf" ) {
      $event.target.value = '';
      this.form.controls['file'].setValue(null);
      return this.form.controls['file'].setErrors({ 'type': true });
    }
    this.form.controls['file'].setErrors(null);
    this.form.controls['file'].setValue($event.target.files[0]);
  }

  create() {
    this.current = {};
    this.createForm();
  }

  edit( row: any ) {
    this.current = row;
    this.current.date = DateGMT(row.date);
    this.createForm();
  }

  reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  search() {
		if ( this.timeout ) clearTimeout(this.timeout);
		this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
			delete this.timeout;
		}, 250);
  }

  searchAutoComplete( q, variable, model ) {
    if ( [ "employees", "seasons" ].indexOf(model) > -1 && this.current ) this.current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( [ "employees", "seasons" ].indexOf(model) > -1 && this.current ) this.current.requesting = false;
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    form.date = this.dateTransform.transform(form.date, "y-MM-dd");

    if ( !form.notes ) delete form.notes;

    const formData = new FormData();

    for ( let key in form ) {
      if ( form[key] ) formData.append(key, form[key] );
    }

    if ( method == "update" ) formData.append('_method', 'PUT');

    this.API[method](MODEL, formData, this.current.id, 'post')
        .subscribe(
          res => {
						this.toaster.pop('success', 'Vacación', `La vacación ha sido ${ this.current.id ? "actualizado" : "creada" } exitosamente`);
            this.current.requesting = false;
						if ( !this.current.id ) this.paginator.pageIndex = 0;
						this.paginator.page.emit();
						jQuery('#form-modal').modal('toggle');
          }, err => {
						this.toaster.pop('error', 'Vacación', `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } la vacación`);
            this.current.requesting = false;
						this.errorMessages = err.error.errors;
						for ( let name in this.errorMessages ) {
							this.form.controls[name].setValue('');
							if ( name == 'file' && this.fileInput ) this.fileInput.nativeElement.value = '';
						}
          })
  }

  delete() {
    this.API.delete(MODEL, this.current.id)
        .subscribe(
          res => {
            if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
            this.paginator.page.emit();
            this.toaster.pop('success', 'Vacación', 'La vacación ha sido eliminado exitosamente');
            this.current = {};
          }, err => {
            this.toaster.pop('error', 'Vacación', 'Ha ocurrido un error al eliminar la vacación');
          })
  }

  download( id ) {
    this.API.custom('get', `api/${MODEL}/${id}/download`)
        .subscribe(
          res => window.open(res.data.file_url),
          err => this.toaster.pop('error', 'Vacación', 'Ha ocurrido un error al descargar el archivo')
        );
  }

  filterReport(output: string = '') {
    let query = new URLSearchParams();
    if (output) query.append('output', output);
    return query;
  }

  generateExportLink(output: string) {
    let query = this.filterReport(output);
    var url = `${this.appConfig.config.API_URL}vacation/reports?${query.toString()}`;
    window.open(url, '_blank');
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
        .startWith(null)
        .switchMap(() => {
          this.loadingResults = true;
          let params = new HttpParams();

          if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
          if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
          if ( this.filter ) params = params.append('q', this.filter);
          if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
          if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

          return this.API.index(MODEL, params);
        })
        .map(data => {
          this.loadingResults = false;
          this.haveError = false;
          this.total = data.total;
          this.limit = data.per_page;

          return data.data;
        })
        .catch( err => {
          this.loadingResults = false;
          this.haveError = true;
          return Observable.of([]);
        })
        .subscribe(data => this.source.data = data);
  }
}
