import { NgModule } from '@angular/core';
import { ApiService } from './../../services/api.service';
import { BankTransactionsComponent } from './bank-transactions.component';
import { BankTransactionsFormComponent } from './bank-transactions-form/bank-transactions-form.component';
import {CommonModule, DatePipe} from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectivesModule } from '../../theme/directives/directives.module';
import { PipesModule } from '../../theme/pipes/pipes.module';
import { DataTableModule } from 'angular2-datatable';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2CompleterModule } from "ng2-completer";
import { MatTooltipModule, MatProgressSpinnerModule, MatTableModule, MatPaginatorModule, MatSortModule, MatInputModule, MatPaginatorIntl, MatAutocompleteModule } from '@angular/material';

export const routes = [
  { path: '', component: BankTransactionsComponent, data: { breadcrumb: 'todos' } },
  { path: 'subir', component: BankTransactionsFormComponent, data: { breadcrumb: 'subir cartola' } }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    PipesModule,
    ReactiveFormsModule,
    DataTableModule,
    NgxPaginationModule,
    Ng2CompleterModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    BankTransactionsComponent,
    BankTransactionsFormComponent
  ],
  providers:[
    ApiService, DatePipe
  ]
})
export class BankTransactionsModule { }
