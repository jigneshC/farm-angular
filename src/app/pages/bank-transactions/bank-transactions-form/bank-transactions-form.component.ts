import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AppConfig } from './../../../app.config';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-bank-transactions-form',
  templateUrl: './bank-transactions-form.component.html',
})
export class BankTransactionsFormComponent {
  @ViewChild('fileInput') fileInput;
  public banks;
  public bank = "";
  public uploadMessage = { type: "", message: "" };
  private blocked: boolean = false;

  constructor(
    private http: HttpClient,
		private router: Router,
    private configService: AppConfig,
    private toaster: ToasterService,
  ) {
    this.findBanks();
  }
  
  findBanks() {
    this.http.get( this.configService.config.API_URL + `api/banks` )
        .map((res: Response) => res)
        .catch((error: any) => Observable.throw(error.error || 'Server error'))
        .subscribe(
          res => {
            this.banks = res.data || [];
          },
          err => {
            setTimeout( () => {
              this.findBanks();
            }, 1000);
          }
        );
  }

  onSubmit() {
    if ( !this.blocked ) {
      let files = this.fileInput.nativeElement.files;

      this.uploadMessage = { type: "", message: "" };
      
      if ( !this.bank ) return this.uploadMessage = { type: "error", message: "Por favor selecciona un banco para asociar el archivo." };
      
      if ( files && files[0] ) {
        if ( files[0].type == "text/plain" ) {
          
          const formData = new FormData();
          this.blocked = true;
          
          formData.append("file", files[0]);
          formData.append("bank", this.bank);
          
          this.uploadMessage = { type: "uploading", message: "Subiendo archivo..." };
          
          this.http.post( this.configService.config.API_URL + `api/banktransactions`, formData )
              .map((res: Response) => res)
              .catch((error: any) => Observable.throw(error.error || 'Server error'))
              .subscribe(res => {
                this.toaster.pop('success', 'Archivo', 'La cartola bancaria ha sido subida exitosamente');
								this.router.navigate(['/pages/transacciones-bancarias/']);
              }, err => {
                this.toaster.pop('error', 'Archivo', 'Ha ocurrido un error al subir la cartola bancaria');
                this.uploadMessage = { type: "error", message: err.reason || "Ha ocurrido un error. Por favor, intente de nuevo." };
                this.blocked = false;
              });
              
          this.fileInput.nativeElement.value = '';
          
        } else {
          this.uploadMessage = { type: "error", message: "El tipo de archivo no es válido. Asegurate que sea un archivo de texto." };
          this.fileInput.nativeElement.value = '';
        }
      } else this.uploadMessage = { type: "error", message: "Por favor, selecciona un archivo" };
    }
  }

}
