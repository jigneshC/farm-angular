import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpParams } from "@angular/common/http";
import { RequestOptions, Headers } from "@angular/http";
import { CompleterService, CompleterData } from 'ng2-completer';
import { Observable } from 'rxjs/Rx';
import { AuthService } from './../../services/auth/auth.service';
import { DatePipe } from '@angular/common';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AppConfig } from './../../app.config';
import { ToasterService } from 'angular2-toaster';

import DateGMT from 'app/classes/date-gmt';

@Component({
  selector: 'app-bank-transactions',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './bank-transactions.component.html',
  styleUrls: ['./bank-transactions.component.scss'],
})
export class BankTransactionsComponent implements OnInit, OnDestroy {
  public filtersForm: FormGroup;
  public itemForm: FormGroup;
  public messages = { emptyMessage: "No hay resultados para mostrar.", totalMessage: "totales" };
  public errors = { bank: "", sinceDate: "", untilDate: "", sinceAmount: "", untilAmount: "", item: "" };
  public banks = [];
  public keys = Object.keys;
  public selected = {};
  private filters = {};
  public rows;
  public page;
  public limit;
  public count;
  private subParams;
  private subFiltersForm;
  private subItemForm;
  protected searchStr: string;
  protected dataService;
  public loadingResults: boolean = true;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private completerService: CompleterService,
    private auth: AuthService,
    private configService: AppConfig,
    private toaster: ToasterService,
    private dateTransform: DatePipe,
  ) {
    this.findBanks();
    this.createFiltersForm();
    this.createItemForm();
    this.initSearchService();
  }
  
  initSearchService() {
    this.dataService = this.completerService.remote( this.configService.config.API_URL + `api/banktransactionitems?q=`, 'name', 'name');
    let options = new RequestOptions({ headers: new Headers() });
    options.headers.set("Authorization", `Bearer ${this.auth.getToken()}`);
    this.dataService.requestOptions(options);
    this.dataService.dataField("data");
  }
  
  findBanks() {
    this.http.get( this.configService.config.API_URL + `api/banks` )
        .map((res: Response) => res)
        .catch((error: any) => Observable.throw(error.error || 'Server error'))
        .subscribe(
          res => this.banks = res.data,
          err => {
            setTimeout( () => {
              this.findBanks();
            }, 1000);
          }
        );
  }
  
  createFiltersForm() {
    this.filtersForm = this.fb.group({
      bank: [ 'all', Validators.required ],
      item: '',
      sinceDate: '',
      untilDate: '',
      sinceAmount: '',
      untilAmount: '',
    });
    this.subscribeFiltersForm();
  }
  
  createItemForm() {
    this.itemForm = this.fb.group({
      item: [ '', Validators.required ]
    });
    this.subscribeItemForm();
  }
  
  subscribeFiltersForm() {
    this.filtersForm.valueChanges.subscribe(form => {
      if ( !form.bank ) this.errors.bank = "Debe escoger un banco o seleccionar 'Todos'";
      else delete this.errors.bank;
      
      let sinceDate = DateGMT( form.sinceDate );
      let untilDate = DateGMT( form.untilDate );
      if ( sinceDate && untilDate && ( sinceDate > untilDate ) ) {
        this.errors.sinceDate = "Selecciona una fecha anterior a la final";
        this.errors.untilDate = "Selecciona una fecha mayor a la inicial";
      } else {
        delete this.errors.sinceDate;
        delete this.errors.untilDate;
      }
      
      if ( form.sinceAmount && form.untilAmount && ( form.sinceAmount > form.untilAmount )  ) {
        this.errors.sinceAmount = "Selecciona un monto menor al final";
        this.errors.untilAmount = "Selecciona un monto mayor al inicial";
      } else {
        delete this.errors.sinceAmount;
        delete this.errors.untilAmount;
      }
    });
  }
  
  subscribeItemForm() {
    this.itemForm.valueChanges.subscribe(form => {
      if ( !form.item ) this.errors.item = "Debe escribir el nombre de un item";
      else delete this.errors.item;
    });
  }
  
  handlePageChange(page: any): void {
    this.page = page;
    
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        page: this.page,
        limit: this.limit
      },
      queryParamsHandling: 'merge'
    });
    
    this.getBankTransactions( this.filters );
  }
  
  getBankTransactions(filters: any): void {
    this.loadingResults = true;
    // this.count = 0;
    let params = new HttpParams();
    params = params.append('limit', `${ this.limit || "" }`);
    params = params.append('page', `${ this.page || "" }`);
    
    if ( filters ) {
      if ( filters.bank && filters.bank != 'all' ) params = params.append('bank', filters.bank);
      if ( filters.item ) params = params.append('item', filters.item);
      if ( filters.sinceDate ) params = params.append('since_date', this.dateTransform.transform(DateGMT(filters.sinceDate), "y-MM-dd"));
      if ( filters.untilDate ) params = params.append('until_date', this.dateTransform.transform(DateGMT(filters.untilDate), "y-MM-dd"));
      if ( filters.sinceAmount ) params = params.append('since_amount', filters.sinceAmount + "");
      if ( filters.untilAmount ) params = params.append('until_amount', filters.untilAmount + "");
    }
    
    this.http.get( this.configService.config.API_URL + `api/banktransactions`, { params: params } )
        .map((res: Response) => res)
        .catch((error: any) => Observable.throw(error.error || 'Server error'))
        .subscribe(
          res => {
            this.loadingResults = false;
            if ( res.last_page < this.page ) return this.handlePageChange( 1 );
            this.count = res.total;
            this.rows = res.data;
          },
          err => {
            setTimeout( () => {
              this.getBankTransactions( this.filters );
            }, 1500);
          }
        );
  }
  
  buildFilters(form): void {
    let errors = 0;
    for ( let key of Object.keys(this.errors) ) if ( this.errors[key] && key != "item" ) errors++;
    
    if ( errors > 0 ) return;
    
    this.filters = form;
    this.handlePageChange( 1 );
  }
  
  resetFilters() {
    this.subFiltersForm && this.subFiltersForm.unsubscribe();
    this.createFiltersForm();
    this.filters = {};
    this.handlePageChange( 1 );
  }
  
  toggleRow(id) {
		if ( !this.selected[id] ) delete this.selected[id];
		
		if ( !this.keys( this.selected ).length ) jQuery("#item").slideUp();
  }
  
  toggleFilters() {
    jQuery("#filters").slideToggle();
  }
  
  toggleItem() {
    jQuery("#item").slideToggle();
  }
  
  addItem(form) {
    if ( !form.item ) return this.errors.item = "Debe escribir el nombre de un item";
    else delete this.errors.item;
    
    let ids = this.keys( this.selected );
    
    if ( !ids.length ) return false;
    
    this.http.put( this.configService.config.API_URL + `api/banktransactions/updatemulti`, { ids: ids, item: form.item })
            .map((res: Response) => res)
            .catch((error: any) => Observable.throw(error.error || 'Server error'))
            .subscribe(res => {
              this.toaster.pop('success', 'Item', 'Los registros seleccionados han sido actualizados exitosamente con el item seleccionado');
              this.errors.item = "";
              jQuery("body").stop().animate({scrollTop:0}, 500, 'swing');
              this.handlePageChange( this.page );
            }, err => {
              this.errors.item = "Ha ocurrido un error al agregar el item. Por favor, intente de nuevo.";
            });
  }

  ngOnInit() {
    this.subParams = this.route.queryParams.subscribe( params => {
      this.limit = +params.limit || 10;
      this.page = +params.page || 1;
      
      this.handlePageChange( this.page );
    });
  }

  ngOnDestroy() {
    this.subParams && this.subParams.unsubscribe();
    this.subFiltersForm && this.subFiltersForm.unsubscribe();
    this.subItemForm && this.subItemForm.unsubscribe();
  }

}
