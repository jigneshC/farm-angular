import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PagesComponent } from './pages.component';

export const routes: Routes = [
    {
        path: '',
        component: PagesComponent,
        children: [
            { path:'', redirectTo:'dashboard', pathMatch:'full', data: { breadcrumb: 'Tablero' } },
            { path: 'dashboard', loadChildren: 'app/pages/dashboard/dashboard.module#DashboardModule', data: { breadcrumb: 'Dashboard' }  },
            { path: 'suelo-clima', loadChildren: 'app/pages/irrigations/irrigations.module#IrrigationsModule', data: { breadcrumb: 'Suelo y Clima' }  },
            { path: 'labores', loadChildren: 'app/pages/tasks/tasks.module#TasksModule', data: { breadcrumb: 'Labores' }  },
            { path: 'productos', loadChildren: 'app/pages/products/products.module#ProductsModule', data: { breadcrumb: 'Productos' }  },
            { path: 'contabilidad', loadChildren: 'app/pages/accounting/accounting.module#AccountingModule', data: { breadcrumb: 'Contabilidad' }  },
            { path: 'aplicaciones', loadChildren: 'app/pages/applications/applications.module#ApplicationsModule', data: { breadcrumb: 'Aplicaciones' }  },
            { path: 'personal', loadChildren: 'app/pages/personal/personal.module#PersonalModule', data: { breadcrumb: 'Personal' }  },
            { path: 'administracion', loadChildren: 'app/pages/administration/administration.module#AdministrationModule', data: { breadcrumb: 'Administracion' }  },
            { path: 'maquinaria', loadChildren: 'app/pages/machinery/machinery.module#MachineryModule', data: { breadcrumb: 'Maquinaria' }  },
            { path: 'transacciones-bancarias', loadChildren: 'app/pages/bank-transactions/bank-transactions.module#BankTransactionsModule', data: { breadcrumb: 'Transacciones bancarias' }  },
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
