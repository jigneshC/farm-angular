import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ToasterService} from 'angular2-toaster';
import {Observable} from 'rxjs/Observable';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {MachineryEntity} from '../../../models/machinery';
import {RestService} from '../../../services/api/rest.service';
import {Pagination} from '../../../services/api/pagination';
import {Subject} from 'rxjs/Subject';
import {ISubscription} from 'rxjs/Subscription';
import {Configuration} from '../../../models/configuration';
import {ConfigurationFormComponent} from './configuration-form/configuration-form.component';
import {ConfigurationService} from '../../../services/api/administration/configuration.service';
import {Router} from '@angular/router';

@Component({
  selector: 'az-configurations',
  templateUrl: './configurations.component.html',
  styleUrls: ['./configurations.component.scss']
})
export class ConfigurationsComponent implements OnInit, OnDestroy, AfterViewInit {
  // Table attributes.
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public total = 0;
  public tableData: MatTableDataSource<Configuration>;
  public isLoading = false;
  public displayedColumns = ['label', 'actions'];

  public error: string;
  public filter: string;

  public filterChanged = new Subject<string>();
  public subscriptions: ISubscription[] = [];

  // Selected entity for delete modal.
  public selectedEntity: Configuration;

  constructor(protected restService: ConfigurationService, protected  dialog: MatDialog, protected toaster: ToasterService, protected router: Router) {

  }

  ngOnInit() {
    this.tableData = new MatTableDataSource<Configuration>([]);
    this.subscriptions.push(
      this.filterChanged
        .debounceTime(300)
        .distinctUntilChanged()
        .subscribe(() => {
          this.getData()
        }));
    this.subscriptions.push(this.filterChanged);

  }

  getData(): void {
    this.paginator.page.emit();
  }

  refreshData(): void {
    this.paginator.pageIndex = 0;
    this.getData();
  }

  entityObservable(params): Observable<Pagination<Configuration>> {
    this.setIsLoading(true);
    return this.restService.findPaginated(params);
  }

  createOrEdit(entityId?): void {
    if (!entityId) {
      this.router.navigate(['pages', 'administracion', 'configuraciones', 'crear']);
    } else {
      this.router.navigate(['pages', 'administracion', 'configuraciones', 'editar', entityId]);
    }
  }

  delete() {
    this.restService.delete(String(this.selectedEntity.id))
      .subscribe(
        res => {
          this.toaster.pop('success', 'Configuración', `Configuración eliminada exitosamente.`);
          this.getData();
        },
        error => {
          this.toaster.pop('error', 'Configuración', `Ha ocurrido un error al eliminar la configuración.`);
        }
      );
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      if (subscription && !subscription.closed) {
        subscription.unsubscribe();
      }
    }
  }

  keyupChange(): void {
    this.filterChanged.next(this.filter);
  }

  updatePaginator(pagination: Pagination<any>) {
    this.paginator.pageSize = pagination.per_page;
    this.paginator.pageIndex = pagination.current_page - 1;
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.subscriptions.push(Observable.merge(this.sort.sortChange, this.paginator.page)
      .switchMap(() => {
        return this.entityObservable(this.buildQueryParams());
      })
      .subscribe((result: Pagination<Configuration>) => {
          this.isLoading = false;
          this.updateData(result);
        },
        err => {
          this.isLoading = false;
          this.toaster.pop('error', 'Configuración', `Ha ocurrido un error al buscar las configuraciones.`);
        }));
    this.paginator.page.emit();
  }

  protected updateData(result: Pagination<Configuration>) {
    this.total = result.total;
    this.updatePaginator(result);
    this.tableData.data = result.data;
  }

  protected buildQueryParams(): any {
    const params = <any> {};
    params.page = this.paginator.pageIndex + 1;
    params.p = this.paginator.pageSize;
    if (this.filter) {
      params.q = this.filter;
    }
    if (this.sort.active) {
      params.sort_by = this.sort.active;
    }
    if (this.sort.direction) {
      params.sort_dir = this.sort.direction;
    }
    return params;
  }

  protected setIsLoading(val: boolean): void {
    setTimeout(() => {
      this.isLoading = val;
    })
  }
}
