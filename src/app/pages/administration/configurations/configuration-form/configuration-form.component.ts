import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {ToasterService} from 'angular2-toaster';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ConfigurationService} from '../../../../services/api/administration/configuration.service';
import JSONEditor from 'jsoneditor';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'az-configuration-form',
  templateUrl: './configuration-form.component.html',
  styleUrls: ['./configuration-form.component.scss']
})
export class ConfigurationFormComponent implements OnInit {

  public title: string;
  public errorMessages = [];
  public form: FormGroup;
  public isLoading = false;
  public errors: any = {};
  public isEdit: boolean;
  public entityId: any;

  @ViewChild('jsonEditorRef') jsonEditorRef;
  @Output() afterSave = new EventEmitter<string>(true);

  protected jsonEditor: JSONEditor;

  constructor(protected formBuilder: FormBuilder,
              protected route: ActivatedRoute,
              protected router: Router,
              protected restService: ConfigurationService,
              protected toaster: ToasterService) {
    this.entityId = route.snapshot.params['id'];
  }

  ngOnInit() {
    this.initForm();
    if (this.isThisEdit()) {
      this.initJsonEditor();
      this.initializeForEdition()
    } else {
      this.initJsonEditor();
      this.initializeForCreation()
    }
  }

  protected initJsonEditor() {
    this.jsonEditor = new JSONEditor(this.jsonEditorRef.nativeElement);
  }

  protected isThisEdit(): boolean {
    return this.entityId;
  }

  protected initForm(): void {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      label: ['', [Validators.required, Validators.maxLength(50)]]
    });
  }

  protected initializeForCreation(): void {
    this.isEdit = false;
    this.title = `Crear Configuración`;
  }

  protected initializeForEdition(): void {
    this.isEdit = true;
    this.title = `Editar Configuración`;
    this.isLoading = true;
    this.restService.findOne(this.entityId)
      .subscribe(
        result => {
          this.isLoading = false;
          this.updateForm(result);
        },
        error => {
          this.errorMessages = [`Ha ocurrido un error al obtener la Configuración`];
          this.isLoading = false;
        });
  }

  submit(): void {
    if (this.isEdit) {
      const entity = this.buildEntityForSubmit();
      this.isLoading = true;
      this.restService.update(this.entityId, entity)
        .subscribe(
          res => {
            this.isLoading = false;
            this.afterSave.emit('success');
            this.toaster.pop('success', `Configuración`, `Configuración editada exitosamente.`);
          },
          error => {
            this.isLoading = false;
            this.handleError(error);
          });
    } else {
      const entity = this.buildEntityForSubmit();
      this.isLoading = true;
      this.restService.create(entity)
        .subscribe(res => {
            this.isLoading = false;
            this.afterSave.emit('success');
            this.toaster.pop('success', `Configuración`, `Configuración guardada exitosamente.`);
          },
          error => {
            this.isLoading = false;
            this.handleError(error);
          });
    }
  }

  // Workaround to update form validity.
  protected updateForm(value) {
    setTimeout(() => {
      this.form.patchValue({name: value.name, label: value.label});
      this.jsonEditor.set(JSON.parse(value.value));
      this.jsonEditor.expandAll();
    });
  }

  evaluateClassError(formControlName: string, errorAttribute?: string): any {
    if (formControlName && errorAttribute) {
      const form = this.form.get(formControlName);
      return {
        'has-danger': form.touched && (form.invalid || this.errors[errorAttribute]),
        'has-success': form.touched && (form.valid || !this.errors[errorAttribute])
      };
    } else if (formControlName && !errorAttribute) {
      const form = this.form.get(formControlName);
      return {
        'has-danger': form.touched && form.invalid,
        'has-success': form.touched && form.valid
      };
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error) {
      const errorObj = error.error;
      this.errorMessages = ['Ha ocurrido un error al intentar guardar.'];
      if (errorObj.errors) {
        this.errors = errorObj.errors;
      }
    }
  }

  protected buildEntityForSubmit(): any {
    const entity = Object.assign({id: null, value: null}, this.form.value);
    if (this.isEdit) {
      entity.id = this.entityId;
    }
    entity.value = JSON.stringify(this.jsonEditor.get());
    return entity;
  }

  public goBack(): void {
    this.router.navigate(['pages', 'administracion', 'configuraciones']);
  }

  refreshData() {
    this.isLoading = true;
    this.restService.findOne(this.entityId)
      .subscribe(
        result => {
          this.isLoading = false;
          this.updateForm(result);
        },
        error => {
          this.errorMessages = [`Ha ocurrido un error al obtener la Configuración`];
          this.isLoading = false;
        });
  }

}
