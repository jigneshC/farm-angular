import {NgxPaginationModule} from 'ngx-pagination';
import {Ng2CompleterModule} from 'ng2-completer';
import {FormValidationService} from './../../services/form-validation.service';
import {CrudComponentsModule} from './../../theme/components/crud-components/crud-components.module';
import {SelectHelperService} from './../../services/select-helper.service';
import {DataTableModule} from 'angular2-datatable';
import {ApiService} from './../../services/api.service';
import {UsersComponent} from './users/users.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DirectivesModule} from '../../theme/directives/directives.module';
import {PipesModule} from '../../theme/pipes/pipes.module';
import {ChartsModule} from 'ng2-charts';
import {MatTooltipModule} from '@angular/material/tooltip';
import 'chart.js/dist/Chart.js';
import {PermissionComponent} from './permission/permission.component';
import {RoleComponent} from './role/role.component';
import {
  MatAutocompleteModule, MatCheckboxModule, MatDialogModule, MatInputModule, MatListModule, MatPaginatorModule,
  MatProgressSpinnerModule, MatSelectModule, MatSortModule, MatTableModule
} from '@angular/material';
import {PermissionService} from '../../services/api/administration/permission.service';
import {PermissionFormComponent} from './permission/permission-form/permission-form.component';
import {RoleFormComponent} from './role/role-form/role-form.component';
import {RoleService} from '../../services/api/administration/role.service';
import {RolePermissionComponent} from './role/role-permission/role-permission.component';
import {UserService} from '../../services/api/administration/user.service';
import {ConfigurationsComponent} from './configurations/configurations.component';
import {ConfigurationFormComponent} from './configurations/configuration-form/configuration-form.component';
import {ConfigurationService} from '../../services/api/administration/configuration.service';

export const routes = [

    { path: 'usuarios', component: UsersComponent, data: { breadcrumb: 'usuarios' } },
    { path: 'permisos', component: PermissionComponent, data: { breadcrumb: 'permisos' } },
    { path: 'roles', component: RoleComponent, data: { breadcrumb: 'roles' } },
    { path: 'configuraciones', component: ConfigurationsComponent, data: { breadcrumb: 'configuraciones' } },
    { path: 'configuraciones/editar/:id', component: ConfigurationFormComponent, data: { breadcrumb: 'editar configuracion' } },
    { path: 'configuraciones/crear', component: ConfigurationFormComponent, data: { breadcrumb: 'crear configuracion' } }
];

@NgModule({
  entryComponents: [PermissionFormComponent, PermissionComponent, RoleFormComponent, RolePermissionComponent],
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule,
    DirectivesModule,
    DataTableModule,
    PipesModule,
    FormsModule,
    Ng2CompleterModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    CrudComponentsModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatDialogModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatListModule,
    MatCheckboxModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    PermissionComponent,
    PermissionFormComponent,
    RoleComponent,
    RoleFormComponent,
    UsersComponent,
    RolePermissionComponent,
    ConfigurationsComponent,
    ConfigurationFormComponent,
  ],
  providers: [
    ApiService,
    SelectHelperService,
    FormValidationService,
    PermissionService,
    RoleService,
    UserService,
    ConfigurationService
  ]
})

export class AdministrationModule { }
