import {Component, OnDestroy, Inject, Output, EventEmitter, OnInit} from '@angular/core';
import {Permission} from '../../../models/permission';
import {PermissionService} from '../../../services/api/administration/permission.service';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {PermissionFormComponent} from './permission-form/permission-form.component';
import {RoleService} from '../../../services/api/administration/role.service';
import {ToasterService} from 'angular2-toaster';
import {Subscription} from 'rxjs/Subscription';
import {Subject} from 'rxjs/Subject';
import {Role} from '../../../models/role';

import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/finally'
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
  selector: 'az-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss'],
  providers:[{
    provide: MatDialogRef,
    useValue: {
      close: (dialogResult: any) => { }
    }
  },
  { provide: MAT_DIALOG_DATA, useValue: [] }]
})
export class PermissionComponent implements OnInit, OnDestroy {

  public displayedColumns = ['name', 'description', 'actions'];
  public tableData: MatTableDataSource<Permission>;
  public loadingResults = false;
  public error: string;
  public filter: string;
  public selectedPermission: Permission;
  public roles: any = [];

  public filterChanged = new Subject<string>();
  public filterSubscription = new Subscription();
  public activePermissionWithRoles: any = [];
  public activePermissionWithRolesID: any = { id: null, name: null };

  protected formResultSubscription: Subscription;

  @Output() afterSave = new EventEmitter<string>(true);

  constructor(
    protected dialogRef: MatDialogRef<PermissionComponent>,
    protected roleService: RoleService,
    protected permissionService: PermissionService,
    protected dialog: MatDialog, 
    private toaster: ToasterService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.filterSubscription =
      this.filterChanged
        .debounceTime(300)
        .distinctUntilChanged()
        .subscribe(filter => {
          this.search(filter)
        });
    this.tableData = new MatTableDataSource<Permission>();
    this.getData();
    this.getRoles();
  }

  getData(): void {
    this.loadingResults = true;
    this.permissionService.findAll()
      .catch(error => {
        this.error = error;
        return Observable.of([]);
      })
      .finally(() => {
        this.loadingResults = false;
      })
      .subscribe((data: Array<Permission>) => {
          this.tableData.data = data;
        }
      );
  }

  getRoles(): void {
    this.roleService.findAll()
      .catch(error => {
        this.error = error;
        return Observable.of([]);
      })
      .subscribe((data: Array<Role>) => {
          this.roles = data;
        }
      );
  }

  openRoles(row){
    this.activePermissionWithRoles = [];
    row.roles.forEach(element => {
      this.activePermissionWithRoles.push(element.id);
    });
    this.activePermissionWithRolesID = { id: row.id, name: row.name };
  }

  selectRoles(row): Boolean {
    for (const rol of this.activePermissionWithRoles) {
      if (rol === row.id) {
        return true;
      }
    }
    return false;
  }

  saveRolesSelection(){
    this.loadingResults = true;
    const entity = <any> Object.assign({roles: this.activePermissionWithRoles}, this.activePermissionWithRolesID);
    this.permissionService.update(String(entity.id), entity)
    .subscribe(
      res => {
        this.getData();
      },
      error => {
      });
  }

  onListControlChanged(list){
    let roles = list.selectedOptions.selected.map(item => item.value);
    this.activePermissionWithRoles = [];
    roles.forEach(element => {
      this.activePermissionWithRoles.push(element.id);
    });
  }

  createOrEdit(idPermission?): void {
    const data = idPermission ? {idPermission: idPermission} : {};
    const form = this.dialog.open(PermissionFormComponent, {
      width: '800px',
      data
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.getData();
      }
    })
  }

  delete() {
    this.permissionService.delete(String(this.selectedPermission.id))
      .subscribe(
        res => {
          this.toaster.pop('success', 'Permiso', 'Permiso eliminado exitosamente.');
          this.getData();
        },
        error => {
          this.toaster.pop('error', 'Permiso', 'Ha ocurrido un error al eliminar el permiso.');
        }
      );
  }

  ngOnDestroy() {
    if (this.formResultSubscription) {
      this.formResultSubscription.unsubscribe();
    }
    if (this.filterSubscription) {
      this.filterSubscription.unsubscribe();
    }
    if (this.filterChanged) {
      this.filterChanged.unsubscribe();
    }
  }

  keyupChange(): void {
    this.filterChanged.next(this.filter);
  }

  search(filter: string): void {
    if (!filter) {
      this.getData();
    }
    else {
      this.loadingResults = true;
      this.permissionService.findAll({
        'q': filter
      })
      .subscribe(
        res => {
          this.tableData.data = res;
          this.loadingResults = false;
        },
        error2 => {
          this.loadingResults = false;
        })
    }
  }

}
