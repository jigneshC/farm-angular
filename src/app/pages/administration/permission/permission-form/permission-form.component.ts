import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PermissionService} from '../../../../services/api/administration/permission.service';
import 'rxjs/add/operator/catch';
import {ToasterService} from 'angular2-toaster';

@Component({
  selector: 'az-permission-form',
  templateUrl: './permission-form.component.html',
  styleUrls: ['./permission-form.component.scss']
})
export class PermissionFormComponent implements OnInit {

  public title: string;
  public errorMessages = [];
  public form: FormGroup;
  public isLoading = false;
  public isEdit = false;

  @Output() afterSave = new EventEmitter<string>(true);

  constructor(protected dialogRef: MatDialogRef<PermissionFormComponent>,
              protected formBuilder: FormBuilder,
              protected permissionService: PermissionService,
              protected toaster: ToasterService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    if (this.data.idPermission) {
      this.isEdit = true;
      this.title = 'Editar Permiso';
      this.isLoading = true;
      this.permissionService.findOne(this.data.idPermission)
        .subscribe(
          permission => {
            this.form.patchValue(permission);
            this.isLoading = false;
          },
          error => {
            this.errorMessages = ['Ha ocurrido un error al obtener el permiso.'];
            this.isLoading = false;
          });
    }
    else {
      this.isEdit = false;
      this.title = 'Crear Permiso';
    }
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(255)]]
    });
  }

  close(): void {
    this.dialogRef.close();
  }

  submit(): void {
    if (this.isEdit) {
      const entity = this.form.value;
      entity.id = this.data.idPermission;
      this.permissionService.update(entity.id, entity)
        .subscribe(
          res => {
            this.afterSave.emit('success');
            this.dialogRef.afterClosed().subscribe(() => {
              this.toaster.pop('success', 'Permiso', 'Permiso editado exitosamente.');
            });
            this.dialogRef.close();

          },
          error => {
            this.errorMessages.push('Ha ocurrido un error al intentar guardar.');
          })
    }
    else {
      this.permissionService.create(this.form.value)
        .subscribe(res => {
            this.afterSave.emit('success');
            this.dialogRef.afterClosed().subscribe(() => {
              this.toaster.pop('success', 'Permiso', 'Permiso guardado exitosamente.');
            });
            this.dialogRef.close();
          },
          error => {
            this.errorMessages.push('Ha ocurrido un error al intentar guardar.');
          });
    }
  }
}
