import {AuthService} from './../../../services/auth/auth.service';
import {Headers, RequestOptions} from '@angular/http';
import {FormValidationService} from './../../../services/form-validation.service';
import {SelectHelperService} from './../../../services/select-helper.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppConfig} from './../../../app.config';
import {ApiService} from './../../../services/api.service';
import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {CompleterService} from 'ng2-completer';
import {ToasterService} from 'angular2-toaster';
import {MatAutocomplete, MatPaginator, MatTableDataSource} from '@angular/material';
import {Pagination} from '../../../services/api/pagination';
import {UserService} from '../../../services/api/administration/user.service';
import {User} from '../../../models/user';
import {Subscription} from 'rxjs/Subscription';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMapTo';
import {RoleService} from '../../../services/api/administration/role.service';
import {Role} from '../../../models/role';

@Component({
  selector: 'app-users',
  templateUrl: 'users.component.html',
  styleUrls: ['users.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class UsersComponent implements OnInit, AfterViewInit, OnDestroy {
  config: any;
  create: boolean = true;
  userId: number = 0;
  messages: any;
  costCenters: Array<any>;
  public error: any;
  public errorMessages: any;
  public dataService;

  // User form
  @ViewChild('rolesInputText') rolesInputText;
  public userForm: FormGroup;
  public selectedRoles: Array<Role> = [];
  public roleInput = '';
  public roles: Array<Role> = [];
  public selectedUser: User;

  // Table attributes
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public total: number = 0;
  public tableData: MatTableDataSource<User>;
  public loadingResults = false;
  public displayedColumns = ['name', 'email', 'created_at', 'actions'];

  // Search field..
  public searchChanged = new Subject<string>();
  public searchSubscription = new Subscription();
  public searchText: any = '';

  constructor(private API: ApiService,
              private _config: AppConfig,
              private _fb: FormBuilder,
              private _select: SelectHelperService,
              private completerService: CompleterService,
              private configService: AppConfig,
              private auth: AuthService,
              private _fv: FormValidationService,
              private toaster: ToasterService,
              private userService: UserService,
              private roleService: RoleService) {
    this.tableData = new MatTableDataSource<User>([]);
    this.config = this._config.config;
    this.userForm = _fb.group({
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, _fv.emailValidator])],
      password: [''],
      password_confirmation: ['']
    }, {validator: _fv.matchingPasswords('password', 'password_confirmation')});
  }

  ngOnInit() {
    this.loadingResults = true;
    this.searchSubscription = this.searchChanged
      .debounceTime(300)
      .distinctUntilChanged()
      .subscribe(() => {
        this.searchUsers();
      });

    this.messages = this._config.config.messages;
    this._select.getContCenters().then((res) => {
      this.costCenters = res;
    });
    this.initSearchService();
  }

  saveUser() {
    const user = this.buildUserForRequest();
    this.API.save('users', user)
      .subscribe(
        res => {
          this.toaster.pop('success', 'Usuario', 'El usuario ha sido guardado exitosamente');
          this.resetStatus();
          this.loadusers();
          jQuery('#new-user-modal').modal('toggle');
          this.error = false;
        }, err => {
          this.toaster.pop('error', 'Usuario', 'Ha ocurrido un error al guardar el usuario');
          this.error = true;
          this.errorMessages = err.errors;
          for (const name in this.errorMessages) {
            this.userForm.controls[name].setValue('');
          }
        })
  }

  private getUsers(): Observable<any> {
    const queryParams = <any> {page: this.paginator.pageIndex + 1, limit: this.paginator.pageSize};
    if (this.searchText) {
      queryParams.q = this.searchText;
    }
    return this.userService.findPaginated(queryParams);
  }

  loadusers() {
    this.loadingResults = true;
    this.getUsers()
      .subscribe((result: Pagination<User>) => {
          this.total = result.total;
          this.updatePaginator(result);
          this.tableData.data = result.data;
          this.loadingResults = false;
        },
        err => {
          this.toaster.pop('error', 'Usuarios', 'Ha ocurrido un error al buscar los usuarios');
          this.loadingResults = false;
        });
  }

  updatePaginator(pagination: Pagination<any>) {
    this.paginator.pageSize = pagination.per_page;
    this.paginator.pageIndex = pagination.current_page - 1;
  }

  editUser() {
    const user = this.buildUserForRequest();
    // Si no tiene password se quita del request.
    if (!user.password) {
      delete user.password;
      delete user.password_confirmation;
    }
    this.API.update('users', user, this.userId)
      .subscribe(
        res => {
          this.toaster.pop('success', 'Usuario', 'El usuario ha sido actualizado exitosamente');
          this.resetStatus();
          this.loadusers();
          jQuery('#new-user-modal').modal('toggle');
          this.error = false;
        }, err => {
          this.toaster.pop('error', 'Usuario', 'Ha ocurrido un error al actualizar el usuario');
          this.error = true;
          this.errorMessages = err.errors;
          for (var name in this.errorMessages) {
            this.userForm.controls[name].setValue('');
          }
        })
  }

  deleteuser() {
    this.API.delete('users', this.userId)
      .subscribe(
        res => {
          this.toaster.pop('success', 'Usuario', 'El usuario ha sido eliminado exitosamente');
          this.resetStatus();
          this.loadusers();
        }, err => {
          this.toaster.pop('error', 'Usuario', 'Ha ocurrido un error al eliminar el usuario');
        })
  }

  detailuser(user) {
    this.resetStatus();
    this.selectedUser = user;
    this.getRoles();
    this.userId = user.id;
    this.userForm.patchValue(user);
    this.create = false;
    this.error = false;
  }

  resetStatus() {
    this.selectedUser = null;
    this.selectedRoles = [];
    this.userForm.setValue({
      name: '',
      email: '',
      password: '',
      password_confirmation: ''
    });
    this.create = true;
    this.error = false;
  }

  initSearchService() {
    this.dataService = this.completerService.remote(this.configService.config.API_URL + `api/roles?q=`, 'name', 'name');
    const options = new RequestOptions({headers: new Headers()});
    options.headers.set('Authorization', `Bearer ${this.auth.getToken()}`);
    this.dataService.requestOptions(options);
    this.dataService.dataField('data');
  }

  searchUsers() {
    this.loadingResults = true;
    this.userService.findPaginated({q: this.searchText, limit: this.paginator.pageSize, page: 1 })
      .subscribe(
        (res: Pagination<User>) => {
          this.total = res.total;
          this.updatePaginator(res);
          this.tableData.data = res.data;
          this.loadingResults = false;
        },
        error2 => {
          this.error = ['Ha ocurrido un error al buscar.']
          this.loadingResults = false;
        });
  }

  ngAfterViewInit(): void {
    this.paginator.page
      .switchMap((page) => {
        return this.getUsers();
      })
      .subscribe((result: Pagination<User>) => {
          this.total = result.total;
          this.updatePaginator(result);
          this.tableData.data = result.data;
          this.loadingResults = false;
        },
        err => {
          this.loadingResults = false;
          this.toaster.pop('error', 'Usuarios', 'Ha ocurrido un error al buscar los usuarios');
        });
    this.paginator.page.emit();
  }

  ngOnDestroy() {
    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
    }
  }

  getRoles(): void {
    this.roleService.findAll()
      .subscribe((array: Array<Role>) => {
        this.roles = array;
        if (!this.create) {
          const user = this.selectedUser;
          if (user.roles && user.roles.length > 0) {
            for (const role of user.roles) {
              const userRole = this.roles.find(r => r.id === role.id);
              if (userRole) {
                this.selectedRoles.push(userRole);
              }
            }
          }
        }
      });
  }

  public addRole(role: Role) {
    this.roleInput = '';
    this.selectedRoles.push(role);
    // Lose focus to prevent bug mat-autocomplete wont open again..
    this.rolesInputText.nativeElement.blur();
  }

  public removeRole(index) {
    this.selectedRoles.splice(index, 1);
  }

  private buildUserForRequest(): any {
    const user = new User();
    const selectedRoles = this.selectedRoles.map(role => {
      return role.id;
    });
    Object.assign(user, this.userForm.value, {roles: selectedRoles});
    return user;
  }

}
