import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {Subscription} from 'rxjs/Subscription';
import {ToasterService} from 'angular2-toaster';
import {RoleService} from '../../../services/api/administration/role.service';
import {Role} from '../../../models/role';

import 'rxjs/add/operator/catch'
import 'rxjs/add/operator/finally'
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {RoleFormComponent} from './role-form/role-form.component';
import {RolePermissionComponent} from './role-permission/role-permission.component';

@Component({
  selector: 'az-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit, OnDestroy {
  public displayedColumns = ['name', 'description', 'actions'];
  public tableData: MatTableDataSource<Role>;
  public loadingResults = false;
  public error: string;
  public filter: string;
  public selectedRole: Role;

  public filterChanged = new Subject<string>();
  public filterSubscription = new Subscription();

  protected formResultSubscription: Subscription;

  constructor(protected roleService: RoleService, protected  dialog: MatDialog, private toaster: ToasterService) {

  }

  ngOnInit() {
    this.filterSubscription =
      this.filterChanged
        .debounceTime(300)
        .distinctUntilChanged()
        .subscribe(filter => {
          this.search(filter)
        });
    this.tableData = new MatTableDataSource<Role>();
    this.getData();
  }

  getData(): void {
    this.loadingResults = true;
    this.roleService.findAll()
      .catch(error => {
        this.error = error;
        return Observable.of([]);
      })
      .finally(() => {
        this.loadingResults = false;
      })
      .subscribe((data: Array<Role>) => {
          this.tableData.data = data;
        }
      );
  }

  createOrEdit(idRole?): void {
    const data = idRole ? {idRole: idRole} : {};
    const form = this.dialog.open(RoleFormComponent, {
      width: '800px',
      data
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.getData();
      }
    })
  }

  editPermissions(idRole): void {
    const form = this.dialog.open(RolePermissionComponent, {
      width: '800px',
      data: {idRole: idRole}
    });
    this.formResultSubscription = form.componentInstance.afterSave.subscribe((result) => {
      if (result === 'success') {
        this.getData();
      }
    })
  }

  delete() {
    this.roleService.delete(String(this.selectedRole.id))
      .subscribe(
        res => {
          this.toaster.pop('success', 'Rol', 'Rol eliminado exitosamente.');
          this.getData();
        },
        error => {
          this.toaster.pop('error', 'Rol', 'Ha ocurrido un error al eliminar el rol.');
        }
      );
  }

  ngOnDestroy() {
    if (this.formResultSubscription) {
      this.formResultSubscription.unsubscribe();
    }
    if (this.filterSubscription) {
      this.filterSubscription.unsubscribe();
    }
    if (this.filterChanged) {
      this.filterChanged.unsubscribe();
    }
  }

  keyupChange(): void {
    this.filterChanged.next(this.filter);
  }

  search(filter: string): void {
    if (!filter) {
      this.getData();
    }
    else {
      this.loadingResults = true;
      this.roleService.findAll({
        'q': filter
      })
        .subscribe(
          res => {
            this.tableData.data = res;
            this.loadingResults = false;
          },
          error2 => {
            this.loadingResults = false;
          })
    }
  }

}
