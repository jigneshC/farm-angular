import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ToasterService} from 'angular2-toaster';
import {RoleFormComponent} from '../role-form/role-form.component';
import {PermissionService} from '../../../../services/api/administration/permission.service';
import {RoleService} from '../../../../services/api/administration/role.service';
import {Observable} from 'rxjs/Observable';
import {Permission} from '../../../../models/permission';
import {Role} from '../../../../models/role';

@Component({
  selector: 'az-role-permission',
  templateUrl: './role-permission.component.html',
  styleUrls: ['./role-permission.component.scss']
})
export class RolePermissionComponent extends RoleFormComponent {

  private role: Role;

  constructor(protected dialogRef: MatDialogRef<RoleFormComponent>,
              protected formBuilder: FormBuilder,
              protected roleService: RoleService,
              protected permissionService: PermissionService,
              protected toaster: ToasterService,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    super(dialogRef, formBuilder, roleService, permissionService, toaster, data);

  }

  ngOnInit() {
    this.initForm();
    this.isLoading = true;
    Observable.forkJoin(this.roleService.findOne(this.data.idRole), this.permissionService.findAll())
      .subscribe(
        result => {
          this.role = result[0];
          this.selectedPermissions = result[1].filter(p => {
            return this.role.permissions.filter(per => p.id === per.id).length > 0;
          });
          this.permissionList = result[1];
          this.updateForm({permissions: this.selectedPermissions});
          this.isLoading = false;
        },
        error => {
          this.errorMessages = ['Ha ocurrido un error al obtener el rol.'];
          this.isLoading = false;
        });
  }

  submit(): void {
    const entity = this.buildEntityForSubmit();
    this.isLoading = true;
    this.roleService.update(String(entity.id), entity)
      .subscribe(
        res => {
          this.isLoading = false;
          this.afterSave.emit('success');
          this.dialogRef.afterClosed().subscribe(() => {
            this.toaster.pop('success', 'Rol', 'Rol editado exitosamente.');
          });
          this.dialogRef.close();
        },
        error => {
          this.isLoading = false;
          this.errorMessages.push('Ha ocurrido un error al intentar guardar.');
        });
  }

  protected buildEntityForSubmit(): any {
    const permissions = (<Array<Permission>> this.form.get('permissions').value).map( p => {
      return p.id;
    });
    const entity = <any> Object.assign({permissions: null}, this.role);
    entity.permissions = permissions;
    if (this.isEdit) {
      entity.id = this.data.idRole;
    }
    return entity;
  }


  protected initForm() {
    this.form = this.formBuilder.group({
      permissions: [[], [Validators.required]]
    });

  }
}
