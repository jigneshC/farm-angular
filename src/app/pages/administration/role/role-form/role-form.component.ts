import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {RoleService} from '../../../../services/api/administration/role.service';
import {Permission} from '../../../../models/permission';
import {PermissionService} from '../../../../services/api/administration/permission.service';
import {Role} from '../../../../models/role';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'az-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.scss']
})
export class RoleFormComponent implements OnInit {

  public title: string;
  public errorMessages = [];
  public permissionList: Array<Permission> = [];
  public form: FormGroup;
  public isLoading = false;
  public isEdit = false;
  public selectedPermissions: Array<Permission> = [];

  @Output() afterSave = new EventEmitter<string>(true);

  constructor(protected dialogRef: MatDialogRef<RoleFormComponent>,
              protected formBuilder: FormBuilder,
              protected roleService: RoleService,
              protected permissionService: PermissionService,
              protected toaster: ToasterService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.initForm();
    if (this.data.idRole) {
      this.initializeForEdition()

    } else {
      this.initializeForCreation()
    }
  }

  protected initForm() {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(255)]],
      permissions: [[], [Validators.required]]
    });
  }

  protected getPermisos() {
    this.isLoading = true;
    this.permissionService.findAll()
      .subscribe(
        permisos => {
          this.isLoading = false;
          this.permissionList = permisos;
        },
        error => {
          this.isLoading = false;
          this.errorMessages.push('Ocurrió un error al obtener los permisos.');
        }
      )
  }

  close(): void {
    this.dialogRef.close();
  }

  protected initializeForCreation(): void {
    this.getPermisos();
    this.isEdit = false;
    this.title = 'Crear Rol';
  }

  protected initializeForEdition(): void {
    this.isEdit = true;
    this.title = 'Editar Rol';
    this.isLoading = true;
    Observable.forkJoin(this.roleService.findOne(this.data.idRole), this.permissionService.findAll())
      .subscribe(
        result => {
          const role = result[0];
          this.selectedPermissions = result[1].filter(p => {
            return role.permissions.filter(per => p.id === per.id).length > 0;
          });
          this.permissionList = result[1];
          this.updateForm({name: role.name, description: role.description, permissions: this.selectedPermissions})
          this.isLoading = false;
        },
        error => {
          this.errorMessages = ['Ha ocurrido un error al obtener el rol.'];
          this.isLoading = false;
        });
  }

  submit(): void {
    if (this.isEdit) {
      const entity = this.buildEntityForSubmit();
      this.isLoading = true;
      this.roleService.update(String(entity.id), entity)
        .subscribe(
          res => {
            this.isLoading = false;
            this.afterSave.emit('success');
            this.dialogRef.afterClosed().subscribe(() => {
              this.toaster.pop('success', 'Rol', 'Rol editado exitosamente.');
            });
            this.dialogRef.close();
          },
          error => {
            this.isLoading = false;
            this.errorMessages.push('Ha ocurrido un error al intentar guardar.');
          });
    } else {
      const entity = this.buildEntityForSubmit();
      this.isLoading = true;
      this.roleService.create(entity)
        .subscribe(res => {
            this.isLoading = false;
            this.afterSave.emit('success');
            this.dialogRef.afterClosed().subscribe(() => {
              this.toaster.pop('success', 'Rol', 'Rol guardado exitosamente.');
            });
            this.dialogRef.close();
          },
          error => {
            this.isLoading = false;
            this.errorMessages.push('Ha ocurrido un error al intentar guardar.');
          });
    }
  }

  // Workaround to update form validity.
  protected updateForm(value) {
    setTimeout(() => {
      this.form.patchValue(value);
    });
  }

  protected buildEntityForSubmit(): Role {
    const permissions = (<Array<Permission>> this.form.get('permissions').value).map( p => {
      return p.id;
    });
    const entity = Object.assign({id: null, permissions: null}, this.form.value);
    entity.permissions = permissions;
    if (this.isEdit) {
      entity.id = this.data.idRole;
    }
    return entity;
  }

  protected validateSelectedPermissions(): boolean {
    return this.selectedPermissions.length !== 0;
  }

  isPermissionSelected(permission: Permission): boolean {
    for (const permiso of this.selectedPermissions) {
      if (permiso.id === permission.id) {
        return true;
      }
    }
    return false;
  }

}
