import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleComponent } from './role.component';
import {Ng2CompleterModule} from 'ng2-completer';
import {PipesModule} from '../../../theme/pipes/pipes.module';
import {DirectivesModule} from '../../../theme/directives/directives.module';
import {
  MatAutocompleteModule,
  MatCheckboxModule,
  MatDialogModule, MatInputModule, MatListModule, MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule, MatSortModule,
  MatTableModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DataTableModule} from 'angular2-datatable';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ChartsModule} from 'ng2-charts';
import {CommonModule} from '@angular/common';
import {CrudComponentsModule} from '../../../theme/components/crud-components/crud-components.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {RoleService} from '../../../services/api/administration/role.service';
import {PermissionService} from '../../../services/api/administration/permission.service';

describe('RoleComponent', () => {
  let component: RoleComponent;
  let fixture: ComponentFixture<RoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        FormsModule,
        FormsModule,
        ReactiveFormsModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatInputModule,
        MatDialogModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatListModule,
        MatCheckboxModule],
      providers: [RoleService, PermissionService],
      declarations: [RoleComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
