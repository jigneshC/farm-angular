import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { CapsWarningService } from './../../services/caps-warning.service';
import { AuthService } from './../../services/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, AbstractControl, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'az-login',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	public router: Router;
	public form:FormGroup;
	public email:AbstractControl;
	public password:AbstractControl;
	public error: string;

	constructor(router:Router, fb:FormBuilder,public auth:AuthService, private capslock: CapsWarningService) {
		this.router = router;
		this.form = fb.group({
			'email': ['', Validators.compose([Validators.required, emailValidator])],
			'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
		});

		this.email = this.form.controls['email'];
		this.password = this.form.controls['password'];
	}

  public onSubmit(values: Object): void {
    this.error = null;
    this.auth.login(this.form.value.email, this.form.value.password)
      .subscribe(
        response => {
          // Obtenemos el token antes de logearnos
          console.log(response);
          this.auth.setToken(response);
          this.auth.getUserPermissionsByToken()
            // Convert to array
            .map( p => {
                return Object.keys(p).map( val => {
                  return p[val];
                })
              }
            )
            .subscribe(
              (permissions) => {
                // Si los dos pasos tuvieron exito, entonces podemos logearnos.
                // Guardamos el token aqui.
                this.auth.storeUserPermissions(permissions);
                this.router.navigate(['pages/dashboard']);
              },
              error2 => {
                this.error = 'Error al obtener los permisos del usuario.';
              })
        },
        error => {
          console.log(error);
          this.error = 'El correo y/o la contraseña son incorrectos.';
        }
      );
  }

	ngOnInit() {
		this.capslock.init();
	}
}

export function emailValidator(control: FormControl): {[key: string]: any} {
	let emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
	if (control.value && !emailRegexp.test(control.value)) {
		return { invalidEmail: true };
	}
}
