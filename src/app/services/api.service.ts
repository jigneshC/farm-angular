import {Observable} from 'rxjs/Rx';
import {Headers, RequestOptions, Response, Http} from '@angular/http';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {AuthService} from './auth/auth.service';
import {AppConfig} from './../app.config';
import {Injectable} from '@angular/core';

@Injectable()
export class ApiService {

    constructor(
			public config: AppConfig,
			public auth: AuthService,
			public http: Http,
			private _http: HttpClient
    ) {}

    index(model, params = {}) {
        return this._http.get(this.config.config.API_URL + `api/${model}`, { params: params })
            .map((res: Response) => res)
            .catch(this.handleError);
    }

    show(model, id) {
        return this._http.get(this.config.config.API_URL + `api/${model}/${id}`)
            .map((res: Response) => res)
            .catch(this.handleError);
    }

    save(model, request) {
        return this._http.post(this.config.config.API_URL + `api/${model}`, request)
            .map((res: Response) => res)
            .catch(this.handleError);

    }

    update(model, request, id, method = "put") {
        return this._http[method](this.config.config.API_URL + `api/${model}/${id}`, request)
            .map((res: Response) => res)
            .catch(this.handleError);
    }

    delete(model, id) {
        return this._http.delete(this.config.config.API_URL + `api/${model}/${id}`)
            .map((res: Response) => res)
            .catch(this.handleError);
    }

    custom(method, url, options?) {
        return this._http[method](this.config.config.API_URL + url, options)
            .map((res: Response) => res)
            .catch(this.handleError);
    }

    getRain(model, params) {
        let headers = new Headers({'Authorization': 'Bearer ' + this.auth.getToken()});
        let options = new RequestOptions({headers: headers});
        let url = `api/days/rain?start=${params.start}&end=${params.end}`;
        if (params.month && params.year){
            url = `api/days/rain?start=${params.start}&end=${params.end}&month=${params.month}&year=${params.year}`;
        }

        return this.http.get(this.config.config.API_URL + url, options).map((res: Response) => res)
            .catch(this.handleError);
    }
    getWeatherGraph(model,params) {
        let headers = new Headers({'Authorization': 'Bearer ' + this.auth.getToken()});
        let options = new RequestOptions({headers: headers});
        let url = `api/weather/graph?from=${params.from}`;
        if (params.to){
            url = `api/weather/graph?from=${params.from}&to=${params.to}`;
        }
        if (params.station){
            url = url + `&station=${params.station}`;
        }

        return this.http.get(this.config.config.API_URL + url, options).map((res: Response) => res)
            .catch(this.handleError);
    }

    //specific endpoints  **etomax , etomin**
    specificAction(model, action, params = null) {
        let headers = new Headers({'Authorization': 'Bearer ' + this.auth.getToken()});
        let options = new RequestOptions({headers: headers});
        let url = null;
        (params) ? url = `api/${model}/${action}?month=${params.month}&year=${params.year}` : url = `api/${model}/${action}`;
        // get users from api
        return this.http.get(this.config.config.API_URL + url, options).map((res: Response) => res)
            .catch(this.handleError);
    }

    //search
    searchModel(model, param) {
        let headers = new Headers({'Authorization': 'Bearer ' + this.auth.getToken()});
        let options = new RequestOptions({headers: headers});
        // get users from api
        return this.http.get(this.config.config.API_URL + `api/${model}?q=${param}`, options).map((res: Response) => res)
    }

    private handleError(error: HttpErrorResponse) {
        return Observable.throw(error || 'Ha ocurrido un error. Por favor intenta de nuevo.');
    }

    public getCurrencies() {
      return [ { value: 0, name: 'PESOS' }, { value: 1, name: 'USD' } ];
    }
}
