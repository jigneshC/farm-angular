import { Injectable } from '@angular/core';
import * as esLocale from 'date-fns/locale/es';

@Injectable()
export class TranslateService {

  DateTimePicker = {
    es: {
      firstDayOfWeek: 1,
      dayNames:["domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
      dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
      monthNames: [ "enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre" ],
      monthNamesShort: [ "ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic" ],
      dateFns: esLocale
    }
  };

  constructor() {}
}