import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { User } from './../../models/user';
import { JwtHelper } from 'angular2-jwt';
import { AppConfig } from './../../app.config';

@Injectable()
export class AuthService {
  readonly USER_TOKEN = 'token';
  readonly USER_REFRESH_TOKEN = 'refresh_token';
  readonly USER_PERMISSIONS = 'user_permissions';

  public user: User;

  constructor(
    public http: HttpClient,
    public router: Router,
    public configService:AppConfig
  ) {}

  public getUser(): Observable<User> {
    return this.http.get( this.configService.config.API_URL + 'api/user' )
                    .map((res: Response) => res)
                    .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  public setUser( user ) {
    return this.user = user;
  }

  public login(username, password): Observable<any> {

    return this.http.post(this.configService.config.API_LOGIN, {
      username: username,
      password: password,
      client_id: this.configService.config.CLIENT_ID,
      client_secret: this.configService.config.CLIENT_SECRET,
      grant_type: 'password',
      scope: '*',
    })
      .map((res: Response) => res)
      .catch(this.handleError);
  }

  public recover(email) : Observable<any> {

    return this.http.post(this.configService.config.API_URL + 'api/auth/password/email', {
      email: email
    })
      .map((res: Response) => res)
      .catch(this.handleError);
  }

  public verifyResetToken(email, token) : Observable<any> {

    return this.http.post(this.configService.config.API_URL + 'api/auth/password/verify', {
      email: email,
      token: token,
    })
      .map((res: Response) => res)
      .catch(this.handleError);
  }

  public resetPassword(email, token, password) : Observable<any> {

    return this.http.post(this.configService.config.API_URL + 'api/auth/password/reset', {
      email: email,
      token: token,
      password: password,
      password_confirmation: password,
    })
      .map((res: Response) => res)
      .catch((err: any) => Observable.throw(err.error));
  }

  public getUserPermissionsByToken(): Observable<any> {
    return this.http.get(`${this.configService.config.API_URL}api/auth/token/permissions` );
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }

  public logout() {
    localStorage.removeItem(this.USER_TOKEN);
    localStorage.removeItem(this.USER_REFRESH_TOKEN);
    localStorage.removeItem(this.USER_PERMISSIONS);
    return this.router.navigate(['/iniciar-sesion']);
  }

  public isAuthenticated(): boolean {
    let jwtHelper: JwtHelper = new JwtHelper();

    return this.getToken() ? !jwtHelper.isTokenExpired( this.getToken() ) : false;
  }

  public setToken(credentials) {
    localStorage.setItem(this.USER_REFRESH_TOKEN, credentials.refresh_token);
    return localStorage.setItem(this.USER_TOKEN, credentials.access_token);
  }

  public getToken() {
    return localStorage.getItem(this.USER_TOKEN);
  }

  public storeUserPermissions(permissions: any) {
    return localStorage.setItem(this.USER_PERMISSIONS, JSON.stringify(permissions));
  }

  public getUserPermissions(): Array<string> {
    return JSON.parse(localStorage.getItem(this.USER_PERMISSIONS));
  }

  public userHasPermissions(allowedPermissions: Array<string>) {
    if (!allowedPermissions || allowedPermissions.length === 0) {
      return true;
    }
    const userPermissions = this.getUserPermissions();
    // if user does not have ALL of the permissions return false
    if (userPermissions && userPermissions.length > 0) {
      for (const role of allowedPermissions) {
        if (!userPermissions.some(x => x === role)) {
          return false;
        }
      }
    } else {
      return false;
    }
    return true;
  }

  public userHasParentPermission(parentPermissions: Array<string>) {
    if (!parentPermissions || parentPermissions.length === 0) {
      return true;
    }
    const userPermissions = this.getUserPermissions();
    // if user does not have ALL of the permissions return false
    if (userPermissions && userPermissions.length > 0) {
      for (const parentPermission of parentPermissions) {
        if (!userPermissions.some(x => x.startsWith(parentPermission))) {
          return false;
        }
      }
    } else {
      return false;
    }
    return true;
  }
}
