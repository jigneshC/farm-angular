import {Injectable, Injector} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Rx';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	
  constructor( 
    public inj: Injector,
  ) {}
  
  intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {
    
    const auth = this.inj.get(AuthService);
    const snackBar: MatSnackBar = this.inj.get(MatSnackBar);

    request = request.clone({
      setHeaders: {
				"Accept": "application/json",
        // "Content-Type": "application/json", NO ACTIVAR ESTA OPCION
				"Authorization": `Bearer ${auth.getToken()}`
      }
    });
    
  return next
    .handle(request)
    .do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        if (event.status === 403) {
          const config: MatSnackBarConfig = {
            duration: 3000,
            verticalPosition: 'top'
          };
          snackBar.open('No dispones de los permisos para realizar esta acción', null, config);
        }
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          auth.logout()
        } else if (err.status === 403) {
          const config: MatSnackBarConfig = {
            duration: 3000,
            verticalPosition: 'top'
          };
          snackBar.open('No dispones de los permisos para realizar esta acción', null, config);
        }
      }
    });
  }
}