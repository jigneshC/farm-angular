import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/of';

@Injectable()
export class ResetTokenGuardService implements CanActivate {
	
  constructor(
		public auth: AuthService,
		public router: Router,
  ) {}
  
  canActivate( route: ActivatedRouteSnapshot ): Observable<boolean> {
    return this.auth.verifyResetToken( route.params.email, route.params.token )
										.map( verify => true )
										.catch(() => {
											this.router.navigate(['/recover-password']);
											return Observable.of(false);
										});
  }
  
}
