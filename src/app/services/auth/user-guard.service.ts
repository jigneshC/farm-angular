import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/of';

@Injectable()
export class UserGuardService implements CanActivate {
	
  constructor(public auth: AuthService, public router: Router) {}
  
  canActivate(): Observable<any> {
    return this.auth.getUser().map( user => {
      if ( user ) {
        this.auth.setUser( user );
        return true;
      } else {
        this.auth.logout();
        return Observable.of(false);
      }
    }).catch(() => {
      this.auth.logout();
      return Observable.of(false);
    });
  }
  
}
