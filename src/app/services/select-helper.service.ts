import { ApiService } from './api.service';
import { Injectable } from '@angular/core';

@Injectable()
export class SelectHelperService {

    constructor(private API:ApiService) { }
    
    getVarieties(){  
        let options = [];
          return this.API.index('varieties').toPromise().then((res)=>{
              let varieties = res.data;
                varieties.map((result)=>{
                    let obj = {
                        value: result.id,
                        label: result.name 
                    }
                    options.push(obj);

              });
                return options;
          });
  
        
    }
    getContCenters(){  
        let options = [];
          return this.API.index('costcenters').toPromise().then((res)=>{
              let costcenters = res.data;
                costcenters.map((result)=>{
                    let obj = {
                        value: result.id,
                        label: result.name 
                    }
                    options.push(obj);

              });
                return options;
          });
  
        
    }
}