import { Injectable } from '@angular/core';

@Injectable()
export class TimeCalculatorService {
	constructor() {
	}

    /**
     * Calculate time on hours an minutes
     * @param {number} minutes Minutes
     * @returns {string}
     * @author David López <dlopez@hsd.cl>
     */
    toHumanReadFromMinutes(minutes: number) {
        let d, h, m, rest;
        let ans: string = '';
        if (minutes >= 1440) {
            d = minutes / 1440;
            ans = `${ (d > 1) ?  `${ Math.floor(d) } días, ` : `${ d } día, `}`;
            rest = minutes % 1440;
        } else {
            rest = minutes;
        }
        h = rest / 60;
        m = rest % 60;
        if (h >= 1) {
            ans += `${ (h > 1) ? `${ Math.floor(h) } horas ` : `${ h } hora `}`;
        }
        if (m === 0) {
            return ans;
        }
        if (ans.length > 0) {
            ans += ' y ';
        }
        ans += `${ (m > 1) ? `${ Math.floor(m) } minutos` : `${ m } minuto`}`;
        return ans;
    }
}