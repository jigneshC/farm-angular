import { Injectable } from '@angular/core';

@Injectable()
export class TimeKeyInputService {

    formatHour(evt, form, type = "start"){
        let deleteCode = 8;
        let backspaceCode = 46;
        let input = evt.target.value;

        let cond = false;
        var charCode = evt.keyCode;
        if (charCode == 8 || charCode == 46 || (charCode >= 96 && charCode <= 105)) cond = true;
        if (!cond){
          if ((evt.shiftKey || (charCode < 48 || charCode > 57)) && (charCode < 96 || charCode > 105)) {
            form.controls[type].setValue(input.slice(0, (input.length - 1)));
            return false;
          }
        }

         form.controls[type].setValue(input.replace( /\[\d+\]/g, ''));
          if (input && input.length > 5) { form.controls[type].setValue(input.slice(0, (input.length - 1))); return false; }
          if ((input && (input.length > 0 && input.length < 6))){
            let date = input.trim();
            if( charCode != 8 && charCode != 46){
              if (date.length === 2 && date.indexOf(':') === -1){
                if (parseInt(date) > 24) date = 24;
                form.controls[type].setValue(date+':');

              } else if (date.length === 4){
                let splitted = date.split(':');
                if (parseInt(splitted[1]) >= 6) form.controls[type].setValue(splitted[0]+':'+6);
                if (parseInt(splitted[1]) < 5) form.controls[type].setValue(splitted[0]+':'+splitted[1]);
              }
            } else if (charCode === 8 || charCode === 46){
              if (date.length > 2 && date.indexOf(':') === -1){
                form.controls[type].setValue(date.substr(0, 2)+':'+date.substr(2));
              } else if (date.length < 3 && date.indexOf(':') != -1){
                form.controls[type].setValue(date.replace(/:/g, ''));
              }
            }
          }
      }
}
