import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {Machine} from '../../../models/machinery';
import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class MachineService extends RestService<Machine> {

  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'machines');
  }

}
