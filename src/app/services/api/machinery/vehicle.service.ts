import {RestService} from '../rest.service';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {Vehicle} from '../../../models/machinery';

@Injectable()
export class VehicleService extends RestService<Vehicle> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'vehicles');
  }
}
