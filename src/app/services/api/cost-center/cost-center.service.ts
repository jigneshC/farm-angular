import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';
import {CostCenter} from '../../../models/cost-center';

@Injectable()
export class CostCenterService extends RestService<CostCenter> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'costcenters');
  }

}
