import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {TechnicalReference} from '../../../models/technical-reference';

@Injectable()
export class TechnicalReferenceService extends RestService<TechnicalReference> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'technicalreferences');
  }

}
