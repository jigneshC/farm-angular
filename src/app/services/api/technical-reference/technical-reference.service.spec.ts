import { TestBed, inject } from '@angular/core/testing';

import { TechnicalReferenceService } from './technical-reference.service';

describe('TechnicalReferenceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TechnicalReferenceService]
    });
  });

  it('should be created', inject([TechnicalReferenceService], (service: TechnicalReferenceService) => {
    expect(service).toBeTruthy();
  }));
});
