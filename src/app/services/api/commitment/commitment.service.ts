import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';
import {Commitment, Compromise} from '../../../models/commitment';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CommitmentService extends RestService<Commitment> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'invoices/compromises');
  }
  getAll(queryParams?: any): Observable<Commitment> {
    return this.http.get<Commitment>(this.url, { params: queryParams}).map(response => {
      return response;
    });
  }
  generateExportLink(parameters: URLSearchParams): string {
    return `${this.config.config.API_URL}reports/compromises?${parameters.toString()}`;
  }
}
