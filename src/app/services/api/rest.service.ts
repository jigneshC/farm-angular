import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {IRestService} from './irest.service';
import {Pagination} from './pagination';
import 'rxjs/add/operator/map';
import {AppConfig} from '../../app.config';

export abstract class RestService<T> implements IRestService<T> {

  protected url: string;

  constructor(protected http: HttpClient, public config: AppConfig, protected resource: string) {
    this.url = this.config.config.API_URL + 'api/' + resource;
  }

  findAll(queryParams?: any): Observable<T[]> {
    /*Si va el parametro -1 en p, trae todos los registros*/
    if (queryParams instanceof HttpParams) {
      queryParams.append('p', '-1')
    } else {
      if (!queryParams) {
        queryParams = {};
      }
      Object.assign( queryParams,  {p: -1})
    }
    return this.http.get<any>(this.url, { params: queryParams})
      .map(response => {
        return response.data ? response.data : response;
      });
  }

  findPaginated(queryParams?: any): Observable<Pagination<T>> {
    return this.http.get<Pagination<T>>(this.url, {params: queryParams});
  }

  findOne(id: string): Observable<T> {
    return this.http.get<T>(`${this.url}/${id}`);
  }

  create(entity: T): Observable<T> {
    return this.http.post<T>(this.url, entity);
  }

  update(id: string, entity: T): Observable<T> {
    return this.http.put<T>(`${this.url}/${id}`, entity);
  }

  update_post(id: string, entity: T): Observable<T> {
    return this.http.post<T>(`${this.url}/${id}`, entity);
  }

  delete(id: string): Observable<T> {
    return this.http.delete<T>(`${this.url}/${id}`);
  }

}
