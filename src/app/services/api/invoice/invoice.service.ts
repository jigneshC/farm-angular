import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';
import {Invoice} from '../../../models/invoice';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class InvoiceService extends RestService<Invoice> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'invoices');
  }

  downloadPDF(id: number) {
    return this.http.get(this.url + `/${id}/download`)
  }

  generateLinkReport(parameters: URLSearchParams): string {
    return `${this.config.config.API_URL}reports/invoices?${parameters.toString()}`;
  }

  generateExportLink(parameters: URLSearchParams): string {
    return `${this.config.config.API_URL}invoice/reports?${parameters.toString()}`;
  }
}
