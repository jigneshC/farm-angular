import {Observable} from 'rxjs/Observable';
import {Pagination} from './pagination';

export interface IRestService<T> {
    findAll(): Observable<T[]>;
    findPaginated(queryParams: any): Observable<Pagination<T>>;
    findOne(id: string): Observable<T>;
    create(entity: T): Observable<T>;
    update(id: string, entity: T): Observable<T>;
    delete(id: string): Observable<T>;
}
