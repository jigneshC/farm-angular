import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';
import {Expense} from '../../../models/expense';

@Injectable()
export class ExpenseService extends RestService<Expense> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'expenses');
  }

  downloadPDF(id: number) {
    return this.http.get(this.url + `/${id}/download`)
  }

  generateLinkReport(parameters: URLSearchParams): string {
    return `${this.config.config.API_URL}expenses/report?${parameters.toString()}`;
  }
}
