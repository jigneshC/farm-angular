import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {Machine} from '../../../models/machine';

@Injectable()
export class MachineService extends RestService<Machine> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'machines');
  }

}
