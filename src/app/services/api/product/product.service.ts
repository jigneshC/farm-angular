import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {Product} from '../../../models/product';

@Injectable()
export class ProductService extends RestService<Product> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'products');
  }
  linkReportLink(output: string): string {
    return `${this.config.config.API_URL}reports/products?` + 'output=' + output;
  }
  getPackages(): any {
    return [ { value: 0, name: 'LITROS' }, { value: 1, name: 'KILOS' } ];
  }
}
