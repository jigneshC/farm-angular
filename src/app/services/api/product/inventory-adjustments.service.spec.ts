import { TestBed, inject } from '@angular/core/testing';

import { InventoryAdjustmentsService } from './inventory-adjustments.service';

describe('InventoryAdjustmentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InventoryAdjustmentsService]
    });
  });

  it('should be created', inject([InventoryAdjustmentsService], (service: InventoryAdjustmentsService) => {
    expect(service).toBeTruthy();
  }));
});
