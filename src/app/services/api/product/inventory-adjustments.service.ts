import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {InventoryAdjustment} from '../../../models/inventory-adjustment';

@Injectable()
export class InventoryAdjustmentService extends RestService<InventoryAdjustment> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'inventoryadjustments');
  }

}
