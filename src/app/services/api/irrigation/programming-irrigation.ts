import {HttpClient} from '@angular/common/http';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {Injectable} from '@angular/core';
import {ProgrammingIrrigation} from '../../../models/programming-irrigation';

@Injectable()
export class ProgrammingIrrigationService extends RestService<ProgrammingIrrigation> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'programmingirrigations');
  }

  start(mask: number, device: number) {
    return this.http.post(this.url + '/start', { mask: mask, device: device });
  }

  stop (device: number) {
    return this.http.post(this.url + '/stop', { device: device });
  }
}
