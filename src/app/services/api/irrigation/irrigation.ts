import {HttpClient} from '@angular/common/http';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {Irrigation} from '../../../models/irrigation';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class IrrigationService extends RestService<Irrigation> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'irrigations');
  }

  getDevices(queryParams?: any): Observable<any> {
    return this.http.get(this.config.config.API_URL + 'api/' + 'irrigationdevices');
  }
}
