import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {Sector} from '../../../models/sector';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class SectorService extends RestService<Sector> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'sectors');
  }

  findAllProgrammable(queryParams?: any): Observable<any> {
    /*Si va el parametro -1 en p, trae todos los registros*/
    if (queryParams instanceof HttpParams) {
      queryParams.append('p', '-1')
    } else {
      if (!queryParams) {
        queryParams = {};
      }
      Object.assign( queryParams,  {p: -1})
    }
    return this.http.get<any>(this.url + '/programmable', { params: queryParams})
      .map(response => {
        return response.data ? response.data : response;
      });
  }
}
