import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';
import {Provider} from '../../../models/provider';

@Injectable()
export class ProviderService extends RestService<Provider> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'providers');
  }

}
