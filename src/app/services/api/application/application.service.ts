import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ApplicationService extends RestService<any> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'applications');
  }

  getLnkPDF(id: string): string {
    return `${this.config.config.API_URL}applications/download/${id}`;
  }

  getApplicators() {
    return [
      {id: 'Carlos Ilabaca', name: 'Carlos Ilabaca'},
      {id: 'Claudio Piñones', name: 'Claudio Piñones'},
      {id: 'Gabriel Tapia', name: 'Gabriel Tapia'},
      {id: 'Luis Atilio', name: 'Luis Atilio'},
      {id: 'Maicol Robles', name: 'Maicol Robles'},
      {id: 'Omar Valencia', name: 'Omar Valencia'},
      {id: 'Rosamel Araya', name: 'Rosamel Araya'},
      {id: 'Contratista', name: 'Contratista'},
      {id: 'Sergio Cortes', name: 'Segio Cortes'}
    ];
  }

}
