import {Injectable} from '@angular/core';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';
import {Product} from '../../../models/product';

@Injectable()
export class NpkService extends RestService<Product> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'npks');
  }

}
