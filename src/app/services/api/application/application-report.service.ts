import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {ApplicationReportModel} from '../../../models/application-report';

export abstract class  ApplicationReportType  {
  public static readonly HECTAREA   = 'byhas';
  public static readonly SECTOR     = 'bysector';
  public static readonly NPK        = 'bynpks'
}

@Injectable()
export class ApplicationReportService {
  protected url: string;
  constructor(protected http: HttpClient, public config: AppConfig) {
    this.url = this.config.config.API_URL + 'api/' + 'applications/';
  }

  get(reportType: string, queryParams?: any): Observable<ApplicationReportModel[]> {
    let resource: string = null;
    switch (reportType) {
      case ApplicationReportType.HECTAREA:
        resource = ApplicationReportType.HECTAREA;
        break;
      case ApplicationReportType.NPK:
        resource = ApplicationReportType.NPK;
        break;
      case ApplicationReportType.SECTOR:
        resource = ApplicationReportType.SECTOR;
        break;
      default:
        throw new Error('Wrong report type');
    }
    if (queryParams) {
      queryParams['page'] = -1;
    } else {
      queryParams = { page: -1 };
    }
    return this.http.get(this.url + resource, { params: queryParams})
      .map( (response: any) => {
        if (response.success) {
          return response.data.map( val => {
            const model = new ApplicationReportModel();
            model.sectors = val.sectors;
            model.product = val.npk || val.product;
            return model;
          })
        } else {
          throw new Error(`Error al obtener los datos del reporte ${resource}`);
        }
      })
  }

}
