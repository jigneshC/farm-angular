import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {Tractor} from '../../../models/tractor';

@Injectable()
export class TractorService extends RestService<Tractor> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'tractors');
  }

}
