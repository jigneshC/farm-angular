import { TestBed, inject } from '@angular/core/testing';

import { TractorService } from './tractor.service';

describe('TractorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TractorService]
    });
  });

  it('should be created', inject([TractorService], (service: TractorService) => {
    expect(service).toBeTruthy();
  }));
});
