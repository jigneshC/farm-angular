import {RestService} from '../rest.service';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {Configuration} from '../../../models/configuration';

@Injectable()
export class ConfigurationService extends RestService<Configuration> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'configs');
  }
}
