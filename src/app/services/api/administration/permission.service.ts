import {RestService} from '../rest.service';
import {Permission} from '../../../models/permission';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';

@Injectable()
export class PermissionService extends RestService<Permission> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'permissions');
  }
}
