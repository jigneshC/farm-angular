import {RestService} from '../rest.service';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {Role} from '../../../models/role';

@Injectable()
export class RoleService extends RestService<Role> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'roles');
  }
}
