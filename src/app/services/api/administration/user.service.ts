import {RestService} from '../rest.service';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {User} from '../../../models/user';

@Injectable()
export class UserService extends RestService<User> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'users');
  }
}
