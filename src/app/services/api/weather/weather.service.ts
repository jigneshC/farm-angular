import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {Sector} from '../../../models/sector';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class WeatherService extends RestService<Sector> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'weather');
  }

  getLastUpdate(): Observable<any> {
    let query = new HttpParams();
    query = query.append('p', '1');
    return this.http.get<any>(this.url + '/archive', { params: query})
      .map(response => {
        return response.data ? response.data[0] : response;
      });
  }
}
