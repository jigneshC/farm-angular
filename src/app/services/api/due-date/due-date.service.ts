import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {HttpClient, HttpParams} from '@angular/common/http';
import {DueDate} from '../../../models/due-date';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class DueDateService extends RestService<DueDate> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'invoices/dues');
  }

/*  getAll(queryParams?: any): Observable<DueDate> {
    /!*Si va el parametro -1 en page, trae todos los registros*!/
    const requestParams = Object.assign({}, queryParams, {page: -1});
    return this.http.get<DueDate>(this.url, { params: requestParams})
      .map(response => {
        return response;
      });
  }*/

  getAll(queryParams?: any): Observable<DueDate> {
    return this.http.get<DueDate>(this.url, { params: queryParams}).map(response => {
      return response;
    });
  }

  downloadReport(month: string, year: string) {
    const queryParams = new HttpParams();
    queryParams.append('month', month);
    queryParams.append('year', year);
    return this.config.config.API_URL + `reports/due?month=${month}&year=${year}`;
  }

}
