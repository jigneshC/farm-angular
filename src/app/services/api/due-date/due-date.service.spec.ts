import { TestBed, inject } from '@angular/core/testing';

import { DueDateService } from './due-date.service';

describe('DueDateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DueDateService]
    });
  });

  it('should be created', inject([DueDateService], (service: DueDateService) => {
    expect(service).toBeTruthy();
  }));
});
