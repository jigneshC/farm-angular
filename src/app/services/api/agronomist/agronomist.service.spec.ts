import { TestBed, inject } from '@angular/core/testing';

import { AgronomistService } from './agronomist.service';

describe('AgronomistService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgronomistService]
    });
  });

  it('should be created', inject([AgronomistService], (service: AgronomistService) => {
    expect(service).toBeTruthy();
  }));
});
