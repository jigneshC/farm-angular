import { Injectable } from '@angular/core';
import {AppConfig} from '../../../app.config';
import {RestService} from '../rest.service';
import {HttpClient} from '@angular/common/http';
import {Agronomist} from '../../../models/agronomist';

@Injectable()
export class AgronomistService extends RestService<Agronomist> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'technicalreferences/agronomists');
  }

}
