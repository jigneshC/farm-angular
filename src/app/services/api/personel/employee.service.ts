import {RestService} from '../rest.service';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {Employee} from '../../../models/liquidation';

@Injectable()
export class EmployeeService extends RestService<Employee> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'employees');
  }

  generateLinkReport(month: string, year: string): string {
    const params = new URLSearchParams();
    params.set('month', month);
    params.set('year', year);
    return `${this.config.config.API_URL}reports/display?${params.toString()}`;
  }
}
