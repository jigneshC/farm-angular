import {RestService} from '../rest.service';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {LiquidationModel} from '../../../models/liquidation';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LiquidationService extends RestService<LiquidationModel> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'liquidations');
  }

  postFile(formData: FormData, employeeId: string, month: string, year: string): Observable<any> {
    return this.http.post(`${this.url}/upload/${employeeId}/${month}/${year}`, formData);
  }

  getFile(employeeId: string, month: string, year: string): Observable<any> {
    return this.http.get(`${this.url}/download/${employeeId}/${month}/${year}`);
  }

  linkGenerateLiquidation(month: string, year: string): string {
    const params = new URLSearchParams();
    params.set('month', month);
    params.set('year', year);
    return `${this.config.config.API_URL}employees/liquidation?${params.toString()}`;
  }
}
