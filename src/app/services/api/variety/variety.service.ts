import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../../../app.config';
import {RestService} from '../rest.service';
import {Variety} from '../../../models/variety';

@Injectable()
export class VarietyService extends RestService<Variety> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'varieties');
  }

}
