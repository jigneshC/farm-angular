import { Injectable } from '@angular/core';
import {RestService} from '../rest.service';
import {AppConfig} from '../../../app.config';
import {HttpClient} from '@angular/common/http';
import {Maintenance} from '../../../models/maintenance';

@Injectable()
export class MaintenanceService extends RestService<Maintenance> {
  constructor(protected http: HttpClient, public config: AppConfig) {
    super(http, config, 'maintenances');
  }
}
