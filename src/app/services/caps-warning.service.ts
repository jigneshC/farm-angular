import { Injectable } from '@angular/core';

@Injectable()
export class CapsWarningService {
	capslock;
	
	constructor() {
		let txtWarning = `Cuidado! la tecla 'Bloq. Mayús.' está activa`;
		let capslock = {
			init: function() {
				if (!(<any>document).getElementsByTagName) {
					return;
				}
				let inps = (<any>document).getElementsByTagName("input");
				for (let i=0, l=inps.length; i<l; i++) {
					if (inps[i].type == "password") {
						capslock.addEvent(inps[i], "keypress", capslock.keypress);
					}
				}
			},
			addEvent: function(obj,evt,fn) {
				if ((<any>document).addEventListener) {
					capslock.addEvent = function (obj,evt,fn) {
						obj.addEventListener(evt,fn,false);
					};
					capslock.addEvent(obj,evt,fn);
				} else if ((<any>document).attachEvent) {
					capslock.addEvent = function (obj,evt,fn) {
						obj.attachEvent('on'+evt,fn);
					};
					capslock.addEvent(obj,evt,fn);
				} else { }
			},
			keypress: function(e) {
				let ev = e ? e : window.event;
				if (!ev) {
					return;
				}
				let targ = ev.target ? ev.target : ev.srcElement;
				let which = -1;
				if (ev.which) {
					which = ev.which;
				} else if (ev.keyCode) {
					which = ev.keyCode;
				}
				let shift_status = false;
				if (ev.shiftKey) {
					shift_status = ev.shiftKey;
				} else if (ev.modifiers) {
					shift_status = !!(ev.modifiers & 4);
				}
				if (((which >= 65 && which <=  90) && !shift_status) ||
						((which >= 97 && which <= 122) && shift_status)) {
					capslock.show_warning(targ, this.parentNode);
				} else {
					capslock.hide_warning(targ, this.parentNode);
				}
			},
			show_warning: function(targ, elem) {
				if (!targ.warning) {
					let text = (<any>document).createTextNode(txtWarning);
					targ.warning = (<any>document).createElement("SPAN");
					targ.warning.setAttribute("class", "help-block text-warning d-block");
					targ.warning.appendChild(text);
					elem.appendChild(targ.warning);
				}
			},
			hide_warning: function(targ, elem) {
				if (targ.warning) {
					elem.removeChild(targ.warning);
					targ.warning = null;
				}
			}
		}
		this.capslock = capslock;
	}
	
	init() {
		this.capslock.init();
	}
}