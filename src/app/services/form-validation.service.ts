import { Injectable } from '@angular/core';

@Injectable()
export class FormValidationService {

    emailValidator(control): {[key: string]: any} {
        let emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
        if (control.value && !emailRegexp.test(control.value)) {
            return {invalidEmail: true};
        }
    }

    matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
        return (group) => {
            let password= group.controls[passwordKey];
            let passwordConfirmation= group.controls[passwordConfirmationKey];
            if (password.value !== passwordConfirmation.value) {
                return passwordConfirmation.setErrors({mismatchedPasswords: true})
            }
        }
    }

     numberValidator(control): {[key: string]: any} {
        let onlyNumberRegexp = /.*[^0-9].*/;
        if (control.value && onlyNumberRegexp.test(control.value)) {
            return {invalidNumber: true};
        }
    }
    capValidator(control): {[key: string]: any} {
        let notCapRegexp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
        if (control.value && notCapRegexp.test(control.value)) {
            return {invalidCap: true};
        }
    }
}
