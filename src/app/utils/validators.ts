import {AbstractControl, ValidatorFn, FormControl} from '@angular/forms';
import * as moment from 'moment';

export class DateValidators {

  /**
   * Evalua si la primera fecha es menor a la segunda.
   * @param {string} dateField1 - primera fecha
   * @param {string} dateField2 - segunda fecha
   * @param validatorField - ValidatorField a retornar
   * @returns {ValidatorFn}
   */
  static dateLessThan(dateField1: string, dateField2: string, validatorField: { [key: string]: boolean }): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
      const date1 = c.get(dateField1).value;
      const date2 = c.get(dateField2).value;
      if ((date1 !== null && date2 !== null) && date1 > date2) {
        return validatorField;
      }
      return null;
    };
  }

  /**
   * Evalua si la primera fecha es mayor a la segunda.
   * @param {string} dateField1 - primera fecha
   * @param {string} dateField2 - segunda fecha
   * @param validatorField - ValidatorField a retornar
   * @returns {ValidatorFn}
   */
  static dateGreaterThan(dateField1: string, dateField2: string, validatorField: { [key: string]: boolean }): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
      const date1 = c.get(dateField1).value;
      const date2 = c.get(dateField2).value;
      if ((date1 !== null && date2 !== null) && date1 < date2) {
        return validatorField;
      }
      return null;
    };
  }

  /**
   * Evalua si hay un periodo de 'n' dias entre la primera fecha y la segunda fecha.
   * @param {string} dateField1 - primera fecha
   * @param {string} dateField2 - segunda fecha
   * @param {number} days - numero de dias
   * @param validatorField - ValidatorField a retornar
   * @returns {ValidatorFn}
   */
  static dateInRangeOf(dateField1: string, dateField2: string, days: number, validatorField: { [key: string]: boolean }): ValidatorFn {
    return (c: AbstractControl): { [key: string]: boolean } | null => {
      const date1 = c.get(dateField1).value;
      const date2 = c.get(dateField2).value;
      if (date1 && date2) {
        const daysBetween = moment(date1).diff(moment(date2), 'days');
        if (Math.abs(daysBetween) > days) {
          return validatorField;
        }
      }
      return null;
    };

  }

  /**
   * Valida el rut con su cadena completa "XXXXXXXX-X"
   * @param {string} myRut - primera fecha
   * @param validatorField - ValidatorField a retornar
   * @returns {ValidatorFn}
   */
  static validateRut(control: FormControl) {
    let myRut = control.value;
    if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( myRut )) return { "Rut": true };
    let tmp 	= myRut.split('-');
    let digv	= tmp[1]; 
    let rut 	= tmp[0];
    if ( digv == 'K' ) digv = 'k' ;
    if (DateValidators.dv(rut) == digv) {
      return null;
    } else {
      return { "Rut": true }
    }
  }
    
  static dv(T){
    var M=0,S=1;
    for(;T;T=Math.floor(T/10))
      S=(S+T%10*(9-M++%6))%11;
    return S?S-1:'k';
  }
}
