/**
 * Interfaz padre para todos los parsers de graficas.
 */
export interface IGraphParser<T, E> {
  parse(t: T[]): E[];
}

/**
 * Modelo de datos para las graficas stackedBar
 * Se pueden ir agregando sucesivamente para PieCharts, Line Charts, etc.
 * Ver https://swimlane.github.io/ngx-charts/#/ngx-charts/bar-vertical
 */
export class StackedBarModel {
  name: string;
  series: Array<{name: string, value: number, data?: any}>;
  id: number;
}

/**
 * Interfaz Parser para las graficas stackedBar. Implementarla cuando se necesita una funcionalidad que convierta datos
 * de logica de negocio a grafica. Ejemplo: ApplicationReportParser (parser.ts)
 */
export interface StackedBarParser<T> extends IGraphParser<T, StackedBarModel> {
  parse(t: T[]): StackedBarModel[];
}
