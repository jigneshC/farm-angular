export default function DateGTM ( dateString ) : Date {
  let date = new Date(dateString);
  date = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
  return date;
}
