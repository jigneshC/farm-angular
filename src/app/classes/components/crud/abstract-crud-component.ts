import {AfterViewInit, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormGroup} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../services/api.service';
import {Observable} from 'rxjs/Observable';
import {HttpParams} from '@angular/common/http';

export abstract class AbstractCrudComponent implements AfterViewInit, OnInit{

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  source = new MatTableDataSource();
  form: FormGroup;
  total: number = 0;
  limit: number = 10;
  loadingResults: boolean = true;
  haveError: boolean = false;
  protected _current: any = {};
  filter: string;
  timeout;
  errorMessages;

  protected constructor(
    private model: string,
    public columns: string[],
    private messagesConfig: IMessagesConfig,
    private toaster: ToasterService,
    private API: ApiService
  ) {

  }

  protected abstract createForm(): FormGroup;

  protected abstract onAfterViewInit(): void;

  ngOnInit(): void {
    this.form = this.createForm();
  }

  ngAfterViewInit(): void {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );

    Observable.merge(this.sort.sortChange, this.paginator.page)
      .startWith(null)
      .switchMap(() => {
        this.loadingResults = true;
        let params = new HttpParams();

        if ( this.paginator.pageIndex >= 0 ) params = params.append('page', `${this.paginator.pageIndex+1}`);
        if ( this.paginator.pageSize ) params = params.append('p', `${this.paginator.pageSize}`);
        if ( this.filter ) params = params.append('q', this.filter);
        if ( this.sort.active ) params = params.append('sort_by', this.sort.active);
        if ( this.sort.direction ) params = params.append('sort_dir', this.sort.direction);

        return this.API.index(this.model, params);
      })
      .map(data => {
        this.loadingResults = false;
        this.haveError = false;
        this.total = data.total;
        this.limit = data.per_page;

        return data.data;
      })
      .catch( err => {
        this.loadingResults = false;
        this.haveError = true;
        return Observable.of([]);
      })
      .subscribe(data => this.source.data = data);
    this.onAfterViewInit();
  }

  public reload() {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.source.data = [];
    this.filter = "";
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    this.limit = 10;
    this.paginator.page.emit();
  }

  public create() {
    this._current = {};
    this.form = this.createForm();
  }

  public get current(): any {
    return this._current;
  }

  public set current(value: any) {
    this._current = value;
  }

  public edit( row: any ): void {
    this._current = row;
    this.form = this.createForm();
  }

  public save(form) {
    if ( !this.form.valid ) return;
    this.current.requesting = true;
    this.errorMessages = undefined;
    let method = "save";

    if ( this.current.id ) method = "update";

    this.cleanForm();

    this.API[method](this.model, form, this.current.id)
      .subscribe(
        res => {
          this.toaster.pop('success', this.messagesConfig.title, `El item ha sido ${ this.current.id ? "actualizado" : "creado" } exitosamente`);
          this.current.requesting = false;
          if ( !this.current.id ) this.paginator.pageIndex = 0;
          this.paginator.page.emit();
          jQuery('#form-modal').modal('toggle');
        }, err => {
          this.toaster.pop('error', this.messagesConfig.title, `Ha ocurrido un error al ${ this.current.id ? "actualizar" : "crear" } el item`);
          this.current.requesting = false;
          this.errorMessages = err.error.errors;
          for ( let name in this.errorMessages ) {
            this.form.controls[name].setValue('');
          }
        })
  }

  protected abstract cleanForm();

  public delete() {
    this.API.delete(this.model, this._current.id)
      .subscribe(
        res => {
          if ( this.source.data.length == 1 && this.paginator.pageIndex > 0 ) this.paginator.pageIndex--;
          this.paginator.page.emit();
          this.toaster.pop('success', this.messagesConfig.title, this.messagesConfig.successDeleteBody);
          this._current = {};
        }, err => {
          this.toaster.pop('error', this.messagesConfig.title, this.messagesConfig.errorDeleteBody);
        })
  }


  public search(): void {
    if ( this.timeout ) clearTimeout(this.timeout);
    this.timeout = setTimeout( () => {
      this.paginator.pageIndex = 0;
      this.paginator.page.emit();
      delete this.timeout;
    }, 250);
  }

  public selectRow( row, model, form ): void {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  public searchAutoComplete( q, variable, model ) {
    if ( model && this._current ) this._current.requesting = true;
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      if ( model && this._current ) this._current.requesting = false;
      this[variable] = data.data;
    });
  }
}

export interface IMessagesConfig {

  title: string;

  successDeleteBody: string;

  errorDeleteBody: string;

}
