import { HttpParams } from '@angular/common/http';
import { ApiService } from '../../../services/api.service';
import { FormGroup } from '@angular/forms';
import { EventEmitter, Output } from '@angular/core';

export abstract class AbstractCrudFilterComponent {

  @Output() filter: EventEmitter<any> = new EventEmitter();

  public form: FormGroup;

  constructor(
    private API: ApiService
  ) {

  }

  searchAutoComplete( q, variable, model ) {
    let params = new HttpParams();
    if ( q ) params = params.append('q', q);
    return this.API.index(model, params).subscribe(data => {
      this[variable] = data.data;
    });
  }

  selectRow( row, model, form ) {
    this[form].controls[model+"_id"].setValue(row.id);
  }

  runFiltering() {
    this.filter.emit(
      this.form.value
    )
  }

}
