export class Commitment {
  data: {
    total: number;
    compromises: Compromise[]
  }
}

export class Compromise {
  date: string;
  amount: number;
}
