export class Variety {
  id: number;
  name: string;
  specie: object;
}
