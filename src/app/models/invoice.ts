import {Provider} from './provider';

export class Invoice {
  id: number;
  provider: Provider;
  provider_id: number;
  type: number;
  season: {
    id: number;
    year: number;
  };
  number: number;
  issue_date: string;
  due_date: string;
  gross_value: number;
  tax_payed: boolean;
  exchange_rate: number;
  state: number;
  accounting: boolean;
  dollar_amount: number;
  tax: number;
  tax_specific: number;
  exempt_amount: number;
  is_unseasonable: boolean;
  notes: string;
  created_at: string;
  updated_at: string;
}
