export class Product {
  id: number;
  name: string;
  package: number;
  quantity_per_package: number;
  formule: string;
  classification: number;
  reentry: number;
  notes: string;
  objective: string;
  phone: string;
  address: string;
  created_at: string;
  updated_at: string;
}
