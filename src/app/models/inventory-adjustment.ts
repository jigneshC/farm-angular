import {Product} from './product';

export class InventoryAdjustment {
  id: number;
  date: string;
  notes: string;
  products: Product[];
}
