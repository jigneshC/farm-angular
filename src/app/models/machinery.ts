export class MachineryEntity {
  id: number;
}
export class Vehicle extends MachineryEntity {
  brand: string;
  model: string;
  year: string;
  identifier: string;
  notes: string;
  image_url: string;
  created_at: string;
  updated_at: string;
}

export class Machine extends Vehicle {

}
