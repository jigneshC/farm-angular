export class ProgrammingIrrigation {
  sector_id: number;
  start: string;
  end: string;
  notes: string
}
