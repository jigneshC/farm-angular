import {Product} from './product';

export class ApplicationReportModel {
  product: Product;
  sectors: Array<SectorInfo>
}

export class SectorInfo {
  sector_name: string;
  sector_id: number;
  quantity: number;
}
