import {Contact} from './contact';

export class Provider {
  id: number;
  name: string;
  tax_id: string;
  address: string;
  city: string;
  business_activity: string;
  email: string;
  phone: string;
  notes: string;
  contact: Contact;
  is_customer: boolean;
  is_provider: boolean;
  allow_invoice: boolean;
  created_at: string;
  updated_at: string;
}
