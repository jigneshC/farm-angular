export class Contact {
  id: number;
  first_name: string;
  last_name: string;
  phone: string;
  address: string;
  notes: string
}
