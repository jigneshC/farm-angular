export class TechnicalReference {
  id: number;
  agronomist: string;
  number: string;
  date: string;
  notes: string;
  season: string;
  file: any;
  variety_id: number;
  variety: any
}
