import {Provider} from './provider';

export class DueDate {
  data: [{
    id: number;
    provider: Provider;
    type: number;
    season: {
      id: number;
      year: number;
    };
    number: number;
    issue_date: string;
    due_date: string;
    gross_value: number;
    tax_payed: boolean;
    exchange_rate: number;
    state: number;
    accounting: boolean;
    dollar_amount: number;
    tax: number;
    tax_specific: number;
    exempt_amount: number;
    is_unseasonable: boolean;
    notes: string;
    created_at: string;
    updated_at: string;
  }];
  links: {
    first: string;
    last: string;
    prev: string;
    next: string
  };
  meta: {
    current_page: string;
    from: number;
    last_page: number;
    path: string;
    per_page: number;
    to: number;
    total: number;
    totals: {
      total: number;
      total_gross: number;
      total_tax: number;
      total_specific_tax: number
    }
  }
}
