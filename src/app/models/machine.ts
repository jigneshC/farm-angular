export class Machine {
  id: number;
  brand: string;
  model: string;
  year: string;
  identifier: string;
  notes: string;
}
