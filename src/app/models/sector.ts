export class Sector {
  id: number;
  size: number;
  distance: string;
  precipitation: number;
  plantation_year: number;
  name: string;
  variety: {}
}
