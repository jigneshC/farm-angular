import {Permission} from './permission';

export class Role {
  id: number;
  name: string;
  display_name: string;
  description: string;
  permissions: Array<Permission>;
  created_at: string;
  updated_at: string;
}
