import {Tractor} from './tractor';
import {Machine} from './machine';
import {Vehicle} from './machinery';
import {Invoice} from './invoice';
import {Expense} from './expense';

export class Maintenance {
  id: number;
  type: number;
  description: string;
  repairer: string;
  date: string;
  tractor: Tractor;
  machine: Machine;
  vehicle: Vehicle;
  invoice: Invoice;
  expense: Expense;
  created_at: string;
  updated_at: string;
}
