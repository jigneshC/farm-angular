export class CostCenter {
  id: number;
  name: string;
  notes: string;
  created_at: string;
  updated_at: string;
}
