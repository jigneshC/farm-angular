export class LiquidationModel {
  public employee: Employee;
  public liquidations: Liquidation[];
}

export class Liquidation {
  date: string;
  has_file: boolean;
}
export class Employee {
  id: number;
  costcenter_id: number;
  name: string;
  tax_id: number;
  birth_date: string;
  contract_date: string;
  fired_date: string;
  labor: string;
  status: number;
  created_at: string;
  updated_at: string;
  afp: number;
  isapre: number;
}
