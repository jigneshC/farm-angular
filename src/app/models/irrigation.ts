export class Irrigation {
  sector_id: number;
  application_id: number;
  start: string;
  end: string;
  notes: string;
  water: number;
  fertilizer1: number;
  fertilizer2: number;
  fertilizer3: number;
  chlorination: true;
  tail: true;
  acidification: true;
  date: string;
}
