import {Invoice} from './invoice';
import {CostCenter} from './cost-center';

export class Expense {
  id: number;
  costcenter_id: number;
  costcenter: CostCenter;
  invoice: Invoice;
  date: string;
  amount: number;
  state: number;
  detail: string;
  notes: string;
  created_at: string;
  updated_at: string;
}
