import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'NumberPipe' })
export class NumberSearchPipe implements PipeTransform {
  transform(value, args?): Array<any> {
    let searchText = new RegExp(args, 'ig');
    if (value) {
      return value.filter(val=> {
        if (val.id) {
          return val.id.toString().search(searchText) !== -1;
        }
      });
    }
  }
}
