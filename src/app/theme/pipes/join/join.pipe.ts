import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'join'
})
export class JoinPipe implements PipeTransform {

  transform (input: any, prop: string, character: string = ','): any {

    if (input.length === 0) {
      return 'No';
    }

    return input.map(data => {
      return data[prop]
    }).join(character);
  }

}
