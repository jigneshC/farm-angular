import { Pipe, PipeTransform  } from '@angular/core';

@Pipe({
  name: 'joinProducts'
})
export class JoinProductPipe implements PipeTransform {

  private packages = ['Litros', 'Kilos'];

  private getPackage(index) {
    if (index > 1) { return 'Kilos' }
    return this.packages[index];
  }


  transform (input: any, character: string = 'No'): any {

    if (input.length === 0) {
      return 'No aplica';
    }

    return input.map(product => {
      return product.name + ' (' + product.pivot.total_quantity + ' ' + this.getPackage(product.package) + ')'}).join(character);
  }
}
