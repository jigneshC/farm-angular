import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'money'})
export class MoneyPipe implements PipeTransform {
  transform( input: string = "0" ):string {
    let amount = parseFloat(input) || 0;
    let formatter = new Intl.NumberFormat('es-CL', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 0,
    }).format(amount);
    
    if ( formatter.indexOf(" $") > -1 ) formatter = formatter.split(" $")[0];
    else if ( formatter.indexOf("$") > -1 ) formatter = formatter.split("$")[1];
    else if ( formatter.indexOf("$ ") > -1 ) formatter = formatter.split("$ ")[1];
    else if ( formatter.indexOf(" US$") > -1 ) formatter = formatter.split(" US$")[1];
    else if ( formatter.indexOf("US$ ") > -1 ) formatter = formatter.split("US$ ")[1];
    else if ( formatter.indexOf("US$") > -1 ) formatter = formatter.split("US$")[1];
    
    return "$ " + formatter;
  }
}
