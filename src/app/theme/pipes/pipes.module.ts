import { KeysPipe } from './search/keypipe';
import { NumberSearchPipe } from './search/number-search.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppPicturePipe } from './appPicture/appPicture.pipe';
import { ProfilePicturePipe } from './profilePicture/profilePicture.pipe';
import { MailSearchPipe } from './search/mail-search.pipe';
import { SearchPipe } from './search/search.pipe';
import { MoneyPipe } from './money/money.pipe';
import {JoinProductPipe} from './join/join-product.pipe';
import {OrderByPipe} from './order-by/order-by.pipe';
import {WithoutPipe} from './without/without.pipe';
import { JoinPipe } from './join/join.pipe';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        AppPicturePipe,
        ProfilePicturePipe,
        MailSearchPipe,
        SearchPipe,
        MoneyPipe,
        NumberSearchPipe,
        KeysPipe,
        JoinProductPipe,
        OrderByPipe,
        WithoutPipe,
        OrderByPipe,
        JoinPipe
    ],
    exports: [
      AppPicturePipe,
      ProfilePicturePipe,
      MailSearchPipe,
      SearchPipe,
      MoneyPipe,
      NumberSearchPipe,
      KeysPipe,
      JoinProductPipe,
      OrderByPipe,
      WithoutPipe,
      JoinPipe
    ]
})
export class PipesModule { }
