import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Employee} from '../../../../models/liquidation';
import {CostCenter} from '../../../../models/cost-center';
import {Observable} from 'rxjs/Observable';

export interface EmployeeDialogModel {
  costCenter: any;
  employee: Employee;
}

@Component({
  selector: 'az-employee-dialog',
  templateUrl: './employee-dialog.component.html',
  styleUrls: ['./employee-dialog.component.scss']
})
export class EmployeeDialogComponent implements OnInit {

  public costCenter: CostCenter;
  public employee: Employee;

  constructor(@Inject(MAT_DIALOG_DATA) public model: EmployeeDialogModel, protected dialogRef: MatDialogRef<any>,) {
    this.employee = model.employee;
  }

  ngOnInit() {
    if (this.model.costCenter instanceof Observable) {
      this.model.costCenter.subscribe(cc => this.costCenter = cc);
    } else {
      this.costCenter = <CostCenter> this.model.costCenter;
    }
  }

  close() {
    this.dialogRef.close();
  }

}
