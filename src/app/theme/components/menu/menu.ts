/**
 * Porfavor en caso de cambiar algun titulo, se deberia actualizar el permiso.
 *
 */
export const menuItems = [
  {
    title: 'Tablero',
    routerLink: 'dashboard',
    icon: 'fa-bar-chart fa-lg',
    selected: false,
    expanded: false,
    order: 0
  },
  {
    title: 'Suelo y Clima',
    routerLink: 'charts',
    icon: 'fa-tint fa-lg',
    selected: false,
    expanded: false,
    order: 200,
    permissionsRequired: ['suelo-clima'],
    subMenu: [
      {
        title: 'Días',
        routerLink: 'suelo-clima/dias',
        permissionsRequired: ['suelo-clima:dias']
      },
      {
        title: 'Sectores',
        routerLink: 'suelo-clima/sectores',
        permissionsRequired: ['suelo-clima:sectores']
      },
      {
        title: 'Riegos',
        routerLink: 'suelo-clima/riegos',
        permissionsRequired: ['suelo-clima:riegos']
      },
      {
        title: 'Programación de Riegos',
        routerLink: 'suelo-clima/programacion-riegos',
        permissionsRequired: ['suelo-clima:programacion-riegos']
      },
      {
        title: 'Reposición de Bandeja',
        routerLink: 'suelo-clima/reposicion-bandeja',
        permissionsRequired: ['suelo-clima:reposicion-bandeja']
      },
      {
        title: 'Calicatas',
        routerLink: 'suelo-clima/calicatas',
        permissionsRequired: ['suelo-clima:calicatas']
      },
      {
        title: 'Sondas',
        routerLink: 'suelo-clima/sondas',
        permissionsRequired: ['suelo-clima:sondas']
      },
      {
        title: 'Metereología',
        routerLink: 'suelo-clima/metereologia',
        permissionsRequired: ['suelo-clima:metereologia']
      }


    ]
  },
  {
    title: 'Contabilidad',
    routerLink: 'contabilidad',
    icon: 'fa-calculator fa-lg',
    selected: false,
    expanded: false,
    order: 200,
    permissionsRequired: ['contabilidad'],
    subMenu: [
      {
        title: 'Compras',
        routerLink: 'contabilidad/compras',
        permissionsRequired: ['contabilidad:compras']
      },
      {
        title: 'Libro de Compras',
        routerLink: 'contabilidad/libro-de-compras',
        permissionsRequired: ['contabilidad:libro-de-compras']
      },
      {
        title: 'Ventas',
        routerLink: 'contabilidad/ventas',
        permissionsRequired: ['contabilidad:ventas']
      },
      {
        title: 'Items Venta',
        routerLink: 'contabilidad/items-venta',
        permissionsRequired: ['contabilidad:items-venta']
      },
      {
        title: 'Gastos',
        routerLink: 'contabilidad/gastos',
        permissionsRequired: ['contabilidad:gastos']
      },
      {
        title: 'Gastos Fiscales',
        routerLink: 'contabilidad/gastos-fiscales',
        permissionsRequired: ['contabilidad:gastos-fiscales']
      },
      {
        title: 'Vencimientos',
        routerLink: 'contabilidad/vencimientos',
        permissionsRequired: ['contabilidad:vencimientos']
      },
      {
        title: 'Total Compromisos',
        routerLink: 'contabilidad/compromisos',
        permissionsRequired: ['contabilidad:total-compromisos']
      },
      {
        title: 'Proveedores',
        routerLink: 'contabilidad/proveedores',
        permissionsRequired: ['contabilidad:proveedores']
      },
      {
        title: 'Centro de Costos',
        routerLink: 'contabilidad/centro-costos',
        permissionsRequired: ['contabilidad:centro-costos']
      },
      {
        title: 'Reportes de Compras de Proveedores',
        routerLink: 'contabilidad/reportes-compras',
        permissionsRequired: ['contabilidad:reportes-compras']
      },
      {
        title: 'Temporadas',
        routerLink: 'contabilidad/temporadas',
        permissionsRequired: ['contabilidad:temporadas']
      },
      {
        title: 'Préstamos Bancarios',
        routerLink: 'contabilidad/prestamos-bancarios',
        permissionsRequired: ['contabilidad:prestamos-bancarios']
      }


    ]
  },
  {
    title: 'Aplicaciones',
    routerLink: 'aplicaciones',
    icon: 'fa-hand-o-right fa-lg',
    selected: false,
    expanded: false,
    order: 200,
    permissionsRequired: ['aplicaciones'],
    subMenu: [
      {
        title: 'Lista de Aplicaciones',
        routerLink: 'aplicaciones/aplicaciones',
        permissionsRequired: ['aplicaciones:lista-aplicaciones']
      },
      {
        title: 'Aplicaciones por Sector',
        routerLink: 'aplicaciones/sectores',
        permissionsRequired: ['aplicaciones:aplicaciones-sector']
      },
      {
        title: 'Aplicaciones por NPK',
        routerLink: 'aplicaciones/reportes/npk',
        permissionsRequired: ['aplicaciones:reporte-por-npks']
      },
      {
        title: 'Aplicaciones por Hectarea',
        routerLink: 'aplicaciones/reportes/hectarea',
        permissionsRequired: ['aplicaciones:reporte-por-has']
      },
      {
        title: 'Referencias Técnicas',
        routerLink: 'aplicaciones/referencia-tecnica',
        permissionsRequired: ['aplicaciones:referencias-tecnicas']
      }
    ]
  },
  {
    title: 'Labores',
    routerLink: 'labores',
    icon: 'fa-briefcase fa-lg',
    selected: false,
    expanded: false,
    order: 200,
    permissionsRequired: ['labores'],
    subMenu: [
      {
        title: 'Contratistas',
        routerLink: 'labores/contratistas',
        permissionsRequired: ['labores:contratistas']
      },
      {
        title: 'Labores',
        routerLink: 'labores/lista-labores',
        permissionsRequired: ['labores:labores']
      }
    ]
  },
  {
    title: 'Productos',
    routerLink: 'productos',
    icon: 'fa-shopping-basket fa-lg',
    selected: false,
    expanded: false,
    order: 200,
    permissionsRequired: ['productos'],
    subMenu: [
      {
        title: 'Lista de Productos',
        routerLink: 'productos/lista-productos',
        permissionsRequired: ['productos:lista-productos']
      },
      {
        title: 'NPK',
        routerLink: 'productos/npk',
        permissionsRequired: ['productos:npk']
      },
      {
        title: 'Reportes por Fecha',
        routerLink: 'productos/reportes-fechas',
        permissionsRequired: ['productos:reportes-fecha']
      },
      {
        title: 'Reportes Compras por Producto',
        routerLink: 'productos/reportes-fechas',
        permissionsRequired: ['productos:reportes-compras']
      },
      {
        title: 'Dosis',
        routerLink: 'productos/dosis',
        permissionsRequired: ['productos:dosis']
      },
      {
        title: 'Carencias',
        routerLink: 'productos/carencias',
        permissionsRequired: ['productos:carencias']
      },
      {
        title: 'Reporte de Compra por Producto',
        routerLink: 'productos/reporte-compras',
        permissionsRequired: ['productos:reporte-compras']
      },
      {
        title: 'Ajuste de Inventario',
        routerLink: 'productos/ajuste-inventario',
        permissionsRequired: ['productos:ajuste-inventario']
      }
    ]
  },
  {
    title: 'Personal',
    routerLink: 'personal',
    icon: 'fa-users fa-lg',
    selected: false,
    expanded: false,
    order: 200,
    permissionsRequired: ['personal'],
    subMenu: [
      {
        title: 'Empleados',
        routerLink: 'personal/empleados',
        permissionsRequired: ['personal:empleados']
      },
      {
        title: 'Sueldos',
        routerLink: 'personal/sueldos',
        permissionsRequired: ['personal:sueldos']
      },
      {
        title: 'Horas Extras',
        routerLink: 'personal/horas-extras',
        permissionsRequired: ['personal:horas-extras']
      },
      {
        title: 'Ausencias',
        routerLink: 'personal/ausencias',
        permissionsRequired: ['personal:ausencias']
      },
      {
        title: 'Préstamos',
        routerLink: 'personal/prestamos',
        permissionsRequired: ['personal:prestamos']
      },
      {
        title: 'Vacaciones',
        routerLink: 'personal/vaciones',
        permissionsRequired: ['personal:vacaciones']
      },
      {
        title: 'Liquidaciones',
        routerLink: 'personal/liquidaciones',
        permissionsRequired: ['personal:liquidaciones']
      },
      {
        title: 'Reportes',
        routerLink: 'personal/reportes',
        permissionsRequired: ['personal:reportes']
      }
    ]
  },
  {
    title: 'Maquinaria',
    routerLink: 'maquinaria',
    icon: 'fa-truck fa-lg',
    selected: false,
    expanded: false,
    order: 200,
    permissionsRequired: ['maquinaria'],
    subMenu: [
      {
        title: 'Tractores',
        routerLink: 'maquinaria/tractores',
        permissionsRequired: ['maquinaria:tractores']
      },
      {
        title: 'Maquinas',
        routerLink: 'maquinaria/maquinas',
        permissionsRequired: ['maquinaria:maquinas']
      },
      {
        title: 'Vehiculos',
        routerLink: 'maquinaria/vehiculos',
        permissionsRequired: ['maquinaria:vehiculos']
      },
      {
        title: 'Mantenciones',
        routerLink: 'maquinaria/mantenciones',
        permissionsRequired: ['maquinaria:mantenciones']
      }
    ]
  },
  {
    title: 'Administracion',
    routerLink: 'administracion',
    icon: 'fa-vcard-o fa-lg',
    selected: false,
    expanded: false,
    order: 200,
    permissionsRequired: ['administracion'],
    subMenu: [
      {
        title: 'Usuarios',
        routerLink: 'administracion/usuarios',
        permissionsRequired: ['administracion:usuarios']
      },
      {
        title: 'Roles',
        routerLink: 'administracion/roles',
        permissionsRequired: ['administracion:roles']
      },
      {
        title: 'Permisos',
        routerLink: 'administracion/permisos',
        permissionsRequired: ['administracion:permisos']
      },
      {
        title: 'Configuraciones',
        routerLink: 'administracion/configuraciones',
        permissionsRequired: ['administracion:configuraciones']
      }
    ]
  },
  {
    title: 'Transacciones Bancarias',
    routerLink: 'transacciones-bancarias',
    icon: 'fa-usd fa-lg',
    selected: false,
    expanded: false,
    order: 200,
    permissionsRequired: ['transacciones-bancarias']
  },
];
