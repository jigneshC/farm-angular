import {Component, OnDestroy, OnInit} from '@angular/core';
import {MQTTService, TransportState} from "../../../services/mqtt";
import {Observable} from "rxjs/Observable";
import {ConfigService} from "../../../services/config/config.service";
import { Packet } from 'mqtt';
import {Config} from "../../../services/config";
import { trigger, state, style, transition, animate } from '@angular/animations';


@Component({
  selector: 'app-irrigation-heartbeat',
  templateUrl: './irrigation-heartbeat.component.html',
  styleUrls: ['./irrigation-heartbeat.component.scss'],
})
export class IrrigationHeartbeatComponent implements OnInit, OnDestroy {
  public STATES: any = ['Regando', 'Detenido', 'Falla'];
  public state: number = 2;
  public label: string = '';
  public count = 0;
  public messages: Observable<Packet>;
  public message: any;
  constructor (private mqttService: MQTTService, private configService: ConfigService) {
  }
  ngOnInit(): void {
    const config: any = {
      host: 'listener.easywater.cl',
      port: 8083,
      path: 'mqtt',
      ssl: false,
      user: '',
      pass: '',
      publish: [],
      subscribe: ['/antumalal/services/irrigation'],
      keepalive: 60
    }
    this.mqttService.configure(config);
    this.mqttService.try_connect()
      .then(this.on_connect)
      .catch(this.on_error);
  }

  ngOnDestroy() {
    this.mqttService.disconnect();
  }

  /** Callback on_connect to queue */
  public on_connect = () => {

    // Store local reference to Observable
    // for use with template ( | async )
    this.messages = this.mqttService.messages;

    // Subscribe a function to be run on_next message
    this.messages.subscribe(this.on_next);
  }

  /** Consume a message from the _mqService */
  public on_next = (message: Packet) => {
    // Store message in "historic messages" queue
    this.message = JSON.parse(message.toString());
    if (this.message.status.sectors > 0) {
      this.state = 0;
    } else {
      this.state = 1;
    }

    // Count it
    this.count++;
  }

  public on_error = () => {
    console.error('Ooops, error in RawDataComponent');
    this.state = 2;
  }
}
