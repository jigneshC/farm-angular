import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDeleteComponent } from './delete-confirm-modal/delete-confirm.component';
import { ModalFormComponent } from './modal-form/modal-form.component';


@NgModule({
    imports: [ 
        CommonModule
    ],
    declarations: [
        ConfirmDeleteComponent,
        ModalFormComponent,
    ],
    exports: [
        ConfirmDeleteComponent,
        ModalFormComponent,
    ]
})
export class CrudComponentsModule { }
