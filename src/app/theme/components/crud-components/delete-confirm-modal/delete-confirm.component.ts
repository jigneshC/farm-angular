import { AppConfig } from './../../../../app.config';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';


@Component({
    selector: 'delete-confirm',
    templateUrl: 'delete-confirm.component.html',
    styleUrls:['delete-confirm.component.scss']
})

export class ConfirmDeleteComponent implements OnInit {
    messages:any;
    @Output() confirmed:EventEmitter<boolean> = new EventEmitter();

    constructor(private _config:AppConfig){
    }
    ngOnInit() {
     this.messages = this._config.config.messages;
        
     }
     delete(){
        this.confirmed.emit(true);
     }
}