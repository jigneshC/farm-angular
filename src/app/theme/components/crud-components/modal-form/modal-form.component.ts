import { AppConfig } from './../../../../app.config';
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'modal-form',
	templateUrl: 'modal-form.component.html',
	styleUrls:['modal-form.component.scss']
})

export class ModalFormComponent implements OnInit {
	messages: any;
	@Output() confirmed: EventEmitter<boolean> = new EventEmitter();
	@Input() form: FormGroup;
	@Input() errorMessages: any;
	
	constructor(
		private _config: AppConfig
	) {}
	
	ngOnInit() {
		console.log(this.form);
		console.log(this.confirmed);
		this.messages = this._config.config.messages;
	}
	
	delete(){
		this.confirmed.emit(true);
	}
}