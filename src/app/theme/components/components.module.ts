import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImageDialogComponent} from './image-dialog/image-dialog.component';
import { EmployeeDialogComponent } from './employee-dialog/employee-dialog/employee-dialog.component';
import {MatCardModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatListModule} from '@angular/material';
import {ConfigService} from '../../services/config/config.service';
import {MQTTService} from '../../services/mqtt';


@NgModule({
  entryComponents: [ImageDialogComponent, EmployeeDialogComponent],
  imports: [
    CommonModule,
    MatDialogModule
  ],
  providers: [
    ConfigService,
    MQTTService
  ],
  declarations: [
    ImageDialogComponent,
    EmployeeDialogComponent,
  ],
  exports: [
    ImageDialogComponent, EmployeeDialogComponent
  ]
})

export class ComponentsModule {
}
