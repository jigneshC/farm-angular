import {Directive, ElementRef, HostListener, Input, OnInit, Renderer2} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Directive({
  selector: '[enableIfRoles]'
})
export class EnableIfRolesDirective implements OnInit {

  protected hasRoles = false;

  @Input('enableIfRoles') permissions: Array<string>;

  @HostListener('click', ['$event'])
  clickEvent(event) {
    if (!this.hasRoles) {
      event.preventDefault();
      event.stopPropagation();
      const config: MatSnackBarConfig = {
        duration: 3000,
        verticalPosition: 'top'
      };
      this.snackBar.open('No dispones de los permisos para realizar esta acción', null, config);
    }
  }

  constructor(private authService: AuthService, private snackBar: MatSnackBar, private element: ElementRef, protected renderer: Renderer2) {
  }

  ngOnInit() {
    this.hasRoles = this.authService.userHasPermissions(this.permissions);
    if (!this.hasRoles) {
      this.renderer.setStyle(this.element.nativeElement,  'opacity', '0.6');
    }
  }

}