import {Directive, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';

@Directive({
  selector: '[hasParentPermission]',
})
export class ParentPermissionDirective {

  constructor(private _templateRef: TemplateRef<any>,
              private _viewContainer: ViewContainerRef,
              private authServcie: AuthService) {
  }

  @Input() set hasParentPermission(parentPermissions: Array<string>) {
    const shouldShow = this.authServcie.userHasParentPermission(parentPermissions);
    if (shouldShow) {
      this._viewContainer.createEmbeddedView(this._templateRef);
    } else {
      this._viewContainer.clear();
    }
  }
}