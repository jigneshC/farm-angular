import {Directive, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from '../../../services/auth/auth.service';

@Directive({
  selector: '[hasPermissions]'
})
export class PermissionDirective {

  constructor(private _templateRef: TemplateRef<any>,
              private _viewContainer: ViewContainerRef,
              private authServcie: AuthService) {

  }

  @Input() set hasPermissions(permissions: Array<string>) {
    const shouldShow = this.authServcie.userHasPermissions(permissions);
    if (shouldShow) {
      this._viewContainer.createEmbeddedView(this._templateRef);
    } else {
      this._viewContainer.clear();
    }
  }

}
