Changelog
=========

[2.0.2] - 2017-12-1
------------
###Added
- Tasks module
- Irrigations module
- Weather module
- Contractors module
- Menu
- New datatables

[2.0.1] - 2017-11-22
------------
###Added
- Sectors module
- Login page
- Forget password
- Employees module
- Menu
- Days module
- Providers module
- Cost centers module
- Bank transactions module
- Support for angular 5


