# Farm

This project is the web version for HSD Farm App.

##Getting Started 

This project is made on [angular](https://angular.io/).

### Local development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Prerequisites

* [node](https://github.com/nodejs/node)
* [npm](https://www.npmjs.com/)

## Deployment

Run `git push heroku develop:master`

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/aaranedac/farm-frontend/tags).


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details 

## Authors

* **David Lopez** - *Initial work* -

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.
